/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2013, MPL CodeInside http://codeinside.ru
 */
package ru.codeinside.gses.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;
import java.nio.charset.Charset;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.impl.ServiceImpl;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import ru.codeinside.adm.database.DefinitionStatus;
import ru.codeinside.adm.database.Procedure;
import ru.codeinside.adm.database.ProcedureProcessDefinition;
import ru.codeinside.adm.database.ProcedureProcessDefinition_;
import ru.codeinside.adm.database.ProcedureType;
import ru.codeinside.adm.database.Procedure_;
import ru.codeinside.adm.database.Service;
import ru.codeinside.adm.database.Service_;
import ru.codeinside.adm.database.SmevChain;
import ru.codeinside.gses.activiti.SubmitStartFormCommand;
import ru.codeinside.gses.activiti.forms.Signatures;
import ru.codeinside.gses.beans.ActivitiReceiptContext;
import ru.codeinside.gses.service.BidID;
import ru.codeinside.gses.service.DeclarantService;
import ru.codeinside.gses.webui.form.SignatureType;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.SetJoin;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static javax.ejb.TransactionAttributeType.REQUIRED;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.activiti.engine.impl.ExecutionQueryImpl;
import org.activiti.engine.impl.ExecutionVariableQueryImpl;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.gses.beans.ActivitiBean;
import ru.codeinside.gses.manager.ManagerService;
import ru.codeinside.gses.service.Functions;
import ru.codeinside.gses.service.PF;
import ru.codeinside.gws.api.ContextSearchParameter;

@TransactionAttribute(REQUIRED)
@TransactionManagement
@Singleton
@Lock(LockType.READ)
public class DeclarantServiceImpl implements DeclarantService {

    @PersistenceContext(unitName = "myPU")
    EntityManager em;

    @Override
    public BidID declare(ProcessEngine engine, String processDefinitionId,
            Map<String, Object> properties, Map<SignatureType, Signatures> signatures,
            String declarer) {
        return commandExecutor(engine).execute(
                new SubmitStartFormCommand(null, null, processDefinitionId, properties, signatures, declarer, null, null)
        );
    }

    @Override
    public BidID smevDeclare(SmevChain smevChain, String componentName,
            ProcessEngine engine, String processDefinitionId,
            Map<String, Object> properties, String declarer, String tag) {
        return commandExecutor(engine).execute(
                new SubmitStartFormCommand(smevChain, componentName, processDefinitionId, properties, null, declarer, tag, null)
        );
    }

    @Override
    public List<String> getBids(long gid) {
        List<Long> list = em.createQuery("select e.id from Bid e where e.glue.id = :gid", Long.class)
                .setParameter("gid", gid).getResultList();
        List<String> strings = new ArrayList<String>(list.size());
        for (Long id : list) {
            strings.add(Long.toString(id));
        }
        return strings;
    }

    @Override
    public long getGlueIdByRequestIdRef(String requestIdRef) {
        List<Long> rs = em.createQuery(
                "select s.id from ExternalGlue s where s.requestIdRef=:requestIdRef", Long.class)
                .setParameter("requestIdRef", requestIdRef)
                .getResultList();
        if (rs.isEmpty()) {
            return 0L;
        }
        return rs.get(0);
    }

    @Override
    public void updateContext(final ProcessEngine engine, final String processInstanceId, final Map<String, Object> values) {
        commandExecutor(engine).execute(new Command<Void>() {
            @Override
            public Void execute(CommandContext commandContext) {
                DelegateExecution execution = commandContext.getExecutionManager().findExecutionById(processInstanceId);
                if (execution == null) {
                    return null;
                }
                ActivitiReceiptContext context = new ActivitiReceiptContext(execution, 0);
                for (Map.Entry<String, Object> entry : values.entrySet()) {
                    context.setVariable(entry.getKey(), entry.getValue());
                }
                return null;
            }
        });
    }
    
    
    
    @Override
    public void updateRegisteredContextVariables(ProcessEngine engine, final ContextSearchParameter searchParameter) {
        commandExecutor(engine).execute(new Command<Void>() {
            @Override
            public Void execute(CommandContext commandContext) {
                List<ExecutionEntity> executionsList = getExecutionsList(commandContext, searchParameter);

                if (executionsList == null) {
                    return null;
                }

                if (executionsList.isEmpty()) {
                    return null;
                }

                // If search was return more than one value - take the first one
                ExecutionEntity executionEntity = executionsList.get(0);
                if (executionEntity == null) {
                    return null;
                }

                ActivitiReceiptContext context = new ActivitiReceiptContext((DelegateExecution) executionEntity, 0);
                for (Map.Entry<String, Object> entry : searchParameter.getExchangeableValues().entrySet()) {
                    context.setVariable(entry.getKey(), entry.getValue());
                }
                
                return null;
            }
        });
    }
    

    @Override
    public String getRegisteredContextVariable(ProcessEngine engine, final ContextSearchParameter searchParameter) {
        final String[] searchString = new String[1];
        
        commandExecutor(engine).execute(new Command<Void>() {
            @Override
            public Void execute(CommandContext commandContext) {
                List<ExecutionEntity> executionsList = getExecutionsList(commandContext, searchParameter);

                if (executionsList == null) {
                    return null;
                }

                if (executionsList.isEmpty()) {
                    return null;
                }

                // If search was return more than one value - take the first one
                ExecutionEntity executionEntity = executionsList.get(0);
                if (executionEntity == null) {
                    return null;
                }

                DelegateExecution delegateExecution = (DelegateExecution) executionEntity;
                Object sObject = delegateExecution.getVariable(searchParameter.getSearchValue());

                if (sObject == null) {
                    return null;
                }
                
                // Do some a little hack for access to outer variable
                searchString[0] = (String) sObject;
                return null;
            }
        });
        
        if (searchString.length == 0) {
            return null;
        }
        
        return searchString[0];
    }

    
    private List<ExecutionEntity> getExecutionsList(CommandContext commandContext, ContextSearchParameter searchParameter) {
        ExecutionVariableQueryImpl executionVariableQuery = new ExecutionQueryImpl();
        for (ContextSearchParameter.ContextSearchCondition searchCondition : searchParameter.getSearchConditions()) {
            switch (searchCondition.getSearchType()) {
                case IS_EQUAL:
                    executionVariableQuery.variableValueEquals(searchCondition.getKey(), searchCondition.getValue());
                    break;
                case IS_NOT_EQUAL:
                    executionVariableQuery.variableValueNotEquals(searchCondition.getKey(), searchCondition.getValue());
                    break;
            }
        }

        return executionVariableQuery.executeList(commandContext, null);
    }
    
    

    @Override
    public int activeProceduresCount(ProcedureType type, long serviceId) {
        return proceduresCount(type, serviceId, true);
    }

    @Override
    public List<Procedure> selectActiveProcedures(ProcedureType type, long serviceId, int start, int count) {
        return selectProcedures(type, serviceId, start, count, true);
    }

    @Override
    public List<Procedure> selectDeclarantProcedures(ProcedureType type, long serviceId, int start, int count) {
        return selectProcedures(type, serviceId, start, count, false);
    }

    @Override
    public List<Procedure> selectDeclarantPresettedProcedures(ProcedureType type, long serviceId, int start, int count) {
        return selectProcedures(type, serviceId, start, count, false);
    }

    @Override
    public ProcedureProcessDefinition selectActive(long procedureId) {
        CriteriaBuilder b = em.getCriteriaBuilder();
        CriteriaQuery<ProcedureProcessDefinition> query = b.createQuery(ProcedureProcessDefinition.class);
        Root<ProcedureProcessDefinition> def = query.from(ProcedureProcessDefinition.class);
        Path<Procedure> procedure = def.get(ProcedureProcessDefinition_.procedure);
        query.select(def).where(
                b.and(
                        b.equal(procedure.get(Procedure_.id), procedureId),
                        b.equal(def.get(ProcedureProcessDefinition_.status), DefinitionStatus.Work)
                )
        );
        List<ProcedureProcessDefinition> defs = em.createQuery(query).setMaxResults(1).getResultList();
        return defs.isEmpty() ? null : defs.get(0);
    }

    @Override
    public LinkedHashSet<Procedure> selectFilteredProcedures(String employee, ProcessEngine engine, Boolean presets) {
        final List<ProcessDefinition> pdl = engine.getRepositoryService().createProcessDefinitionQuery()
                .startableByUser(employee).list();
        LinkedHashSet<Procedure> result = new LinkedHashSet<Procedure>();
        for (ProcessDefinition pd : pdl) {

            Procedure p = AdminServiceProvider.get().getProcedureByProcessDefinitionId(pd.getId());

            if (presets) {
                if (!p.getParentId().equals("") && p.getStatus().equals("Работает")) { //DefinitionStatus.Work
                    result.add(p);
                }
            } else {
                if (p.getParentId().equals("") && p.getStatus().equals("Работает")) {  //DefinitionStatus.Work
                    result.add(p);
                }
            }

        }
        return result;
    }

    @Override
    public List<Procedure> selectFilteredPresets() {
        return null;
    }

    @Override
    public void savePreset(Map<String, String> variables, Procedure parentProcedure, String login, String presetName) {
        Procedure preset;
        String descDummy = login + "_user_saved_preset";
        String parentSId;
        try {
            parentSId = parentProcedure.getService().getId().toString();
        } catch (Exception e) {
            parentSId = "";
        }

        ProcedureProcessDefinition parentPPD
                = ManagerService.get().getActiveProcessDefenition(parentProcedure);

        Procedure found = ManagerService.get().getSamePreset(parentProcedure.getIdRaw(), login, presetName);
        if (found != null) {
            String id = found.getId();
            ManagerService.get().updateProcedure(id, presetName, descDummy, parentSId, null);
            preset = ManagerService.get().getProcedure(id);
            String presetDefName = login + "-" + preset.getId() + "-" + parentProcedure.getId();
            byte[] presetContent = buildPreset(parentPPD, variables, login, presetDefName);
            ProcedureProcessDefinition oldPD = ManagerService.get().getActiveProcessDefenition(found);
            ManagerService.get().updateProcessDefinationStatus(oldPD.getProcessDefinitionId(), DefinitionStatus.Archive);
            em.persist(oldPD);

            Deployment deploy = ActivitiBean.get().deploy(presetDefName + ".bpmn", presetContent);
            ProcessDefinition pd = ActivitiBean.get().getProcessDefinitionByDeployment(deploy.getId(), login);
            ProcedureProcessDefinition newPD = ManagerService.get().createProcessDefination(preset.getId(), pd, login, null);
            ManagerService.get().updateProcessDefinationStatus(newPD.getProcessDefinitionId(), DefinitionStatus.Debugging);
            ManagerService.get().updateProcessDefinationStatus(newPD.getProcessDefinitionId(), DefinitionStatus.Done);
            ManagerService.get().updateProcessDefinationStatus(newPD.getProcessDefinitionId(), DefinitionStatus.Work);
            em.persist(newPD);
        } else {
            preset = ManagerService.get().createProcedure(presetName, descDummy,
                    parentSId, null, login, parentProcedure.getType());
            preset.setParentId(parentProcedure.getIdRaw());
            String presetDefName = login + "-" + preset.getId() + "-" + parentProcedure.getId();
            byte[] presetContent = buildPreset(parentPPD, variables, login, presetDefName);

            Deployment deploy = ActivitiBean.get().deploy(presetDefName + ".bpmn", presetContent);
            ProcessDefinition pd = ActivitiBean.get().getProcessDefinitionByDeployment(deploy.getId(), login);
            ProcedureProcessDefinition ppd = ManagerService.get().createProcessDefination(preset.getId(), pd, login, null);
            ManagerService.get().updateProcessDefinationStatus(ppd.getProcessDefinitionId(), DefinitionStatus.Debugging);
            ManagerService.get().updateProcessDefinationStatus(ppd.getProcessDefinitionId(), DefinitionStatus.Done);
            ManagerService.get().updateProcessDefinationStatus(ppd.getProcessDefinitionId(), DefinitionStatus.Work);
            em.persist(ppd);

        }
        preset.setParentVersion(parentProcedure.getVersion());
        em.persist(preset);
    }

    @Override
    public void archPreset(Procedure preset) {
        ProcedureProcessDefinition pd = ManagerService.get().getActiveProcessDefenition(preset);
        ManagerService.get().updateProcessDefinationStatus(pd.getProcessDefinitionId(), DefinitionStatus.Archive);
        em.persist(pd);
        String id = preset.getId();
        ManagerService.get().updateProcedure(id, preset.getName(), preset.getCreator() + "_deleted_preset", "", null);
        Procedure upd = ManagerService.get().getProcedure(id);
        em.persist(upd);
    }

    @Override
    public boolean updateOnParentChange(Procedure original, Procedure parent) {
        String id = original.getId();
        String descDummy = original.getCreator() + "_user_saved_preset";
        String parentSId;
        try {
            parentSId = parent.getService().getId().toString();
        } catch (Exception e) {
            parentSId = "";
        }
        ProcedureProcessDefinition parentPPD
                = ManagerService.get().getActiveProcessDefenition(parent);
        ProcedureProcessDefinition originalPPD
                = ManagerService.get().getActiveProcessDefenition(original);

        ManagerService.get().updateProcedure(id, original.getName(), descDummy, parentSId, null);
        Procedure preset = ManagerService.get().getProcedure(id);
        String presetDefName = original.getCreator() + "-" + preset.getId() + "-" + parent.getId();
        byte[] presetContent = buildPreset(parentPPD, getOriginalVars(originalPPD), original.getCreator(), presetDefName);
        ProcedureProcessDefinition oldPD = ManagerService.get().getActiveProcessDefenition(original);
        ManagerService.get().updateProcessDefinationStatus(oldPD.getProcessDefinitionId(), DefinitionStatus.Archive);
        em.persist(oldPD);

        try {
            Deployment deploy = ActivitiBean.get().deploy(presetDefName + ".bpmn", presetContent);
            ProcessDefinition pd = ActivitiBean.get().getProcessDefinitionByDeployment(deploy.getId(), original.getCreator());
            ProcedureProcessDefinition newPD = ManagerService.get().createProcessDefination(preset.getId(), pd, original.getCreator(), null);
            ManagerService.get().updateProcessDefinationStatus(newPD.getProcessDefinitionId(), DefinitionStatus.Debugging);
            ManagerService.get().updateProcessDefinationStatus(newPD.getProcessDefinitionId(), DefinitionStatus.Done);
            ManagerService.get().updateProcessDefinationStatus(newPD.getProcessDefinitionId(), DefinitionStatus.Work);
            em.persist(newPD);
            return true;
        } catch (Exception e) {
            return false;
            //when old preset can not be restored (used values that not present in new bpmn)
        }

    }

    @Override
    public ProcedureProcessDefinition getProcessDefinition(Procedure p) {
        return ManagerService.get().getActiveProcessDefenition(p);
    }

    private URL getFormPropertySchema() {
        return getClass().getClassLoader().getResource("formProps.xsd");
    }

    private Map<String, String> getOriginalVars(final ProcedureProcessDefinition originalPPD) {
        Map<String, String> result = new LinkedHashMap<String, String>();
        final String actNS = "http://activiti.org/bpmn";
        try {
            InputStream IS = getProcessDefinitionIS(originalPPD);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            Document doc = dbf.newDocumentBuilder().parse(IS);

            Element start = (Element) doc.getElementsByTagName("startEvent").item(0);
            NodeList props = start.getElementsByTagNameNS(actNS, "formProperty");
            for (int i = 0; i < props.getLength(); i++) {
                if (props.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    Element variable = (Element) props.item(i);
                    if (variable.hasAttribute("id") && variable.hasAttribute("default")) {
                        result.put(variable.getAttribute("id"), variable.getAttribute("default"));
                    }
                }
            }
            return result;
        } catch (Exception ex) {
            throw new IllegalStateException("Не удалось построить шаблон.");
        }
    }
    
    private byte[] buildPreset(final ProcedureProcessDefinition parentPPD, Map<String, String> variables, String login, String presetId) {
        final String actNS = "http://activiti.org/bpmn";
        try {
            InputStream parentIS = getProcessDefinitionIS(parentPPD);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);

            //нужна универсальная схема для всех бпмн, проще отказатся от getElementById
//            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
//            Schema schema = schemaFactory.newSchema(getFormPropertySchema());
//            dbf.setSchema(schema);
            Document doc = dbf.newDocumentBuilder().parse(parentIS);
            Node process = doc.getElementsByTagName("process").item(0);
            process.getAttributes().removeNamedItemNS(actNS, "candidateStarterGroups");
            Node starter = doc.createAttributeNS(actNS, "candidateStarterUsers");
            starter.setPrefix("activiti");
            starter.setNodeValue(login);
            process.getAttributes().setNamedItemNS(starter);
            process.getAttributes().getNamedItem("id").setNodeValue(presetId);

            NodeList logic = process.getChildNodes();
            for (int i = 0; i < logic.getLength(); i++) {
                if (logic.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    Element task = (Element) logic.item(i);
                    if (task.hasAttributeNS(actNS, "candidateGroups")) {
                        task.removeAttributeNS(actNS, "candidateGroups");
                        Node candidate = doc.createAttributeNS(actNS, "candidateUsers");
                        candidate.setPrefix("activiti");
                        candidate.setNodeValue(login);
                        task.getAttributes().setNamedItemNS(candidate);
                    }
                    if (task.getLocalName().equals("serviceTask")
                            && task.getAttributes().getNamedItem("name").getNodeValue().equals("СМЭВ")) {
                        Element ext = (Element) ((Element) task).getElementsByTagName("extensionElements").item(0);
                        if (ext != null) {
                            Node fld = ext.getFirstChild();
                            while (fld != null) {
                                if (fld.getNodeType() == Node.ELEMENT_NODE) {
                                    if (fld.getAttributes().getNamedItem("name").getNodeValue()
                                            .toLowerCase().equals("исполнители")) {
                                        Node value = ((Element) fld).getElementsByTagNameNS(actNS, "string").item(0);
                                        value.setTextContent(login);
                                    }
                                }
                                fld = fld.getNextSibling();
                            }
                        }
                    }
                }
            }

            Element start = (Element) doc.getElementsByTagName("startEvent").item(0);
            NodeList props = start.getElementsByTagNameNS(actNS, "formProperty");
            for (int i = 0; i < props.getLength(); i++) {
                if (props.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    Element variable = (Element) props.item(i);
                    if (variable.hasAttribute("id") && (variables.get(variable.getAttribute("id")) != null)
                            && (variables.get(variable.getAttribute("id")) != "")) {
                        variable.setAttribute("default", variables.get(variable.getAttribute("id")));
                    }
                }
            }

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            DOMSource source = new DOMSource(doc);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            StreamResult result = new StreamResult(bos);
            transformer.transform(source, result);
            return bos.toByteArray();
        } catch (Exception ex) {
            throw new IllegalStateException("Не удалось построить шаблон.");
        }
    }

    private String docToString(Document document) {
        try {
            StringWriter sw = new StringWriter();
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            transformer.transform(new DOMSource(document), new StreamResult(sw));

            return sw.toString();

        } catch (Exception ex) {
            throw new IllegalStateException(" docToString failure ");
        }
    }

    private String nodeToString(Node node) throws TransformerException {
        StringWriter sw = new StringWriter();
        Transformer t = TransformerFactory.newInstance().newTransformer();
        t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        t.transform(new DOMSource(node), new StreamResult(sw));
        return sw.toString();
    }

    private InputStream getProcessDefinitionIS(final ProcedureProcessDefinition pd) {
        return Functions.withEngine(new PF<InputStream>() {
            private static final long serialVersionUID = 1L;

            public InputStream apply(ProcessEngine s) {
                return s.getRepositoryService().getProcessModel(pd.getProcessDefinitionId());
            }
        });
    }

    @Override
    public int activeServicesCount(ProcedureType type) {
        CriteriaBuilder b = em.getCriteriaBuilder();
        CriteriaQuery<Number> query = b.createQuery(Number.class);
        Root<Service> service = query.from(Service.class);
        query.select(b.countDistinct(service))
                .where(typeAndStatus(type, b, service));
        return count(query);
    }

    @Override
    public List<Service> selectActiveServices(ProcedureType type, int start, int count) {
        CriteriaBuilder b = em.getCriteriaBuilder();
        CriteriaQuery<Service> query = b.createQuery(Service.class);
        Root<Service> service = query.from(Service.class);
        query.select(service)
                .where(typeAndStatus(type, b, service))
                .distinct(true)
                .orderBy(b.asc(service.get(Service_.name)));
        return chunk(start, count, query);
    }

    // ---- internals ----
    private CommandExecutor commandExecutor(ProcessEngine engine) {
        return ((ServiceImpl) engine.getFormService()).getCommandExecutor();
    }

    private int proceduresCount(ProcedureType type, long serviceId, boolean usePathToArchive) {
        CriteriaBuilder b = em.getCriteriaBuilder();
        CriteriaQuery<Number> query = b.createQuery(Number.class);
        Root<Procedure> procedures = query.from(Procedure.class);
        query.select(b.countDistinct(procedures))
                .where(typeAndService(type, serviceId, b, procedures, usePathToArchive));
        return count(query);
    }

    private List<Procedure> selectProcedures(ProcedureType type, long serviceId, int start, int count, boolean usePathToArchive) {
        CriteriaBuilder b = em.getCriteriaBuilder();
        CriteriaQuery<Procedure> query = b.createQuery(Procedure.class);
        Root<Procedure> procedures = query.from(Procedure.class);
        query.select(procedures)
                .distinct(true)
                .where(typeAndService(type, serviceId, b, procedures, usePathToArchive))
                .orderBy(b.asc(procedures.get(Procedure_.name)));
        return chunk(start, count, query);
    }

    private <T> List<T> chunk(int start, int count, CriteriaQuery<T> query) {
        return em.createQuery(query).setFirstResult(start).setMaxResults(count).getResultList();
    }

    private int count(final CriteriaQuery<Number> query) {
        return em.createQuery(query).getSingleResult().intValue();
    }

    private Predicate typeAndService(ProcedureType type, long serviceId, CriteriaBuilder b,
            Root<Procedure> procedure, boolean findInPathToArchive) {
        Predicate typeAndStatus = typeAndStatus(type, b, procedure, findInPathToArchive);
        if (serviceId < 0) {
            return typeAndStatus;
        }
        Path<Service> service = procedure.get(Procedure_.service);
        return b.and(
                typeAndStatus,
                b.equal(service.get(Service_.id), serviceId)
        );
    }

    private Predicate typeAndStatus(ProcedureType type, CriteriaBuilder b, Root<Service> service) {
        return typeAndStatus(type, b, service.join(Service_.procedures), true);
    }

    private Predicate typeAndStatus(ProcedureType type, CriteriaBuilder b,
            From<?, Procedure> procedures, boolean findInPathToArchive) {
        SetJoin<Procedure, ProcedureProcessDefinition> defs = procedures.join(Procedure_.processDefinitions);
        if (findInPathToArchive) {
            return b.and(
                    b.equal(procedures.get(Procedure_.type), type),
                    b.or(
                            b.equal(defs.get(ProcedureProcessDefinition_.status), DefinitionStatus.Work),
                            b.equal(defs.get(ProcedureProcessDefinition_.status), DefinitionStatus.PathToArchive)
                    )
            );
        }
        return b.and(
                b.equal(procedures.get(Procedure_.type), type),
                b.equal(defs.get(ProcedureProcessDefinition_.status), DefinitionStatus.Work)
        );
    }

}
