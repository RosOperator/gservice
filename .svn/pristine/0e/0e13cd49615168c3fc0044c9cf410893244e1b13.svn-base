<?xml version="1.0"?>
<!--
  ~ This Source Code Form is subject to the terms of the Mozilla Public
  ~ License, v. 2.0. If a copy of the MPL was not distributed with this
  ~ file, You can obtain one at http://mozilla.org/MPL/2.0/.
  ~ Copyright (c) 2014, MPL CodeInside http://codeinside.ru
  -->

<project xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
         xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <modelVersion>4.0.0</modelVersion>
    <artifactId>smev-v3-service-api</artifactId>
    <version>1.0.3</version>
    <packaging>bundle</packaging>
    <name>SMEV 3 Service API</name>
    <parent>
        <groupId>ru.codeinside</groupId>
        <artifactId>gws-smev3</artifactId>
        <version>1.0.0</version>
    </parent>
    <dependencies>

        <dependency>
            <groupId>ru.codeinside</groupId>
            <artifactId>smev-v3-crypto-api</artifactId>
            <version>${smev-v3-crypto-api.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>ru.codeinside</groupId>
            <artifactId>gws-api</artifactId>
            <version>${gws-api.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.fasterxml.uuid</groupId>
            <artifactId>java-uuid-generator</artifactId>
            <version>3.1.4</version>
        </dependency>
        <dependency>
            <groupId>commons-codec</groupId>
            <artifactId>commons-codec</artifactId>
        </dependency>
        
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.11</version>
            <scope>test</scope>
        </dependency>

    </dependencies>
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.1</version>
                <configuration>
                    <source>1.6</source>
                    <target>1.6</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.felix</groupId>
                <artifactId>maven-bundle-plugin</artifactId>
                <version>2.5.3</version>
                <extensions>true</extensions>
                <configuration>
                    <instructions>
                        <Embed-Dependency>
                            groupId=com.fasterxml.uuid;artifactId=java-uuid-generator,
                            groupId=commons-codec;artifactId=commons-codec
                        </Embed-Dependency>
                        <Export-Package>
                            ru.codeinside.smev.v3.service.api,
                            ru.codeinside.smev.v3.service.api.consumer,
                            ru.codeinside.smev.v3.service.api.provider
                        </Export-Package>
                        <Import-Package>
                            ru.codeinside.gws.api,
                            ru.codeinside.smev.v3.crypto.api,

                            org.w3c.dom,
                            javax.xml.bind,
                            javax.xml.parsers
                        </Import-Package>
                    </instructions>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>2.6</version>
                <configuration>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
