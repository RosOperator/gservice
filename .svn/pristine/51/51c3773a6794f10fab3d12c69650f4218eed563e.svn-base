package ru.codeinside.smev.v3.service.impl.provider;

import ru.codeinside.smev.v3.service.api.PersonalData;
import ru.codeinside.smev.v3.service.api.UUID;
import ru.codeinside.smev.v3.service.api.provider.ProvidedResponse;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponse;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseBuilder;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseData;
import ru.codeinside.smev.v3.service.impl.OSGi;
import ru.codeinside.smev.v3.service.impl.Utils;
import ru.codeinside.smev.v3.transport.api.SmevTypesProvider;

@SuppressWarnings("PackageAccessibility")
public class ProviderResponseBuilderImpl implements ProviderResponseBuilder {
    @Override
    public ProviderResponseData createPersonalResponse(ProvidedResponse providedResponse) {
        PersonalData personalData = new PersonalData();
        personalData.data = providedResponse.getResponseData();
        if (providedResponse.isSignatureRequired()) {
            Utils.ensureIdAttr(personalData.data, "PersonalSignatureId");
            Utils.prepareToSign(personalData);
        }
        ProviderResponseData result = new ProviderResponseData();
        result.to = providedResponse.getReplyTo();
        result.personalData = personalData;
        result.enclosures = providedResponse.getEnclosures();
        result.requestStatus = providedResponse.getRequestStatus();
        result.requestRejected = providedResponse.getRequestsRejected();
        result.messageID = UUID.generate();
        result.id = "SenderProvidedResponseDataId";
        return result;
    }

    @Override
    public ProviderResponse createProviderResponse(ProviderResponseData responseData) {
        SmevTypesProvider typesProvider = OSGi.service(SmevTypesProvider.class);

        ProviderResponse response = new ProviderResponse();
        response.data = responseData;
        Utils.prepareToSign(typesProvider.senderProvidedResponseData(responseData, false), response, responseData.id);
        return response;
    }
}
