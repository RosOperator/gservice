package ru.codeinside.smev.v3.transport.v1_1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import ru.codeinside.smev.v3.crypto.api.XmlSignature;
import ru.codeinside.smev.v3.crypto.bc.GostR3410Impl;
import ru.codeinside.smev.v3.crypto.bc.GostR3411Impl;
import ru.codeinside.smev.v3.crypto.sunpkcs7.SunSignatureAssembler;
import ru.codeinside.smev.v3.crypto.xmldsign.XmlSignatureAssemblerImpl;
import ru.codeinside.smev.v3.service.api.AsyncProcessingStatus;
import ru.codeinside.smev.v3.service.api.HaunterLog;
import ru.codeinside.smev.v3.service.api.InteractionStatus;
import ru.codeinside.smev.v3.service.api.provider.ProvidedResponse;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponse;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseData;
import ru.codeinside.smev.v3.transport.Mocks;
import ru.codeinside.smev.v3.transport.api.SmevTypesProvider;
import ru.codeinside.smev.v3.transport.api.v1_1.SMEVMessageExchangePortType;
import ru.codeinside.smev.v3.transport.api.v1_1.SendRequestResponse;
import ru.codeinside.smev.v3.transport.api.v1_1.SendResponseRequest;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

import static org.mockito.Mockito.mock;

public class SmevTransportImplTest extends Assert {

    private GostR3411Impl gostR3411 = new GostR3411Impl();
    private GostR3410Impl gostR3410 = new GostR3410Impl();
    private SunSignatureAssembler signatureAssembler = new SunSignatureAssembler();
    private final ru.codeinside.smev.v3.crypto.api.XmlNormalizer normalizer = new ru.codeinside.smev.v3.xml.XmlNormalizerImpl();
    private final ru.codeinside.gws.api.XmlNormalizer normalizerC14n = new ru.codeinside.gws.xml.normalizer.XmlNormalizerImpl();
    private final XmlSignatureAssemblerImpl xmlSignatureAssembler = new XmlSignatureAssemblerImpl();

    private SmevTransportImpl transport;

    @Before
    public void setUp() throws Exception {
        new ru.codeinside.smev.v3.crypto.xmldsign.Activator().start(Mocks.mockContext(normalizerC14n));

        SMEVMessageExchangePortType mockPortType = mock(SMEVMessageExchangePortType.class);

        transport = new SmevTransportImpl(mockPortType);
        transport.bindGostR3411(gostR3411);
        transport.bindGostR3410(gostR3410);
        transport.bindSignatureAssembler(signatureAssembler);
        transport.bindXmlSignatureAssembler(xmlSignatureAssembler);
    }

    @Test
    public void testGetRequestT() throws Exception {
        SmevTypesProvider typesProvider = new SmevTypesProviderImpl();
        ProviderResponse response = new ProviderResponse();

        response.data = new ProviderResponseData();
        response.data.messageID = "1234567890";
        response.data.id = "dsadsadsa";
        response.data.to = "12345";


        AsyncProcessingStatus asyncProcessingStatus = new AsyncProcessingStatus();
        asyncProcessingStatus.statusCategory = InteractionStatus.InteractionStatusType.REQUEST_IS_ACCEPTED_BY_SMEV;
        asyncProcessingStatus.originalMessageId = "1234567890";
        asyncProcessingStatus.statusDetails = "Status Details";

        response.data.asyncProcessingStatus = asyncProcessingStatus;

        response.signData = new byte[]{};
        response.signature =mock(XmlSignature.class);

        SendResponseRequest sendResponseRequest = typesProvider.sendResponseRequest(response);

        DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = docBuilder.newDocument();
        JAXBContext jaxbContext = JAXBContext.newInstance(sendResponseRequest.getClass());
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.marshal(sendResponseRequest, doc);

        System.out.println(documentToString(doc, false));



    }

    public static String documentToString(Document arg, boolean omitxmldeclaration) {
        try {
            DOMSource domSource = new DOMSource(arg);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);

            Transformer transformer = getSyncTransformer();
            if (omitxmldeclaration) {
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            }
            transformer.transform(domSource, result);
            writer.flush();
            String xml = writer.toString();
            writer.close();
            transformer.reset();
            return xml;
        } catch (Exception e) {
            }
        return null;
    }

    public static synchronized Transformer getSyncTransformer() throws TransformerConfigurationException {
        TransformerFactory tf = TransformerFactory.newInstance();
        return tf.newTransformer();
    }

    @Test
    public void testGetRequest() throws Exception {
        SendRequestResponse response = responseFromXml();
        XmlSignature xmlSignature = xmlSignatureAssembler.disassemble(response.getSMEVSignature().getAny());
        byte[] signData = normalizeData(response.getMessageMetadata());

        boolean result = transport.validateSignature(xmlSignature.getSignature(), signData);
        assertTrue(result);
    }

    private SendRequestResponse responseFromXml() {
        File fXmlFile = new File(getClass().getClassLoader().getResource("response_example.xml").getFile());
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setNamespaceAware(true);
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();

            JAXBContext jaxbContext = JAXBContext.newInstance(SendRequestResponse.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            return (SendRequestResponse) unmarshaller.unmarshal(doc.getDocumentElement());
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] normalizeData(Object object) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        if (object instanceof Element) {
            normalizer.normalizeDom((Element) object, os);
        } else {
            try {
                normalizer.normalizeJaxb(object, os);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return os.toByteArray();
    }

}