/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2013, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.gses.webui;

import com.vaadin.terminal.PaintException;
import com.vaadin.terminal.PaintTarget;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.themes.Reindeer;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.adm.database.Employee;
import ru.codeinside.adm.database.Role;
import ru.codeinside.gses.webui.components.TabChanger;
import ru.codeinside.gses.webui.components.UserInfoPanel;
import ru.codeinside.gses.webui.declarant.DeclarantFactory;
import ru.codeinside.gses.webui.executor.ArchiveFactory;
import ru.codeinside.gses.webui.executor.ExecutorFactory;
import ru.codeinside.gses.webui.manager.ManagerWorkplace;
import ru.codeinside.gses.webui.supervisor.SupervisorWorkplace;
import ru.codeinside.gses.webui.supervisor.TaskManager;

import java.util.Set;

final public class Workplace extends CustomComponent {

  public Workplace(String login, Set<Role> roles, boolean production) {

    TabSheet tabSheet = new TabSheet();
    tabSheet.setSizeFull();
    tabSheet.setStyleName(Reindeer.TABSHEET_BORDERLESS);
    tabSheet.setCloseHandler(new DelegateCloseHandler());

    if (!production) {
      new MemoryUsage(tabSheet);
    }

    if (roles.contains(Role.Declarant)) {
      new TabChanger(tabSheet).set(DeclarantFactory.create(), "Запросы");
    }

    if (roles.contains(Role.Executor)) {
      TabChanger executorChanger = new TabChanger(tabSheet);
      executorChanger.set(ExecutorFactory.create(executorChanger, tabSheet), "Ответы");

      TabChanger archiveChanger = new TabChanger(tabSheet);
      archiveChanger.set(ArchiveFactory.create(), "Архив");
    }

	if (roles.contains(Role.Executor) || roles.contains(Role.Declarant) || roles.contains(Role.Supervisor) || roles.contains(Role.SuperSupervisor)) {
          SmevTasksPanel smevTasksPanel = new SmevTasksPanel();
          tabSheet.addTab(smevTasksPanel, "Уведомления");
          tabSheet.addListener(smevTasksPanel);
    }

    if (roles.contains(Role.Supervisor) || roles.contains(Role.SuperSupervisor)) {
      new TabChanger(tabSheet).set(new SupervisorWorkplace(), "Контроль исполнения");
    }

    if (roles.contains(Role.SuperSupervisor)) {
      new TabChanger(tabSheet).set(new TaskManager(), "Состояние исполнения");
    }

    if (roles.contains(Role.Manager)) {
      new TabChanger(tabSheet).set(new ManagerWorkplace(), "Управление видами сведений");
    }

    Employee employee = AdminServiceProvider.get().findEmployeeByLogin(login);
    String userName = (employee != null && employee.getFio() != null) ? employee.getFio() : login;
    UserInfoPanel.addClosableToTabSheet(tabSheet, login, userName);
    tabSheet.setSelectedTab(0);
    setCompositionRoot(tabSheet);

    setSizeFull();
  }

  @Override
  public void paint(PaintTarget target) throws PaintException {
    super.paint(target);

    StringBuilder script = new StringBuilder();
    script.append("document.getElementsByClassName('v-caption-closable')[0].parentNode.parentNode.className += \" usertab\";");
    getWindow().executeJavaScript(script.toString());
  }
}
