package ru.codeinside.smev.v3.server_stub.impl;

import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import ru.codeinside.gws.api.Signature;
import ru.codeinside.smev.v3.crypto.api.*;
import ru.codeinside.smev.v3.server_stub.AwaitingAck;
import ru.codeinside.smev.v3.server_stub.Smev3Logic;
import ru.codeinside.smev.v3.server_stub.osgi.Activator;
import ru.codeinside.smev.v3.server_stub.services.StubFTPService;
import ru.codeinside.smev.v3.service.api.InformationType;
import ru.codeinside.smev.v3.transport.api.v1_1.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.Logger;


@SuppressWarnings({"PackageAccessibility", "unused"})
@Singleton
public class Smev3LogicImpl implements Smev3Logic {
    private final Logger log = Logger.getLogger(Smev3LogicImpl.class.getName());

    private final ConcurrentMap<InformationType, Queue<SendRequestRequest>> requests =
            new ConcurrentHashMap<InformationType, Queue<SendRequestRequest>>();
    private final ConcurrentMap<InformationType, Queue<SendResponseRequest>> responses =
            new ConcurrentHashMap<InformationType, Queue<SendResponseRequest>>();

    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    private final List<AwaitingAck<SendRequestRequest>> requestAcks = new CopyOnWriteArrayList<AwaitingAck<SendRequestRequest>>();
    private final List<AwaitingAck<SendResponseRequest>> responseAcks = new CopyOnWriteArrayList<AwaitingAck<SendResponseRequest>>();

    @Inject
    StubFTPService ftpService;

    @PostConstruct
    public void postConstruct() {
        executorService.scheduleAtFixedRate(new AckProcessing(), 0, 60, TimeUnit.SECONDS);
    }

    @PreDestroy
    public void preDestroy() {
        executorService.shutdown();
    }

    @Override
    public SendRequestResponse putRequest(SendRequestRequest request) throws SMEVFailureException {
        InformationType informationType = parseInformationType(
                request.getSenderProvidedRequestData().getMessagePrimaryContent().getAny());
        informationType = normalize(informationType);
        getQueue(informationType, requests).add(request);

        MessageMetadata.Recipient recipient = new MessageMetadata.Recipient();
        recipient.setMnemonic("SIU TEST");
        recipient.setHumanReadableName("Тестовая среда СИУ");

        MessageMetadata.Sender sender = new MessageMetadata.Sender();
        sender.setMnemonic("SIU TEST");
        sender.setHumanReadableName("Тестовая среда СИУ");

        MessageMetadata messageMetadata = new MessageMetadata();
        messageMetadata.setDeliveryTimestamp(toXmlDate(new Date()));
        messageMetadata.setDestinationName("Тестовая система");
        messageMetadata.setId("MessageMetadataId");
        messageMetadata.setMessageType(MessageTypeType.REQUEST);
        messageMetadata.setRecipient(recipient);
        messageMetadata.setSender(sender);
        messageMetadata.setSendingTimestamp(toXmlDate(new Date()));
        messageMetadata.setStatus(InteractionStatusType.REQUEST_IS_ACCEPTED_BY_SMEV);

        SendRequestResponse requestResponse = new SendRequestResponse();
        requestResponse.setMessageMetadata(messageMetadata);
        XMLDSigSignatureType smevSignature;
        try {
            smevSignature = smevSign(messageMetadata, messageMetadata.getId());
        } catch (Exception e) {
            throw new SMEVFailureException(e.getMessage(), null, e);
        }
        requestResponse.setSMEVSignature(smevSignature);
        return requestResponse;
    }

    @Override
    public SendResponseResponse putResponse(SendResponseRequest response) throws SMEVFailureException {

        InformationType informationType = buildInformationType(response.getSenderProvidedResponseData().getTo());
        getQueue(informationType, responses).add(response);

        MessageMetadata.Recipient recipient = new MessageMetadata.Recipient();
        recipient.setMnemonic("SIU TEST");
        recipient.setHumanReadableName("Тестовая среда СИУ");

        MessageMetadata.Sender sender = new MessageMetadata.Sender();
        sender.setMnemonic("SIU TEST");
        sender.setHumanReadableName("Тестовая среда СИУ");

        MessageMetadata messageMetadata = new MessageMetadata();
        messageMetadata.setDeliveryTimestamp(toXmlDate(new Date()));
        messageMetadata.setDestinationName("Тестовая система");
        messageMetadata.setId("MessageMetadataId");
        messageMetadata.setMessageType(MessageTypeType.RESPONSE);
        messageMetadata.setRecipient(recipient);
        messageMetadata.setSender(sender);
        messageMetadata.setSendingTimestamp(toXmlDate(new Date()));
        messageMetadata.setStatus(InteractionStatusType.RESPONSE_IS_ACCEPTED_BY_SMEV);

        SendResponseResponse _response = new SendResponseResponse();
        _response.setMessageMetadata(messageMetadata);
        XMLDSigSignatureType smevSignature;
        try {
            smevSignature = smevSign(messageMetadata, messageMetadata.getId());
        } catch (Exception e) {
            throw new SMEVFailureException(e.getMessage(), null, e);
        }
        _response.setSMEVSignature(smevSignature);
        return _response;
    }

    @Override
    public GetRequestResponse getRequest(InformationType informationType) throws SMEVFailureException {
        informationType = normalize(informationType);
        log.info("получаю IType - " + informationType.getNamespace()+ " " + informationType.getRootLocalElementName());
        SendRequestRequest request = getQueue(informationType, requests).poll();
        if (request != null) {
            log.info("REQ_CNT - " +String.valueOf(requests.size()));
            log.info("получаю MSGID - " + request.getSenderProvidedRequestData().getMessageID());
        
        }
        return buildGetRequestResponse(request, informationType);
    }

    @Override
    public GetResponseResponse getResponse(InformationType informationType) throws SMEVFailureException {
        informationType = normalize(informationType);
        SendResponseRequest response = getQueue(informationType, responses).poll();

        return buildGetResponseResponse(response);
    }

    @Override
    public void ack(String id) {
        if (validateAck(requestAcks, id)) {
            return;
        }
        validateAck(responseAcks, id);
    }

    private boolean validateAck(List<? extends AwaitingAck<?>> list, String id) {
        int deletePos = -1;
        for (int i = 0; i < list.size(); ++i) {
            AwaitingAck<?> item = list.get(i);
            if (item.getId().equals(id)) {
                deletePos = i;
                break;
            }
        }
        if (deletePos > -1) {
            list.remove(deletePos);
            return true;
        }
        return false;
    }

    private <T> Queue<T> getQueue(InformationType informationType, ConcurrentMap<InformationType, Queue<T>> queueMap) {
        Queue<T> emptyQueue = new ConcurrentLinkedQueue<T>();
        Queue<T> exist = queueMap.putIfAbsent(informationType, emptyQueue);
        return exist == null ? emptyQueue : exist;
    }

    private GetRequestResponse buildGetRequestResponse(SendRequestRequest request, InformationType informationType) throws SMEVFailureException {
        if (request == null) {
            return new GetRequestResponse();
        }

        String replyTo = buildReplyTo(informationType, request.getSenderProvidedRequestData().getMessageID());
        String id = request.getSenderProvidedRequestData().getMessageID();
        requestAcks.add(new AwaitingAck<SendRequestRequest>(request, id));

        MessageMetadata.Recipient recipient = new MessageMetadata.Recipient();
        recipient.setMnemonic("SIU TEST");
        recipient.setHumanReadableName("Тестовая среда СИУ");

        MessageMetadata.Sender sender = new MessageMetadata.Sender();
        sender.setMnemonic("SIU TEST");
        sender.setHumanReadableName("Тестовая среда СИУ");

        MessageMetadata messageMetadata = new MessageMetadata();
        messageMetadata.setDeliveryTimestamp(toXmlDate(new Date()));
        messageMetadata.setDestinationName("Тестовая система");
        messageMetadata.setId("MessageMetadataId");
        messageMetadata.setMessageType(MessageTypeType.REQUEST);
        messageMetadata.setRecipient(recipient);
        messageMetadata.setSender(sender);
        messageMetadata.setSendingTimestamp(toXmlDate(new Date()));
        messageMetadata.setStatus(InteractionStatusType.REQUEST_IS_QUEUED);

        Request _request = new Request();
        _request.setId("RequestId");
        _request.setReplyTo(replyTo);
        _request.setSenderProvidedRequestData(request.getSenderProvidedRequestData());
        _request.setSenderInformationSystemSignature(request.getCallerInformationSystemSignature());
        _request.setMessageMetadata(messageMetadata);
        _request.setFSAttachmentsList(ftpService.buildFSAttachmentList(request.getSenderProvidedRequestData().getRefAttachmentHeaderList()));

        GetRequestResponse.RequestMessage requestMessage = new GetRequestResponse.RequestMessage();
        requestMessage.setRequest(_request);
        requestMessage.setAttachmentContentList(request.getAttachmentContentList());
        try {
            requestMessage.setSMEVSignature(smevSign(_request, _request.getId()));
        } catch (Exception e) {
            throw new SMEVFailureException(e.getMessage(), null, e);
        }

        GetRequestResponse response = new GetRequestResponse();
        response.setRequestMessage(requestMessage);
        return response;
    }

    private GetResponseResponse buildGetResponseResponse(SendResponseRequest response) throws SMEVFailureException {
        if (response == null) {
            return new GetResponseResponse();
        }

        String id = response.getSenderProvidedResponseData().getMessageID();
        responseAcks.add(new AwaitingAck<SendResponseRequest>(response, id));

        MessageMetadata.Recipient recipient = new MessageMetadata.Recipient();
        recipient.setMnemonic("SIU TEST");
        recipient.setHumanReadableName("Тестовая среда СИУ");

        MessageMetadata.Sender sender = new MessageMetadata.Sender();
        sender.setMnemonic("SIU TEST");
        sender.setHumanReadableName("Тестовая среда СИУ");

        MessageMetadata messageMetadata = new MessageMetadata();
        messageMetadata.setDeliveryTimestamp(toXmlDate(new Date()));
        messageMetadata.setDestinationName("Тестовая система");
        messageMetadata.setId("MessageMetadataId");
        messageMetadata.setMessageType(MessageTypeType.RESPONSE);
        messageMetadata.setRecipient(recipient);
        messageMetadata.setSender(sender);
        messageMetadata.setSendingTimestamp(toXmlDate(new Date()));
        messageMetadata.setStatus(InteractionStatusType.RESPONSE_IS_DELIVERED);

        Response _response = new Response();
        _response.setId("ResponseId");
        _response.setMessageMetadata(messageMetadata);
        _response.setFSAttachmentsList(ftpService.buildFSAttachmentList(response.getSenderProvidedResponseData().getRefAttachmentHeaderList()));
        _response.setOriginalMessageId(response.getSenderProvidedResponseData().getTo().split("\\|")[2]);
        _response.setSenderProvidedResponseData(response.getSenderProvidedResponseData());
        _response.setSenderInformationSystemSignature(response.getCallerInformationSystemSignature());

        GetResponseResponse.ResponseMessage responseMessage = new GetResponseResponse.ResponseMessage();
        responseMessage.setResponse(_response);
        responseMessage.setAttachmentContentList(response.getAttachmentContentList());
        XMLDSigSignatureType smevSignature;
        try {
            smevSignature = smevSign(_response, _response.getId());
        } catch (Exception e) {
            throw new SMEVFailureException(e.getMessage(), null, e);
        }
        responseMessage.setSMEVSignature(smevSignature);

        GetResponseResponse responseResponse = new GetResponseResponse();
        responseResponse.setResponseMessage(responseMessage);
        return responseResponse;
    }

    private InformationType parseInformationType(Element element) {
        String namespaceURI = element.getNamespaceURI();
        String rootElementLocalName = element.getLocalName().replace("Response", "Request");
        return new InformationType(namespaceURI, rootElementLocalName);
    }

    private XMLGregorianCalendar toXmlDate(Date date) {
        GregorianCalendar timestamp = new GregorianCalendar();
        timestamp.setTime(date);

        XMLGregorianCalendar gregorianCalendar = null;
        try {
            gregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(timestamp);
        } catch (DatatypeConfigurationException e) {
            log.severe("Can't get DatatypeFactory instance: " + e.getMessage());
        }
        return gregorianCalendar;
    }

    private XMLDSigSignatureType smevSign(Object jaxb, String id) throws ParserConfigurationException, IOException, SAXException, JAXBException, XPathExpressionException {
        XmlNormalizer normalizer = Activator.getService(XmlNormalizer.class);
        if (normalizer == null) {
            log.info("Не удалось получить нормалайзер.");
            return null;
        }

        ByteArrayOutputStream normalizedOut = new ByteArrayOutputStream();
        normalizer.normalizeJaxb(jaxb, normalizedOut);
        byte[] normalizedBytes = normalizedOut.toByteArray();

        GostR3411 gostR3411 = Activator.getService(GostR3411.class);
        if (gostR3411 == null) {
            log.info("Не удалось получить ГОСТ-Р 3411.");
            return null;
        }
        byte[] digest = gostR3411.digest(new ByteArrayInputStream(normalizedBytes));
        Signature signature = new Signature(null, (byte[]) null, null, digest, false);

        XmlSignature xmlSignature = new XmlSignature(signature);
        xmlSignature.setElementReference("#" + id);

        XmlSignatureAssembler assembler = Activator.getService(XmlSignatureAssembler.class);
        if (assembler == null) {
            log.info("Не удалось получить ассемблер сигнатуры.");
            return null;
        }
        XMLDSigSignatureType xmldSigSignatureType = new XMLDSigSignatureType();
        xmldSigSignatureType.setAny(assembler.assemble(xmlSignature));
        XPathExpression signedInfoExpression =
                XPathFactory.newInstance().newXPath().compile("//*[local-name()='SignedInfo']");
        Element signedInfo = (Element) signedInfoExpression.evaluate(xmldSigSignatureType.getAny(), XPathConstants.NODE);

        ru.codeinside.gws.api.XmlNormalizer c14Normalizer = Activator.getService(ru.codeinside.gws.api.XmlNormalizer.class);
        if (c14Normalizer == null) {
            log.info("Не удалось получить C14 нормалайзер.");
            return null;
        }

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        c14Normalizer.normalize(signedInfo, os);
        byte[] signedInfoBytes = os.toByteArray();

        GostR3410 gostR3410 = Activator.getService(GostR3410.class);
        if (gostR3410 == null) {
            log.info("Не удалось получить ГОСТ-Р 3410.");
            return null;
        }
        Signature sign = gostR3410.sign(new ByteArrayInputStream(signedInfoBytes));

        xmlSignature.setSignature(new Signature(sign.certificate, (byte[]) null, sign.sign, digest, true));
        xmldSigSignatureType.setAny(assembler.assemble(xmlSignature));
        return xmldSigSignatureType;
    }

    private String buildReplyTo(InformationType informationType, String messageId) {
        StringBuilder builder = new StringBuilder("8133");
        builder.append("|");
        String[] replyTo = informationType.getNamespace().split("\\/");

        for (String reply : replyTo) {
            builder.append(reply);
            builder.append("_");
        }
        builder.append(informationType.getRootLocalElementName());
        builder.append("|");
        builder.append(messageId);
        return builder.toString();
    }

    private InformationType buildInformationType(String reply) {
        String[] replyTo = reply.split("\\|")[1].split("_");
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < replyTo.length - 1; i++) {
            stringBuilder.append(replyTo[i]);
            stringBuilder.append("/");
        }
        int index = stringBuilder.lastIndexOf("/");
        stringBuilder.deleteCharAt(index);

        return new InformationType(stringBuilder.toString(), replyTo[replyTo.length - 1]);
    }

    private InformationType normalize(InformationType informationType) {
        String namespace = informationType.getNamespace();
        namespace = namespace.replaceFirst("https://", "").replaceFirst("http://", "").replaceFirst("urn://", "");
        return new InformationType(namespace, informationType.getRootLocalElementName());
    }

    /* Inner classes */

    private class AckProcessing implements Runnable {

        @Override
        public void run() {
            if (Thread.currentThread().isInterrupted()) {
                return;
            }
            processRequests();
            processResponses();
        }

        private void processRequests() {
            Iterator<AwaitingAck<SendRequestRequest>> requestsIterator = requestAcks.iterator();
            while (requestsIterator.hasNext()) {
                AwaitingAck<SendRequestRequest> item = requestsIterator.next();
                if (item.isOutOfDate()) {
                    try {
                        putRequest(item.getData());
                        requestAcks.remove(item);
                    } catch (SMEVFailureException e) {
                        log.severe("Не удалось вернуть запрос (" + item.getId() + ") в очередь: " + e.getMessage());
                    }
                }
            }
        }

        private void processResponses() {
            Iterator<AwaitingAck<SendResponseRequest>> responseIterator = responseAcks.iterator();
            while (responseIterator.hasNext()) {
                AwaitingAck<SendResponseRequest> item = responseIterator.next();
                if (item.isOutOfDate()) {
                    try {
                        putResponse(item.getData());
                        responseAcks.remove(item);
                    } catch (SMEVFailureException e) {
                        log.severe("Не удалось вернуть ответ (" + item.getId() + ") в очередь: " + e.getMessage());
                    }
                }
            }
        }
    }
}
