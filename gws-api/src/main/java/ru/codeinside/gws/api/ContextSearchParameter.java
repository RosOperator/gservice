/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.codeinside.gws.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author akaranets
 */
public class ContextSearchParameter {
    private List<ContextSearchCondition> searchConditions;
    
    private String searchValue;
    private Map<String, Object> exchangeableValues;
    
    public enum SEARCH_TYPE {
        IS_EQUAL,
        IS_NOT_EQUAL
    }

    public ContextSearchParameter() {
        searchConditions = new ArrayList<ContextSearchCondition>();
        exchangeableValues = new HashMap<String, Object>();
    }
    
    public boolean hasConditions() {
        return searchConditions.size() > 0;
    }
    
    public void addSearchCondition(String key, String value, SEARCH_TYPE searchType) {
        if (key == null) {
            return;
        }
        searchConditions.add(new ContextSearchCondition(key, value, searchType));
    }

    public List<ContextSearchCondition> getSearchConditions() {
        return searchConditions;
    }

    public void setSearchConditions(List<ContextSearchCondition> searchConditions) {
        this.searchConditions = searchConditions;
    }
    

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String sValue) {
        this.searchValue = sValue;
    }
    
    public void addExchangeableValue(String key, Object value) {
        exchangeableValues.put(key, value);
    }

    public Map<String, Object> getExchangeableValues() {
        return exchangeableValues;
    }
    
    
    
    public class ContextSearchCondition {
        private String key;
        private String value;
        private SEARCH_TYPE searchType;

        public ContextSearchCondition(String key, String value, SEARCH_TYPE searchType) {
            this.key = key;
            this.value = value;
            this.searchType = searchType;
        }
        
        public String getKey() {
            return key;
        }

        public SEARCH_TYPE getSearchType() {
            return searchType;
        }

        public String getValue() {
            return value;
        }
        
    }
}
