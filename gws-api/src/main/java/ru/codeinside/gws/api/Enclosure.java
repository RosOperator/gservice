/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2013, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.gws.api;

import java.io.*;

/**
 * Вложение СМЭВ.
 */
final public class Enclosure {

    /**
     * Ссылка на файл
     */
    private File file;

    /**
     * Имя файла документа
     */
    public String fileName;

    /**
     * Идентификатор документа в описателе.
     */
    public String id;

    /**
     * Код документа
     */
    public String code;

    /**
     * Номер документа
     */
    public String number;

    /**
     * Содержимое
     */
    public byte[] content;

    /**
     * Относительный путь к файлу внутри архива
     */
    public String zipPath;

    /**
     * Тип содержимого(например application/pdf)
     */
    public String mimeType;

    /**
     * Хеш-код вложения ГОСТ 34.11-94.
     */
    public byte[] digest;

    /**
     * Подпись
     */
    public Signature signature;

    public Enclosure(final String zipPath, final byte[] content) {
        if (zipPath == null || content == null) {
            throw new NullPointerException();
        }
        this.zipPath = zipPath;
        this.content = content;
        this.file = null;
    }

    public Enclosure(final String zipPath, final String name, final byte[] content) {
        this(zipPath, content);
        this.fileName = name;
    }

    public Enclosure(final String zipPath, final File file) {
        if (zipPath == null || file == null) {
            throw new NullPointerException();
        }
        this.zipPath = zipPath;
        this.file = file;
        this.content = null;
    }

    public Enclosure(final String zipPath, final String name, final File file) {
        this(zipPath, file);
        this.fileName = name;
    }

    public InputStream getContent() {
        if (getFile().getName().isEmpty()) {
            return new ByteArrayInputStream(new byte[] {0});
        }
        try {
            return new BufferedInputStream(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Файл не найден: " + e.getMessage());
        }
    }

    public long size() {
        if (content != null && file == null) {
            return content.length;
        }
        
        return file.length();
    }

    public File getFile() {
        if (content != null && file == null) {
            OutputStream out = null;
            try {
                this.file = File.createTempFile("enc-", "");
                out = new FileOutputStream(this.file);
                out.write(content);
                out.flush();
            } catch (IOException e) {
                throw new RuntimeException("Не удалось преобразовать вложение во временный файл: " + e.getMessage(), e);
            } finally {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException ignored) {
                    }
                }
            }
        }
        return this.file;
    }

    @Override
    public String toString() {
        return "{" +
                (fileName == null ? "" : "fileName='" + fileName + '\'') +
                (id == null ? "" : (", id='" + id + '\'')) +
                (code == null ? "" : (", code='" + code + '\'')) +
                (number == null ? "" : (", number='" + number + '\'')) +
                ", zipPath='" + zipPath + '\'' +
                (mimeType == null ? "" : (", mimeType='" + mimeType + '\'')) +
                (digest == null ? "" : (", digest[]=" + digest.length)) +
                (signature == null ? "" : (", signature[]=" + signature)) +
                '}';
    }
}