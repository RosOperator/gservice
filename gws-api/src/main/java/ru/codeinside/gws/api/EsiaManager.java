package ru.codeinside.gws.api;

/**
 * API сервиса интеграции с ЕСИА
 */
public interface EsiaManager {

    /**
     * Удаление сессии ЕСИА с указанным идентификатором
     * @param sessionId       идентификатор сессии
     */
    void destroyEsiaSession(String sessionId);

    /**
     * Получение СНИЛС пользователя из сессии ЕСИА с указанным идентификатором
     * @param sessionId       идентификатор сессии
     */

    String getSnilsAttribute(String sessionId);

    /**
     * Валидация сессии ЕСИА с указанным идентификатором
     * @param sessionId       идентификатор сессии
     */
    boolean isValidEsiaSession(String sessionId);
}
