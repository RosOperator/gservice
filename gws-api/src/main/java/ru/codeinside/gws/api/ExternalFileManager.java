package ru.codeinside.gws.api;

/**
 * Интерфейс внешнего хранилища вложений
 */
public interface ExternalFileManager {

    class ExternalFile {
        public ExternalFile(String type, String name, byte[] content) {
            this.type = type;
            this.name = name;
            this.content = content;
        }
        public String type;
        public String name;
        public byte[] content;
    }

    /**
     * Загружает файл во внешнее хранилище. Возвращает ссылку на загруженный файл
     *
     * @param externalFile загружаемый файл
     */
    String putFile(ExternalFile externalFile);

    /**
     * Выгружает файл из внешнего хранилища
     *
     * @param externalLink ссылка на загруженный файл
     */
    ExternalFile getFile(String externalLink);

    /**
     * Удаляет файл из внешнего хранилища
     *
     * @param externalLink ссылка на загруженный файл
     */
    void deleteFile(String externalLink);

    /**
     * Возвращает контрольную сумму загруженного файла
     *
     * @param externalLink ссылка на загруженный файл
     */
    String getFileChecksum(String externalLink);
}