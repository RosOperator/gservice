package ru.codeinside.gws.api;

/**
 * Структура данных, содержащая мнемонику сервиса и номер его версии.
 * Значения элементов данной структуры используются сервисом динамической маршрутизации
 * для определения конечной точки маршрутизации сообщения.
 */
public class Service {

    /**
     * Мнемоника сервиса
     */
    public String mnemonic;

    /**
     * Версия сервиса
     */
    public String version;

    public Service(String mnemonic, String version) {
        this.mnemonic = mnemonic;
        this.version = version;
    }
}
