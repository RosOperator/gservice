package ru.codeinside.gws.api;

import org.w3c.dom.Element;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Апи сервиса обертки смев 3
 */
public interface Smev3Wrapper {
    /**
     * @param source       данные 
     * @param outputStream исходящий поток результата
     */
    void doSomethin(InputStream source, OutputStream outputStream);

    /**
     * @param sourceElement элемент xml
     * @param outputStream  исходящий поток результата
     */
    void doSomethingWithXml(Element sourceElement, OutputStream outputStream);
    
    String test() throws Exception;
}
