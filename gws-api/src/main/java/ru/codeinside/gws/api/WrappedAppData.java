/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2015, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.gws.api;

public class WrappedAppData {
    /**
     * Данные обернутые в тег AppData
     */
    private final String wrappedAppData;

    /**
     * Подпись содержимого
     */
    private final Signature signature;
    /**
     * Признак позиции подписи в Xml элементе, если false то в начало, если true то в конец
     */
    private final Boolean signAfterContent;

    public WrappedAppData(final String wrappedAppData, final Signature signature, final Boolean signAfterContent) {
        this.wrappedAppData = wrappedAppData;
        this.signature = signature;
        this.signAfterContent = signAfterContent;
    }

    public String getWrappedAppData() {
        return wrappedAppData;
    }

    public Signature getSignature() {
        return signature;
    }
    
    public Boolean getSignAfterContent() {
        return signAfterContent;
    }
}
