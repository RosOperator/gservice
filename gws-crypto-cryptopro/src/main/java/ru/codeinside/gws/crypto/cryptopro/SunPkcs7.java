/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2013, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.gws.crypto.cryptopro;

import java.io.ByteArrayInputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ru.codeinside.gws.api.Signature;
import sun.security.pkcs.ContentInfo;
import sun.security.pkcs.PKCS7;
import sun.security.pkcs.ParsingException;
import sun.security.pkcs.SignerInfo;
import sun.security.util.DerOutputStream;
import sun.security.util.ObjectIdentifier;
import sun.security.x509.AlgorithmId;
import sun.security.x509.X500Name;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;

final public class SunPkcs7 {

    final static ObjectIdentifier GOST3410;
    final static ObjectIdentifier GOST3411;
    final static ObjectIdentifier GOST3410GOST3411;

    static {
        try {
            GOST3410 = new ObjectIdentifier("1.2.643.2.2.19");
            GOST3411 = new ObjectIdentifier("1.2.643.2.2.9");
            GOST3410GOST3411 = new ObjectIdentifier("1.2.643.2.2.3");
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    final private static Log log = LogFactory.getLog(CryptoProvider.class);

    public static byte[] toPkcs7(final Signature signature) {
        final X509Certificate certificate = signature.certificate;
        final byte[] sign = signature.sign;
        X500Name issuer = X500Name.asX500Name(certificate.getIssuerX500Principal());
        final AlgorithmId digestAlgorithmId = new AlgorithmId(GOST3411);
        final AlgorithmId signAlgorithmId = new AlgorithmId(GOST3410);
        SignerInfo sInfo = new SignerInfo(issuer, certificate.getSerialNumber(), digestAlgorithmId, signAlgorithmId, sign);
        ContentInfo cInfo = new ContentInfo(ContentInfo.DATA_OID, null);
        PKCS7 pkcs7 = new PKCS7(
                new AlgorithmId[]{digestAlgorithmId},
                cInfo,
                new X509Certificate[]{certificate},
                new SignerInfo[]{sInfo});
        final ByteArrayOutputStream bOut = new DerOutputStream();
        try {
            pkcs7.encodeSignedData(bOut);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return bOut.toByteArray();
    }
    
    public static Signature bcFromPkcs7(final byte[] bytes) throws CertificateException, CMSException, IOException {
        CMSSignedData signedData = new CMSSignedData(bytes);
        
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        List<X509Certificate> certificates = new ArrayList<X509Certificate>();
        for (X509CertificateHolder holder : (Collection<X509CertificateHolder>) signedData.getCertificates().getMatches(null)) {
            certificates.add((X509Certificate) certificateFactory.generateCertificate(new ByteArrayInputStream(holder.getEncoded())));
        }
        
        if (certificates == null || certificates.isEmpty()) {
                log.info("BOUNCYCASTLE: no certificate in pkcs7");
        } else {
            final X509Certificate certificate = certificates.get(0);
            Boolean certIsValid;
            try {
                certificate.checkValidity();
                certIsValid = true;
            } catch (CertificateExpiredException e){
                certIsValid = false;
            } catch (CertificateNotYetValidException e) {
                certIsValid = false;
            }
            if (certIsValid) {
                SignerInformationStore siStore = signedData.getSignerInfos();
                if (siStore == null || siStore.size() == 0) {
                    log.info("BOUNCYCASTLE: no signerInfos in pkcs7");
                } else {
                    final ObjectIdentifier digestOID = new AlgorithmId(GOST3411).getOID();
                    final ObjectIdentifier signOID = new AlgorithmId(GOST3410GOST3411).getOID();
                    Collection<SignerInformation> c = siStore.getSigners();
                    for (SignerInformation signer : c)
                    {
                        
                        if (!signer.getDigestAlgOID().equals(digestOID.toString())) {
                            log.info("BOUNCYCASTLE: no GOST3411 in pkcs7");
                        } else if (!certificate.getSigAlgOID().equals(signOID.toString())) {
                            log.info("BOUNCYCASTLE: no GOST3410 in pkcs7");
                        } else {
                            return new Signature(certificate, (InputStream) null, signer.getSignature(), true);
                        }
                    }              
                    
                }
            } else {
                log.info("BOUNCYCASTLE: invalid certificate");
            }                
        }     
        return new Signature(null, (InputStream) null, null, false);
    }

    public static Signature fromPkcs7(final byte[] bytes) {
        final PKCS7 pkcs7;
        try {
            pkcs7 = new PKCS7(bytes);
        } catch (ParsingException e) {
            try {
                return bcFromPkcs7(bytes);
            } catch (Exception ex) {
                log.info("fail parse sunpkcs7: ", e);
                log.info("fail parse bouncycastle: ", ex);
                return new Signature(null, (InputStream) null, null, false);
            }
        }

        final AlgorithmId digestAlgorithmId = new AlgorithmId(GOST3411);
        final AlgorithmId signAlgorithmId = new AlgorithmId(GOST3410);
        final AlgorithmId[] digestAlgorithmIds = pkcs7.getDigestAlgorithmIds();

        if (digestAlgorithmIds == null || digestAlgorithmIds.length == 0) {
            log.info("no digestAlgorithm in pkcs7");
        } else if (!digestAlgorithmIds[0].equals(digestAlgorithmId)) {
            log.info("no GOST3411 in pkcs7");
        } else {
            final X509Certificate[] certificates = pkcs7.getCertificates();
            if (certificates == null || certificates.length == 0) {
                log.info("no certificate in pkcs7");
            } else {
                final X509Certificate certificate = certificates[0];
                final SignerInfo[] signerInfos = pkcs7.getSignerInfos();
                if (signerInfos == null || signerInfos.length == 0) {
                    log.info("no signerInfos in pkcs7");
                } else {
                    final SignerInfo signerInfo = signerInfos[0];
                    if (!signerInfo.getIssuerName().equals(X500Name.asX500Name(certificate.getIssuerX500Principal()))) {
                        log.info("invalid issuerX500Principal in pkcs7");
                    } else if (!signerInfo.getDigestAlgorithmId().equals(digestAlgorithmId)) {
                        log.info("no GOST3411 in pkcs7");
                    } else if (!signerInfo.getDigestEncryptionAlgorithmId().equals(signAlgorithmId)) {
                        log.info("no GOST3410 in pkcs7");
                    } else if (!signerInfo.getCertificateSerialNumber().equals(certificate.getSerialNumber())) {
                        log.info("invalid certificate serial number in pkcs7");
                    } else {
                        return new Signature(certificate, (byte[]) null, signerInfo.getEncryptedDigest(), true);
                    }
                }
            }
        }
        return new Signature(null, (byte[]) null, null, false);
    }

}
