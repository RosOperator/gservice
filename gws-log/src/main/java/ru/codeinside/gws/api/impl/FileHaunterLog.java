package ru.codeinside.gws.api.impl;

import ru.codeinside.gws.log.format.Pack;
import ru.codeinside.smev.v3.service.api.HaunterLog;
import ru.codeinside.smev.v3.service.api.MessageMetadata;
import ru.codeinside.smev.v3.service.api.MsgDataField;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.EnumMap;


final class FileHaunterLog extends FileLog implements HaunterLog {

    final Class<String> stringClass = String.class;
    final Class<Date> dateClass = Date.class;
    final Class<MsgDataField> msgDataFieldClass = MsgDataField.class;
    final EnumMap<MapType, EnumMap<MsgDataField, Object>> logData = new EnumMap<MapType, EnumMap<MsgDataField, Object>>(MapType.class);
    final Boolean exception = true;
    final Boolean noException = false;


    private enum MapType {
        other, receive, send;

        private static MapType fromMessageState(final String messageState) {
            final String initialRegex = "((Get)|(Send)|(Both))((Request)|(Response)|(Both))(";
            final String receiveTypeRegex = initialRegex + "Response)$";
            final String sendTypeRegex = initialRegex + "Request)$";
            if (messageState.matches(sendTypeRegex)) {
                return MapType.send;
            } else if (messageState.matches(receiveTypeRegex)) {
                return MapType.receive;
            } else {
                return MapType.other;
            }
        }
    }


    public FileHaunterLog(boolean isLogEnabled, boolean isErrorLogEnabled, String status) {
        super(isLogEnabled, isErrorLogEnabled, status);
        logData.put(MapType.other, new EnumMap<MsgDataField, Object>(msgDataFieldClass));
        logData.put(MapType.send, new EnumMap<MsgDataField, Object>(msgDataFieldClass));
        logData.put(MapType.receive, new EnumMap<MsgDataField, Object>(msgDataFieldClass));
    }

    private Boolean addField(final MapType mapType, final MsgDataField key, final Object value) {
        final EnumMap<MsgDataField, Object> typedLogData = logData.get(mapType);
        return !typedLogData.containsKey(key) && value != null && typedLogData.put(key, value) == null;
    }

    @Override
    public void addData(final String messageState, final MsgDataField key, final Object value) {
        this.addField(MapType.fromMessageState(messageState), key, value);
    }

    @Override
    public void addData(final MessageMetadata messageMetadata) {
        final MapType mapType = MapType.receive;
        Date messageTimestamp = messageMetadata.sendTime;

        final String messageState = safe(logData.get(MapType.other).get(MsgDataField.messageState), stringClass, exception);
        final EnumMap<MsgDataField, Object> sendLogData = logData.get(MapType.send);


        this.addField(mapType, MsgDataField.sender, messageMetadata.recipient.code);
        this.addField(mapType, MsgDataField.recipient, messageMetadata.sender.code);
        this.addField(mapType, MsgDataField.typeCode, messageState.replaceFirst("Request$", ""));
        this.addField(mapType, MsgDataField.status, messageMetadata.interactionStatus != null
                ? messageMetadata.interactionStatus.value()
                : null);
        this.addField(mapType, MsgDataField.date, messageTimestamp);
        this.addField(mapType, MsgDataField.requestIdRef, messageMetadata.messageId != null
                ? messageMetadata.messageId
                : sendLogData.get(MsgDataField.requestIdRef));
        this.addField(mapType, MsgDataField.serviceName, "SMEV 3");
        this.addField(mapType, MsgDataField.originRequestIdRef, sendLogData.get(MsgDataField.originRequestIdRef));

        addSendData(MsgDataField.sender, messageMetadata.sender.code);
        addSendData(MsgDataField.recipient, messageMetadata.recipient.code);
        addSendData(MsgDataField.typeCode, messageState.replaceFirst("Request$", ""));
    }

    public void addOtherData(final MsgDataField key, final Object value) {
        this.addField(MapType.other, key, value);
    }

    @Override
    public void addSendData(final MsgDataField key, final Object value) {
        this.addField(MapType.send, key, value);
    }

    @Override
    public void addReceiveData(final MsgDataField key, final Object value) {
        this.addField(MapType.receive, key, value);
    }

    @Override
    public Boolean createLog() {
        this.setMetadata();
        return Files.writeMetadataToSpool(metadata, dirName);
    }

    private <T> T safe(Object obj, final Class<T> tClass, Boolean isNeedsException) {
        final IllegalStateException illegalStateException = new IllegalStateException(
                "Ошибка логирования сообщения, некоторые данные отсутствуют или имеют некорректный тип");
        if (obj == null) {
            if (isNeedsException) {
                illegalStateException.initCause(new NullPointerException());
                throw illegalStateException;
            } else {
                return null;
            }
        } else {
            try {
                return tClass.cast(obj);
            } catch (ClassCastException e) {
                illegalStateException.initCause(e);
                throw illegalStateException;
            }
        }
    }

    private void setPacketData(MapType mapType) {
        Pack pack = new Pack();
        OutputStream stream;
        final EnumMap<MsgDataField, Object> msgLogData = logData.get(mapType);

        pack.serviceName = safe(msgLogData.get(MsgDataField.serviceName), stringClass, exception);
        pack.sender = safe(msgLogData.get(MsgDataField.sender), stringClass, noException);
        pack.recipient = safe(msgLogData.get(MsgDataField.recipient), stringClass, noException);
        pack.typeCode = safe(msgLogData.get(MsgDataField.typeCode), stringClass, exception);
        pack.status = safe(msgLogData.get(MsgDataField.status), stringClass, noException);
        pack.date = safe(msgLogData.get(MsgDataField.date), dateClass, exception);
        pack.requestIdRef = safe(msgLogData.get(MsgDataField.requestIdRef), stringClass, exception);
        pack.originRequestIdRef = safe(msgLogData.get(MsgDataField.originRequestIdRef), stringClass, exception);

        if (MapType.send.equals(mapType)) {
            this.metadata.send = pack;
            stream = this.getHttpOutStream();
        } else {
            this.metadata.receive = pack;
            stream = this.getHttpInStream();
        }
        try {
            safe(msgLogData.get(MsgDataField.SOAPMessage), SOAPMessage.class, exception).writeTo(stream);
        } catch (SOAPException e) {
            this.log(e);
        }
        catch (IOException e) {
            this.log(e);
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (IOException e) {
                this.log(e);
            }
        }
    }

    private Boolean setMetadata() {
        final EnumMap<MsgDataField, Object> otherLogData = logData.get(MapType.other);
        this.metadata.date = this.safe(otherLogData.get(MsgDataField.date), dateClass, exception);
        this.metadata.processInstanceId = this.safe(otherLogData.get(MsgDataField.processInstanceId), stringClass, exception);
        this.metadata.bid = this.safe(otherLogData.get(MsgDataField.bid), Long.class, exception);
        this.metadata.client = this.safe(otherLogData.get(MsgDataField.client), Boolean.class, exception);
        this.metadata.componentName = this.safe(otherLogData.get(MsgDataField.componentName), stringClass, exception);
        this.metadata.error = this.safe(otherLogData.get(MsgDataField.error), stringClass, noException);

        this.setPacketData(MapType.send);
        this.setPacketData(MapType.receive);
        return true;
    }
}
