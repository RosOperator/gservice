package ru.codeinside.smev.v3.cerasis;

import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.gws.api.ExchangeContext;
import ru.codeinside.gws.api.Revision;
import ru.codeinside.smev.v3.cerasis.pojo.*;
import ru.codeinside.smev.v3.crypto.api.GostR3410;
import ru.codeinside.smev.v3.crypto.api.GostR3411;
import ru.codeinside.smev.v3.crypto.api.XmlNormalizer;
import ru.codeinside.smev.v3.service.api.InformationType;
import ru.codeinside.smev.v3.service.api.RequestRejected;
import ru.codeinside.smev.v3.service.api.consumer.Consumer;
import ru.codeinside.smev.v3.service.api.consumer.ConsumerRequest;
import ru.codeinside.smev.v3.service.api.consumer.Response;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseData;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CerasisConsumer implements Consumer {
	
    public static final String NAMESPACE = "urn://ru.codeinside.smev.v3/cerasis/1.0.0";
    public static final String ROOT_ELEMENT = "CerasisRequest";

    private volatile GostR3410 gostR3410;
    private volatile GostR3411 gostR3411;
    private volatile XmlNormalizer normalizer;

    private Logger logger = Logger.getLogger(CerasisConsumer.class.getName());

	@Override
	public Revision getRevision() {
		return Revision.rev30981;
	}

	/* (non-Javadoc)
	 * @see ru.codeinside.smev.v3.service.api.consumer.Consumer#createRequest(ru.codeinside.gws.api.ExchangeContext)
	 */
	@Override
	public ConsumerRequest createRequest(ExchangeContext businessProcess) throws IllegalArgumentException {
        try {
            ConsumerRequest consumerRequest = new ConsumerRequest();
            consumerRequest.setTest(true);
            consumerRequest.setPersonalData(createCerasisRequest(businessProcess));
            consumerRequest.setSignatureRequired(true);
        	for (String name : businessProcess.getVariableNames()) {
        		if (name.startsWith("appData_") && businessProcess.isEnclosure(name)) {
    				final Enclosure enclosure = businessProcess.getEnclosure(name);
    				enclosure.code = name;
    				consumerRequest.addAttachment(enclosure);
        		}
        	}
            return consumerRequest;
        } catch (JAXBException e) {
            throw new IllegalStateException("Parsing XML error");
        } catch (ParserConfigurationException e) {
            throw new IllegalStateException("Parsing XML error");
        }
	}
	
	private CerasisRequest createCerasisRequest(ExchangeContext businessProcess){
		CerasisRequest cerasisRequest = new CerasisRequest();
	    Set<String> variableNames = new LinkedHashSet<String>();
	    for (String name : businessProcess.getVariableNames()) {
	      if (name.startsWith("appData_") || name.startsWith("+appData_")) {
	        variableNames.add(name);
	      }
	    }
	    List<CerasisRequestType> request = new ArrayList<CerasisRequestType>();
	    for (String variable : variableNames) {
	    	if (!businessProcess.isEnclosure(variable) && businessProcess.getVariable(variable) != null) {
	    		CerasisRequestType cerasisRequestType = new CerasisRequestType();
	    		cerasisRequestType.setKey(variable);
	    		if (businessProcess.getVariable(variable) instanceof Date) {
	    			String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(businessProcess.getVariable(variable));
	    			cerasisRequestType.setValue(formattedDate);
	    		} else {
	    			cerasisRequestType.setValue(businessProcess.getVariable(variable).toString());
	    		}
	    		request.add(cerasisRequestType);
	        }
	    }
	    cerasisRequest.setRequest(request);
	    List<CerasisAttachment> attachments = new ArrayList<CerasisAttachment>();
    	for (String name : businessProcess.getVariableNames()) {
    		if (name.startsWith("appData_") && businessProcess.isEnclosure(name)) {
				final Enclosure enclosure = businessProcess.getEnclosure(name);
				enclosure.code = name;
				attachments.add(new CerasisAttachment(name, enclosure.number, enclosure.fileName));
    		}
    	}
    	cerasisRequest.setAttachments(attachments);
		return cerasisRequest;
	}

	@Override
	public String processResponse(Response response, ExchangeContext businessProcess) {
        ProviderResponseData providerResponseData = response.data.providerResponse.data;
        CerasisResponse cerasisResponse;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(CerasisResponse.class);
            Unmarshaller un = jaxbContext.createUnmarshaller();
            cerasisResponse = (CerasisResponse) un.unmarshal(providerResponseData.personalData.data);
            if (providerResponseData.requestRejected != null && providerResponseData.requestRejected.size() != 0) {
                businessProcess.setVariable("smevReject", true);
                for (RequestRejected rejected : providerResponseData.requestRejected) {
                    businessProcess.setVariable("rejectionReason", rejected.rejectionReasonDescription);
                    businessProcess.setVariable("rejectionReasonCode", rejected.rejectionReasonCode);
                }
            } else {
                businessProcess.setVariable("smevReject", false);
                if (cerasisResponse != null) {
                    for (CerasisResponseType row : cerasisResponse.getResponse()) {
                        businessProcess.setVariable(row.getKey(), row.getValue());
                    }
                    putEnclosures(businessProcess, cerasisResponse, providerResponseData.enclosures);
                }
            }
        } catch (JAXBException e) {
            logger.log(Level.WARNING, "Unmarshal response.", e);
            throw new RuntimeException(e);
        }
        return providerResponseData.messageID;
	}
	
    private void putEnclosures(ExchangeContext businessProcess, CerasisResponse cerasisResponse, List<Enclosure> enclosures) {
        List<CerasisAttachment> attachments = cerasisResponse.getAttachments();
        if (attachments == null || attachments.isEmpty()) {
            return;
        }
        for (CerasisAttachment attachment : attachments) {
            Enclosure enclosure = extractEnclosure(attachment.getAttachmentRefType().getAttachmentId(), enclosures);
            if (enclosure != null) {
                enclosure.fileName = attachment.getFileName();
            	businessProcess.addEnclosure(attachment.getPropertyName(), enclosure);
            } else {
                logger.info("Не удалось сопоставить вложение " + attachment.getPropertyName() + ":" +
                        attachment.getAttachmentRefType().getAttachmentId());
            }
        }
    }

	@Override
	public InformationType getInformationType() {
		return new InformationType(NAMESPACE, ROOT_ELEMENT);
	}
	
    private Enclosure extractEnclosure(String id, List<Enclosure> enclosures) {
        for (Enclosure enclosure : enclosures) {
            if (id.equals(enclosure.id)) {
                return enclosure;
            }
        }

        return null;
    }


    /**
     *
     * Staff methods
     *
     */
    public void setGostR3410(GostR3410 gostR3410) {
        if (this.gostR3410 == null) {
            this.gostR3410 = gostR3410;
        }
    }

    public void removeGostR3410(GostR3410 gostR3410) {
        if (this.gostR3410 == gostR3410) {
            this.gostR3410 = null;
        }
    }

    public void setGostR3411(GostR3411 gostR3411) {
        if (this.gostR3411 == null) {
            this.gostR3411 = gostR3411;
        }
    }

    public void removeGostR3411(GostR3411 gostR3411) {
        if (this.gostR3411 == gostR3411) {
            this.gostR3411 = null;
        }
    }

    public void setNormalizer(XmlNormalizer normalizer) {
        if (this.normalizer == null) {
            this.normalizer = normalizer;
        }
    }

    public void removeNormalizer(XmlNormalizer normalizer) {
        if (this.normalizer == normalizer) {
            this.normalizer = null;
        }
    }	
}
