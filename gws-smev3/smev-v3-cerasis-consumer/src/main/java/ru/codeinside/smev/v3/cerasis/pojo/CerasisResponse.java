package ru.codeinside.smev.v3.cerasis.pojo;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CerasisResponse")
@XmlRootElement(name = "CerasisResponse")
public class CerasisResponse {
    @XmlElement(name = "Response", required = true)
    private List<CerasisResponseType> response;

    @XmlElement(name = "attachments", nillable = true)
    private List<CerasisAttachment> attachments;

    public List<CerasisResponseType> getResponse() {
        return response;
    }

    public void setResponse(List<CerasisResponseType> response) {
        this.response = response;
    }

    public List<CerasisAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<CerasisAttachment> attachments) {
        this.attachments = attachments;
    }
}
