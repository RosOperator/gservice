package ru.codeinside.smev.v3.cerasis;

import org.w3c.dom.Element;
import ru.codeinside.gws.api.*;
import ru.codeinside.smev.v3.cerasis.pojo.*;
import ru.codeinside.smev.v3.crypto.api.GostR3410;
import ru.codeinside.smev.v3.crypto.api.GostR3411;
import ru.codeinside.smev.v3.crypto.api.XmlNormalizer;
import ru.codeinside.smev.v3.crypto.api.XmlSignature;
import ru.codeinside.smev.v3.service.api.InformationType;
import ru.codeinside.smev.v3.service.api.PersonalData;
import ru.codeinside.smev.v3.service.api.ProviderRequestContext;
import ru.codeinside.smev.v3.service.api.RejectionCode;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequestData;
import ru.codeinside.smev.v3.service.api.provider.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@SuppressWarnings("PackageAccessibility")
public class CerasisProvider implements Provider {
    public static final String NAMESPACE = "urn://ru.codeinside.smev.v3/cerasis/1.0.0";
    public static final String ROOT_ELEMENT = "CerasisRequest";
    public static final int PROCEDURE_CODE = 308;

    private volatile GostR3410 gostR3410;
    private volatile GostR3411 gostR3411;
    private volatile XmlNormalizer normalizer;

    private Logger logger = Logger.getLogger(CerasisProvider.class.getName());

    @Override
    public ProvidedResponse createResponse(ReceiptContext receiptContext) {
        List<CerasisResponseType> responseTypes = new ArrayList<CerasisResponseType>();
        ProvidedResponse providedResponse = new ProvidedResponse();

        for (String property : receiptContext.getPropertyNames()) {
            Object variable = receiptContext.getVariable(property);
            if (variable != null) {
                CerasisResponseType type = new CerasisResponseType();
                type.setKey(property);
                if (variable instanceof Date) {
                    type.setValue(new SimpleDateFormat("dd/MM/yyyy").format((Date) variable));
                } else {
                    type.setValue(String.valueOf(variable));
                }
                responseTypes.add(type);
            }
        }

        List<CerasisAttachment> attachmentRefs = new ArrayList<CerasisAttachment>(
                receiptContext.getEnclosureNames().size());
        for (String enclosureName : receiptContext.getEnclosureNames()) {
            Enclosure enclosure = receiptContext.getEnclosure(enclosureName);

            attachmentRefs.add(new CerasisAttachment(enclosureName, enclosure.number, enclosure.fileName));
            providedResponse.addEnclosure(enclosure);
        }

        CerasisResponse response = new CerasisResponse();
        response.setResponse(responseTypes);
        response.setAttachments(attachmentRefs);

        providedResponse.setReplyTo((String) receiptContext.getVariableByFullName("replyTo"));
        providedResponse.setSignatureRequired(true);
        try {
            providedResponse.setResponseData(response);
        } catch (JAXBException e) {
            logger.info("Не удалось задать овтет: " + e.getMessage());
            return null;
        } catch (ParserConfigurationException e) {
            logger.info("Не удалось задать овтет: " + e.getMessage());
            return null;
        }

        return providedResponse;
    }

    @Override
    public String processRequest(ProviderRequestContext context) throws ProviderRequestException {
        RequestMessage requestMessage = context.getRequestMessage();
        RequestData requestData = requestMessage.request.data;
        OrganizationRequestData organizationRequestData = requestData.organizationRequest.data;
        PersonalData personalData = organizationRequestData.personalData;

        if (!validatePersonalSignature(personalData)) {
            String info = "Запрос " + organizationRequestData.messageID + " не прошел валидацию СП.";
            logger.info(info);
            throw new ProviderRequestException(RejectionCode.FAILURE, info);
        }

        CerasisRequest cerasisRequest = parseRequestData(personalData.data);
        if (cerasisRequest == null) {
            throw new ProviderRequestException(RejectionCode.FAILURE, "Не удалось преобразовать объект запроса.");
        }

        DeclarerContext declarerContext = context.getDeclarerContext(PROCEDURE_CODE);
        // идентификатор запроса на который потом будет предоставлен ответ
        declarerContext.setValue("replyTo", requestData.replyTo);

        for (CerasisRequestType row : cerasisRequest.getRequest()) {
            if (row.getValue().equals("158")){
                throw new ProviderRequestException(RejectionCode.FAILURE, "Не допустимое значение переменной.");
            }
            declarerContext.setValue(row.getKey(), row.getValue());
        }

        putEnclosures(declarerContext, cerasisRequest, organizationRequestData.enclosures);
        String bidId = declarerContext.declare();
        if (bidId == null || bidId.isEmpty()) {
            logger.info("Не смог подать заявку.");
            throw new ProviderRequestException(RejectionCode.FAILURE, "Не смог подать заявку.");
        }
        return organizationRequestData.messageID;
    }

    @Override
    public Revision getRevision() {
        return Revision.rev30981;
    }

    private CerasisRequest parseRequestData(Element element) {
        try {
            JAXBContext context = JAXBContext.newInstance(CerasisRequest.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            return (CerasisRequest) unmarshaller.unmarshal(element);
        } catch (JAXBException e) {
            logger.info("Не удалось преобразовать объект запроса: " + e.getMessage());
        }
        return null;
    }

    @Override
    public InformationType getInformationType() {
        return new InformationType(NAMESPACE, ROOT_ELEMENT);
    }

    private boolean validatePersonalSignature(PersonalData personalData) {
        XmlSignature xmlSignature = personalData.signature;
        if (xmlSignature == null) {
            logger.info("СП отсутствует. Валидация производиться не будет.");
            return true;
        }
        if (gostR3410 == null) {
            logger.info("Отсутствует модуль криптографии. Проверить СП невозможно.");
            return false;
        }

        byte[] digest = getDigest(personalData.data);
        if (digest == null) {
            return false;
        }

        Signature signature = xmlSignature.getSignature();
        if (!Arrays.equals(digest, signature.getDigest())) {
            logger.info("Не совпал хеш данных при проверка СП.");
            return false;
        }
        return gostR3410.validate(signature.certificate, signature.getContent(), signature.sign);
    }

    private void putEnclosures(DeclarerContext declarerContext, CerasisRequest cerasisRequest, List<Enclosure> enclosures) {
        List<CerasisAttachment> attachments = cerasisRequest.getAttachments();
        if (attachments == null || attachments.isEmpty()) {
            return;
        }
        for (CerasisAttachment attachment : attachments) {
            Enclosure enclosure = extractEnclosure(attachment.getAttachmentRefType().getAttachmentId(), enclosures);
            if (enclosure != null) {
                enclosure.fileName = attachment.getFileName();
                declarerContext.addEnclosure(attachment.getPropertyName(), enclosure);
            } else {
                logger.info("Не удалось сопоставить вложение " + attachment.getPropertyName() + ":" +
                        attachment.getAttachmentRefType().getAttachmentId());
            }
        }
    }

    private Enclosure extractEnclosure(String id, List<Enclosure> enclosures) {
        for (Enclosure enclosure : enclosures) {
            if (id.equals(enclosure.id)) {
                return enclosure;
            }
        }

        return null;
    }

    private byte[] getDigest(Element personalData) {
        byte[] normalized = normalize(personalData);
        if (normalized == null) {
            return null;
        }

        logger.info("PROVIDER: " + new String(normalized));

        if (gostR3411 == null) {
            logger.info("Отсутсвтует модуль хеширования. Проверить СП невозможно.");
            return null;
        }
        return gostR3411.digest(new ByteArrayInputStream(normalized));
    }

    private byte[] normalize(Element personalData) {
        if (normalizer == null) {
            logger.info("Отсутствует модуль нормализации. Проверить СП невозможно.");
            return null;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        normalizer.normalizeDom(personalData, out);
        return out.toByteArray();
    }

    /**
     *
     * Staff methods
     *
     */

    @SuppressWarnings("unused")
    public void setGostR3410(GostR3410 gostR3410) {
        if (this.gostR3410 == null) {
            this.gostR3410 = gostR3410;
        }
    }

    @SuppressWarnings("unused")
    public void removeGostR3410(GostR3410 gostR3410) {
        if (this.gostR3410 == gostR3410) {
            this.gostR3410 = null;
        }
    }

    @SuppressWarnings("unused")
    public void setGostR3411(GostR3411 gostR3411) {
        if (this.gostR3411 == null) {
            this.gostR3411 = gostR3411;
        }
    }

    @SuppressWarnings("unused")
    public void removeGostR3411(GostR3411 gostR3411) {
        if (this.gostR3411 == gostR3411) {
            this.gostR3411 = null;
        }
    }

    @SuppressWarnings("unused")
    public void setNormalizer(XmlNormalizer normalizer) {
        if (this.normalizer == null) {
            this.normalizer = normalizer;
        }
    }

    @SuppressWarnings("unused")
    public void removeNormalizer(XmlNormalizer normalizer) {
        if (this.normalizer == normalizer) {
            this.normalizer = null;
        }
    }
}
