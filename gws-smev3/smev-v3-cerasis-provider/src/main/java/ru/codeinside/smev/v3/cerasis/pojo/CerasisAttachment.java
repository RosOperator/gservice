package ru.codeinside.smev.v3.cerasis.pojo;

import ru.gov.smev.artefacts.x.supplementary.commons._1_0.AttachmentRefType;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "CerasisAttachment")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CerasisAttachment")
public class CerasisAttachment {
    @XmlElement(name = "propertyName", required = true)
    private String propertyName;

    @XmlElement(name = "refAttachmentHeaderType", required = true,
            namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1")
    private AttachmentRefType attachmentRefType;

    @XmlElement(name = "fileName", required = true)
    private String fileName;

    public CerasisAttachment(String propertyName, String attachmentId, String fileName) {
        this.propertyName = propertyName;
        this.fileName = fileName;
        this.attachmentRefType = new AttachmentRefType();
        this.attachmentRefType.setAttachmentId(attachmentId);
    }

    public CerasisAttachment() {
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public AttachmentRefType getAttachmentRefType() {
        return attachmentRefType;
    }

    public void setRefAttachmentHeaderType(AttachmentRefType attachmentRefType) {
        this.attachmentRefType = attachmentRefType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
