package ru.codeinside.smev.v3.cerasis.pojo;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name ="CerasisRequest")
@XmlRootElement(name = "CerasisRequest")
public class CerasisRequest {

    @XmlElement(name = "Request", required = true)
    private List<CerasisRequestType> request;

    @XmlElement(name = "attachments", nillable = true)
    private List<CerasisAttachment> attachments;

    public List<CerasisRequestType> getRequest() {
        return request;
    }

    public void setRequest(List<CerasisRequestType> request) {
        this.request = request;
    }

    public List<CerasisAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<CerasisAttachment> attachments) {
        this.attachments = attachments;
    }
}
