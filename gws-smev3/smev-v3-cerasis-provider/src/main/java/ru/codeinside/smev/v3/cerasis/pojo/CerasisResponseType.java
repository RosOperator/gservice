package ru.codeinside.smev.v3.cerasis.pojo;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CerasisResponseType")
@XmlType(name = "CerasisResponseType", propOrder = {
        "key",
        "value"
})
public class CerasisResponseType {
    @XmlElement(name = "Key", required = true)
    private String key;

    @XmlElement(name = "Value", required = true)
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
