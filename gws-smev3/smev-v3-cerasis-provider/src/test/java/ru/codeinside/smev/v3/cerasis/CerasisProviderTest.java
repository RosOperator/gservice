package ru.codeinside.smev.v3.cerasis;

import org.junit.Assert;
import org.junit.Test;
import ru.codeinside.smev.v3.cerasis.pojo.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class CerasisProviderTest extends Assert {

    @Test
    public void testStandardRequest() throws Exception {
        CerasisRequestType cerasisRequestType = new CerasisRequestType();
        cerasisRequestType.setKey("SecretPhrase");
        cerasisRequestType.setValue("Top secret");

        CerasisRequestType cerasisRequestType1 = new CerasisRequestType();
        cerasisRequestType1.setKey("scheduleOn");
        cerasisRequestType1.setValue(new SimpleDateFormat("dd.MM.yyyy hh:mm:ss").format(new Date()));

        CerasisRequestType cerasisRequestType2 = new CerasisRequestType();
        cerasisRequestType2.setKey("people");
        cerasisRequestType2.setValue(String.valueOf(105));

        List<CerasisRequestType> requestTypes = new ArrayList<CerasisRequestType>(3);
        requestTypes.add(cerasisRequestType);
        requestTypes.add(cerasisRequestType1);
        requestTypes.add(cerasisRequestType2);

        List<CerasisAttachment> attachments = new ArrayList<CerasisAttachment>(1);
        attachments.add(new CerasisAttachment("ticket", UUID.randomUUID().toString(), "fake.txt"));

        CerasisRequest request = new CerasisRequest();
        request.setRequest(requestTypes);
        request.setAttachments(attachments);

        String standardRequest = toXml(request);
        assertNotNull(standardRequest);
        assertFalse(standardRequest.isEmpty());

        System.out.println("Эталонный запрос: " + standardRequest);
    }

    @Test
    public void testStandardResponse() throws Exception {
        CerasisResponseType cerasisResponseType = new CerasisResponseType();
        cerasisResponseType.setKey("IsAvailable");
        cerasisResponseType.setValue(String.valueOf(true));

        CerasisResponseType cerasisResponseType1 = new CerasisResponseType();
        cerasisResponseType1.setKey("startDate");
        cerasisResponseType1.setValue(new SimpleDateFormat("dd.MM.yyyy hh:mm:ss").format(new Date()));

        CerasisResponseType cerasisResponseType2 = new CerasisResponseType();
        cerasisResponseType2.setKey("money");
        cerasisResponseType2.setValue(String.valueOf(155.64f));

        List<CerasisResponseType> responseTypes = new ArrayList<CerasisResponseType>(3);
        responseTypes.add(cerasisResponseType);
        responseTypes.add(cerasisResponseType1);
        responseTypes.add(cerasisResponseType2);

        List<CerasisAttachment> attachments = new ArrayList<CerasisAttachment>(1);
        attachments.add(new CerasisAttachment("leaflet", UUID.randomUUID().toString(), "snake.txt"));

        CerasisResponse response = new CerasisResponse();
        response.setResponse(responseTypes);
        response.setAttachments(attachments);

        String standardResponse = toXml(response);
        assertNotNull(standardResponse);
        assertFalse(standardResponse.isEmpty());

        System.out.println("Эталонный ответ: " + standardResponse);
    }

    private String toXml(Object jaxb) {
        try {
            StringWriter sw = new StringWriter();

            JAXBContext context = JAXBContext.newInstance(jaxb.getClass());
            context.createMarshaller().marshal(jaxb, sw);

            return sw.toString();
        } catch (JAXBException e) {
            return null;
        }
    }
}
