package ru.codeinside.smev.v3.crypto.api;

import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.gws.api.Signature;

import javax.annotation.Nonnull;
import java.io.InputStream;
import java.security.cert.X509Certificate;

/**
 * Электронная подпись по ГОСТ Р 34.10-2001
 */
public interface GostR3410 {
    /**
     * Проверка подписи по открытому ключу.
     *
     * @param certificate сертификат с ключём подписи.
     * @param data        данные на которые была сделана подпись.
     * @param signature   подпись для проверки.
     * @return            true, если подпись валидна
     */
    boolean validate(@Nonnull X509Certificate certificate, @Nonnull InputStream data, @Nonnull byte[] signature);

    /**
     * Подписание данных ЭП-ИС
     *
     * @param data  подписываемые данные
     * @return      подпись данных
     */
    Signature sign(@Nonnull InputStream data);

    /**
     * Подписать вложение
     *
     * @param enclosure вложение
     */
    void sign(@Nonnull Enclosure enclosure);
}
