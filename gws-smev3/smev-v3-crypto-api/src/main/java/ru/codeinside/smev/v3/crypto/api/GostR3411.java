package ru.codeinside.smev.v3.crypto.api;

import javax.annotation.Nonnull;
import java.io.InputStream;

/**
 * Хешировано по алгоритму ГОСТ Р 34.11-94
 */
public interface GostR3411 {

    @Nonnull
    byte[] digest(@Nonnull InputStream content);
}
