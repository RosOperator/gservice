package ru.codeinside.smev.v3.crypto.api;

import ru.codeinside.gws.api.Signature;

import javax.annotation.Nonnull;

/**
 * Разбор и формирование контейнера Pkcs7 detached по СМЭВ.
 */
public interface SignatureAssembler {

    /**
     * Формирование контейнера Pkcs7.
     *
     * @param signature описатель подписи.
     * @return данных в фомате ASN DER и Pkcs7
     */
    @Nonnull
    byte[] assemble(@Nonnull Signature signature);


    /**
     * Разбор контейнера Pkcs7.
     *
     * @param derBytes данные в фомате ASN DER и Pkcs7.
     * @return описатель подписи.
     */
    @Nonnull
    Signature disassemble(@Nonnull byte[] derBytes);
}
