package ru.codeinside.smev.v3.crypto.api;

import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.annotation.Nonnull;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Каноникализация Exclusive XML Canonicalization от 18 июля 2002 (http://www.w3.org/2001/10/xml-exc-c14n#)
 * Нормализация СМЭВ (urn://smev-gov-ru/xmldsig/transform)
 */
public interface XmlNormalizer {

    /**
     * @param object       jaxb объект для нормализации
     * @param outputStream исходящий поток результата
     */
    void normalizeJaxb(@Nonnull Object object, @Nonnull OutputStream outputStream)
            throws JAXBException, ParserConfigurationException, IOException, SAXException;

    /**
     * @param element      элемент для нормализации
     * @param outputStream исходящий поток результата
     */
    void normalizeDom(@Nonnull Element element, @Nonnull OutputStream outputStream);
}
