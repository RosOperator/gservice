package ru.codeinside.smev.v3.crypto.api;

import ru.codeinside.gws.api.Signature;

/**
 * Описатель подписи в формате xml.
 */
public class XmlSignature {

    /**
     * Ссылка на подписанный элемент в документе.
     */
    private String elementReference;

    /**
     * Данные ЭЦП
     */
    private Signature signature;

    public XmlSignature() {

    }

    public XmlSignature(Signature signature) {
        setSignature(signature);
    }

    public Signature getSignature() {
        return signature;
    }

    public void setSignature(Signature signature) {
        this.signature = signature;
    }

    public String getElementReference() {
        return elementReference;
    }

    public void setElementReference(String elementReference) {
        this.elementReference = elementReference;
    }
}
