package ru.codeinside.smev.v3.crypto.api;

import org.w3c.dom.Element;

import javax.annotation.Nonnull;

/**
 * Преобразование из xml и обратно с учётом СМЭВ.
 * Расчет хеш-суммы  - ГОСТ Р 34.11-94 (http://www.w3.org/2001/04/xmldsig-more#gostr3411)
 * Формирование подписи - ГОСТ Р 34.10-2001 (http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411)
 * Каноникализация - Exclusive XML Canonicalization от 18 июля 2002 (http://www.w3.org/2001/10/xml-exc-c14n#)
 * Дополнительная нормализация - СМЭВ (urn://smev-gov-ru/xmldsig/transform)
 */
public interface XmlSignatureAssembler {

    /**
     * Сборка из объекта подписи элемента XMLDSign.
     *
     * @param signature данные подписи.
     * @return xml элемент.
     */
    @Nonnull
    Element assemble(@Nonnull XmlSignature signature);

    /**
     * Разбор xml элемента в объект подписи.
     *
     * @param element элемент для разбора.
     * @return объект подписи.
     */
    @Nonnull
    XmlSignature disassemble(@Nonnull Element element);
}
