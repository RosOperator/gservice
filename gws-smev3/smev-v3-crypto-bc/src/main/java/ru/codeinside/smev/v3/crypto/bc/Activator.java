package ru.codeinside.smev.v3.crypto.bc;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import ru.codeinside.smev.v3.crypto.api.GostR3410;
import ru.codeinside.smev.v3.crypto.api.GostR3411;

final public class Activator implements BundleActivator {

    ServiceRegistration gostR3411Registration;
    ServiceRegistration gostR3410Registration;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        gostR3411Registration = bundleContext.registerService(GostR3411.class.getName(), new GostR3411Impl(), null);
        gostR3410Registration = bundleContext.registerService(GostR3410.class.getName(), new GostR3410Impl(), null);
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        if (gostR3411Registration != null) {
            gostR3411Registration.unregister();
            gostR3411Registration = null;
        }

        if (gostR3410Registration != null) {
            gostR3410Registration.unregister();
            gostR3410Registration = null;
        }
        BCRegistrant.unregisterIfNeed();
    }
}
