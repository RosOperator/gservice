package ru.codeinside.smev.v3.crypto.bc;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;
import java.util.concurrent.atomic.AtomicBoolean;

public class BCRegistrant {

    public BCRegistrant() {
        registerIfNeed();
    }

    private final static AtomicBoolean LAZY_REGISTRATION = new AtomicBoolean();

    static void registerIfNeed() {
        if (LAZY_REGISTRATION.compareAndSet(false, true)) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    static void unregisterIfNeed() {
        if (LAZY_REGISTRATION.compareAndSet(true, false)) {
            Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
        }
    }
}
