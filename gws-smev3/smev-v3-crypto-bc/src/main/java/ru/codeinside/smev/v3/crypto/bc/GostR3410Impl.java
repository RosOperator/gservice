package ru.codeinside.smev.v3.crypto.bc;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.smev.v3.crypto.api.GostR3410;
import ru.codeinside.smev.v3.crypto.api.GostR3411;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GostR3410Impl extends BCRegistrant implements GostR3410 {

  public static final String ALGORITHM_ID = "ECGOST3410";

  private final static Logger log = Logger.getLogger(GostR3410Impl.class.getName());
  private final static String DEFAULT_CERT_NAME = "ep_is";
  private final static String DEFAULT_CERT_PASS = "123456";
  private final static String DEFAULT_KEYSTORE_PASS = "123456";
  private final static String DEFAULT_KEYSTORE_LOCATION = "JKS.keystore";
  private volatile static boolean started;
  private static PrivateKey privateKey;
  private static X509Certificate cert;

  /**
   * Проверка подписи по открытому ключу.
   *
   * @param certificate сертификат с ключём подписи.
   * @param data        данные на которые была сделана подпись (обычно хеш).
   * @param signature   подпись для проверки.
   * @return <code>true</code>, если валидация прошла успешно, иначе <code>false</code>
   */
  @Override
  public boolean validate(@Nonnull X509Certificate certificate, @Nonnull InputStream data, @Nonnull byte[] signature) {
    try {
      Signature sign = Signature.getInstance(ALGORITHM_ID, BouncyCastleProvider.PROVIDER_NAME);
      sign.initVerify(certificate);
      update(sign, data);
      return sign.verify(signature);
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    } catch (NoSuchProviderException e) {
      throw new RuntimeException(e);
    } catch (SignatureException e) {
      throw new RuntimeException(e);
    } catch (InvalidKeyException e) {
      throw new RuntimeException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public ru.codeinside.gws.api.Signature sign(@Nonnull InputStream data) {
    try {
      loadCertificate();
      Signature sign = Signature.getInstance(ALGORITHM_ID, BouncyCastleProvider.PROVIDER_NAME);
      sign.initSign(privateKey);
      update(sign, data);
      GostR3411 gostR3411 = new GostR3411Impl();
      byte[] digest = gostR3411.digest(data);
      byte[] signBytes = sign.sign();
      return new ru.codeinside.gws.api.Signature(cert, data, signBytes, digest, true);
    } catch (RuntimeException e) {
      throw e;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void sign(@Nonnull Enclosure enclosure) {
    throw new UnsupportedOperationException("Operation does not implemented.");
  }

  private void update(Signature signature, InputStream is) throws IOException, SignatureException {
    byte[] buffer = new byte[50 * 1024];
    int count;
    is.reset();
    while ((count = is.read(buffer, 0, buffer.length)) > -1) {
      signature.update(buffer, 0, count);
    }
  }

  static void loadCertificate() throws KeyStoreException, NoSuchAlgorithmException, CertificateException,
      IOException, UnrecoverableKeyException {
    if (!started) {
      synchronized (GostR3410Impl.class) {
        if (!started) {
          final long startMs = System.currentTimeMillis();

          final Properties properties = new Properties();
          properties.setProperty("bc_name", DEFAULT_CERT_NAME);
          properties.setProperty("bc_pass", DEFAULT_CERT_PASS);
          properties.setProperty("bc_keystore_pass", DEFAULT_KEYSTORE_PASS);

          final File userHome = new File(System.getProperty("user.home"));
          final File keyFile = new File(userHome, "gses-key.properties");
          if (!keyFile.exists()) {
            log.warning(keyFile + " не обнаружен, используются ключи тестовой системы");
          } else {
            final FileInputStream is = new FileInputStream(keyFile);
            properties.load(is);
            is.close();
          }

          final String keystoreLocation = properties.getProperty("bc_keystore_location");
          final String keystorePass = properties.getProperty("bc_keystore_pass");

          final KeyStore keystore = KeyStore.getInstance("JKS");
          InputStream keyStoreFile = keystoreLocation == null
              ? GostR3410Impl.class.getClassLoader().getResourceAsStream(DEFAULT_KEYSTORE_LOCATION)
              : new FileInputStream(keystoreLocation);

          keystore.load(keyStoreFile, keystorePass.toCharArray());

          final String certName = properties.getProperty("bc_name");
          final String certPass = properties.getProperty("bc_pass");

          privateKey = ((PrivateKey) keystore.getKey(certName, certPass.toCharArray()));
          cert = ((X509Certificate) keystore.getCertificate(certName));

          try {
            cert.checkValidity();
            log.info("Загружен действующий до " + cert.getNotAfter() + " сертификат " + cert.getSubjectDN().getName());
          } catch (CertificateExpiredException e) {
            log.warning("Закончилось время действия для сертификата " + cert.getSubjectDN().getName());
            cert = null;
            privateKey = null;
          } catch (CertificateNotYetValidException e) {
            log.warning("Не началось время действия для сертификата " + cert.getSubjectDN().getName());
            cert = null;
            privateKey = null;
          }
          if ((privateKey != null) && (cert != null)) {
            started = true;
          }
          if (log.isLoggable(Level.FINE)) {
            log.fine("LOAD CERTIFICATE: " + (System.currentTimeMillis() - startMs) + "ms");
          }
        }
      }
    }
  }
}
