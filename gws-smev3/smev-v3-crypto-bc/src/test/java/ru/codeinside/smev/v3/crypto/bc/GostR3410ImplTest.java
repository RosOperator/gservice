package ru.codeinside.smev.v3.crypto.bc;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.junit.Assert;
import org.junit.Test;

import javax.security.auth.x500.X500Principal;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.security.spec.ECGenParameterSpec;
import java.util.Date;

public class GostR3410ImplTest extends Assert {
    @Test
    public void test_verifySignature() throws Exception {
        GostR3410Impl gostR3410 = new GostR3410Impl();

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance(GostR3410Impl.ALGORITHM_ID, BouncyCastleProvider.PROVIDER_NAME);
        keyGen.initialize(new ECGenParameterSpec("GostR3410-2001-CryptoPro-A"));
        KeyPair pair = keyGen.generateKeyPair();

        Date startDate = new Date();
        Date expiryDate = new Date(startDate.getTime() + 10000);
        BigInteger serialNumber = new BigInteger("123456789");
        X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
        X500Principal dnName = new X500Principal("CN=Test CA Certificate");
        certGen.setSerialNumber(serialNumber);
        certGen.setIssuerDN(dnName);
        certGen.setNotBefore(startDate);
        certGen.setNotAfter(expiryDate);
        certGen.setSubjectDN(dnName);
        certGen.setPublicKey(pair.getPublic());
        certGen.setSignatureAlgorithm("GOST3411withECGOST3410");
        X509Certificate certificate = certGen.generate(pair.getPrivate(), "BC");

        Signature signature = Signature.getInstance(GostR3410Impl.ALGORITHM_ID, BouncyCastleProvider.PROVIDER_NAME);
        signature.initSign(pair.getPrivate());
        byte[] data = "Hello World!".getBytes("UTF8");
        signature.update(data);

        assertTrue(gostR3410.validate(certificate, new ByteArrayInputStream(data), signature.sign()));
    }

    @Test
    public void test_bc_sign() throws Exception {
        GostR3410Impl gostR3410 = new GostR3410Impl();
        byte[] signedData = "test signed data".getBytes("UTF-8");
        ru.codeinside.gws.api.Signature signature = gostR3410.sign(new ByteArrayInputStream(signedData));
        Assert.assertNotNull(signature.certificate);
        Assert.assertNotNull(signature.sign);
        Assert.assertNotNull(signature.getDigest());
    }

//    Создание keystore для bouncyCastle
//    @Test
//    public void store_certificate() throws Exception {
//        Security.addProvider( new org.bouncycastle.jce.provider.BouncyCastleProvider() );
//        KeyPair pair = genKeyPair();
//        X509Certificate certificate = genCertificate(pair);
//
//        KeyStore store = KeyStore.getInstance("BKS");
//        store.load(null, null);
//        store.setKeyEntry("ep_is", pair.getPrivate(), "123456".toCharArray(), new X509Certificate[]{certificate});
//
//        FileOutputStream os = new FileOutputStream("BKS.keystore");
//        store.store(os, "123456".toCharArray());
//    }
//
//    private X509Certificate genCertificate(KeyPair pair) throws Exception {
//        Date startDate = new Date();
//        Date expiryDate = new Date( startDate.getTime() + TimeUnit.DAYS.toMillis( 365 ) );
//        BigInteger serialNumber = new BigInteger("123456789");
//        X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
//        X500Principal dnName = new X500Principal("CN=Test CA Certificate");
//        certGen.setSerialNumber(serialNumber);
//        certGen.setIssuerDN(dnName);
//        certGen.setNotBefore(startDate);
//        certGen.setNotAfter(expiryDate);
//        certGen.setSubjectDN(dnName);
//        certGen.setPublicKey(pair.getPublic());
//        certGen.setSignatureAlgorithm("GOST3411withECGOST3410");
//        return certGen.generate(pair.getPrivate(), "BC");
//    }
//
//    private KeyPair genKeyPair() throws Exception {
//        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("ECGOST3410", "BC");
//        keyGen.initialize(new ECGenParameterSpec("GostR3410-2001-CryptoPro-A"));
//        return keyGen.generateKeyPair();
//    }
}
