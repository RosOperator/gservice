package ru.codeinside.smev.v3.crypto.bc;

import org.junit.Assert;
import org.junit.Test;
import ru.codeinside.smev.v3.crypto.api.GostR3411;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class GostR3411ImplTest extends Assert {

    @Test
    public void test_digest_1() throws IOException {
        validateString(
                "This is message, length=32 bytes",
                "2CEFC2F7B7BDC514E18EA57FA74FF357E7FA17D652C75F69CB1BE7893EDE48EB");
    }

    @Test
    public void test_digest_2() throws IOException {
        validateString(
                "Suppose the original message has length = 50 bytes",
                "C3730C5CBCCACF915AC292676F21E8BD4EF75331D9405E5F1A61DC3130A65011");
    }

    private void validateString(String message, String expectedHex) throws IOException {
        GostR3411 gost = new GostR3411Impl();
        byte[] textBytes = message.getBytes("UTF8");
        InputStream is = new ByteArrayInputStream(textBytes);
        byte[] digest = gost.digest(is);
        assertArrayEquals(getBytesFromHex(expectedHex), digest);
    }

    private byte[] getBytesFromHex(String hex) {
        byte[] result = new byte[hex.length() / 2];
        for (int i = 0; i < result.length; ++i) {
            result[i] = (byte) Integer.parseInt(hex.substring(i * 2, i * 2 + 2), 16);
        }
        return result;
    }
}
