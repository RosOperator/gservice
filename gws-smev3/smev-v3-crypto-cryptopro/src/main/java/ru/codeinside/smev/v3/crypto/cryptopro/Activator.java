package ru.codeinside.smev.v3.crypto.cryptopro;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import ru.codeinside.smev.v3.crypto.api.GostR3410;
import ru.codeinside.smev.v3.crypto.api.GostR3411;

import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.util.logging.Logger;

final public class Activator implements BundleActivator {

    ServiceRegistration gostR3410Registration;
    ServiceRegistration gostR3411Registration;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        if (hasCryptoPro()) {
            gostR3410Registration = bundleContext.registerService(GostR3410.class.getName(), new Gost_R_3410_2001(), null);
            gostR3411Registration = bundleContext.registerService(GostR3411.class.getName(), new Gost_R_3411_1994(), null);
        } else {
            Logger.getLogger(getClass().getName()).severe("'" + Gost_R_3410_2001.ALGORITHM_ID + "' not installed");
        }
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        if (gostR3410Registration != null) {
            gostR3410Registration.unregister();
            gostR3410Registration = null;
        }
        if (gostR3411Registration != null) {
            gostR3411Registration.unregister();
            gostR3411Registration = null;
        }
    }

    private boolean hasCryptoPro() {
        try {
            Signature.getInstance(Gost_R_3410_2001.ALGORITHM_ID);
        } catch (NoSuchAlgorithmException e) {
            return false;
        }
        return true;
    }
}
