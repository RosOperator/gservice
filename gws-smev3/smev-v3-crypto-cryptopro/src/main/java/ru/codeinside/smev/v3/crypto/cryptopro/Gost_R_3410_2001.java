package ru.codeinside.smev.v3.crypto.cryptopro;

import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.smev.v3.crypto.api.GostR3410;
import ru.codeinside.smev.v3.crypto.api.GostR3411;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

final public class Gost_R_3410_2001 implements GostR3410 {
    public static final String ALGORITHM_ID = "GOST3411withGOST3410EL";
    private final static String DEFAULT_CERT_NAME = "KSPGMU";
    private final static String DEFAULT_CERT_PASS = "12345678";

    private final static Logger log = Logger.getLogger(Gost_R_3410_2001.class.getName());

    private volatile static boolean started;
    private static PrivateKey privateKey;
    private static X509Certificate cert;

    @Override
    public boolean validate(@Nonnull X509Certificate certificate, @Nonnull InputStream data, @Nonnull byte[] signature) {
        try {
            Signature gost3410 = Signature.getInstance(ALGORITHM_ID);
            gost3410.initVerify(certificate);
            update(gost3410, data);
            return gost3410.verify(signature);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        } catch (SignatureException e) {
            throw new IllegalStateException(e);
        } catch (InvalidKeyException e) {
            throw new IllegalStateException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public ru.codeinside.gws.api.Signature sign(@Nonnull InputStream data) {
        try {
            loadCertificate();
            Signature sign = Signature.getInstance(ALGORITHM_ID);
            sign.initSign(privateKey);
            update(sign, data);
            GostR3411 gostR3411 = new Gost_R_3411_1994();
            data.reset();
            byte[] digest = gostR3411.digest(data);
            byte[] signBytes = sign.sign();
            return new ru.codeinside.gws.api.Signature(cert, data, signBytes, digest, true);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void sign(@Nonnull Enclosure enclosure) {
        try {
            loadCertificate();
            Signature signature = Signature.getInstance(ALGORITHM_ID);
            signature.initSign(privateKey);

            InputStream toSign = enclosure.getContent();
            update(signature, toSign);
            toSign.close();

            GostR3411 gostR3411 = new Gost_R_3411_1994();
            InputStream toDigest = enclosure.getContent();
            byte[] digest = gostR3411.digest(toDigest);
            toDigest.close();

            byte[] signBytes = signature.sign();
            enclosure.signature = new ru.codeinside.gws.api.Signature(cert, enclosure.getContent(), signBytes, digest, true);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void update(Signature signature, InputStream is) throws IOException, SignatureException {
        byte[] buffer = new byte[50 * 1024];
        int count;
        while ((count = is.read(buffer, 0, buffer.length)) > -1) {
            signature.update(buffer, 0, count);
        }
    }

    static void loadCertificate() throws KeyStoreException, NoSuchAlgorithmException, CertificateException,
            IOException, UnrecoverableKeyException {
        if (!started) {
            synchronized (Gost_R_3410_2001.class) {
                if (!started) {
                    final long startMs = System.currentTimeMillis();

                    final Properties properties = new Properties();
                    properties.setProperty("name", DEFAULT_CERT_NAME);
                    properties.setProperty("pass", DEFAULT_CERT_PASS);

                    final File userHome = new File(System.getProperty("user.home"));
                    final File keyFile = new File(userHome, "gses-key.properties");
                    if (!keyFile.exists()) {
                        log.warning(keyFile + " не обнаружен, используются ключи тестовой системы");
                    } else {
                        final FileInputStream is = new FileInputStream(keyFile);
                        properties.load(is);
                        is.close();
                    }

                    final KeyStore keystore = KeyStore.getInstance("HDImageStore");
                    keystore.load(null, null);

                    final String certName = properties.getProperty("name");
                    final String certPass = properties.getProperty("pass");

                    privateKey = ((PrivateKey) keystore.getKey(certName, certPass.toCharArray()));
                    cert = ((X509Certificate) keystore.getCertificate(certName));

                    try {
                        cert.checkValidity();
                        log.info("Загружен действующий до " + cert.getNotAfter() + " сертификат " + cert.getSubjectDN().getName());
                    } catch (CertificateExpiredException e) {
                        log.warning("Закончилось время действия для сертификата " + cert.getSubjectDN().getName());
                        cert = null;
                        privateKey = null;
                    } catch (CertificateNotYetValidException e) {
                        log.warning("Не началось время действия для сертификата " + cert.getSubjectDN().getName());
                        cert = null;
                        privateKey = null;
                    }
                    if ((privateKey != null) && (cert != null)) {
                        started = true;
                    }
                    if (log.isLoggable(Level.FINE)) {
                        log.fine("LOAD CERTIFICATE: " + (System.currentTimeMillis() - startMs) + "ms");
                    }
                }
            }
        }
    }
}
