package ru.codeinside.smev.v3.crypto.cryptopro;

import ru.codeinside.smev.v3.crypto.api.GostR3411;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Gost_R_3411_1994 implements GostR3411 {
    @Nonnull
    @Override
    public byte[] digest(@Nonnull InputStream content) {
        try {
            MessageDigest md = MessageDigest.getInstance("GOST3411");
            byte[] buff = new byte[1024];
            int readCount;
            while ((readCount = content.read(buff)) > 0) {
                md.update(buff, 0, readCount);
            }
            return md.digest();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
