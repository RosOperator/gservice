package ru.codeinside.smev.v3.crypto.osgi;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import ru.codeinside.gws.xml.normalizer.XmlNormalizerImpl;
import ru.codeinside.smev.v3.crypto.api.*;
import ru.codeinside.smev.v3.service.api.consumer.ConsumerRequestBuilder;
import ru.codeinside.smev.v3.transport.api.SmevTypesProvider;
import ru.codeinside.smev.v3.transport.v1_1.SmevTypesProviderImpl;

import javax.inject.Inject;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import static org.ops4j.pax.exam.CoreOptions.*;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class ModulesIT {

    @Inject
    BundleContext context;

    @Before
    public void setUp() throws Exception {
        // инициализация декларативных сервисов.
        context.registerService(ru.codeinside.gws.api.XmlNormalizer.class, new XmlNormalizerImpl(), null);
        context.registerService(SmevTypesProvider.class, new SmevTypesProviderImpl(), null);
    }

    @Configuration
    public Option[] config() {
        return options(
                mavenSibling("gws-api", "1.0.13"),
                mavenSibling("gws-xml-normalizer", "1.0.1"),
                mavenSibling("smev-v3-crypto-api", "1.0.2"),
                mavenSibling("smev-v3-service-api", "1.0.3"),
                mavenSibling("smev-v3-transport-api", "1.0.2"),
                mavenSibling("smev-v3-crypto-cryptopro", "1.0.2"),
                mavenSibling("smev-v3-crypto-bc", "1.0.0"),
                mavenSibling("smev-v3-crypto-sun-pkcs7", "1.0.2"),
                mavenSibling("smev-v3-crypto-xml-dsign", "1.0.2"),
                mavenSibling("smev-v3-xml-normalizer", "1.0.1"),
                mavenSibling("smev-v3-service-impl", "1.0.4"),
                mavenSibling("smev-v3-transport-smev", "1.0.3"),
                junitBundles()
        );
    }

    private Option mavenSibling(String name, String version) {
        Path parentPom = Paths.get("").toAbsolutePath();
        if (parentPom.getFileName().toString().equals("smev-v3-crypto-osgi-test")) {
            parentPom = parentPom.resolve("..");
        }
        if (name.startsWith("gws")) {
            parentPom = parentPom.resolve("..");
        }
        return bundle("file:" + parentPom.resolve(Paths.get(name, "target", name + "-" + version + ".jar")));
    }

    @Test
    public void checkContext() throws InvalidSyntaxException {
        requiredBundleBySymbolicName("ru.codeinside.smev-v3-crypto-cryptopro");
        requiredBundleBySymbolicName("ru.codeinside.smev-v3-crypto-bc");
        requiredBundleBySymbolicName("ru.codeinside.smev-v3-crypto-xml-dsign");
        requiredBundleBySymbolicName("ru.codeinside.smev-v3-crypto-sun-pkcs7");
        requiredBundleBySymbolicName("ru.codeinside.smev-v3-transport-api");
        requiredBundleBySymbolicName("ru.codeinside.gws-api");
        Assert.assertNotNull(context.getServiceReference(GostR3410.class));
        Assert.assertNotNull(context.getServiceReference(GostR3411.class));
        Assert.assertNotNull(context.getServiceReference(SignatureAssembler.class));
        Assert.assertNotNull(context.getServiceReference(XmlSignatureAssembler.class));
        Assert.assertNotNull(context.getServiceReference(XmlNormalizer.class));
        Assert.assertNotNull(context.getServiceReference(ConsumerRequestBuilder.class));
        Assert.assertNotNull(context.getServiceReference(ru.codeinside.gws.api.XmlNormalizer.class));
        Assert.assertNotNull(context.getServiceReference(SmevTypesProvider.class));
    }

    private Bundle requiredBundleBySymbolicName(String name) {
        Bundle[] bundles = context.getBundles();
        for (Bundle bundle : bundles) {
            if (bundle.getSymbolicName().equals(name)) {
                return bundle;
            }
        }
        throw new IllegalStateException("Bundle '" + name + "' not found in " + Arrays.asList(bundles));
    }
}