package ru.codeinside.smev.v3.crypto.sunpkcs7;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import ru.codeinside.smev.v3.crypto.api.SignatureAssembler;

public class Activator implements BundleActivator {
    private ServiceRegistration serviceRegistration;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        SignatureAssembler assembler = new SunSignatureAssembler();
        serviceRegistration = bundleContext.registerService(SignatureAssembler.class.getName(), assembler, null);
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        if (serviceRegistration != null) {
            serviceRegistration.unregister();
        }
    }
}
