package ru.codeinside.smev.v3.crypto.sunpkcs7;

import ru.codeinside.gws.api.Signature;
import ru.codeinside.smev.v3.crypto.api.SignatureAssembler;
import sun.security.pkcs.ContentInfo;
import sun.security.pkcs.PKCS7;
import sun.security.pkcs.ParsingException;
import sun.security.pkcs.SignerInfo;
import sun.security.util.DerOutputStream;
import sun.security.util.ObjectIdentifier;
import sun.security.x509.AlgorithmId;
import sun.security.x509.X500Name;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.cert.X509Certificate;
import java.util.logging.Logger;

final public class SunSignatureAssembler implements SignatureAssembler {

    private final static ObjectIdentifier GOST3410;
    private final static ObjectIdentifier GOST3411;

    static {
        try {
            GOST3410 = new ObjectIdentifier("1.2.643.2.2.19");
            GOST3411 = new ObjectIdentifier("1.2.643.2.2.9");
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    final static private Logger log = Logger.getLogger(SunSignatureAssembler.class.getName());

    /**
     * Формирование контейнера Pkcs7.
     *
     * @param signature описатель подписи.
     * @return данных в фомате ASN DER и Pkcs7
     */
    @Override
    @Nonnull
    public byte[] assemble(@Nonnull Signature signature) {
        final X509Certificate certificate = signature.certificate;
        final byte[] sign = signature.sign;
        X500Name issuer = X500Name.asX500Name(certificate.getIssuerX500Principal());
        final AlgorithmId digestAlgorithmId = new AlgorithmId(GOST3411);
        final AlgorithmId signAlgorithmId = new AlgorithmId(GOST3410);
        SignerInfo sInfo = new SignerInfo(issuer, certificate.getSerialNumber(), digestAlgorithmId, signAlgorithmId, sign);
        ContentInfo cInfo = new ContentInfo(ContentInfo.DATA_OID, null);
        PKCS7 pkcs7 = new PKCS7(
                new AlgorithmId[]{digestAlgorithmId},
                cInfo,
                new X509Certificate[]{certificate},
                new SignerInfo[]{sInfo});
        final ByteArrayOutputStream bOut = new DerOutputStream();
        try {
            pkcs7.encodeSignedData(bOut);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return bOut.toByteArray();
    }

    /**
     * Разбор контейнера Pkcs7.
     *
     * @param derBytes данные в фомате ASN DER и Pkcs7.
     * @return описатель подписи.
     */
    @Override
    @Nonnull
    public Signature disassemble(@Nonnull byte[] derBytes) {
        final PKCS7 pkcs7;
        try {
            pkcs7 = new PKCS7(derBytes);
        } catch (ParsingException e) {
            log.info("Fail parse pkcs7: " + e.getMessage());
            return new Signature(null, (byte[]) null, null, false);
        }

        final AlgorithmId digestAlgorithmId = new AlgorithmId(GOST3411);
        final AlgorithmId signAlgorithmId = new AlgorithmId(GOST3410);
        final AlgorithmId[] digestAlgorithmIds = pkcs7.getDigestAlgorithmIds();

        if (digestAlgorithmIds == null || digestAlgorithmIds.length == 0) {
            log.info("No digestAlgorithm in pkcs7");
        } else if (!digestAlgorithmIds[0].equals(digestAlgorithmId)) {
            log.info("No GOST3411 in pkcs7");
        } else {
            final X509Certificate[] certificates = pkcs7.getCertificates();
            if (certificates == null || certificates.length == 0) {
                log.info("No certificate in pkcs7");
            } else {
                final X509Certificate certificate = certificates[0];
                final SignerInfo[] signerInfos = pkcs7.getSignerInfos();
                if (signerInfos == null || signerInfos.length == 0) {
                    log.info("No signerInfos in pkcs7");
                } else {
                    final SignerInfo signerInfo = signerInfos[0];
                    if (!signerInfo.getIssuerName().equals(X500Name.asX500Name(certificate.getIssuerX500Principal()))) {
                        log.info("Invalid issuerX500Principal in pkcs7");
                    } else if (!signerInfo.getDigestAlgorithmId().equals(digestAlgorithmId)) {
                        log.info("No GOST3411 in pkcs7");
                    } else if (!signerInfo.getDigestEncryptionAlgorithmId().equals(signAlgorithmId)) {
                        log.info("No GOST3410 in pkcs7");
                    } else if (!signerInfo.getCertificateSerialNumber().equals(certificate.getSerialNumber())) {
                        log.info("Invalid certificate serial number in pkcs7");
                    } else {
                        ByteArrayInputStream content = null;
                        try {
                            byte[] contentBytes = pkcs7.getContentInfo().getContentBytes();
                            if (contentBytes != null) {
                                content = new ByteArrayInputStream(contentBytes);
                            }
                        } catch (IOException e) {
                            log.info("Could not get content bytes.");
                        }
                        byte[] encryptedDigest = signerInfo.getEncryptedDigest();
                        return new Signature(certificate, content, encryptedDigest, true);
                    }
                }
            }
        }
        return new Signature(null, (byte[]) null, null, false);
    }
}
