package ru.codeinside.smev.v3.crypto.sunpkcs7;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.Test;
import ru.codeinside.gws.api.Signature;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.SignatureException;

public class SunSignatureAssemblerTest extends Assert {

    public SunSignatureAssemblerTest() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    public void test_disassemble_assemble() throws IOException, NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        SunSignatureAssembler assembler = new SunSignatureAssembler();
        byte[] sigBytes = getBytes("req.sig");

        Signature signature = assembler.disassemble(sigBytes);
        String subjectDN =
                "T=Генеральный директор, CN=Семенкин Максим Викторович, O=ООО КодИнсайд, " +
                "L=Пенза, ST=58 Пензенская область, C=RU, EMAILADDRESS=maxim.semenkin@gmail.ru, " +
                "OID.1.2.643.3.131.1.1=5837040135, OID.1.2.643.100.1=1095837000929";
        assertEquals(subjectDN, signature.certificate.getSubjectDN().toString());
        assertNotNull(signature.certificate);

        byte[] data = getBytes("req.xml");
        java.security.Signature validateSign = java.security.Signature.getInstance("ECGOST3410", "BC");
        validateSign.initVerify(signature.certificate);
        validateSign.update(data);
        assertTrue(validateSign.verify(signature.sign));
        assertArrayEquals(sigBytes, assembler.assemble(signature));
    }

    private byte[] getBytes(String filename) throws IOException {
        InputStream is = getClass().getClassLoader().getResourceAsStream(filename);
        int count, total = 0;
        byte[] buff = new byte[1024], tmp, result = new byte[0];
        while ((count = is.read(buff, 0, buff.length)) > -1) {
            total += count;
            tmp = new byte[total];
            System.arraycopy(result, 0, tmp, 0, result.length);
            System.arraycopy(buff, 0, tmp, result.length, count);
            result = tmp;
        }
        return result;
    }
}
