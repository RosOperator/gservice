package ru.codeinside.smev.v3.crypto.xmldsign;


import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import ru.codeinside.smev.v3.crypto.api.XmlSignatureAssembler;

public class Activator implements BundleActivator {
    static BundleContext context;

    private ServiceRegistration serviceRegistration;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        context = bundleContext;

        XmlSignatureAssembler assembler = new XmlSignatureAssemblerImpl();
        serviceRegistration = bundleContext.registerService(XmlSignatureAssembler.class.getName(), assembler, null);
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        if (serviceRegistration != null) {
            serviceRegistration.unregister();
        }

        context = null;
    }

    public static BundleContext context() {
        return context;
    }
}