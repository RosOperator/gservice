package ru.codeinside.smev.v3.crypto.xmldsign;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.codeinside.smev.v3.crypto.api.XmlSignature;
import ru.codeinside.smev.v3.crypto.api.XmlSignatureAssembler;
import ru.codeinside.smev.v3.crypto.xmldsign.entity.XMLDSign;

import javax.annotation.Nonnull;
import javax.xml.bind.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.logging.Logger;

@SuppressWarnings("PackageAccessibility")
public class XmlSignatureAssemblerImpl implements XmlSignatureAssembler {
    private Logger log = Logger.getLogger(XmlSignatureAssemblerImpl.class.getCanonicalName());
    /**
     * Сборка из объекта подписи элемента XMLDSign.
     *
     * @param signature данные подписи.
     * @return xml элемент.
     */
    @Override
    @Nonnull
    public Element assemble(@Nonnull XmlSignature signature) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(XMLDSign.class);
            Marshaller marshaller = jaxbContext.createMarshaller();

            DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            marshaller.marshal(new XMLDSign(signature), doc);
            return doc.getDocumentElement();
        } catch (PropertyException e) {
            log.severe("Unable to set property Marshaller.JAXB_FORMATTED_OUTPUT: " + e.getMessage());
            throw new RuntimeException(e);
        } catch (JAXBException e) {
            log.severe("Error to get JAXBContext: " + e.getMessage());
            throw new RuntimeException(e);
        } catch (ParserConfigurationException e) {
            log.severe("Can't get DocumentBuilder: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * Разбор xml элемента в объект подписи.
     *
     * @param element элемент для разбора.
     * @return объект подписи.
     */
    @Override
    @Nonnull
    public XmlSignature disassemble(@Nonnull Element element) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(XMLDSign.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            XMLDSign xmlDSign = (XMLDSign) unmarshaller.unmarshal(element);
            return xmlDSign.getXmlSignature();
        } catch (JAXBException e) {
            log.severe("Error to get JAXBContext: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
