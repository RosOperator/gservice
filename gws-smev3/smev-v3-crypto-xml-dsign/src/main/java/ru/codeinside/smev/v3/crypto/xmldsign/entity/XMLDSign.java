package ru.codeinside.smev.v3.crypto.xmldsign.entity;

import org.apache.commons.codec.binary.Base64;
import org.osgi.framework.ServiceReference;
import ru.codeinside.gws.api.Signature;
import ru.codeinside.gws.api.XmlNormalizer;
import ru.codeinside.smev.v3.crypto.api.XmlSignature;
import ru.codeinside.smev.v3.crypto.xmldsign.Activator;
import sun.security.util.DerOutputStream;

import javax.xml.bind.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

@SuppressWarnings("PackageAccessibility")
@XmlRootElement(name = "Signature", namespace = XMLDSign.XMLNS)
@XmlAccessorType(XmlAccessType.FIELD)
public class XMLDSign {
    public static final String XMLNS = "http://www.w3.org/2000/09/xmldsig#";
    public static final String DIGEST_METHOD = "http://www.w3.org/2001/04/xmldsig-more#gostr3411";
    public static final String SIGNATURE_METHOD = "http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411";
    public static final String CANONICALIZATION_METHOD = "http://www.w3.org/2001/10/xml-exc-c14n#";
    public static final String ADDITIONAL_NORMALIZE_METHOD = "urn://smev-gov-ru/xmldsig/transform";

    @XmlElement(name = "SignedInfo", namespace = XMLDSign.XMLNS)
    private SignedInfo signedInfo;

    @XmlElement(name = "SignatureValue", namespace = XMLNS)
    private String signatureValue;

    private byte[] getSignatureValue() {
        return Base64.decodeBase64(signatureValue);
    }

    private void setSignatureValue(byte[] signatureValue) {
        this.signatureValue = Base64.encodeBase64String(signatureValue);
    }

    @XmlElement(name = "KeyInfo", namespace = XMLNS)
    private KeyInfo keyInfo = new KeyInfo();

    @SuppressWarnings("unused")
    public XMLDSign() {

    }

    public XMLDSign(XmlSignature xmlSignature) {
        signedInfo = new SignedInfo(xmlSignature);
        setSignatureValue(xmlSignature.getSignature().sign);
        if (xmlSignature.getSignature().certificate != null) {
            keyInfo.x509Data.setCertificate(xmlSignature.getSignature().certificate);
            keyInfo.x509Data.setSubjectName(xmlSignature.getSignature().certificate.getSubjectDN().toString());
        }
    }

    public XmlSignature getXmlSignature() {
        if (!SIGNATURE_METHOD.equals(signedInfo.signatureMethod.algorithm)) {
            throw new RuntimeException("Unexpected signature method: " + signedInfo.signatureMethod.algorithm);
        }
        if (!DIGEST_METHOD.equals(signedInfo.reference.digestMethod.algorithm)) {
            throw new RuntimeException("Unexpected digest method: " + signedInfo.reference.digestMethod.algorithm);
        }

        ByteArrayInputStream normalizedContent;
        try {
            JAXBElement<SignedInfo> jaxbElement = new JAXBElement<SignedInfo>(
                    new QName(XMLNS, SignedInfo.class.getSimpleName(), "ds"), SignedInfo.class, signedInfo);
            Marshaller marshaller = JAXBContext.newInstance(SignedInfo.class).createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            marshaller.marshal(jaxbElement, baos);
            normalizedContent = new ByteArrayInputStream(normalizeContent(baos.toByteArray()));
        } catch (PropertyException e) {
            throw new RuntimeException(e);
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }

        Signature signature = new Signature(keyInfo.x509Data.getCertificate(), normalizedContent,
                getSignatureValue(), signedInfo.reference.getDigestValue(), true);

        XmlSignature xmlSignature = new XmlSignature(signature);
        xmlSignature.setElementReference(signedInfo.reference.uri);
        return xmlSignature;
    }

    private byte[] normalizeContent(byte[] content) {
        ServiceReference serviceReference = null;
        try {
            serviceReference = Activator.context().getServiceReference(XmlNormalizer.class.getName());
            if (serviceReference == null) {
                throw new RuntimeException("Не удалось получить ссылку на сервис нормализации.");
            }
            XmlNormalizer normalizer = (XmlNormalizer) Activator.context().getService(serviceReference);
            if (normalizer == null) {
                throw new RuntimeException("Не удалось получить объект сервиса нормализации.");
            }
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            normalizer.normalize(new ByteArrayInputStream(content), out);
            return out.toByteArray();
        } finally {
            if (serviceReference != null) {
                Activator.context().ungetService(serviceReference);
            }
        }

    }


    /** INNER CLASSES **/

    static class KeyInfo {
        @XmlElement(name = "X509Data", namespace = XMLNS)
        public X509Data x509Data = new X509Data();
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    static class X509Data {
        @XmlElement(name = "X509Certificate", namespace = XMLNS)
        private String x509Certificate = "";
        @SuppressWarnings("unused")
        @XmlElement(name = "X509SubjectName", namespace = XMLNS)
        private String x509SubjectName = "";

        public void setSubjectName(String subjectName) {
            this.x509SubjectName = subjectName;
        }

        @SuppressWarnings("Duplicates")
        public void setCertificate(X509Certificate certificate) {
            DerOutputStream derEncoder;
            try {
                derEncoder = new DerOutputStream();
                derEncoder.write(certificate.getEncoded());
            } catch (CertificateEncodingException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            this.x509Certificate = Base64.encodeBase64String(derEncoder.toByteArray());
        }

        @SuppressWarnings("Duplicates")
        public X509Certificate getCertificate() {
            try {
                byte[] derBytes = Base64.decodeBase64(x509Certificate);
                ByteArrayInputStream is = new ByteArrayInputStream(derBytes);
                return (X509Certificate) CertificateFactory.getInstance("X509").generateCertificate(is);
            } catch (CertificateException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
