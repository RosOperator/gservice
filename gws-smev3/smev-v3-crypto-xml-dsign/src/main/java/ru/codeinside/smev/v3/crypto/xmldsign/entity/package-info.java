@XmlSchema(xmlns = {@XmlNs(prefix = "ds", namespaceURI = XMLDSign.XMLNS)})
package ru.codeinside.smev.v3.crypto.xmldsign.entity;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;