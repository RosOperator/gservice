package ru.codeinside.smev.v3.crypto.xmldsign;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import ru.codeinside.gws.api.XmlNormalizer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class Mocks {
    public static BundleContext mockContext() {
        XmlNormalizer normalizer = mock(XmlNormalizer.class);
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocationOnMock) throws Throwable {
                ByteArrayOutputStream out = invocationOnMock.getArgumentAt(1, ByteArrayOutputStream.class);
                out.write(new byte[] {1,2,3,4,5});
                return null;
            }
        }).when(normalizer).normalize(any(ByteArrayInputStream.class), any(ByteArrayOutputStream.class));

        ServiceReference serviceReference = mock(ServiceReference.class);

        BundleContext context = mock(BundleContext.class);
        when(context.getServiceReference(XmlNormalizer.class.getName())).thenReturn(serviceReference);
        when(context.getService(serviceReference)).thenReturn(normalizer);
        return context;
    }
}
