package ru.codeinside.smev.v3.crypto.xmldsign;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import ru.codeinside.smev.v3.crypto.api.XmlSignature;
import ru.codeinside.smev.v3.crypto.xmldsign.entity.XMLDSign;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by A.Sivishkin on 27.10.15.
 */
public class XMLDSignTest extends Assert {
    @Before
    public void setUp() throws Exception {
        Activator.context = Mocks.mockContext();
    }

    @Test
    public void testValidate() {

        Element element = readXml();
        assertNotNull(element);
        XMLDSign xmlDSign = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(XMLDSign.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            xmlDSign = (XMLDSign) unmarshaller.unmarshal(element);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        assertNotNull(xmlDSign);
        XmlSignature signature = xmlDSign.getXmlSignature();
        assertNotNull(signature);
        byte[] sigContent = readStream(signature.getSignature().getContent());
        assertArrayEquals(new byte[] {1,2,3,4,5}, sigContent);
    }

    private Element readXml() {

        File fXmlFile = new File(getClass().getClassLoader().getResource("xml_sign_example.xml").getFile());
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setNamespaceAware(true);
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            return doc.getDocumentElement();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] readStream(InputStream is) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int b;
        try {
            while ((b = is.read()) != -1) {
                out.write(b);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out.toByteArray();
    }
}
