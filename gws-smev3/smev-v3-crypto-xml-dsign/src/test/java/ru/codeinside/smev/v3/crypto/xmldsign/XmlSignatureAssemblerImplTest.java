package ru.codeinside.smev.v3.crypto.xmldsign;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.codeinside.smev.v3.crypto.api.XmlSignature;
import ru.codeinside.smev.v3.crypto.api.XmlSignatureAssembler;
import ru.codeinside.smev.v3.crypto.xmldsign.entity.XMLDSign;
import sun.security.util.DerOutputStream;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.security.*;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class XmlSignatureAssemblerImplTest extends Assert {

    public XmlSignatureAssemblerImplTest() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Before
    public void setUp() throws Exception {
        Activator.context = Mocks.mockContext();
    }

    @Test
    public void test_assemble() throws JAXBException, TransformerException, InvalidAlgorithmParameterException, CertificateException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException, IOException, URISyntaxException, KeyStoreException, UnrecoverableKeyException, ParserConfigurationException {
        XmlSignatureAssembler assembler = new XmlSignatureAssemblerImpl();

        XmlSignature xmlSignature = buildXmlSignature();

        Element element = assembler.assemble(xmlSignature);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        StreamResult result = new StreamResult(new StringWriter());
        transformer.transform(new DOMSource(element), result);
        String assembledSig = result.getWriter().toString();

        Element buildElement = buildXmlSignatureElement(xmlSignature);
        result = new StreamResult(new StringWriter());
        transformer.transform(new DOMSource(buildElement), result);
        String buildSig = result.getWriter().toString();

        assertEquals(buildSig, assembledSig);
    }

    @Test
    public void test_disassemble() throws CertificateException, UnrecoverableKeyException, NoSuchAlgorithmException, IOException, KeyStoreException, SignatureException, NoSuchProviderException, InvalidKeyException, ParserConfigurationException {
        XmlSignature xmlSignature = buildXmlSignature();
        Element xmlSignatureElement = buildXmlSignatureElement(xmlSignature);
        XmlSignatureAssembler assembler = new XmlSignatureAssemblerImpl();
        XmlSignature disassembledSignature = assembler.disassemble(xmlSignatureElement);
        assertEquals(xmlSignature.getElementReference(), disassembledSignature.getElementReference());
        assertEquals(xmlSignature.getSignature().certificate, disassembledSignature.getSignature().certificate);
        assertArrayEquals(xmlSignature.getSignature().getDigest(), disassembledSignature.getSignature().getDigest());
        assertArrayEquals(xmlSignature.getSignature().sign, disassembledSignature.getSignature().sign);
    }

    private XmlSignature buildXmlSignature() throws CertificateException, NoSuchAlgorithmException, IOException, KeyStoreException, NoSuchProviderException, InvalidKeyException, SignatureException, UnrecoverableKeyException {
        KeyStore store = KeyStore.getInstance("JKS");
        store.load(getClass().getClassLoader().getResourceAsStream("test_store.keystore"),
                "test_pass".toCharArray());

        X509Certificate certificate = (X509Certificate) store.getCertificate("test_cert");
        PrivateKey privateKey = (PrivateKey) store.getKey("test_cert", "test_pass".toCharArray());

        Signature signature = Signature.getInstance("ECGOST3410", "BC");
        signature.initSign(privateKey);
        byte[] data = "Hello World!".getBytes("UTF8");
        signature.update(data);

        MessageDigest digest = MessageDigest.getInstance("GOST3411", "BC");
        digest.update(data);

        ru.codeinside.gws.api.Signature sign =
            new ru.codeinside.gws.api.Signature(certificate, (byte[]) null, signature.sign(), digest.digest(), true);

        XmlSignature xmlSignature = new XmlSignature(sign);
        xmlSignature.setElementReference("#TEST_DATA");
        return xmlSignature;
    }

    private Element buildXmlSignatureElement(XmlSignature xmlSignature) throws ParserConfigurationException, CertificateEncodingException, IOException {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = builder.newDocument();

        Element canMethodElement = document.createElementNS(XMLDSign.XMLNS, "ds:CanonicalizationMethod");
        canMethodElement.setAttribute("Algorithm", XMLDSign.CANONICALIZATION_METHOD);

        Element signatureMethodElement = document.createElementNS(XMLDSign.XMLNS, "ds:SignatureMethod");
        signatureMethodElement.setAttribute("Algorithm", XMLDSign.SIGNATURE_METHOD);

        Element transform1Element = document.createElementNS(XMLDSign.XMLNS, "ds:Transform");
        transform1Element.setAttribute("Algorithm", XMLDSign.CANONICALIZATION_METHOD);

        Element transform2Element = document.createElementNS(XMLDSign.XMLNS, "ds:Transform");
        transform2Element.setAttribute("Algorithm", XMLDSign.ADDITIONAL_NORMALIZE_METHOD);

        Element transformsElement = document.createElementNS(XMLDSign.XMLNS, "ds:Transforms");
        transformsElement.appendChild(transform1Element);
        transformsElement.appendChild(transform2Element);

        Element digestMethodElement = document.createElementNS(XMLDSign.XMLNS, "ds:DigestMethod");
        digestMethodElement.setAttribute("Algorithm", XMLDSign.DIGEST_METHOD);

        Element digestValueElement = document.createElementNS(XMLDSign.XMLNS, "ds:DigestValue");
        digestValueElement.setTextContent(Base64.encode(xmlSignature.getSignature().getDigest()));

        Element referenceElement = document.createElementNS(XMLDSign.XMLNS, "ds:Reference");
        referenceElement.setAttribute("URI", xmlSignature.getElementReference());
        referenceElement.appendChild(transformsElement);
        referenceElement.appendChild(digestMethodElement);
        referenceElement.appendChild(digestValueElement);

        Element signedInfoElement = document.createElementNS(XMLDSign.XMLNS, "ds:SignedInfo");
        signedInfoElement.appendChild(canMethodElement);
        signedInfoElement.appendChild(signatureMethodElement);
        signedInfoElement.appendChild(referenceElement);

        Element signatureValueElement = document.createElementNS(XMLDSign.XMLNS, "ds:SignatureValue");
        signatureValueElement.setTextContent(Base64.encode(xmlSignature.getSignature().sign));

        DerOutputStream derEncoder = new DerOutputStream();
        derEncoder.write(xmlSignature.getSignature().certificate.getEncoded());

        Element x509CertificateElement = document.createElementNS(XMLDSign.XMLNS, "ds:X509Certificate");
        x509CertificateElement.setTextContent(Base64.encode(derEncoder.toByteArray()));

        Element x509SubjectNameElement = document.createElementNS(XMLDSign.XMLNS, "ds:X509SubjectName");
        x509SubjectNameElement.setTextContent(xmlSignature.getSignature().certificate.getSubjectDN().toString());

        Element x509DataElement = document.createElementNS(XMLDSign.XMLNS, "ds:X509Data");
        x509DataElement.appendChild(x509CertificateElement);
        x509DataElement.appendChild(x509SubjectNameElement);

        Element keyInfoElement = document.createElementNS(XMLDSign.XMLNS, "ds:KeyInfo");
        keyInfoElement.appendChild(x509DataElement);

        Element signatureElement = document.createElementNS(XMLDSign.XMLNS, "ds:Signature");
        signatureElement.appendChild(signedInfoElement);
        signatureElement.appendChild(signatureValueElement);
        signatureElement.appendChild(keyInfoElement);
        return signatureElement;
    }

//    private X509Certificate genCertificate(KeyPair pair) throws NoSuchAlgorithmException, CertificateEncodingException, NoSuchProviderException, InvalidKeyException, SignatureException {
//        Date startDate = new Date();
//        Date expiryDate = new Date(startDate.getTime() + 10000);
//        BigInteger serialNumber = new BigInteger("123456789");
//        X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
//        X500Principal dnName = new X500Principal("CN=Test CA Certificate");
//        certGen.setSerialNumber(serialNumber);
//        certGen.setIssuerDN(dnName);
//        certGen.setNotBefore(startDate);
//        certGen.setNotAfter(expiryDate);
//        certGen.setSubjectDN(dnName);
//        certGen.setPublicKey(pair.getPublic());
//        certGen.setSignatureAlgorithm("GOST3411withECGOST3410");
//        return certGen.generate(pair.getPrivate(), "BC");
//    }
//
//    private KeyPair genKeyPair() throws InvalidAlgorithmParameterException, NoSuchProviderException, NoSuchAlgorithmException {
//        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("ECGOST3410", "BC");
//        keyGen.initialize(new ECGenParameterSpec("GostR3410-2001-CryptoPro-A"));
//        return keyGen.generateKeyPair();
//    }

//    @Test
//    public void store_certificate() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, CertificateException, InvalidKeyException, SignatureException, KeyStoreException, IOException {
//        KeyPair pair = genKeyPair();
//        X509Certificate certificate = genCertificate(pair);
//
//        KeyStore store = KeyStore.getInstance("JKS");
//        store.load(null, null);
//        store.setKeyEntry("test_cert", pair.getPrivate(), "test_pass".toCharArray(), new X509Certificate[]{certificate});
//
//        FileOutputStream os = new FileOutputStream("test_store.keystore");
//        store.store(os, "test_pass".toCharArray());
//    }
}
