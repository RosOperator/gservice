package ru.codeinside.smev.v3.provider;

import com.rstyle.skmv.snils_by_data._1_0.SnilsByDataRequest;
import com.rstyle.skmv.snils_by_data._1_0.SnilsByDataResponse;
import com.rstyle.skmv.snils_by_data._1_0.SnilsByDataResponseType;
import ru.codeinside.gws.api.DeclarerContext;
import ru.codeinside.gws.api.ReceiptContext;
import ru.codeinside.gws.api.Revision;
import ru.codeinside.smev.v3.service.api.InformationType;
import ru.codeinside.smev.v3.service.api.ProviderRequestContext;
import ru.codeinside.smev.v3.service.api.RequestRejected;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequestData;
import ru.codeinside.smev.v3.service.api.provider.ProvidedResponse;
import ru.codeinside.smev.v3.service.api.provider.Provider;
import ru.codeinside.smev.v3.service.api.provider.ProviderRequestException;
import ru.codeinside.smev.v3.service.api.provider.Request;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.ParserConfigurationException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;


public class EmptyProvider implements Provider {
    private static Logger logger = Logger.getLogger(EmptyProvider.class.getName());

    @Override
    public Revision getRevision() {
        return Revision.rev30981;
    }

    @Override
    public ProvidedResponse createResponse(ReceiptContext businessProcess) {
        try {
            ProvidedResponse response = new ProvidedResponse();
            Object replyTo = businessProcess.getVariableByFullName("replyTo");
            if (replyTo != null) {
                response.setReplyTo((String) replyTo);
            } else {
                throw new IllegalStateException("Sender was not found in XML");
            }

            if ((Boolean)businessProcess.getVariableByFullName("isRejected")){
                RequestRejected rejected = new RequestRejected();
                rejected.rejectionReasonCode = "124";
                String reason = (String)businessProcess.getVariableByFullName("rejectedReason");
                if (reason == null || reason.isEmpty()){
                    rejected.rejectionReason = "Отказ обработки оператором";
                } else {
                    rejected.rejectionReason = reason;
                }
                response.addRequestRejeted(rejected);
            } else {
                response.setResponseData(createResponseData(businessProcess));
            }
            return response;
        } catch (JAXBException e) {
            throw new IllegalStateException("Parsing XML error");
        } catch (ParserConfigurationException e) {
            throw new IllegalStateException("Parsing XML error");
        }
    }

    @Override
    public String processRequest(ProviderRequestContext context) throws ProviderRequestException {
        logger.info("СНИЛС: обработка запроса");

        Request request = context.getRequestMessage().request;
        if (request != null) {
            DeclarerContext declarerContext = context.getDeclarerContext(7);
            declarerContext.setValue("replyTo", request.data.replyTo);

            OrganizationRequestData orgData = request.data.organizationRequest.data;
            try {
                JAXBContext jaxbContext = JAXBContext.newInstance(SnilsByDataRequest.class);
                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                SnilsByDataRequest data = (SnilsByDataRequest) unmarshaller.unmarshal(orgData.personalData.data);

                declarerContext.setValue("BirthDate", fromXmlCalendar(data.getBirthDate()));
                declarerContext.setValue("FamilyName", data.getFamilyName());
                declarerContext.setValue("FirstName", data.getFirstName());
                declarerContext.setValue("Patronymic", data.getPatronymic());
                if(data.getGender() != null) {
                    declarerContext.setValue("Gender", data.getGender().value());
                } else{
                    throw new ProviderRequestException("102", "Отсутствует значение поля \"Пол\"");
                }
                String bidID = declarerContext.declare();
                logger.info("Заведена новая процедура №" + bidID);
            } catch (JAXBException e) {
                logger.severe("Ошибка маршалинга: " + e.getMessage());
                return null;
            }

            return orgData.messageID;
        }

        return null;
    }

    private Date fromXmlCalendar(XMLGregorianCalendar xmlDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, xmlDate.getYear());
        calendar.set(Calendar.MONTH, xmlDate.getMonth());
        calendar.set(Calendar.DAY_OF_MONTH, xmlDate.getDay());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    @Override
    public InformationType getInformationType() {
        return new InformationType("http://skmv.rstyle.com/snils-by-data/1.0.0", "snilsByDataRequest");
    }

    private SnilsByDataResponse createResponseData(ReceiptContext businessProcess) {
        SnilsByDataResponseType type = new SnilsByDataResponseType();
        type.setSnils(businessProcess.getVariableByFullName("SNILS").toString());

        SnilsByDataResponse response = new SnilsByDataResponse();
        response.setResponse(type);
        return response;
    }
}
