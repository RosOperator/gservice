package ru.codeinside.smev.v3.server_stub;

import java.util.Calendar;

public class AwaitingAck<T> {
    public static final int MAX_AVAILABLE_MINUTES = 1;

    private final T data;
    private final String id;
    private final Calendar timestamp;

    public AwaitingAck(T data, String id) {
        this.data = data;
        this.id = id;

        this.timestamp = Calendar.getInstance();
        this.timestamp.add(Calendar.MINUTE, MAX_AVAILABLE_MINUTES);
    }

    public T getData() {
        return data;
    }

    public boolean isOutOfDate() {
        return Calendar.getInstance().after(timestamp);
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AwaitingAck)) return false;

        if (hashCode() != o.hashCode()) return false;

        AwaitingAck<?> that = (AwaitingAck<?>) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
