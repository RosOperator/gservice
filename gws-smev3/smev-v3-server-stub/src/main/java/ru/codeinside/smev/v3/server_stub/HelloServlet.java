package ru.codeinside.smev.v3.server_stub;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HelloServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        response.getWriter().write("Hello! SMEV 3 Server Stub module is active.\n" +
                "To access Smev 3 Server use /smev3/SMEVMessageExchangeService");
    }
}
