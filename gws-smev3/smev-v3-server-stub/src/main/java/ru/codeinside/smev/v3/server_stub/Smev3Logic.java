package ru.codeinside.smev.v3.server_stub;

import ru.codeinside.smev.v3.service.api.InformationType;
import ru.codeinside.smev.v3.transport.api.v1_1.*;

public interface Smev3Logic {
    SendRequestResponse putRequest(SendRequestRequest request) throws SMEVFailureException;

    SendResponseResponse putResponse(SendResponseRequest response) throws SMEVFailureException;

    GetRequestResponse getRequest(InformationType informationType) throws SMEVFailureException;

    GetResponseResponse getResponse(InformationType informationType) throws SMEVFailureException;

    void ack(String id);
}
