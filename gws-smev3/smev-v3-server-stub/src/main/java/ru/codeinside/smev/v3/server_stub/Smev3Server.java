package ru.codeinside.smev.v3.server_stub;

import ru.codeinside.smev.v3.service.api.InformationType;
import ru.codeinside.smev.v3.transport.api.v1_1.*;
import ru.codeinside.smev.v3.transport.api.v1_1.Void;

import javax.inject.Inject;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.soap.MTOM;

@SuppressWarnings({"PackageAccessibility", "unused"})
@MTOM
@WebService(
        portName = "SMEVMessageExchangeEndpoint",
        serviceName = "SMEVMessageExchangeService",
        targetNamespace = "urn://x-artefacts-smev-gov-ru/services/message-exchange/1.1",
        endpointInterface = "ru.codeinside.smev.v3.transport.api.v1_1.SMEVMessageExchangePortType",
        wsdlLocation = "smev.wsdl"
)@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public class Smev3Server implements SMEVMessageExchangePortType {

    @Inject
    Smev3Logic logic;

    @Override
    public SendRequestResponse sendRequest(SendRequestRequest parameters) throws AccessDeniedException, AttachmentContentMiscoordinationException, AttachmentSizeLimitExceededException, BusinessDataTypeIsNotSupportedException, DestinationOverflowException, InvalidContentException, InvalidMessageIdFormatException, MessageIsAlreadySentException, QuoteLimitExceededException, RecipientIsNotFoundException, SMEVFailureException, SenderIsNotRegisteredException, SignatureVerificationFaultException, StaleMessageIdException {
        return logic.putRequest(parameters);
    }

    @Override
    public SendResponseResponse sendResponse(SendResponseRequest parameters) throws AttachmentContentMiscoordinationException, AttachmentSizeLimitExceededException, BusinessDataTypeIsNotSupportedException, DestinationOverflowException, IncorrectResponseContentTypeException, InvalidContentException, InvalidMessageIdFormatException, MessageIsAlreadySentException, QuoteLimitExceededException, RecipientIsNotFoundException, SMEVFailureException, SenderIsNotRegisteredException, SignatureVerificationFaultException, StaleMessageIdException {
        return logic.putResponse(parameters);
    }

    @Override
    public GetRequestResponse getRequest(GetRequestRequest parameters) throws InvalidContentException, SMEVFailureException, SenderIsNotRegisteredException, SignatureVerificationFaultException, UnknownMessageTypeException {
        return logic.getRequest(getInformationType(parameters.getMessageTypeSelector()));
    }

    @Override
    public GetStatusResponse getStatus(GetStatusRequest parameters) throws InvalidContentException, SMEVFailureException, SenderIsNotRegisteredException, SignatureVerificationFaultException, UnknownMessageTypeException {
        throw new UnsupportedOperationException("Method does not implemented.");
    }

    @Override
    public GetResponseResponse getResponse(GetResponseRequest parameters) throws InvalidContentException, SMEVFailureException, SenderIsNotRegisteredException, SignatureVerificationFaultException, UnknownMessageTypeException {
        return logic.getResponse(getInformationType(parameters.getMessageTypeSelector()));
    }

    @Override
    public Void ack(AckRequest parameters) throws InvalidContentException, SMEVFailureException, SenderIsNotRegisteredException, SignatureVerificationFaultException, TargetMessageIsNotFoundException {
        logic.ack(parameters.getAckTargetMessage().getValue());
        return new Void();
    }

    @Override
    public GetIncomingQueueStatisticsResponse getIncomingQueueStatistics(GetIncomingQueueStatisticsRequest parameters) throws InvalidContentException, SMEVFailureException, SenderIsNotRegisteredException, SignatureVerificationFaultException {
        throw new UnsupportedOperationException("Method does not implemented.");
    }

    private InformationType getInformationType(MessageTypeSelector selector) {
        String namespaceURI = selector.getNamespaceURI();
        String rootElementLocalName = selector.getRootElementLocalName();
        return new InformationType(namespaceURI, rootElementLocalName);
    }
}
