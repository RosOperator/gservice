package ru.codeinside.smev.v3.server_stub.osgi;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

public class Activator implements BundleActivator {

    private static BundleContext context;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        context = bundleContext;
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        context = null;
    }

    public static <T> T getService(Class<T> clazz) {
        if (context != null) {
            ServiceReference serviceReference = context.getServiceReference(clazz.getName());
            if (serviceReference != null) {
                Object service = context.getService(serviceReference);
                if (service != null) {
                    return clazz.cast(service);
                }
                context.ungetService(serviceReference);
            }
        }
        return null;
    }
}
