package ru.codeinside.smev.v3.server_stub.services;

import org.apache.commons.net.ftp.FTPClient;
import ru.codeinside.smev.v3.service.api.FTPCredentials;
import ru.codeinside.smev.v3.transport.api.v1_1.FSAttachmentsList;
import ru.codeinside.smev.v3.transport.api.v1_1.FSAuthInfo;
import ru.codeinside.smev.v3.transport.api.v1_1.RefAttachmentHeaderList;
import ru.codeinside.smev.v3.transport.api.v1_1.RefAttachmentHeaderType;

import javax.enterprise.context.RequestScoped;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;


@SuppressWarnings({"PackageAccessibility", "unused"})
@RequestScoped
public class StubFTPService {

    private static final Logger log = Logger.getLogger(StubFTPService.class.getName());

    public FSAttachmentsList buildFSAttachmentList(RefAttachmentHeaderList refAttachmentHeaderList) {
        if (refAttachmentHeaderList == null || refAttachmentHeaderList.getRefAttachmentHeader() == null) {
            return null;
        }
        FTPCredentials c = getFtpCredentials();
        FSAttachmentsList attachmentsList = new FSAttachmentsList();
        for (RefAttachmentHeaderType headerType : refAttachmentHeaderList.getRefAttachmentHeader()) {
            FSAuthInfo authInfo = new FSAuthInfo();
            authInfo.setUuid(headerType.getUuid());
            authInfo.setUserName(c.getUser());
            authInfo.setPassword(c.getPassword());
            authInfo.setFileName(getFileName(headerType.getUuid(), c));
            attachmentsList.getFSAttachment().add(authInfo);
        }
        return attachmentsList;
    }

    private String getFileName(String folder, FTPCredentials credentials) {
        FTPClient client = null;
        try {
            client = makeClient(credentials);
            String[] names = client.listNames(folder + "/");
            return names[0].replaceAll(folder + "/", "");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            closeClient(client);
        }
    }

    private FTPClient makeClient(FTPCredentials credentials) throws IOException {
        FTPClient client = new FTPClient();
        client.connect(credentials.getServer(), credentials.getPort());
        if (!client.login(credentials.getUser(), credentials.getPassword())) {
            throw new IOException("Не удалось авторизоваться на FTP-сервере. Неверный логин или пароль.");
        }
        return client;
    }

    private void closeClient(FTPClient client) {
        if (client != null) {
            try {
                client.logout();
                client.disconnect();
            } catch (IOException ignored) {
            }
        }
    }

    private FTPCredentials getFtpCredentials() {
        Properties properties = new Properties();
        File userHome = new File(System.getProperty("user.home"));
        File gsesProperties = new File(userHome, "gses-key.properties");

        if (!gsesProperties.exists()) {
            log.warning("Файл настроек не обнаружен.");
        } else {
            FileInputStream propertiesStream = null;
            try {
                propertiesStream = new FileInputStream(gsesProperties);
                properties.load(propertiesStream);
            } catch (IOException e) {
                log.warning("Не удалось прочитать файл настроек. " + e.getMessage());
            } finally {
                if (propertiesStream != null) {
                    try {
                        propertiesStream.close();
                    } catch (IOException ignored) {
                    }
                }
            }
        }

        String server = properties.getProperty("ftp.server", "localhost");
        int port = Integer.parseInt(properties.getProperty("ftp.port", "2105"));
        String user = properties.getProperty("ftp.user", "user");
        String password = properties.getProperty("ftp.password", "123");

        return new FTPCredentials(server, port, user, password);
    }

}
