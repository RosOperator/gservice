package ru.codeinside.smev.v3.service.api;

public class AcknowledgeRequestData {
    public String messageId;
    public String id;
    public boolean accepted;
}
