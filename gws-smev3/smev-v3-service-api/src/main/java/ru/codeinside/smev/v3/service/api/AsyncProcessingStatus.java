package ru.codeinside.smev.v3.service.api;

public class AsyncProcessingStatus {
    /**
     * UUID сообщения
     */
    public String originalMessageId;

    /**
     * Категория уведомления
     */
    public InteractionStatus.InteractionStatusType statusCategory;

    /**
     * Уведомление об ошибке асинхронной обработки сообщения
     */
    public String statusDetails;

    /**
     * Ошибка СМЭВ
     */
    public SmevFault smevFault;
}
