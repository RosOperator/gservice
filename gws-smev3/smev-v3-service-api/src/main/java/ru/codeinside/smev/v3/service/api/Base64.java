package ru.codeinside.smev.v3.service.api;

public final class Base64 {
    private Base64() {

    }

    public static String encode(byte[] data) {
        return org.apache.commons.codec.binary.Base64.encodeBase64String(data);
    }

    public static byte[] decode(String data) {
        return org.apache.commons.codec.binary.Base64.decodeBase64(data);
    }
}
