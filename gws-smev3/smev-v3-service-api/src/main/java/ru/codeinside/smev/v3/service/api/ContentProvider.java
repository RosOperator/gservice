package ru.codeinside.smev.v3.service.api;

import java.io.InputStream;

/**
 * Механизм доступа к содержимому.
 */
public interface ContentProvider {
    /**
     * Содержимое
     *
     * @return входящий поток, содержащий данные
     */
    InputStream getContent();

    /**
     * Получить размер  данных
     *
     * @return размер данных в байтах
     */
    long size();
}
