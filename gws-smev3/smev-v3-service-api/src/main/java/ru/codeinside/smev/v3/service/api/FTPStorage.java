package ru.codeinside.smev.v3.service.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * API OSGi-сервиса для загрузки файлов через FTP
 */
public interface FTPStorage {
    /**
     * Загрузить файл на FTP-сервер
     *
     * @param credentials учетные данные для подключения к FTP-серверу
     * @param folder имя папки
     * @param file имя файла
     * @param content входной поток с содержимым файла
     */
    void upload(FTPCredentials credentials, String folder, String file, InputStream content) throws IOException;

    /**
     * Загруизить файл с FTP-сервера
     *
     * @param credentials учетные данные для подключения к FTP-серверу
     * @param folder имя папки
     * @param file имя файла
     * @param content выходной поток с содержимым файла
     */
    void download(FTPCredentials credentials, String folder, String file, OutputStream content) throws IOException;
}
