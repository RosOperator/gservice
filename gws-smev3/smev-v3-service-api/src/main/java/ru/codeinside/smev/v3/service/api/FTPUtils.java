package ru.codeinside.smev.v3.service.api;

import ru.codeinside.gws.api.Enclosure;

import java.util.List;

@Deprecated
public final class FTPUtils {

    private static final int THRESHOLD = 5 * 1024 * 1024;  // 5 Mb
    private static final int MAX_THRESHOLD = 1024 * 1024 * 1024;  // 1 GB

    private FTPUtils() {
    }

    public static boolean transferByFtp(List<Enclosure> enclosures) {
        if (enclosures == null) {
            return false;
        }

        int total = 0;
        for (Enclosure enclosure : enclosures) {
            total += enclosure.size();
            if (total > MAX_THRESHOLD || total <= -1) {
                throw new IllegalStateException("Нельзя передавать вложения больше чем 1 Гб.");
            }
        }
        return total > THRESHOLD;
    }
}
