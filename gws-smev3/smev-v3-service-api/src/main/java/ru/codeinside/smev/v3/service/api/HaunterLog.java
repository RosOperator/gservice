package ru.codeinside.smev.v3.service.api;

import ru.codeinside.gws.api.LogService;

import java.io.Closeable;
import java.io.OutputStream;

//TODO: [ilya.boikov] переписать комменты на валидные

/**
 * Журнал действий для экземпляра потребителя СМЭВ на уровне СИУ и на уровне транспорта HTTP.
 * За создание отвечает {@link ru.codeinside.gws.api.LogService#(Boolean, Boolean)}
 *
 * @author ilya.boikov
 * @see LogService
 * @since 1.0.0
 */

public interface HaunterLog extends Closeable {

    /**
     * Регистрация исключительной ситуации.
     *
     * @param e сиутация.
     */
    void log(Throwable e);


    /**
     * Получить поток регистрация данных HTTP из исходящего транспортного потока.
     *
     * @return поток для регистрации.
     */
    OutputStream getHttpOutStream();


    /**
     * Получить поток регистрация данных HTTP из входящего транспортного потока.
     *
     * @return поток для регистрации.
     */
    OutputStream getHttpInStream();


    /**
     * Добавление данных для последующего логгирования.
     *
     * @param messageStateString тип сообщения в строковом представлении.
     * @param dataName           тип логгируемых данных сообщения, хранимый в перечислении MsgDataField.
     * @param data               логгируемый объект.
     */
    void addData(String messageStateString, MsgDataField dataName, Object data);


    /**
     * Добавление данных для последующего логгирования.
     *
     * @param messageMetadata метаданные сообщения.
     */
    void addData(MessageMetadata messageMetadata);


    /**
     * Добавление общих данных сообщения для последующего логгирования.
     *
     * @param dataName тип логгируемых данных сообщения, хранимый в перечислении MsgDataField.
     * @param data     логгируемый объект.
     */
    void addOtherData(MsgDataField dataName, Object data);


    /**
     * Добавление данных отправляемого сообщения для последующего логгирования.
     *
     * @param dataName тип логгируемых данных сообщения, хранимый в перечислении MsgDataField.
     * @param data     логгируемый объект.
     */
    void addSendData(MsgDataField dataName, Object data);


    /**
     * Добавление данных получаемого сообщения для последующего логгирования.
     *
     * @param dataName тип логгируемых данных сообщения, хранимый в перечислении MsgDataField.
     * @param data     логгируемый объект.
     */
    void addReceiveData(MsgDataField dataName, Object data);


    /**
     * Запись всех предварительно сохранённых данных в лог.
     */
    Boolean createLog();


    /**
     * Закрыть журнал.
     */
    void close();

}
