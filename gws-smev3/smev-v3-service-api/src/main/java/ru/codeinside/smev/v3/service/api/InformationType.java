package ru.codeinside.smev.v3.service.api;

/**
 * Вид сведений
 */
public final class InformationType {

    /**
     * Target namespace схемы, в которой описан элемент
     * Передаётся в качестве аргумента MessageTypeSelector/NamespaceURI
     * для задания вида сведений в методах СМЭВ getRequest и getResponse
     */
    private final String namespace;

    /**
     * Local name корневого элемента
     * Пердаётся в качестве аргумента MessageTypeSelector/RootElementLocalName
     * для задания вида сведений в методах СМЭВ getRequest и getResponse
     */
    private final String rootLocalElementName;

    public InformationType(String namespace, String rootElementName) {
        this.namespace = namespace;
        this.rootLocalElementName = rootElementName;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getRootLocalElementName() {
        return rootLocalElementName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InformationType)) return false;

        if (hashCode() != o.hashCode()) return false;

        InformationType that = (InformationType) o;

        return getNamespace().equals(that.getNamespace()) &&
                getRootLocalElementName().equals(that.getRootLocalElementName());
    }

    @Override
    public int hashCode() {
        int result = getNamespace().hashCode();
        result = 31 * result + getRootLocalElementName().hashCode();
        return result;
    }
}
