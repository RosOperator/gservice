package ru.codeinside.smev.v3.service.api;

import java.util.Date;

public class InteractionStatus {
    public InteractionStatusType statusType;
    public Date statusChange;

    public enum InteractionStatusType {
        /**
         * Запрос с таким Id не найден в БД СМЭВ.
         */
        DOES_NOT_EXIST("doesNotExist"),

        /**
         * Запрос находится в очереди на асинхронную валидацию.
         */
        REQUEST_IS_QUEUED("requestIsQueued"),

        /**
         * Запрос доставляется поставщику.
         */
        REQUEST_IS_ACCEPTED_BY_SMEV("requestIsAcceptedBySmev"),

        /**
         * Запрос не прошёл асинхронную валидацию.
         */
        REQUEST_IS_REJECTED_BY_SMEV("requestIsRejectedBySmev"),

        /**
         * Обрабатывается поставщиком сервиса.
         */
        UNDER_PROCESSING("underProcessing"),

        /**
         * Запрос выполнен или отвергнут поставщиком сервиса; ответ находится в очереди СМЭВ.
         */
        RESPONSE_IS_ACCEPTED_BY_SMEV("responseIsAcceptedBySmev"),

        /**
         * Запрос отменён потребителем сервиса.
         */
        CANCELLED("cancelled"),

        /**
         * Ответ получен потребителем сервиса.
         */
        RESPONSE_IS_DELIVERED("responseIsDelivered");

        private final String value;

        InteractionStatusType(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static InteractionStatusType fromValue(String v) {
            for (InteractionStatusType c: InteractionStatusType.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }
    }
}
