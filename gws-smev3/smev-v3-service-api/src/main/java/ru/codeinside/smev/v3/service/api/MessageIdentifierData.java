package ru.codeinside.smev.v3.service.api;

import java.util.GregorianCalendar;

public class MessageIdentifierData {
    public String id;
    public String namespaceURI;
    public String rootElementLocalName;
    public GregorianCalendar timestamp;
}
