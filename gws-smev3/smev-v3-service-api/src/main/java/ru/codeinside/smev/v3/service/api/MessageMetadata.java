package ru.codeinside.smev.v3.service.api;

import ru.codeinside.gws.api.InfoSystem;

import java.util.Date;

/**
 * Метаданные ответа
 */
public class MessageMetadata {
    public MessageType messageType;

    public InfoSystem sender;

    public Date sendTime;

    public String destinationName;

    public InfoSystem recipient;

    public String detectedContentTypeName;

    // TODO: enum
    public String interactionType;

    public InteractionStatus.InteractionStatusType interactionStatus;

    public Date deliveryTime;

    public String id;

    public String messageId;

    public enum MessageType {
        REQUEST,
        BROADCAST,
        RESPONSE,
        CANCEL
    }
}
