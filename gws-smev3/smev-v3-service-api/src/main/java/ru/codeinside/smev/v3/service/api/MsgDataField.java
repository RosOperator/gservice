package ru.codeinside.smev.v3.service.api;

public enum MsgDataField {
    date,
    messageType,
    messageState,
    recipient,
    sender,
    typeCode,
    testMessage,
    status,
    originRequestIdRef,
    requestIdRef,
    serviceName,
    SOAPMessage,
    processInstanceId,
    bid,
    client,
    componentName,
    error,
}
