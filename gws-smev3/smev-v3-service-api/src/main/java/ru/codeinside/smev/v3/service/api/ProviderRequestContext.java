package ru.codeinside.smev.v3.service.api;

import ru.codeinside.gws.api.DeclarerContext;
import ru.codeinside.gws.api.ServerException;
import ru.codeinside.smev.v3.service.api.provider.RequestMessage;

import java.util.List;
import java.util.Map;

/**
 * Контекст запроса для поставщика
 */
public interface ProviderRequestContext {

    /**
     * Запрос потребителя, обрабатываемый в текущем контексте.
     *
     * @return текущий запрос потребителя.
     */
    RequestMessage getRequestMessage();

    /**
     * Идентификатор заявления, связанного с цепочкой.
     *
     * @return null если цепочка не связана с заявлением, иначе идентификатор заявления.
     */
    String getBid();

    /**
     * Идентификаторы заявлений, связанные с цепочкой.
     *
     * @return список идентификаторов заявлений, для которых запущен процесс исполнения.
     * @throws ServerException при ошибках с хранилищем заявок.
     */
    List<String> getBids();

    /**
     * Создание контекста подачи заявления.
     *
     * @param procedureCode код процедуры, которую необходимо исполнить.
     * @return контекст подачи заявления.
     * @throws ServerException при ошибках с хранилищем заявок, или если процедура не найдена.
     */
    DeclarerContext getDeclarerContext(long procedureCode);


    /**
     * Обновить контекст существующей заявки
     *
     * @since 1.0.12
     */
    void updateReceiptContext(Map<String, Object> values);

}
