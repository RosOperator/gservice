package ru.codeinside.smev.v3.service.api;

public enum RejectionCode {
    ACCESS_DENIED,
    NO_DATA,
    UNKNOWN_REQUEST_DESCRIPTION,
    FAILURE;

    public String value() {
        return name();
    }

    public static RejectionCode fromValue(String v) {
        return valueOf(v);
    }
}