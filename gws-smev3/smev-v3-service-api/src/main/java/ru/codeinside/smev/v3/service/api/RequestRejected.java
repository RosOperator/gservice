package ru.codeinside.smev.v3.service.api;

public class RequestRejected {
    public RejectionCode rejectionReasonCode;
    public String rejectionReasonDescription;
}
