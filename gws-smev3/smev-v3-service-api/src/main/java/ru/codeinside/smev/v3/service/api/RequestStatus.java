package ru.codeinside.smev.v3.service.api;

import java.util.LinkedList;
import java.util.List;

public class RequestStatus {

    private int statusCode;
    private List<StatusParameter> requestStatuses = new LinkedList<StatusParameter>();
    private String statusDescription;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public List<StatusParameter> getRequestStatuses() {
        return requestStatuses;
    }

    public void setRequestStatuses(List<StatusParameter> requestStatuses) {
        this.requestStatuses = requestStatuses;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }
}
