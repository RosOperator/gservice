package ru.codeinside.smev.v3.service.api;

import ru.codeinside.smev.v3.crypto.api.XmlSignature;

public class SignedData<T> {

    /**
     * Блок данных.
     */
    public T data;

    /**
     * Нормализованные данные для подписи в пространстве "http://www.w3.org/2000/09/xmldsig#"
     */
    public byte[] signData;

    /**
     * XML подпись блока данных
     * TODO: убрать прямую зависимость от crypto-api?
     */
    public XmlSignature signature;

}
