package ru.codeinside.smev.v3.service.api;


import java.util.GregorianCalendar;

public interface Smev {
    /**
     * Запрос для получения результата обработки данных, переданных в СМЭВ
     */
    MessageIdentifier getResponse(InformationType informationType);

    /**
     * Запрос на получение данных из СМЭВ
     */
    MessageIdentifier getRequest(InformationType informationType);

    /**
     * Запрос подтверждения обработанных данных
     */
    AcknowledgeRequest ackRequest(String messageId, boolean accepted);

    /**
     * Формирование Timestamp по дате
     */
    TimestampRequest getTimestamp(GregorianCalendar date);

    /**
     * Запрос статуса
     */
    MessageReference getInteractionStatus(String messageId);
}
