package ru.codeinside.smev.v3.service.api;

import ru.codeinside.gws.api.LogService;

public interface Smev3LogService extends LogService {

    HaunterLog createHaunterLog(Boolean isLogEnabled, Boolean isErrorLogEnabled);

}
