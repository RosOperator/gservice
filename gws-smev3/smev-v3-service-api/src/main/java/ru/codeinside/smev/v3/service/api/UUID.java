package ru.codeinside.smev.v3.service.api;

import com.fasterxml.uuid.EthernetAddress;
import com.fasterxml.uuid.UUIDTimer;
import com.fasterxml.uuid.impl.TimeBasedGenerator;

import java.io.IOException;
import java.util.Random;

public final class UUID {
    private static TimeBasedGenerator uuidGenerator;

    static {
        Random random = new Random(System.currentTimeMillis());
        UUIDTimer timer = null;
        try {
            timer = new UUIDTimer(random, null);
        } catch (IOException e) {
            // Will never be thrown.
            throw new RuntimeException(e);
        }
        EthernetAddress addr = EthernetAddress.fromInterface();

        uuidGenerator = new TimeBasedGenerator(addr, timer);
    }

    private UUID() {

    }

    public static String generate() {
        return uuidGenerator.generate().toString();
    }
}
