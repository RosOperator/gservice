package ru.codeinside.smev.v3.service.api.consumer;

import ru.codeinside.gws.api.ExchangeContext;
import ru.codeinside.gws.api.Revision;
import ru.codeinside.smev.v3.service.api.InformationType;

/**
 * OSGi сервис потребителя.
 */
public interface Consumer {

    /**
     * Получить номер ревизии рекомендаций СМЭВ
     */
    Revision getRevision();

    /**
     * Формирование ConsumerRequest по данным из бизнес-процесса
     *
     * @return объект пользовательского запроса
     * @throws IllegalStateException
     */
    ConsumerRequest createRequest(ExchangeContext businessProcess) throws IllegalStateException;

    /**
     * Обработать результат ответа из СМЭВ
     *
     * @return Идентификатор сообщений которое было обработано, иначе {@code null}
     */
    String processResponse(Response response, ExchangeContext businessProcess);

    /**
     * Получить вид сведений
     *
     * @return объект вида сведений
     */
    InformationType getInformationType();
}
