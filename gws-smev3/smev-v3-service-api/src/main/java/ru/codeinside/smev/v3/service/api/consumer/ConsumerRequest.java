package ru.codeinside.smev.v3.service.api.consumer;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.codeinside.gws.api.Enclosure;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("PackageAccessibility")
public class ConsumerRequest {

    /**
     * Данные для передачи в СМЭВ
     */
    private Element personalData;

    /**
     * Код ФРГУ ИС-отправителя
     */
    private String frguCode;

    /**
     * Метаданные процесса
     */
    private List<Element> metadata;

    /**
     * Вложения
     */
    private List<Enclosure> enclosures = new LinkedList<Enclosure>();

    /**
     * Признак тестового сообщения
     */
    private boolean test;

    /**
     * Признак наличия подписи СП
     */
    private boolean signatureRequired = false;

    public void setPersonalData(Object jaxbObject) throws JAXBException, ParserConfigurationException {
        DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = docBuilder.newDocument();
        JAXBContext context = JAXBContext.newInstance(jaxbObject.getClass());
        Marshaller marshaller = context.createMarshaller();
        marshaller.marshal(jaxbObject, doc);
        this.personalData = doc.getDocumentElement();
    }

    public void setPersonalData(Element personalData) {
        this.personalData = personalData;
    }

    public Element getPersonalData() {
        return personalData;
    }

    public void setMetadata(List<Element> metadata) {
        this.metadata = metadata;
    }

    public List<Element> getMetadata() {
        return metadata;
    }

    public void addEnclosures(List<Enclosure> enclosures) {
        this.enclosures.addAll(enclosures);
    }

    public void addAttachment(Enclosure enclosure) {
        enclosures.add(enclosure);
    }

    public List<Enclosure> getEnclosures() {
        return Collections.unmodifiableList(enclosures);
    }

    public boolean isTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }

    public boolean isSignatureRequired() {
        return signatureRequired;
    }

    public void setSignatureRequired(boolean signatureRequired) {
        this.signatureRequired = signatureRequired;
    }

    public String getFrguCode() {
        return frguCode;
    }

    public void setFrguCode(String frguCode) {
        this.frguCode = frguCode;
    }
}
