package ru.codeinside.smev.v3.service.api.consumer;

/**
 * Поэтапное построение запроса в разные моменты времени.
 * Реализация связана с типами данных транспортного слоя.
 */
public interface ConsumerRequestBuilder {
    /**
     * Шаг 1 - запрос служащего и вложения.
     * Затем Шаг 2а - подписание запроса/вложений ЭП-СП в другом контексте
     * Затем Шаг 2b - подписание вложений ЭП-ОП в другом контексте
     */
    OrganizationRequestData createPersonalRequest(ConsumerRequest requestData);

    /**
     * Шаг 3 - запрос ОВ.
     * Затем Шаг 4 - подписание вложений/запроса ЭП-ОВ в другом контексте.
     */
    OrganizationRequest createOrganizationRequest(OrganizationRequestData requestData, boolean test);
}
