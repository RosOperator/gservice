package ru.codeinside.smev.v3.service.api.consumer;

import org.w3c.dom.Element;
import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.smev.v3.service.api.PersonalData;

import java.util.List;

/**
 * Данные запроса ОВ
 */
public class OrganizationRequestData {
    /**
     * Запрос от имени человека.
     */
    public PersonalData personalData;

    /**
     * Код ФРГУ ИС-отправителя
     */
    public String frguCode;

    /**
     * Уникальнй идентификатор сообщения.
     */
    public String messageID;

    /**
     * Вложения.
     */
    public List<Enclosure> enclosures;

    /**
     * Метаданные.
     */
    public List<Element> metadata;

    /**
     * Режим тестирования.
     */
    public boolean testMode;

    /**
     * Идентификатор сообщения ОВ.
     */
    public String id;

    public String messageReferenceID;
}
