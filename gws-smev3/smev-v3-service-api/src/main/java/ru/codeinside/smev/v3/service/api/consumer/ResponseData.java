package ru.codeinside.smev.v3.service.api.consumer;

import ru.codeinside.smev.v3.service.api.MessageMetadata;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponse;

public class ResponseData {
    public String id;
    public String referenceMessageID;
    public String originalMessageId;
    public ProviderResponse providerResponse;
    public MessageMetadata messageMetadata;
}
