package ru.codeinside.smev.v3.service.api.provider;

import ru.codeinside.smev.v3.service.api.MessageMetadata;
import ru.codeinside.smev.v3.service.api.MessageReference;

public class Cancel {
    public MessageReference messageReference;
    public String messageID;
    public MessageMetadata messageMetadata;
    public String id;
}
