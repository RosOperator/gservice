package ru.codeinside.smev.v3.service.api.provider;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.smev.v3.service.api.AsyncProcessingStatus;
import ru.codeinside.smev.v3.service.api.RequestRejected;
import ru.codeinside.smev.v3.service.api.RequestStatus;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("PackageAccessibility")
public class ProvidedResponse {
    /**
     * Ответные данные для передачи в СМЭВ
     */
    private Element responseData;

    /**
     * Идентификатор сообщения на который посылается ответ
     */
    private String replyTo;

    /**
     * Отказы
     */
    private List<RequestRejected> requestsRejected = new LinkedList<RequestRejected>();

    /**
     * Бизнес статус запроса
     */
    private RequestStatus requestStatus;

    /**
     * Статус асинхронной проверки запроса
     */
    private AsyncProcessingStatus asyncProcessingStatus;

    /**
     * Вложения
     */
    private List<Enclosure> enclosures = new LinkedList<Enclosure>();

    /**
     * Признак наличия подписи СП
     */
    private boolean signatureRequired = false;

    public void setResponseData(Object jaxbObject) throws JAXBException, ParserConfigurationException {
        DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = docBuilder.newDocument();
        JAXBContext context = JAXBContext.newInstance(jaxbObject.getClass());
        Marshaller marshaller = context.createMarshaller();
        marshaller.marshal(jaxbObject, doc);
        this.responseData = doc.getDocumentElement();
    }

    public void setResponseData(Element responseData) {
        this.responseData = responseData;
    }

    public Element getResponseData() {
        return responseData;
    }

    public void addEnclosure(Enclosure enclosure) {
        this.enclosures.add(enclosure);
    }

    public void addEnclosures(List<Enclosure> enclosures) {
        this.enclosures.addAll(enclosures);
    }

    public List<Enclosure> getEnclosures() {
        return Collections.unmodifiableList(enclosures);
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public List<RequestRejected> getRequestsRejected() {
        return Collections.unmodifiableList(requestsRejected);
    }

    public void addRequestsRejected(List<RequestRejected> requestsRejected) {
        this.requestsRejected.addAll(requestsRejected);
    }

    public void addRequestRejeted(RequestRejected requestRejected) {
        this.requestsRejected.add(requestRejected);
    }

    public boolean isSignatureRequired() {
        return signatureRequired;
    }

    public void setSignatureRequired(boolean signatureRequired) {
        this.signatureRequired = signatureRequired;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public AsyncProcessingStatus getAsyncProcessingStatus() {
        return asyncProcessingStatus;
    }
}
