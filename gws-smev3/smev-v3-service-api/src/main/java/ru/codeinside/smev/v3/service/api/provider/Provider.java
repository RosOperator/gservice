package ru.codeinside.smev.v3.service.api.provider;

import ru.codeinside.gws.api.ReceiptContext;
import ru.codeinside.gws.api.Revision;
import ru.codeinside.smev.v3.service.api.InformationType;
import ru.codeinside.smev.v3.service.api.ProviderRequestContext;

/**
 * OSGi-сервис поставщика
 */
public interface Provider {
    /**
     * Получить номер ревизии рекомендаций СМЭВ
     */
    Revision getRevision();

    /**
     * Формирование ответа на полученный запрос от потребителя
     */
    ProvidedResponse createResponse(ReceiptContext receiptContext);

    /**
     * Обработка запроса, полученного из СМЭВ3 от потребителя
     *
     * @return в случае успеха идентификатор обработанного сообщения, иначе  {@code null}
     */
    String processRequest(ProviderRequestContext context) throws ProviderRequestException;

    /**
     * Получить вид сведений
     */
    InformationType getInformationType();
}
