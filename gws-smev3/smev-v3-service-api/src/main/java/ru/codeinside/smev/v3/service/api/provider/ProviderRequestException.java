package ru.codeinside.smev.v3.service.api.provider;

import ru.codeinside.smev.v3.service.api.RejectionCode;
import ru.codeinside.smev.v3.service.api.RequestRejected;

public final class ProviderRequestException extends Exception {

    final private RejectionCode rejectionReasonCode;

    final private String rejectionReasonDescription;

    public ProviderRequestException(RejectionCode rejectionCode, String rejectionReasonDescription) {
        this.rejectionReasonCode = rejectionCode;
        this.rejectionReasonDescription = rejectionReasonDescription;
    }

    public RequestRejected getRequestRejected() {
        RequestRejected rejected = new RequestRejected();
        rejected.rejectionReasonCode = this.rejectionReasonCode;
        rejected.rejectionReasonDescription = this.rejectionReasonDescription;
        return rejected;
    }
}
