package ru.codeinside.smev.v3.service.api.provider;

public interface ProviderResponseBuilder {
    ProviderResponseData createPersonalResponse(ProvidedResponse providedResponse);

    ProviderResponse createProviderResponse(ProviderResponseData responseData);
}
