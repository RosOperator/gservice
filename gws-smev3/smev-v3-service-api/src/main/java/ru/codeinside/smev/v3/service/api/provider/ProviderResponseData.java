package ru.codeinside.smev.v3.service.api.provider;

import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.smev.v3.service.api.AsyncProcessingStatus;
import ru.codeinside.smev.v3.service.api.PersonalData;
import ru.codeinside.smev.v3.service.api.RequestRejected;
import ru.codeinside.smev.v3.service.api.RequestStatus;

import java.util.List;

public class ProviderResponseData {
    /**
     * Идентификатор сообщения
     */
    public String messageID;

    /**
     * Содержимое из //GetRequestResponse/RequestMessage/Request/ReplyTo
     */
    public String to;

    /**
     * Данные для ответа
     */
    public PersonalData personalData;

    /**
     * Вложения
     */
    public List<Enclosure> enclosures;

    /**
     * Причины отказов
     */
    public List<RequestRejected> requestRejected;

    /**
     * Бизнес статус запроса
     */
    public RequestStatus requestStatus;

    /**
     * Статус асинхронной проверки запроса
     */
    public AsyncProcessingStatus asyncProcessingStatus;

    /**
     * Значение атрибута ID из SenderProvidedResponseData
     */
    public String id;
}
