package ru.codeinside.smev.v3.service.api.provider;

import ru.codeinside.smev.v3.service.api.MessageMetadata;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequest;

public class RequestData {
    public OrganizationRequest organizationRequest;
    public MessageMetadata messageMetadata;
    public String replyTo;
    public String id;
}
