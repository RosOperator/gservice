package ru.codeinside.smev.v3.service.api;

import org.junit.Assert;
import org.junit.Test;

public class InformationTypeTest extends Assert {

    @Test
    public void testEquals() throws Exception {
        InformationType type1 = new InformationType(new String("http://skmv.rstyle.com/snils-by-data/1.0.0"), new String("snilsByDataRequest"));
        InformationType type2 = new InformationType(new String("http://skmv.rstyle.com/snils-by-data/1.0.0"), new String("snilsByDataRequest"));
        assertEquals(type1, type2);
    }
}