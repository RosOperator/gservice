package ru.codeinside.smev.v3.service.impl.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;
import ru.codeinside.gws.api.ExchangeContext;
import ru.codeinside.smev.v3.crypto.api.GostR3411;
import ru.codeinside.smev.v3.service.api.consumer.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class ConsumerImplIT extends OSGiContainer {

    @Ignore
    @Test
    public void test_createRequest() throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException, UnrecoverableKeyException, SignatureException, InvalidKeyException {
        KeyStore store = KeyStore.getInstance("JKS");
        store.load(getClass().getClassLoader().getResourceAsStream("test_store.keystore"),
                "test_pass".toCharArray());

        X509Certificate certificate = (X509Certificate) store.getCertificate("test_cert");
        PrivateKey privateKey = (PrivateKey) store.getKey("test_cert", "test_pass".toCharArray());

        ExchangeContext context = new DummyBusinessProcess();
        context.setVariable("test", "test");
        Consumer consumer = this.getServiceImpl(Consumer.class);
        ConsumerRequest consumerRequest = consumer.createRequest(context);
        Assert.assertNotNull(consumerRequest.getPersonalData());

        ConsumerRequestBuilder requestBuilder = this.getServiceImpl(ConsumerRequestBuilder.class);
        OrganizationRequestData organizationRequestData = requestBuilder.createPersonalRequest(consumerRequest);

        GostR3411 gostR3411 = this.getServiceImpl(GostR3411.class);
        byte[] personalData = organizationRequestData.personalData.signData;
        byte[] digest = gostR3411.digest(new ByteArrayInputStream(personalData));

        Signature signature = Signature.getInstance("ECGOST3410");
        signature.initSign(privateKey);
        signature.update(personalData);

        byte[] signBytes = signature.sign();

        ru.codeinside.gws.api.Signature sign = new ru.codeinside.gws.api.Signature(
                certificate, new ByteArrayInputStream(personalData), signBytes, digest, true);
        organizationRequestData.personalData.signature.setSignature(sign);
        organizationRequestData.personalData.signature.setElementReference("REF1");

        OrganizationRequest organizationRequest = requestBuilder.createOrganizationRequest(organizationRequestData, true);
        Assert.assertNotNull(organizationRequest.signData);
    }
}
