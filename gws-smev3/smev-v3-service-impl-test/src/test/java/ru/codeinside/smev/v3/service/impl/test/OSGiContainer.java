package ru.codeinside.smev.v3.service.impl.test;

import org.junit.Assert;
import org.junit.Before;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import ru.codeinside.gws.api.XmlNormalizer;
import ru.codeinside.gws.xml.normalizer.XmlNormalizerImpl;

import javax.inject.Inject;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.ops4j.pax.exam.CoreOptions.*;

public abstract class OSGiContainer {
    @Inject
    BundleContext context;

    @Before
    public void setUp() throws Exception {
        context.registerService(XmlNormalizer.class.getName(), new XmlNormalizerImpl(), null);
    }

    @Configuration
    public Option[] config() {
        return options(
                mavenSibling("gws-api", "1.0.13"),
                mavenSibling("gws-xml-normalizer", "1.0.1"),
                mavenSibling("smev-v3-crypto-api", "1.0.2"),
                mavenSibling("smev-v3-service-api", "1.0.3"),
                mavenSibling("smev-v3-transport-api", "1.0.2"),
                mavenSibling("smev-v3-crypto-bc", "1.0.0"),
                mavenSibling("smev-v3-crypto-xml-dsign", "1.0.2"),
                mavenSibling("smev-v3-crypto-sun-pkcs7", "1.0.2"),
                mavenSibling("smev-v3-xml-normalizer", "1.0.1"),
                mavenSibling("smev-v3-service-impl", "1.0.4"),
                junitBundles()
        );
    }

    protected <S> S getServiceImpl(Class<S> serviceClass) {
        ServiceReference<S> serviceReference = context.getServiceReference(serviceClass);
        S service = context.getService(serviceReference);
        Assert.assertNotNull(service);
        return service;
    }

    private Option mavenSibling(String name, String version) {
        Path parentPom = Paths.get("").toAbsolutePath();
        if (parentPom.getFileName().toString().equals("smev-v3-service-impl-test")) {
            parentPom = parentPom.resolve("..");
        }
        if (name.startsWith("gws")) {
            parentPom = parentPom.resolve("..");
        }
        return bundle("file:" + parentPom.resolve(Paths.get(name, "target", name + "-" + version + ".jar")));
    }

}
