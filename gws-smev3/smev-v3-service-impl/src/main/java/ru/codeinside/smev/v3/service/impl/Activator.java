package ru.codeinside.smev.v3.service.impl;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import ru.codeinside.smev.v3.service.api.FTPStorage;
import ru.codeinside.smev.v3.service.api.Smev;
import ru.codeinside.smev.v3.service.api.consumer.ConsumerRequestBuilder;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseBuilder;
import ru.codeinside.smev.v3.service.impl.consumer.ConsumerRequestBuilderImpl;
import ru.codeinside.smev.v3.service.impl.provider.ProviderResponseBuilderImpl;

public class Activator implements BundleActivator {

    private static BundleContext CONTEXT;

    private ServiceRegistration consumerRequestBuilderService;

    private ServiceRegistration smevService;

    private ServiceRegistration providerResponseBuilderService;

    private ServiceRegistration smevStorageService;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        CONTEXT = bundleContext;

        consumerRequestBuilderService = bundleContext.registerService(
                ConsumerRequestBuilder.class.getName(), new ConsumerRequestBuilderImpl(), null);

        smevService = bundleContext.registerService(Smev.class.getName(), new SmevImpl(), null);

        providerResponseBuilderService = bundleContext.registerService(
                ProviderResponseBuilder.class.getName(), new ProviderResponseBuilderImpl(), null);

        smevStorageService = bundleContext.registerService(
                FTPStorage.class.getName(), new SmevStorage(), null);
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        CONTEXT = null;

        unregisterService(consumerRequestBuilderService);
        consumerRequestBuilderService = null;

        unregisterService(smevService);
        smevService = null;

        unregisterService(providerResponseBuilderService);
        providerResponseBuilderService = null;

        unregisterService(smevStorageService);
        smevStorageService = null;
    }

    private void unregisterService(ServiceRegistration serviceRegistration) {
        if (serviceRegistration != null) {
            serviceRegistration.unregister();
        }
    }

    static BundleContext getContext() {
        return CONTEXT;
    }
}
