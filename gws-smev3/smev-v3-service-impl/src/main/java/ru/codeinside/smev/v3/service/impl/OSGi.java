package ru.codeinside.smev.v3.service.impl;

import org.osgi.framework.ServiceReference;

public final class OSGi {
    private OSGi() {
    }

    public static <T> T service(Class<T> clazz) {
        ServiceReference serviceReference = Activator.getContext().getServiceReference(clazz.getName());
        if (serviceReference == null) {
            throw new RuntimeException("Отсутствует ссылка на сервис " + clazz.getSimpleName());
        }
        try {
            return clazz.cast(Activator.getContext().getService(serviceReference));
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            Activator.getContext().ungetService(serviceReference);
        }
    }
}
