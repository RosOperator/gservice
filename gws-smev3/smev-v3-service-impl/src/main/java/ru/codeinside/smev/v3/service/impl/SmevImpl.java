package ru.codeinside.smev.v3.service.impl;

import ru.codeinside.smev.v3.service.api.*;
import ru.codeinside.smev.v3.transport.api.v1_1.AckTargetMessage;
import ru.codeinside.smev.v3.transport.api.v1_1.MessageTypeSelector;
import ru.codeinside.smev.v3.transport.api.v1_1.Timestamp;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

@SuppressWarnings("PackageAccessibility")
public class SmevImpl implements Smev {
    private Logger log = Logger.getLogger(SmevImpl.class.getName());

    /**
     * Запрос для получения результата обработки данных, переданных в СМЭВ
     */
    @Override
    public MessageIdentifier getResponse(InformationType informationType) {
        return buildMessageIdentifier(informationType);
    }

    /**
     * Запрос на получение данных из СМЭВ
     */
    @Override
    public MessageIdentifier getRequest(InformationType informationType) {
        return buildMessageIdentifier(informationType);
    }

    /**
     * Запрос подтверждения обработанных данных
     */
    @Override
    public AcknowledgeRequest ackRequest(String messageId, boolean accepted) {
        AcknowledgeRequestData ackData = new AcknowledgeRequestData();
        ackData.accepted = accepted;
        ackData.messageId = messageId;
        ackData.id = "AckTargetMessageId";

        AckTargetMessage message = new AckTargetMessage();
        message.setAccepted(ackData.accepted);
        message.setId(ackData.id);
        message.setValue(ackData.messageId);

        AcknowledgeRequest ack = new AcknowledgeRequest();
        ack.data = ackData;
        Utils.prepareToSign(message, ack, ackData.id);
        return ack;
    }

    /**
     * Формирование Timestamp по дате
     */
    @Override
    public TimestampRequest getTimestamp(GregorianCalendar date) {
        TimestampData data = new TimestampData();
        data.id = "TimestampId";
        data.timestamp = date;

        Timestamp timestamp = new Timestamp();
        timestamp.setId(data.id);
        timestamp.setValue(getXmlGregorianCalendar(data.timestamp));

        TimestampRequest timestampRequest = new TimestampRequest();
        timestampRequest.data = data;
        Utils.prepareToSign(timestamp, timestampRequest, data.id);
        return timestampRequest;
    }

    /**
     * Запрос статуса
     */
    @Override
    public MessageReference getInteractionStatus(String messageId) {
        MessageReferenceData data = new MessageReferenceData();
        data.id = "MessageReferenceId";
        data.messageId = messageId;

        MessageReference request = new MessageReference();
        request.data = data;
        Utils.prepareToSign(getMessageReference(data.id, data.messageId), request, data.id);
        return request;
    }

    private byte[] getMessageReference(String id, String messageId) {
        ru.codeinside.smev.v3.transport.api.v1_1.MessageReference reference = new ru.codeinside.smev.v3.transport.api.v1_1.MessageReference();
        reference.setId(id);
        reference.setValue(messageId);
        return Utils.normalizeData(reference);
    }

    private MessageTypeSelector  getMessageTypeSelector(MessageIdentifierData responseData) {
        MessageTypeSelector selector = new MessageTypeSelector();
        selector.setId(responseData.id);
        selector.setNamespaceURI(responseData.namespaceURI);
        selector.setRootElementLocalName(responseData.rootElementLocalName);
        selector.setTimestamp(getXmlGregorianCalendar(responseData.timestamp));
        return selector;
    }

    private XMLGregorianCalendar getXmlGregorianCalendar(GregorianCalendar timestamp) {
        XMLGregorianCalendar gregorianCalendar = null;
        try {
            gregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(timestamp);
        } catch (DatatypeConfigurationException e) {
            log.severe("Can't get DatatypeFactory instance: " + e.getMessage());
        }
        return gregorianCalendar;
    }

    private MessageIdentifier buildMessageIdentifier(InformationType informationType) {
        MessageIdentifierData messageData = new MessageIdentifierData();
        messageData.id = "MessageIdentifierId";
        messageData.namespaceURI = informationType.getNamespace();
        messageData.rootElementLocalName = informationType.getRootLocalElementName();
        messageData.timestamp = new GregorianCalendar();

        MessageIdentifier messageIdentifier = new MessageIdentifier();
        messageIdentifier.data = messageData;
        Utils.prepareToSign(getMessageTypeSelector(messageData), messageIdentifier, messageData.id);
        return messageIdentifier;
    }
}
