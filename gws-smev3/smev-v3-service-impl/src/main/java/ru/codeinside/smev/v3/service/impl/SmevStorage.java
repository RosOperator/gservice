package ru.codeinside.smev.v3.service.impl;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPCmd;
import org.apache.commons.net.ftp.FTPReply;
import ru.codeinside.smev.v3.service.api.FTPCredentials;
import ru.codeinside.smev.v3.service.api.FTPStorage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FTP-хранилище СМЭВ3
 */
class SmevStorage implements FTPStorage {
    private Logger logger = Logger.getLogger(SmevStorage.class.getName());

    @Override
    public void upload(FTPCredentials credentials, String folder, String file, InputStream content) throws IOException {
        FTPClient client = null;
        OutputStream out = null;
        boolean hasError = false;
        try {
            client = makeClient(credentials);
            createFolder(client, folder);
            client.setFileType(FTPClient.BINARY_FILE_TYPE);
            out = client.storeFileStream(folder + "/" + file);
            int total = copy(content, out);
            logger.info("Передан файл " + folder + "/" + file + ". Количество байт = " + total);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Ошибка передачи вложения на FTP: " + e.getMessage(), e);
            hasError = true;
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ignored) {
                }
            }
        }
        if (!hasError) {
            boolean pendingCommand = client.completePendingCommand();
            closeClient(client);
            if (!pendingCommand) {
                throw new RuntimeException("Не удалось загрузить на FTP вложение " + folder + "/" + file);
            }
            return;
        }
        closeClient(client);
    }

    @Override
    public void download(FTPCredentials credentials, String folder, String file, OutputStream content) throws IOException {
        FTPClient client = null;
        InputStream in = null;
        boolean hasError = false;
        try {
            client = makeClient(credentials);
            client.setFileType(FTPClient.BINARY_FILE_TYPE);
            in = client.retrieveFileStream(folder + "/" + file);
            int total = copy(in, content);
            logger.info("Передан файл " + folder + "/" + file + ". Количество байт = " + total);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Ошибка передачи вложения с FTP: " + e.getMessage(), e);
            hasError = true;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ignored) {
                }
            }
        }
        if (!hasError) {
            boolean pendingCommand = client.completePendingCommand();
            closeClient(client);
            if (!pendingCommand) {
                throw new RuntimeException("Не удалось загрузить с FTP вложение " + folder + "/" + file);
            }
            return;
        }
        closeClient(client);
    }

    private int copy(InputStream in, OutputStream out) throws IOException {
        int count, total = 0;
        int BUFFER_SIZE = 50 * 1024;
        byte[] buffer = new byte[BUFFER_SIZE];
        while ((count = in.read(buffer)) != -1) {
            total += count;
            out.write(buffer, 0, count);
        }
        out.flush();
        return total;
    }

    private FTPClient makeClient(FTPCredentials credentials) throws IOException {
        FTPClient client = new FTPClient();
        client.connect(credentials.getServer(), credentials.getPort());
        if (!client.login(credentials.getUser(), credentials.getPassword())) {
            throw new IOException("Не удалось авторизоваться на FTP-сервере. Неверный логин или пароль.");
        }
        return client;
    }

    private void createFolder(FTPClient client, String folder) throws IOException {
        int result = client.sendCommand(FTPCmd.MAKE_DIRECTORY, folder);
        if (FTPReply.isNegativeTransient(result) || FTPReply.isNegativePermanent(result)) {
            throw new IOException("Не удалось создать директорию. Код ошибки: " + result);
        }
    }

    private void closeClient(FTPClient client) {
        if (client != null) {
            try {
                client.logout();
                client.disconnect();
            } catch (IOException ignored) {
            }
        }
    }
}
