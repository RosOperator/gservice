package ru.codeinside.smev.v3.service.impl;

import org.w3c.dom.Element;
import ru.codeinside.gws.api.Signature;
import ru.codeinside.smev.v3.crypto.api.GostR3411;
import ru.codeinside.smev.v3.crypto.api.XmlNormalizer;
import ru.codeinside.smev.v3.crypto.api.XmlSignature;
import ru.codeinside.smev.v3.crypto.api.XmlSignatureAssembler;
import ru.codeinside.smev.v3.service.api.SignedData;
import ru.codeinside.smev.v3.transport.api.v1_1.XMLDSigSignatureType;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.logging.Logger;

@SuppressWarnings("PackageAccessibility")
public final class Utils {
    private static Logger log = Logger.getLogger(Utils.class.getName());

    private Utils() {
    }

    public static void prepareToSign(SignedData<? extends Element> signedData) {
        byte[] normalizedBytes = Utils.normalizeData(signedData.data);
        byte[] digest = Utils.digest(normalizedBytes);

        Signature signature = new Signature(null, (byte[]) null, null, digest, false);
        XmlSignature xmlSignature = new XmlSignature(signature);
        xmlSignature.setElementReference(getIdAttributeValue(signedData.data));

        signedData.signature = xmlSignature;
        signedData.signData = normalizeSignedInfo(xmlSignature);
    }

    public static void prepareToSign(Object jaxb, SignedData<?> signedData, String id) {
        byte[] normalizedBytes = Utils.normalizeData(jaxb);
        byte[] digest = Utils.digest(normalizedBytes);
        Signature signature = new Signature(null, (byte[]) null, null, digest, false);

        XmlSignature xmlSignature = new XmlSignature(signature);
        xmlSignature.setElementReference("#" + id);

        signedData.signature = xmlSignature;
        signedData.signData = normalizeSignedInfo(xmlSignature);
    }

    static byte[] normalizeData(Object data) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            XmlNormalizer normalizer = OSGi.service(XmlNormalizer.class);
            if (data instanceof Element) {
                normalizer.normalizeDom((Element) data, os);
            } else {
                normalizer.normalizeJaxb(data, os);
            }
            return os.toByteArray();
        } catch (Exception e) {
            log.severe("Could not normalized data: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private static XMLDSigSignatureType getXMLDSigSignatureType(XmlSignature xmlSignature) {
        if (xmlSignature == null) {
            return null;
        }
        XmlSignatureAssembler xmlSignatureAssembler = OSGi.service(XmlSignatureAssembler.class);
        XMLDSigSignatureType signature = new XMLDSigSignatureType();
        signature.setAny(xmlSignatureAssembler.assemble(xmlSignature));
        return signature;
    }

    private static byte[] digest(byte[] data) {
        GostR3411 gostR3411 = OSGi.service(GostR3411.class);
        return gostR3411.digest(new ByteArrayInputStream(data));
    }

    private static byte[] normalizeSignedInfo(XmlSignature xmlSignature) {
        XMLDSigSignatureType xmldSigSignatureType = getXMLDSigSignatureType(xmlSignature);

        Element signedInfo;
        try {
            XPathExpression signedInfoExpression =
                    XPathFactory.newInstance().newXPath().compile("//*[local-name()='SignedInfo']");
            signedInfo = (Element) signedInfoExpression.evaluate(xmldSigSignatureType.getAny(), XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ru.codeinside.gws.api.XmlNormalizer normalizer = OSGi.service(ru.codeinside.gws.api.XmlNormalizer.class);
        normalizer.normalize(signedInfo, os);
        return os.toByteArray();
    }

    public static void ensureIdAttr(Element element, String id) {
        String existId = element.getAttribute("Id");
        if (existId == null || existId.isEmpty()) {
            element.setAttribute("Id", id);
        }
    }

    private static String getIdAttributeValue(Element element) {
        return "#" + element.getAttribute("Id");
    }
}
