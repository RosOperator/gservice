package ru.codeinside.smev.v3.service.impl.consumer;

import ru.codeinside.smev.v3.service.api.PersonalData;
import ru.codeinside.smev.v3.service.api.UUID;
import ru.codeinside.smev.v3.service.api.consumer.ConsumerRequest;
import ru.codeinside.smev.v3.service.api.consumer.ConsumerRequestBuilder;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequest;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequestData;
import ru.codeinside.smev.v3.service.impl.OSGi;
import ru.codeinside.smev.v3.service.impl.Utils;
import ru.codeinside.smev.v3.transport.api.SmevTypesProvider;

@SuppressWarnings("PackageAccessibility")
public class ConsumerRequestBuilderImpl implements ConsumerRequestBuilder {
    /**
     * Шаг 1 - запрос служащего и вложения.
     * Затем Шаг 2а - подписание запроса/вложений ЭП-СП в другом контексте
     * Затем Шаг 2b - подписание вложений ЭП-ОП в другом контексте
     */
    @Override
    public OrganizationRequestData createPersonalRequest(ConsumerRequest requestData) {
        PersonalData personalData = new PersonalData();
        personalData.data = requestData.getPersonalData();
        if (requestData.isSignatureRequired()) {
            Utils.ensureIdAttr(personalData.data, "PersonalSignatureId");
            Utils.prepareToSign(personalData);
        }
        OrganizationRequestData organizationRequestData = new OrganizationRequestData();
        organizationRequestData.personalData = personalData;
        organizationRequestData.enclosures = requestData.getEnclosures();
        organizationRequestData.metadata = requestData.getMetadata();
        organizationRequestData.testMode = requestData.isTest();
        organizationRequestData.messageID = UUID.generate();
        organizationRequestData.messageReferenceID = organizationRequestData.messageID;
        organizationRequestData.id = "SenderProvidedRequestDataId";
        organizationRequestData.frguCode = requestData.getFrguCode();
        return organizationRequestData;
    }

    /**
     * Шаг 3 - запрос ОВ.
     * Затем Шаг 4 - подписание вложений/запроса ЭП-ОВ в другом контексте.
     */
    @Override
    public OrganizationRequest createOrganizationRequest(OrganizationRequestData requestData, boolean test) {
        SmevTypesProvider typesProvider = OSGi.service(SmevTypesProvider.class);

        OrganizationRequest request = new OrganizationRequest();
        request.data = requestData;
        Utils.prepareToSign(typesProvider.senderProvidedRequestData(requestData, test, false), request, requestData.id);
        return request;
    }
}
