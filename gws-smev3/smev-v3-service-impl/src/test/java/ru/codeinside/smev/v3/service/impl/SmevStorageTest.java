package ru.codeinside.smev.v3.service.impl;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockftpserver.fake.FakeFtpServer;
import org.mockftpserver.fake.UserAccount;
import org.mockftpserver.fake.filesystem.DirectoryEntry;
import org.mockftpserver.fake.filesystem.FileSystem;
import org.mockftpserver.fake.filesystem.UnixFakeFileSystem;
import ru.codeinside.smev.v3.service.api.FTPCredentials;
import ru.codeinside.smev.v3.service.api.FTPStorage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

public class SmevStorageTest extends Assert {

    private static final String HOME_DIRECTORY = "/home/user";

    private FakeFtpServer fakeFtpServer;
    private FTPStorage ftpStorage;

    @Before
    public void setUp() throws Exception {
        FileSystem fileSystem = new UnixFakeFileSystem();
        fileSystem.add(new DirectoryEntry(HOME_DIRECTORY));

        fakeFtpServer = new FakeFtpServer();
        fakeFtpServer.addUserAccount(new UserAccount("user", "password", HOME_DIRECTORY));
        fakeFtpServer.setServerControlPort(2105);
        fakeFtpServer.setFileSystem(fileSystem);
        fakeFtpServer.start();

        ftpStorage = new SmevStorage();
    }

    @After
    public void tearDown() throws Exception {
        fakeFtpServer.stop();
    }

    @Test
    public void test_upload() throws Exception {
        FTPCredentials credentials = new FTPCredentials("localhost", 2105, "user", "password");
        InputStream file = getClass().getClassLoader().getResourceAsStream("hello.doc");
        ftpStorage.upload(credentials, "myFolder", "hello.doc", file);
        file.close();

        assertTrue(fakeFtpServer.getFileSystem().exists(HOME_DIRECTORY + "/myFolder/hello.doc"));
    }

    @Test
    public void test_download() throws Exception {
        byte[] shadowFile = "my simple content".getBytes(Charset.forName("UTF-8"));
        FTPCredentials credentials = new FTPCredentials("localhost", 2105, "user", "password");
        InputStream fakeInputFile = new ByteArrayInputStream(shadowFile);
        ftpStorage.upload(credentials, "secret", "simple.txt", fakeInputFile);

        ByteArrayOutputStream downloadStream = new ByteArrayOutputStream();
        ftpStorage.download(credentials, "secret", "simple.txt", downloadStream);

        assertArrayEquals(shadowFile, downloadStream.toByteArray());
    }
}