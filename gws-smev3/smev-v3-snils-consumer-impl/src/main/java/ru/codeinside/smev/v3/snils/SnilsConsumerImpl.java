package ru.codeinside.smev.v3.snils;

import com.pfr.kvs.common._1_0.BirthPlaceType;
//import com.pfr.kvs.common._1_0.DocumentType;
import com.pfr.kvs.common._1_0.IdentificationDocumentType;
import com.pfr.kvs.snils_by_data._1_1.SnilsByDataRequest;
import com.pfr.kvs.snils_by_data._1_1.SnilsByDataResponse;
import com.pfr.kvs.snils_by_data._1_1.TwinDataType;
import ru.codeinside.gws.api.ExchangeContext;
import ru.codeinside.gws.api.Revision;
import ru.codeinside.smev.v3.service.api.InformationType;
import ru.codeinside.smev.v3.service.api.RequestRejected;
import ru.codeinside.smev.v3.service.api.consumer.Consumer;
import ru.codeinside.smev.v3.service.api.consumer.ConsumerRequest;
import ru.codeinside.smev.v3.service.api.consumer.Response;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseData;
import ru.gov.smev.artefacts.x.supplementary.commons._1_0.GenderType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.ParserConfigurationException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.gov.smev.artefacts.x.supplementary.commons._1_0.NotRestrictedDocumentType;

@SuppressWarnings("PackageAccessibility")
public class SnilsConsumerImpl implements Consumer {

    private static Logger logger = Logger.getLogger(SnilsConsumerImpl.class.getName());

    @Override
    public Revision getRevision() {
        return Revision.rev30981;
    }

    @Override
    public InformationType getInformationType() {
        //return new InformationType("http://skmv.rstyle.com/snils-by-data/1.0.0", "snilsByDataRequest");
        return new InformationType("http://kvs.pfr.com/snils-by-data/1.1.2", "SnilsByDataRequest");
    }

    @Override
    public ConsumerRequest createRequest(ExchangeContext businessProcess) throws IllegalStateException {
        try {
            ConsumerRequest consumerRequest = new ConsumerRequest();
            consumerRequest.setTest(true);
            consumerRequest.setPersonalData(createPersonalData(businessProcess));
            return consumerRequest;
        } catch (JAXBException e) {
            throw new IllegalStateException("Parsing XML error");
        } catch (ParserConfigurationException e) {
            throw new IllegalStateException("Parsing XML error");
        } catch (DatatypeConfigurationException e) {
            throw new IllegalStateException("Parsing XML error");
        }
    }

    @Override
    public String processResponse(Response response, ExchangeContext context) {
        SnilsByDataResponse snilsResponse;
        ProviderResponseData providerResponseData = response.data.providerResponse.data;

        if (providerResponseData.requestRejected != null && providerResponseData.requestRejected.size() != 0) {
            context.setVariable("smevReject", true);
            for (RequestRejected rejected : providerResponseData.requestRejected) {
                context.setVariable("rejectionReason", rejected.rejectionReasonDescription);
                context.setVariable("rejectionReasonCode", rejected.rejectionReasonCode);
            }
        } else {
            context.setVariable("smevReject", false);
            try {
                JAXBContext jaxbContext = JAXBContext.newInstance(SnilsByDataResponse.class);
                Unmarshaller un = jaxbContext.createUnmarshaller();
                snilsResponse = (SnilsByDataResponse) un.unmarshal(providerResponseData.personalData.data);
            } catch (JAXBException e) {
                logger.log(Level.WARNING, "Unmarshal response.", e);
                throw new RuntimeException(e);
            }

            context.setVariable("SNILS", snilsResponse.getSnils());
            fillTwins(snilsResponse.getTwinData(), context);
        }
        return providerResponseData.messageID;
    }

    private void fillTwins(List<TwinDataType> twinData, ExchangeContext context) {
        if (twinData == null) {
            return;
        }
        context.setVariable("twins", twinData.size());
        for (int i = 0; i < twinData.size(); ++i) {
            TwinDataType twinDataType = twinData.get(i);
            int pos = i + 1;

            context.setVariable("twin_snils_" + pos, twinDataType.getSnils());
            BirthPlaceType birthPlace = twinDataType.getBirthPlace();
            if (birthPlace != null) {
                context.setVariable("twin_country_" + pos, birthPlace.getCountry());
                context.setVariable("twin_district_" + pos, birthPlace.getDistrict());
                context.setVariable("twin_placeType_" + pos, birthPlace.getPlaceType());
                context.setVariable("twin_region_" + pos, birthPlace.getRegion());
                context.setVariable("twin_settlement_" + pos, birthPlace.getSettlement());
            }

            IdentificationDocumentType document = twinDataType.getPfrIdentificationDocument();
            if (document != null) {
                context.setVariable("doc_type_" + pos, document.getType());
                NotRestrictedDocumentType documentType = document.getDocument();
                if (documentType != null) { 
                    context.setVariable("doc_issueAgency_" + pos, documentType.getIssuer());
                    context.setVariable("doc_issueDate_" + pos, fromXmlCalendar(documentType.getIssueDate()));
                    context.setVariable("doc_name_" + pos, document.getType());
                    context.setVariable("doc_number_" + pos, documentType.getNumber());
                    context.setVariable("doc_seriesNumerals_" + pos, documentType.getSeries());
                    //context.setVariable("doc_seriesNumerals_" + pos, getSeries(documentType));
                }
            }
        }
    }

//    private String getSeries(DocumentType documentType) {
//        String seriesRomanNumerals = documentType.getSeriesRomanNumerals();
//        String seriesRussianSymbols = documentType.getSeriesRussianSymbols();
//        if (seriesRomanNumerals != null && !seriesRomanNumerals.isEmpty()) {
//            return seriesRomanNumerals;
//        } else if (seriesRussianSymbols != null && !seriesRussianSymbols.isEmpty()) {
//            return seriesRussianSymbols;
//        } else {
//            return null;
//        }
//    }

    private SnilsByDataRequest createPersonalData(ExchangeContext businessProcess) throws DatatypeConfigurationException {
        SnilsByDataRequest snilsRequest = new SnilsByDataRequest();
        snilsRequest.setFamilyName(businessProcess.getVariable("FamilyName").toString());
        snilsRequest.setFirstName(businessProcess.getVariable("FirstName").toString());
        snilsRequest.setBirthDate(toXMLGregorianCalendar((Date) businessProcess.getVariable("BirthDate")));
        if (businessProcess.getVariable("Gender") != null)
            snilsRequest.setGender(genderFromString(businessProcess.getVariable("Gender").toString()));
        snilsRequest.setPatronymic(businessProcess.getVariable("Patronymic").toString());
        return snilsRequest;
    }


    private GenderType genderFromString(String genderString) {
        if (genderString != null) {
            for (GenderType gender : GenderType.values()) {
                if (gender.value().toLowerCase().equals(genderString.toLowerCase()))
                    return gender;
            }
        }
        return null;
    }

    private XMLGregorianCalendar toXMLGregorianCalendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        XMLGregorianCalendar xmlCalendar = null;
        try {
            xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar();
            xmlCalendar.setYear(calendar.get(Calendar.YEAR));
            xmlCalendar.setMonth(calendar.get(Calendar.MONTH) + 1);
            xmlCalendar.setDay(calendar.get(Calendar.DAY_OF_MONTH));
        } catch (DatatypeConfigurationException ex) {
            ex.printStackTrace();
        }
        return xmlCalendar;
    }

    private Date fromXmlCalendar(XMLGregorianCalendar xmlCalendar) {
        if (xmlCalendar == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, xmlCalendar.getYear());
        calendar.set(Calendar.MONTH, xmlCalendar.getMonth() - 1);
        calendar.set(Calendar.DAY_OF_MONTH, xmlCalendar.getDay() - 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }
}
