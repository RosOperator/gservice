package ru.codeinside.smev.v3.snils;

import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.gws.api.ExchangeContext;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DummyBusinessProcess implements ExchangeContext {
    private Map<String, Object> variables = new HashMap<String, Object>();
    /**
     * Получить локальный объект.
     */
    @Override
    public Object getLocal() {
        return null;
    }

    /**
     * Установить локальный объект. Существует лишь между запросом и отвветом.
     *
     * @param value
     */
    @Override
    public void setLocal(Object value) {

    }

    /**
     * Получить имена переменных из процесса исполнения.
     */
    @Override
    public Set<String> getVariableNames() {
        return variables.keySet();
    }

    /**
     * Получить значение переменной.
     *
     * @param name
     */
    @Override
    public Object getVariable(String name) {
        return variables.get(name);
    }

    /**
     * Ассоциирована ли переменная с вложением.
     *
     * @param name
     */
    @Override
    public boolean isEnclosure(String name) {
        return false;
    }

    /**
     * Сохранить значение переменной в процесс исполнения.
     *
     * @param name
     * @param value
     */
    @Override
    public void setVariable(String name, Object value) {
        variables.put(name, value);
    }

    /**
     * Получить вложение по имени ассоциированой переменной.
     *
     * @param name
     */
    @Override
    public Enclosure getEnclosure(String name) {
        return null;
    }

    @Override
    public void addEnclosure(String name, Enclosure enclosure) {

    }
}
