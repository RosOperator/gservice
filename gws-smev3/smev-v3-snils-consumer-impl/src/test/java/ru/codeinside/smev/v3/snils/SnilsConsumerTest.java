package ru.codeinside.smev.v3.snils;

import com.pfr.kvs.snils_by_data._1_1.SnilsByDataRequest;
import com.pfr.kvs.snils_by_data._1_1.SnilsByDataResponse;
//import com.pfr.kvs.snils_by_data._1_1.SnilsByDataResponseType;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.codeinside.gws.api.ExchangeContext;
import ru.codeinside.smev.v3.service.api.PersonalData;
import ru.codeinside.smev.v3.service.api.consumer.ConsumerRequest;
import ru.codeinside.smev.v3.service.api.consumer.Response;
import ru.codeinside.smev.v3.service.api.consumer.ResponseData;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponse;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseData;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.Date;


public class SnilsConsumerTest extends Assert {

    @Test
    public void testRequest() throws Exception {
        ExchangeContext context = new DummyBusinessProcess();
        context.setVariable("FamilyName", "Иванов");
        context.setVariable("FirstName", "Иван");
        context.setVariable("BirthDate", new Date());
        context.setVariable("Gender", "Female");
        context.setVariable("Patronymic", "Иваныч");
        context.setVariable("test", "test");

        ConsumerRequest request = new SnilsConsumerImpl().createRequest(context);

        assertNotNull(request.getPersonalData());

        Element personalData = request.getPersonalData();
        JAXBContext jaxbContext = JAXBContext.newInstance(SnilsByDataRequest.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        SnilsByDataRequest unmarshaled = (SnilsByDataRequest) unmarshaller.unmarshal(personalData);

        assertEquals("Иванов", unmarshaled.getFamilyName());
    }

    @Test
    public void testResponse() throws Exception {
        ExchangeContext context = new DummyBusinessProcess();
        SnilsByDataResponse snilsResponse = new SnilsByDataResponse();
        
        snilsResponse.setSnils("1324-4567-7894");

        ProviderResponse providerResponse = new ProviderResponse();
        providerResponse.data = new ProviderResponseData();
        providerResponse.data.personalData = new PersonalData();
        DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = docBuilder.newDocument();
        JAXBContext jaxbContext = JAXBContext.newInstance(SnilsByDataResponse.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.marshal(snilsResponse, doc);
        providerResponse.data.personalData.data = doc.getDocumentElement();

        Response response = new Response();
        response.data = new ResponseData();
        response.data.providerResponse = providerResponse;

        new SnilsConsumerImpl().processResponse(response, context);

        assertEquals(3, context.getVariableNames().size());
        assertEquals("1324-4567-7894", context.getVariable("SNILS"));
        assertEquals(0, context.getVariable("twins"));
    }
}
