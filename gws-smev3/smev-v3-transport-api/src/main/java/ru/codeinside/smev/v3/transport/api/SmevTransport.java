package ru.codeinside.smev.v3.transport.api;

import ru.codeinside.smev.v3.service.api.HaunterLog;
import ru.codeinside.smev.v3.service.api.*;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequest;
import ru.codeinside.smev.v3.service.api.consumer.ResponseMessage;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponse;
import ru.codeinside.smev.v3.service.api.provider.RequestMessage;

import java.util.List;

/**
 * OSGi-сервис для вызова методов СМЭВ
 */
public interface SmevTransport {
    /**
     * Отправить заявку на обработку
     */
    SystemMessage sendRequest(OrganizationRequest request, HaunterLog consumerLog);

    /**
     * Отправить результат обработки заявки
     */
    SystemMessage sendResponse(ProviderResponse request, HaunterLog providerLog);

    /**
     * Получить заявки на обработку
     */
    RequestMessage getRequest(MessageIdentifier messageIdentifier, HaunterLog providerLog);

    /**
     * Получить результат обработки заявки
     */
    ResponseMessage getResponse(MessageIdentifier messageIdentifier, HaunterLog consumerLog);

    /**
     * Подтвердить получения сообщения
     */
    void ack(AcknowledgeRequest request);

    /**
     * Получить уведомление из очереди уведомления
     */
    AsyncProcessingStatus getStatus(TimestampRequest timestampRequest);

    /**
     * Получить статистику входящих очередей
     */
    List<QueueStatistic> getIncomingQueueStatistics(TimestampRequest request);

    /**
    * Подготовить клиент перед отсылкой сообщения
    *
    * @param url адрес сервиса СМЭВ3
    * @param test признак тестового сообщения
    */
    void prepare(String url, boolean test);
}

