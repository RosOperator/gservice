package ru.codeinside.smev.v3.transport.api;

import org.w3c.dom.Element;
import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.smev.v3.service.api.*;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequest;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequestData;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponse;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseData;
import ru.codeinside.smev.v3.transport.api.v1_1.*;

import java.util.List;

/**
 * API OSGi сервиса, который преобразовывает обертки типов в типы СМЕВ и наооборот
 */
public interface SmevTypesProvider {
    MessagePrimaryContent messagePrimaryContent(Element element);

    XMLDSigSignatureType xmldSigSignatureType(SignedData<?> signedData, boolean mustBeSigned);

    SenderProvidedRequestData senderProvidedRequestData(OrganizationRequestData organizationRequestData, boolean isTest, boolean uploadFiles);

    SendRequestRequest sendRequestRequest(OrganizationRequest organizationRequest, boolean isTest);

    AttachmentHeaderList attachmentHeaderList(List<Enclosure> enclosures);

    RefAttachmentHeaderList refAttachmentHeaderList(List<Enclosure> enclosures, boolean mustBeUploaded);

    AttachmentContentList attachmentContentList(List<Enclosure> enclosures);

    SenderProvidedResponseData senderProvidedResponseData(ProviderResponseData providerResponse, boolean uploadFiles);

    List<SenderProvidedResponseData.RequestRejected> requestRejected(List<RequestRejected> requestRejects);

    SenderProvidedResponseData.RequestStatus requestStatus(RequestStatus requestStatus);

    SendResponseRequest sendResponseRequest(ProviderResponse providerResponse);

    MessageTypeSelector messageTypeSelector(MessageIdentifier messageIdentifier);

    GetRequestRequest getRequestRequest(MessageIdentifier messageIdentifier);

    GetResponseRequest getResponseRequest(MessageIdentifier messageIdentifier);

    AckTargetMessage ackTargetMessage(AcknowledgeRequest acknowledgeRequest);

    AckRequest ackRequest(AcknowledgeRequest acknowledgeRequest);

    GetStatusRequest getStatusRequest(TimestampRequest timestampRequest);

    Timestamp timestamp(TimestampRequest timestampRequest);

    GetIncomingQueueStatisticsRequest getIncomingQueueStatisticsRequest(TimestampRequest timestampRequest);
}
