
package ru.codeinside.smev.v3.transport.api.v1_2;

import javax.xml.ws.WebFault;


/**
 * 
 *                     Не найден получатель для запроса.
 *                 
 * 
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "RecipientIsNotFound", targetNamespace = "urn://x-artefacts-smev-gov-ru/services/message-exchange/types/faults/1.2")
public class RecipientIsNotFoundException
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private Void faultInfo;

    /**
     * 
     * @param message
     * @param faultInfo
     */
    public RecipientIsNotFoundException(String message, Void faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param message
     * @param faultInfo
     * @param cause
     */
    public RecipientIsNotFoundException(String message, Void faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: ru.codeinside.smev.v3.transport.v1_2.Void
     */
    public Void getFaultInfo() {
        return faultInfo;
    }

}
