
package ru.codeinside.smev.v3.transport.api.v1_2;

import javax.xml.ws.WebFault;


/**
 * 
 *                     ЭП-ОВ не прошла проверку. 
 *                     Действия клиента: см. описание {urn://x-artefacts-smev-gov-ru/smev-core/client-interaction/types/1.0}SignatureVerificationFault.
 *                 
 * 
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "SignatureVerificationFault", targetNamespace = "urn://x-artefacts-smev-gov-ru/services/message-exchange/types/faults/1.2")
public class SignatureVerificationFaultException
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private SignatureVerificationFault faultInfo;

    /**
     * 
     * @param message
     * @param faultInfo
     */
    public SignatureVerificationFaultException(String message, SignatureVerificationFault faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param message
     * @param faultInfo
     * @param cause
     */
    public SignatureVerificationFaultException(String message, SignatureVerificationFault faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: ru.codeinside.smev.v3.transport.v1_2.SignatureVerificationFault
     */
    public SignatureVerificationFault getFaultInfo() {
        return faultInfo;
    }

}
