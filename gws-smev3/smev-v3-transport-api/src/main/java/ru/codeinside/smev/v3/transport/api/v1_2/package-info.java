/**
 * 
 *         Веб-сервис, предоставляемый СМЭВ, через который 
 *         все участники межведомственного взаимодействия обмениваются сообщениями.
 *     
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "urn://x-artefacts-smev-gov-ru/services/message-exchange/types/1.2", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package ru.codeinside.smev.v3.transport.api.v1_2;
