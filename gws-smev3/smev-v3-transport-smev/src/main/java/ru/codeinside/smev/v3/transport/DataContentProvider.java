package ru.codeinside.smev.v3.transport;

import ru.codeinside.smev.v3.service.api.ContentProvider;

import javax.activation.DataHandler;
import java.io.*;

public class DataContentProvider implements ContentProvider {
    private File content;

    public DataContentProvider(DataHandler dataHandler) {
        try {
            writeToTempFile(dataHandler.getInputStream());
        } catch (IOException e) {
            content = new File("");
        }
    }

    @Override
    public InputStream getContent() {
        try {
            return new BufferedInputStream(new FileInputStream(content));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public long size() {
        return content.length();
    }

    public File getFile() {
        return content;
    }

    private void writeToTempFile(InputStream is) throws IOException {
        content = File.createTempFile("data-", "");
        OutputStream out = null;
        byte[] buffer = new byte[1024 * 8];
        int count;
        try {
            out = new BufferedOutputStream(new FileOutputStream(content));
            while ((count = is.read(buffer)) > -1) {
                out.write(buffer, 0, count);
            }
            out.flush();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ignored) {
                }
            }
        }
    }
}
