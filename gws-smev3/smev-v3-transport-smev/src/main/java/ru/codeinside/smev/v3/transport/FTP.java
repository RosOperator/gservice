package ru.codeinside.smev.v3.transport;

import ru.codeinside.smev.v3.service.api.FTPCredentials;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

public final class FTP {

    private FTP() {

    }

    public static FTPCredentials getFtpCredentials() {
        Properties properties = new Properties();
        File userHome = new File(System.getProperty("user.home"));
        File gsesProperties = new File(userHome, "gses-key.properties");

        if (!gsesProperties.exists()) {
            log().warning("Файл настроек не обнаружен.");
        } else {
            FileInputStream propertiesStream = null;
            try {
                propertiesStream = new FileInputStream(gsesProperties);
                properties.load(propertiesStream);
            } catch (IOException e) {
                log().warning("Не удалось прочитать файл настроек. " + e.getMessage());
            } finally {
                if (propertiesStream != null) {
                    try {
                        propertiesStream.close();
                    } catch (IOException ignored) {
                    }
                }
            }
        }

        String server = properties.getProperty("ftp.server", "localhost");
        int port = Integer.parseInt(properties.getProperty("ftp.port", "2105"));
        String user = properties.getProperty("ftp.user", "user");
        String password = properties.getProperty("ftp.password", "123");

        return new FTPCredentials(server, port, user, password);
    }

    private static Logger log() {
        return Logger.getLogger(FTP.class.getClass().getName());
    }
}
