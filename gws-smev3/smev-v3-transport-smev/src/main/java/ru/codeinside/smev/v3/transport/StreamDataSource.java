package ru.codeinside.smev.v3.transport;

import javax.activation.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class StreamDataSource implements DataSource {
  private InputStream content;
  private final String name;
  private final String mimeType;

  public StreamDataSource(InputStream content, String name, String mimeType) {
    this.content = content;
    this.name = name;
    this.mimeType = mimeType;
  }

  @Override
  public String getContentType() {
    return mimeType;
  }

  @Override
  public InputStream getInputStream() throws IOException {
    return content;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public OutputStream getOutputStream() throws IOException {
    return null;
  }
}
