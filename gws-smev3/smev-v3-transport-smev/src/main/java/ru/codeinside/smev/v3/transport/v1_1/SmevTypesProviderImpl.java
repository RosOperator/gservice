package ru.codeinside.smev.v3.transport.v1_1;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.gws.api.Signature;
import ru.codeinside.smev.v3.crypto.api.GostR3410;
import ru.codeinside.smev.v3.crypto.api.SignatureAssembler;
import ru.codeinside.smev.v3.crypto.api.XmlSignature;
import ru.codeinside.smev.v3.crypto.api.XmlSignatureAssembler;
import ru.codeinside.smev.v3.service.api.*;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequest;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequestData;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponse;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseData;
import ru.codeinside.smev.v3.transport.FTP;
import ru.codeinside.smev.v3.transport.StreamDataSource;
import ru.codeinside.smev.v3.transport.api.SmevTypesProvider;
import ru.codeinside.smev.v3.transport.api.v1_1.*;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Logger;

@SuppressWarnings("PackageAccessibility")
public class SmevTypesProviderImpl implements SmevTypesProvider {
    private static final int THRESHOLD = 5 * 1024 * 1024;  // 5 Mb
    private static final int MAX_THRESHOLD = 1024 * 1024 * 1024;  // 1 GB1
    private final Logger log = Logger.getLogger(SmevTypesProviderImpl.class.getName());

    private XmlSignatureAssembler xmlSignatureAssembler;
    private GostR3410 gostR3410;
    private SignatureAssembler signatureAssembler;
    private FTPStorage ftpStorage;


    @Override
    public MessagePrimaryContent messagePrimaryContent(Element element) {
        MessagePrimaryContent messagePrimaryContent = new MessagePrimaryContent();
        messagePrimaryContent.setAny(element);
        return messagePrimaryContent;
    }

    @Override
    public XMLDSigSignatureType xmldSigSignatureType(SignedData<?> signedData, boolean mustBeSigned) {
        XMLDSigSignatureType xmldSigSignatureType = null;

        if (mustBeSigned && !isSignatureSet(signedData)) {
            signedData.signature.setSignature(
                    sign(signedData.signature.getSignature().getDigest(), signedData.signData));
        }

        if (isSignatureSet(signedData)) {
            xmldSigSignatureType = new XMLDSigSignatureType();
            xmldSigSignatureType.setAny(assembleXmlSignature(signedData.signature));
        }
        return xmldSigSignatureType;
    }

    @Override
    public SenderProvidedRequestData senderProvidedRequestData(OrganizationRequestData organizationRequestData, boolean isTest, boolean uploadFiles) {
        SenderProvidedRequestData senderProvidedRequestData = new SenderProvidedRequestData();
        senderProvidedRequestData.setId(organizationRequestData.id);
        senderProvidedRequestData.setMessageID(organizationRequestData.messageID);
        senderProvidedRequestData.setReferenceMessageID(organizationRequestData.messageReferenceID);

        if (isTest && organizationRequestData.testMode) {
            senderProvidedRequestData.setTestMessage(new ru.codeinside.smev.v3.transport.api.v1_1.Void());
        }

        if (useFtp(organizationRequestData.enclosures)) {
            senderProvidedRequestData.setRefAttachmentHeaderList(
                    refAttachmentHeaderList(organizationRequestData.enclosures, uploadFiles));
        } else {
            senderProvidedRequestData.setAttachmentHeaderList(attachmentHeaderList(organizationRequestData.enclosures));
        }

        senderProvidedRequestData.setMessagePrimaryContent(messagePrimaryContent(organizationRequestData.personalData.data));
        senderProvidedRequestData.setPersonalSignature(xmldSigSignatureType(organizationRequestData.personalData, false));

        try {
            SenderProvidedRequestData.BusinessProcessMetadata businessProcessMetadata = new SenderProvidedRequestData.BusinessProcessMetadata();
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element frgu = doc.createElementNS("urn://x-artefacts-smev-gov-ru/services/message-exchange/business-process-metadata/1.0", "tns:frgu");
            frgu.setTextContent(organizationRequestData.frguCode);
            businessProcessMetadata.getAny().add(frgu);
            senderProvidedRequestData.setBusinessProcessMetadata(businessProcessMetadata);
        } catch (ParserConfigurationException e) {
            log.severe(e.getMessage());
            throw new IllegalStateException("Ошибка добавления кода ФРГУ: " + e.getMessage(), e);
        }
        return senderProvidedRequestData;
    }

    @Override
    public SendRequestRequest sendRequestRequest(OrganizationRequest organizationRequest, boolean isTest) {
        SendRequestRequest sendRequestRequest = new SendRequestRequest();
        sendRequestRequest.setSenderProvidedRequestData(senderProvidedRequestData(organizationRequest.data, isTest, true));
        sendRequestRequest.setCallerInformationSystemSignature(xmldSigSignatureType(organizationRequest, true));
        if (!useFtp(organizationRequest.data.enclosures)) {
            sendRequestRequest.setAttachmentContentList(attachmentContentList(organizationRequest.data.enclosures));
        }
        return sendRequestRequest;
    }

    @Override
    public AttachmentHeaderList attachmentHeaderList(List<Enclosure> enclosures) {
        if (enclosures == null || enclosures.size() == 0) {
            return null;
        }
        AttachmentHeaderList headerList = new AttachmentHeaderList();
        for (Enclosure enclosure : enclosures) {
            if (enclosure.signature == null) {
                gostR3410.sign(enclosure);
            }
            AttachmentHeaderType headerType = new AttachmentHeaderType();
            headerType.setContentId(enclosure.number);
            headerType.setMimeType(enclosure.mimeType);
            headerType.setSignaturePKCS7(toPKCS7(enclosure.signature));
            headerList.getAttachmentHeader().add(headerType);
        }
        return headerList;
    }

    @Override
    public RefAttachmentHeaderList refAttachmentHeaderList(List<Enclosure> enclosures, boolean mustBeUploaded) {
        if (enclosures == null || enclosures.size() == 0) {
            return null;
        }

        FTPCredentials credentials = FTP.getFtpCredentials();
        RefAttachmentHeaderList result = new RefAttachmentHeaderList();
        for (Enclosure enclosure : enclosures) {
            if (enclosure.signature == null) {
                gostR3410.sign(enclosure);
            }
            if (mustBeUploaded) {
                InputStream content = enclosure.getContent();
                try {
                    ftpStorage.upload(credentials, enclosure.number, enclosure.fileName, content);
                } catch (IOException e) {
                    log.severe(e.getMessage());
                    throw new IllegalStateException("Ошибка загрузки файла по FTP: " + e.getMessage(), e);
                } finally {
                    try {
                        content.close();
                    } catch (IOException ignored) {
                    }
                }
            }
            RefAttachmentHeaderType header = new RefAttachmentHeaderType();
            header.setUuid(enclosure.number);
            header.setMimeType(enclosure.mimeType);
            header.setHash(Base64.encode(enclosure.digest));
            header.setSignaturePKCS7(toPKCS7(enclosure.signature));
            result.getRefAttachmentHeader().add(header);
        }
        return result;
    }

    @Override
    public AttachmentContentList attachmentContentList(List<Enclosure> enclosures) {
        if (enclosures == null || enclosures.size() == 0) {
            return null;
        }
        AttachmentContentList contentList = new AttachmentContentList();
        for (Enclosure enclosure : enclosures) {
            DataSource ds = new StreamDataSource(enclosure.getContent(), enclosure.fileName, enclosure.mimeType);
            DataHandler handler = new DataHandler(ds);
            AttachmentContentType contentType = new AttachmentContentType();
            contentType.setId(enclosure.number);
            contentType.setContent(handler);
            contentList.getAttachmentContent().add(contentType);
        }
        return contentList;
    }

    @Override
    public SenderProvidedResponseData senderProvidedResponseData(ProviderResponseData providerResponseData, boolean uploadFiles) {
        SenderProvidedResponseData senderProvidedResponseData = new SenderProvidedResponseData();
        senderProvidedResponseData.setId(providerResponseData.id);
        senderProvidedResponseData.setMessageID(providerResponseData.messageID);
        senderProvidedResponseData.setTo(providerResponseData.to);

        if (providerResponseData.requestStatus != null) {
            senderProvidedResponseData.setRequestStatus(requestStatus(providerResponseData.requestStatus));
            return senderProvidedResponseData;
        }

        if (providerResponseData.requestRejected != null && !providerResponseData.requestRejected.isEmpty()) {
            senderProvidedResponseData.getRequestRejected().addAll(requestRejected(providerResponseData.requestRejected));
            return senderProvidedResponseData;
        }

        if (providerResponseData.personalData != null && providerResponseData.personalData.data != null) {
            senderProvidedResponseData.setMessagePrimaryContent(messagePrimaryContent(providerResponseData.personalData.data));
            senderProvidedResponseData.setPersonalSignature(xmldSigSignatureType(providerResponseData.personalData, false));

            if (useFtp(providerResponseData.enclosures)) {
                senderProvidedResponseData.setRefAttachmentHeaderList(refAttachmentHeaderList(providerResponseData.enclosures, uploadFiles));
            } else {
                senderProvidedResponseData.setAttachmentHeaderList(attachmentHeaderList(providerResponseData.enclosures));
            }
        }
        return senderProvidedResponseData;
    }

    @Override
    public List<SenderProvidedResponseData.RequestRejected> requestRejected(List<RequestRejected> requestRejects) {
        List<SenderProvidedResponseData.RequestRejected> result = new ArrayList<SenderProvidedResponseData.RequestRejected>();
        for (RequestRejected reject : requestRejects) {
            SenderProvidedResponseData.RequestRejected requestRejected = new SenderProvidedResponseData.RequestRejected();
            requestRejected.setRejectionReasonCode(RejectCode.fromValue(reject.rejectionReasonCode.value()));
            requestRejected.setRejectionReasonDescription(reject.rejectionReasonDescription);
            result.add(requestRejected);
        }
        return result;
    }

    @Override
    public SenderProvidedResponseData.RequestStatus requestStatus(RequestStatus requestStatus) {
        SenderProvidedResponseData.RequestStatus result = new SenderProvidedResponseData.RequestStatus();
        result.setStatusCode(requestStatus.getStatusCode());
        result.setStatusDescription(requestStatus.getStatusDescription());
        List<SenderProvidedResponseData.RequestStatus.StatusParameter> params =
                new ArrayList<SenderProvidedResponseData.RequestStatus.StatusParameter>();

        for (StatusParameter statusParameter: requestStatus.getRequestStatuses()) {
            SenderProvidedResponseData.RequestStatus.StatusParameter param =
                    new SenderProvidedResponseData.RequestStatus.StatusParameter();
            param.setKey(statusParameter.key);
            param.setValue(statusParameter.value);
            params.add(param);
        }
        result.getStatusParameter().addAll(params);
        return result;
    }

    @Override
    public SendResponseRequest sendResponseRequest(ProviderResponse providerResponse) {
        SendResponseRequest sendResponseRequest = new SendResponseRequest();
        sendResponseRequest.setSenderProvidedResponseData(senderProvidedResponseData(providerResponse.data, true));
        sendResponseRequest.setCallerInformationSystemSignature(xmldSigSignatureType(providerResponse, true));
        if (!useFtp(providerResponse.data.enclosures)) {
            sendResponseRequest.setAttachmentContentList(attachmentContentList(providerResponse.data.enclosures));
        }
        return sendResponseRequest;
    }

    @Override
    public MessageTypeSelector messageTypeSelector(MessageIdentifier messageIdentifier) {
        MessageTypeSelector messageTypeSelector = new MessageTypeSelector();
        messageTypeSelector.setId(messageIdentifier.data.id);
        messageTypeSelector.setNamespaceURI(messageIdentifier.data.namespaceURI);
        messageTypeSelector.setRootElementLocalName(messageIdentifier.data.rootElementLocalName);
        messageTypeSelector.setTimestamp(toXmlGregorianCalendar(messageIdentifier.data.timestamp));
        return messageTypeSelector;
    }

    @Override
    public GetRequestRequest getRequestRequest(MessageIdentifier messageIdentifier) {
        GetRequestRequest getRequestRequest = new GetRequestRequest();
        getRequestRequest.setMessageTypeSelector(messageTypeSelector(messageIdentifier));
        getRequestRequest.setCallerInformationSystemSignature(xmldSigSignatureType(messageIdentifier, true));
        return getRequestRequest;
    }

    @Override
    public GetResponseRequest getResponseRequest(MessageIdentifier messageIdentifier) {
        GetResponseRequest getResponseRequest = new GetResponseRequest();
        getResponseRequest.setMessageTypeSelector(messageTypeSelector(messageIdentifier));
        getResponseRequest.setCallerInformationSystemSignature(xmldSigSignatureType(messageIdentifier, true));
        return getResponseRequest;
    }

    @Override
    public AckTargetMessage ackTargetMessage(AcknowledgeRequest acknowledgeRequest) {
        AckTargetMessage targetMessage = new AckTargetMessage();
        targetMessage.setAccepted(acknowledgeRequest.data.accepted);
        targetMessage.setId(acknowledgeRequest.data.id);
        targetMessage.setValue(acknowledgeRequest.data.messageId);
        return targetMessage;
    }

    @Override
    public AckRequest ackRequest(AcknowledgeRequest acknowledgeRequest) {
        AckRequest ackRequest = new AckRequest();
        ackRequest.setAckTargetMessage(ackTargetMessage(acknowledgeRequest));
        ackRequest.setCallerInformationSystemSignature(xmldSigSignatureType(acknowledgeRequest, true));
        return ackRequest;
    }

    @Override
    public GetStatusRequest getStatusRequest(TimestampRequest timestampRequest) {
        GetStatusRequest getStatusRequest = new GetStatusRequest();
        getStatusRequest.setTimestamp(timestamp(timestampRequest));
        getStatusRequest.setCallerInformationSystemSignature(xmldSigSignatureType(timestampRequest, true));
        return getStatusRequest;
    }

    @Override
    public Timestamp timestamp(TimestampRequest timestampRequest) {
        Timestamp timestamp = new Timestamp();
        timestamp.setId(timestampRequest.data.id);
        timestamp.setValue(toXmlGregorianCalendar(timestampRequest.data.timestamp));
        return timestamp;
    }

    @Override
    public GetIncomingQueueStatisticsRequest getIncomingQueueStatisticsRequest(TimestampRequest timestampRequest) {
        GetIncomingQueueStatisticsRequest getIncomingQueueStatisticsRequest = new GetIncomingQueueStatisticsRequest();
        getIncomingQueueStatisticsRequest.setTimestamp(timestamp(timestampRequest));
        getIncomingQueueStatisticsRequest.setCallerInformationSystemSignature(xmldSigSignatureType(timestampRequest, true));
        return getIncomingQueueStatisticsRequest;
    }

    /* Helpers methods */

    private XMLGregorianCalendar toXmlGregorianCalendar(GregorianCalendar timestamp) {
        try {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(timestamp);
        } catch (DatatypeConfigurationException e) {
            log.severe("Unable to initialize DatatypeFactory: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private boolean isSignatureSet(SignedData<?> signedData) {
        XmlSignature signature = signedData.signature;
        return signature != null &&
                signature.getSignature() != null &&
                signature.getSignature().sign != null &&
                signature.getSignature().sign.length > 0;
    }

    private byte[] toPKCS7(Signature signature) {
        return signatureAssembler.assemble(signature);
    }

    private Element assembleXmlSignature(XmlSignature xmlSignature) {
        if (xmlSignature == null) {
            return null;
        }
        return xmlSignatureAssembler.assemble(xmlSignature);
    }

    private Signature sign(byte[] digest, byte[] data) {
        Signature signature = gostR3410.sign(new ByteArrayInputStream(data));
        return new Signature(signature.certificate, signature.content, signature.sign, digest, true);
    }

    private boolean useFtp(List<Enclosure> enclosures) {
        if (enclosures == null) {
            return false;
        }

        int total = 0;
        for (Enclosure enclosure : enclosures) {
            total += enclosure.size();
            if (total > MAX_THRESHOLD || total <= -1) {
                throw new IllegalStateException("Нельзя передавать вложения больше чем 1 Гб.");
            }
        }
        return total > THRESHOLD;
    }

    /* OSGi's bind / unbind functions */

    @SuppressWarnings("unused")
    public void bindXmlSignatureAssembler(XmlSignatureAssembler assembler) {
        this.xmlSignatureAssembler = assembler;
    }

    @SuppressWarnings("unused")
    public void unbindXmlSignatureAssembler(XmlSignatureAssembler assembler) {
        if (this.xmlSignatureAssembler == assembler) {
            this.xmlSignatureAssembler = null;
        }
    }

    @SuppressWarnings("unused")
    public void bindGostR3410(GostR3410 gostR3410) {
        this.gostR3410 = gostR3410;
    }

    @SuppressWarnings("unused")
    public void unbindGostR3410(GostR3410 gostR3410) {
        if (this.gostR3410 == gostR3410) {
            this.gostR3410 = null;
        }
    }

    @SuppressWarnings("unused")
    public void bindSignatureAssembler(SignatureAssembler assembler) {
        this.signatureAssembler = assembler;
    }

    @SuppressWarnings("unused")
    public void unbindSignatureAssembler(SignatureAssembler assembler) {
        if (this.signatureAssembler == assembler) {
            this.signatureAssembler = null;
        }
    }

    @SuppressWarnings("unused")
    public void bindFtpStorage(FTPStorage storage) {
        this.ftpStorage = storage;
    }

    @SuppressWarnings("unused")
    public void unbindFtpStorage(FTPStorage storage) {
        if (this.ftpStorage == storage) {
            this.ftpStorage = null;
        }
    }
}
