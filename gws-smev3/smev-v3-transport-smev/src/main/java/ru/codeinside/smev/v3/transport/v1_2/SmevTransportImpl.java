package ru.codeinside.smev.v3.transport.v1_2;

import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.gws.api.InfoSystem;
import ru.codeinside.gws.api.Signature;
import ru.codeinside.smev.v3.crypto.api.*;
import ru.codeinside.smev.v3.service.api.*;
import ru.codeinside.smev.v3.service.api.AsyncProcessingStatus;
import ru.codeinside.smev.v3.service.api.MessageMetadata;
import ru.codeinside.smev.v3.service.api.MessageReference;
import ru.codeinside.smev.v3.service.api.SmevFault;
import ru.codeinside.smev.v3.service.api.consumer.*;
import ru.codeinside.smev.v3.service.api.consumer.Response;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponse;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseData;
import ru.codeinside.smev.v3.service.api.provider.RequestData;
import ru.codeinside.smev.v3.service.api.provider.RequestMessage;
import ru.codeinside.smev.v3.transport.DataContentProvider;
import ru.codeinside.smev.v3.transport.StreamDataSource;
import ru.codeinside.smev.v3.transport.api.SmevTransport;
import ru.codeinside.smev.v3.transport.api.v1_2.*;
import ru.codeinside.smev.v3.transport.api.v1_2.Void;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPBinding;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.logging.Logger;

public class SmevTransportImpl implements SmevTransport {
    private final Logger log = Logger.getLogger(SmevTransportImpl.class.getName());
    private final Object urlMonitor = new Object();

    private SMEVMessageExchangePortType client;
    private GostR3410 gostR3410;
    private XmlSignatureAssembler xmlSignatureAssembler;
    private SignatureAssembler signatureAssembler;
    private XmlNormalizer normalizer;

    private volatile URL url;
    private volatile boolean test;

    @SuppressWarnings("unused")
    public SmevTransportImpl() { }

    public SmevTransportImpl(SMEVMessageExchangePortType client) {
        this.client = client;
    }

    /**
     * Отправить заявку на обработку
     */
    @SuppressWarnings("Duplicates")
    @Override
    public SystemMessage sendRequest(OrganizationRequest request, HaunterLog consumerLog) {
        MessagePrimaryContent primaryContent = new MessagePrimaryContent();
        primaryContent.setAny(request.data.personalData.data);

        XMLDSigSignatureType personalSignature = new XMLDSigSignatureType();
        personalSignature.setAny(assembleXmlSignature(request.data.personalData.signature));

        SenderProvidedRequestData senderProvidedRequestData = new SenderProvidedRequestData();
        senderProvidedRequestData.setId(request.data.id);
        senderProvidedRequestData.setMessageID(request.data.messageID);
        if (!test && request.data.testMode) {
            senderProvidedRequestData.setTestMessage(new Void());
        }
        senderProvidedRequestData.setAttachmentHeaderList(buildAttachmentHeader(request.data.enclosures));
        senderProvidedRequestData.setMessagePrimaryContent(primaryContent);
        senderProvidedRequestData.setPersonalSignature(personalSignature);

        if (!checkSignature(request.signature.getSignature())) {
            request.signature = sign(senderProvidedRequestData);
        }

        XMLDSigSignatureType callerSignature = new XMLDSigSignatureType();
        callerSignature.setAny(assembleXmlSignature(request.signature));

        SendRequestRequest sendRequest = new SendRequestRequest();
        sendRequest.setSenderProvidedRequestData(senderProvidedRequestData);
        sendRequest.setAttachmentContentList(buildAttachmentContent(request.data.enclosures));
        sendRequest.setCallerInformationSystemSignature(callerSignature);
        try {
            SendRequestResponse sendRequestResponse = getClient().sendRequest(sendRequest);
            return buildSystemMessage(sendRequestResponse.getMessageMetadata(), sendRequestResponse.getSMEVSignature());
        } catch (Exception e) {
            log.severe("Unable to send request: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * Отправить результат обработки заявки
     */
    @Override
    public SystemMessage sendResponse(ProviderResponse request, HaunterLog providerLog) {
        MessagePrimaryContent messagePrimaryContent = new MessagePrimaryContent();
        messagePrimaryContent.setAny(request.data.personalData.data);

        XMLDSigSignatureType personalSignature = new XMLDSigSignatureType();
        personalSignature.setAny(assembleXmlSignature(request.data.personalData.signature));

        SenderProvidedResponseData senderProvidedResponseData = new SenderProvidedResponseData();
        senderProvidedResponseData.setAttachmentHeaderList(buildAttachmentHeader(request.data.enclosures));
        senderProvidedResponseData.setId(request.data.id);
        senderProvidedResponseData.setMessageID(request.data.messageID);
        senderProvidedResponseData.setMessagePrimaryContent(messagePrimaryContent);
        senderProvidedResponseData.setPersonalSignature(personalSignature);
        senderProvidedResponseData.setTo(request.data.to);

        if (!checkSignature(request.signature.getSignature())) {
            request.signature = sign(senderProvidedResponseData);
        }

        XMLDSigSignatureType callerSignature = new XMLDSigSignatureType();
        callerSignature.setAny(assembleXmlSignature(request.signature));

        SendResponseRequest sendResponseRequest = new SendResponseRequest();
        sendResponseRequest.setSenderProvidedResponseData(senderProvidedResponseData);
        sendResponseRequest.setCallerInformationSystemSignature(callerSignature);
        sendResponseRequest.setAttachmentContentList(buildAttachmentContent(request.data.enclosures));
        try {
            SendResponseResponse sendResponseResponse = getClient().sendResponse(sendResponseRequest);
            return buildSystemMessage(sendResponseResponse.getMessageMetadata(), sendResponseResponse.getSMEVSignature());
        } catch (Exception e) {
            log.severe("Unable to send request: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * Получить заявки на обработку
     */
    @Override
    public RequestMessage getRequest(MessageIdentifier messageIdentifier, HaunterLog providerLog) {
        MessageTypeSelector messageTypeSelector = buildMessageTypeSelector(messageIdentifier);

        messageIdentifier.signature = sign(messageTypeSelector);

        XMLDSigSignatureType callerSignature = new XMLDSigSignatureType();
        callerSignature.setAny(assembleXmlSignature(messageIdentifier.signature));

        GetRequestRequest getRequestRequest = new GetRequestRequest();
        getRequestRequest.setMessageTypeSelector(messageTypeSelector);
        getRequestRequest.setCallerInformationSystemSignature(callerSignature);
        try {
            return buildRequestMessage(getClient().getRequest(getRequestRequest));
        } catch (Exception e) {
            log.severe("Unable to get request: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * Получить результат обработки заявки
     */
    @Override
    public ResponseMessage getResponse(MessageIdentifier messageIdentifier, HaunterLog consumerLog) {
        MessageTypeSelector messageTypeSelector = buildMessageTypeSelector(messageIdentifier);
        messageIdentifier.signature = sign(messageTypeSelector);

        XMLDSigSignatureType callerSignature = new XMLDSigSignatureType();
        callerSignature.setAny(xmlSignatureAssembler.assemble(messageIdentifier.signature));

        GetResponseRequest request = new GetResponseRequest();
        request.setMessageTypeSelector(messageTypeSelector);
        request.setCallerInformationSystemSignature(callerSignature);

        try {
            return buildResponseMessage(getClient().getResponse(request));
        } catch (Exception e) {
            log.severe("Unable to get response: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * Подтвердить получения сообщения
     */
    @Override
    public void ack(AcknowledgeRequest request) {
        AckTargetMessage targetMessage = new AckTargetMessage();
        targetMessage.setAccepted(request.data.accepted);
        targetMessage.setId(request.data.id);
        targetMessage.setValue(request.data.messageId);

        if (!checkSignature(request.signature.getSignature())) {
            request.signData = normalizeData(targetMessage);
            request.signature = sign(request.signData);
        }

        XMLDSigSignatureType callerInformationSignature = new XMLDSigSignatureType();
        callerInformationSignature.setAny(xmlSignatureAssembler.assemble(request.signature));

        AckRequest ack = new AckRequest();
        ack.setAckTargetMessage(targetMessage);
        ack.setCallerInformationSystemSignature(callerInformationSignature);

        try {
            getClient().ack(ack);
        } catch (Exception e) {
            log.severe("Unable to send AckRequest: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * Получить статус запроса
     */
    @Override
    public AsyncProcessingStatus getStatus(TimestampRequest date) {
        Timestamp timestamp = new Timestamp();
        timestamp.setId("TimestampId");
        timestamp.setValue(toXmlGregorianCalendar(date.data.timestamp));

        XmlSignature signature = sign(normalizeData(timestamp));
        XMLDSigSignatureType callerInformationSystem = new XMLDSigSignatureType();
        callerInformationSystem.setAny(xmlSignatureAssembler.assemble(signature));

        GetStatusRequest statusRequest = new GetStatusRequest();
        statusRequest.setTimestamp(timestamp);
        statusRequest.setCallerInformationSystemSignature(callerInformationSystem);
        try {
            return buildAsyncProcessingStatus(getClient().getStatus(statusRequest));
        } catch (Exception e) {
            log.severe("Unable to get async processing status: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * Получить статистику входящих очередей
     */
    @Override
    public List<QueueStatistic> getIncomingQueueStatistics(TimestampRequest request) {
        Timestamp timestamp = new Timestamp();
        timestamp.setId(request.data.id);
        timestamp.setValue(toXmlGregorianCalendar(request.data.timestamp));

        if (!checkSignature(request.signature.getSignature())) {
            request.signData = normalizeData(timestamp);
            request.signature = sign(request.signData);
        }

        XMLDSigSignatureType callerInformationSignature = new XMLDSigSignatureType();
        callerInformationSignature.setAny(xmlSignatureAssembler.assemble(request.signature));

        GetIncomingQueueStatisticsRequest statisticsRequest = new GetIncomingQueueStatisticsRequest();
        statisticsRequest.setTimestamp(timestamp);
        statisticsRequest.setCallerInformationSystemSignature(callerInformationSignature);

        try {
            return buildQueueStatistics(getClient().getIncomingQueueStatistics(statisticsRequest));
        } catch (Exception e) {
            log.severe("Unable to get incoming queue statistic: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public void prepare(String url, boolean test) {
        this.test = test;
        try {
            this.url = new URL(url);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("Duplicates")
    private SMEVMessageExchangePortType getClient() {
        if (client != null) {
            return client;
        }

        if (url == null) {
            synchronized (urlMonitor) {
                if (url == null) {
                    throw new RuntimeException("Клиент сервиса СМЭВ3 не сконфигурирован.");
                }
            }
        }

        SMEVMessageExchangeService service = new SMEVMessageExchangeService(url);
        SMEVMessageExchangePortType portType = service.getSMEVMessageExchangeEndpoint();
        SOAPBinding binding = (SOAPBinding) ((BindingProvider) portType).getBinding();
        binding.setMTOMEnabled(true);
        return portType;
    }

    private Element assembleXmlSignature(XmlSignature xmlSignature) {
        return xmlSignatureAssembler.assemble(xmlSignature);
    }

    private XmlSignature disassembleXmlSignature(Element element) {
        return xmlSignatureAssembler.disassemble(element);
    }

    @SuppressWarnings("Duplicates")
    private byte[] normalizeData(Object jaxb) {
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            normalizer.normalizeJaxb(jaxb, os);
            return os.toByteArray();
        } catch (Exception e) {
            log.severe("Can't normalize data: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("Duplicates")
    private SystemMessage buildSystemMessage(ru.codeinside.smev.v3.transport.api.v1_2.MessageMetadata metadata, XMLDSigSignatureType signatureType) {
        if (signatureType != null) {
            XmlSignature xmlSignature = disassembleXmlSignature(signatureType.getAny());
            byte[] signData = normalizeData(metadata);

            if (!validateSignature(xmlSignature.getSignature(), new ByteArrayInputStream(signData))) {
                throw new RuntimeException("Ответ из СМЭВ не прошел валидацию подписи.");
            }
        }

        SystemMessage systemMessage = new SystemMessage();
        systemMessage.data = buildMessageMetadata(metadata);

        return systemMessage;
    }

    private boolean validateSignature(Signature signature, InputStream data) {
        return gostR3410.validate(signature.certificate, data, signature.sign);
    }

    @SuppressWarnings("Duplicates")
    private XmlSignature sign(Object jaxbToSign) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            normalizer.normalizeJaxb(jaxbToSign, os);
            Signature sign = gostR3410.sign(new ByteArrayInputStream(os.toByteArray()));
            return new XmlSignature(sign);
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (SAXException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("Duplicates")
    private MessageMetadata buildMessageMetadata(ru.codeinside.smev.v3.transport.api.v1_2.MessageMetadata messageMetadata) {
        MessageMetadata result = new MessageMetadata();
        // TODO: 23.10.15 добавился идентификтор сообщения в версии 1.2
       //result.messageId = messageMetadata.getMessageId();
        result.deliveryTime = fromXmlCalendarToDate(messageMetadata.getDeliveryTimestamp());
        result.destinationName = messageMetadata.getDestinationName();
        if (messageMetadata.getSupplementaryData() != null) {
            result.detectedContentTypeName = messageMetadata.getSupplementaryData().getDetectedContentTypeName();
            result.interactionType = messageMetadata.getSupplementaryData().getInteractionType().value();
        }
        result.id = messageMetadata.getId();
        if (messageMetadata.getMessageType() != null) {
            result.messageType = MessageMetadata.MessageType.valueOf(messageMetadata.getMessageType().value());
        }
        result.recipient = buildInfoSystem(messageMetadata.getRecipient());
        result.sender = buildInfoSystem(messageMetadata.getSender());
        result.sendTime = fromXmlCalendarToDate(messageMetadata.getSendingTimestamp());
        return result;
    }

    private InfoSystem buildInfoSystem(ru.codeinside.smev.v3.transport.api.v1_2.MessageMetadata.Recipient recipient) {
        if (recipient == null) {
            return null;
        }
        return getInfoSystem(recipient.getHumanReadableName(), recipient.getMnemonic());
    }

    private InfoSystem buildInfoSystem(ru.codeinside.smev.v3.transport.api.v1_2.MessageMetadata.Sender sender) {
        if (sender == null) {
            return null;
        }
        return getInfoSystem(sender.getHumanReadableName(), sender.getMnemonic());
    }

    private InfoSystem getInfoSystem(String name, String code) {
        return new InfoSystem(code, name);
    }

    @SuppressWarnings("Duplicates")
    private Date fromXmlCalendarToDate(XMLGregorianCalendar xmlCalendar) {
        if (xmlCalendar == null) {
            return null;
        }

        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, xmlCalendar.getYear());
        c.set(Calendar.MONTH, xmlCalendar.getMonth());
        c.set(Calendar.DATE, xmlCalendar.getDay());
        c.set(Calendar.HOUR_OF_DAY, xmlCalendar.getHour());
        c.set(Calendar.MINUTE, xmlCalendar.getMinute());
        c.set(Calendar.SECOND, xmlCalendar.getSecond());
        return c.getTime();
    }

    @SuppressWarnings("Duplicates")
    private AttachmentHeaderList buildAttachmentHeader(List<Enclosure> enclosures) {
        AttachmentHeaderList headerList = new AttachmentHeaderList();
        for (Enclosure enclosure : enclosures) {
            AttachmentHeaderType headerType = new AttachmentHeaderType();
            headerType.setContentId(enclosure.id);
            headerType.setMimeType(enclosure.mimeType);
            headerType.setSignaturePKCS7(toPKCS7(enclosure.signature));
            headerList.getAttachmentHeader().add(headerType);
        }
        return headerList;
    }

    @SuppressWarnings("Duplicates")
    private AttachmentContentList buildAttachmentContent(List<Enclosure> enclosures) {
        AttachmentContentList contentList = new AttachmentContentList();
        for (Enclosure enclosure : enclosures) {
            DataSource ds = new StreamDataSource(enclosure.getContent(), enclosure.fileName, enclosure.mimeType);
            DataHandler handler = new DataHandler(ds);
            AttachmentContentType contentType = new AttachmentContentType();
            contentType.setId(enclosure.id);
            contentType.setContent(handler);
            contentList.getAttachmentContent().add(contentType);
        }
        return contentList;
    }

    private byte[] readContent(InputStream content) {
        byte[] buff = new byte[1024], result = new byte[0];
        int count, total = 0;
        try {
            while ((count = content.read(buff)) > -1) {
                total += count;
                byte[] tmp = new byte[total];
                System.arraycopy(result, 0, tmp, 0, result.length);
                System.arraycopy(buff, 0, tmp, result.length, count);
                result = tmp;
            }
            return result;
        } catch (IOException e) {
            log.severe("Unable to read content from attachment: " + e.getMessage());
            throw new RuntimeException(e);
        } finally {
            try {
                content.close();
            } catch (IOException e) {
                log.severe("Unable to close attachment InputStream: " + e.getMessage());
            }
        }
    }

    private byte[] toPKCS7(Signature signature) {
        return signatureAssembler.assemble(signature);
    }


    private XMLGregorianCalendar toXmlGregorianCalendar(GregorianCalendar timestamp) {
        try {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(timestamp);
        } catch (DatatypeConfigurationException e) {
            log.severe("Unable to initialize DatatypeFactory: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }


    private List<QueueStatistic> buildQueueStatistics(GetIncomingQueueStatisticsResponse incomingQueueStatistics) {
        if (incomingQueueStatistics.getQueueStatistics() == null) {
            return Collections.emptyList();

        }
        List<QueueStatistic> result = new ArrayList<QueueStatistic>(incomingQueueStatistics.getQueueStatistics().size());
        for (GetIncomingQueueStatisticsResponse.QueueStatistics item : incomingQueueStatistics.getQueueStatistics()) {
            QueueStatistic statistic = new QueueStatistic();
            statistic.queueName = item.getQueueName();
            statistic.pendingMessageNumber = item.getPendingMessageNumber();
            result.add(statistic);
        }

        return result;
    }

    private AsyncProcessingStatus buildAsyncProcessingStatus(GetStatusResponse status) {
        SmevAsyncProcessingMessage smevAsyncProcessingMessage = status.getSmevAsyncProcessingMessage();
        XMLDSigSignatureType xmlSmevSignatureType = smevAsyncProcessingMessage.getSMEVSignature();
        XmlSignature smevSignature = xmlSignatureAssembler.disassemble(xmlSmevSignatureType.getAny());

        byte[] normalizeData = normalizeData(smevAsyncProcessingMessage.getAsyncProcessingStatusData());
        if (!validateSignature(smevSignature.getSignature(), new ByteArrayInputStream(normalizeData))) {
            throw new RuntimeException("Ответ из СМЭВ не прошел валидацию подписи.");
        }

        AsyncProcessingStatusData asyncProcessingStatusData = smevAsyncProcessingMessage.getAsyncProcessingStatusData();
        ru.codeinside.smev.v3.transport.api.v1_2.AsyncProcessingStatus asyncProcessingStatus = asyncProcessingStatusData.getAsyncProcessingStatus();

        SmevFault smevFault = new SmevFault();
        smevFault.code = asyncProcessingStatus.getSmevFault().getCode();
        smevFault.description = asyncProcessingStatus.getSmevFault().getDescription();

        AsyncProcessingStatus result = new AsyncProcessingStatus();
        result.originalMessageId = asyncProcessingStatus.getOriginalMessageId();
        result.statusDetails = asyncProcessingStatus.getStatusDetails();
        result.statusCategory = InteractionStatus.InteractionStatusType.fromValue(asyncProcessingStatus.getStatusCategory().value());
        result.smevFault = smevFault;

        return result;
    }

    private MessageTypeSelector buildMessageTypeSelector(MessageIdentifier messageIdentifier) {
        MessageTypeSelector messageTypeSelector = new MessageTypeSelector();
        messageTypeSelector.setId(messageIdentifier.data.id);
        messageTypeSelector.setNamespaceURI(messageIdentifier.data.namespaceURI);
        messageTypeSelector.setRootElementLocalName(messageIdentifier.data.rootElementLocalName);
        messageTypeSelector.setTimestamp(toXmlGregorianCalendar(messageIdentifier.data.timestamp));
        return messageTypeSelector;
    }

    private ResponseMessage buildResponseMessage(GetResponseResponse getResponse) {
        XmlSignature smevSignature = xmlSignatureAssembler.disassemble(
                getResponse.getResponseMessage().getSMEVSignature().getAny());
        byte[] normalizedBytes = normalizeData(getResponse.getResponseMessage().getResponse());
        if (!validateSignature(smevSignature.getSignature(), new ByteArrayInputStream(normalizedBytes))) {
            throw new RuntimeException("Ответ СМЭВ не прошел валидацию подписи");
        }

        Response response = new Response();
        response.signature = smevSignature;
        response.data = buildResponseData(getResponse.getResponseMessage());

        ResponseMessage result = new ResponseMessage();
        result.response = response;

        return result;
    }

    private ResponseData buildResponseData(GetResponseResponse.ResponseMessage responseMessage) {
        ru.codeinside.smev.v3.transport.api.v1_2.Response response = responseMessage.getResponse();

        ResponseData result = new ResponseData();
        result.messageMetadata = buildMessageMetadata(response.getMessageMetadata());
        result.originalMessageId = response.getOriginalMessageId();
        result.referenceMessageID = response.getReferenceMessageID();
        result.id = response.getId();
        result.providerResponse = buildProviderResponse(
                response.getSenderProvidedResponseData(), response.getSenderInformationSystemSignature(),
                responseMessage.getAttachmentContentList());

        return result;
    }

    private ProviderResponse buildProviderResponse(
            SenderProvidedResponseData senderProvidedResponseData,
            XMLDSigSignatureType signatureType,
            AttachmentContentList attachmentContentList) {

        ProviderResponse result = new ProviderResponse();
        result.signature = xmlSignatureAssembler.disassemble(signatureType.getAny());
        result.signData = normalizeData(senderProvidedResponseData);
        result.data = buildProviderResponseData(senderProvidedResponseData, attachmentContentList);

        return result;
    }

    private ProviderResponseData buildProviderResponseData(
            SenderProvidedResponseData senderProvidedResponseData, AttachmentContentList attachmentContentList) {
        ProviderResponseData result = new ProviderResponseData();
        result.id = senderProvidedResponseData.getId();
        result.to = senderProvidedResponseData.getTo();
        result.messageID = senderProvidedResponseData.getMessageID();
        result.requestRejected = buildRequestRejected(senderProvidedResponseData.getRequestRejected());
        result.personalData = buildPersonalData(
                senderProvidedResponseData.getMessagePrimaryContent(), senderProvidedResponseData.getPersonalSignature());
        result.enclosures = buildAttachments(senderProvidedResponseData.getAttachmentHeaderList(), attachmentContentList);

        return result;
    }

    private List<RequestRejected> buildRequestRejected(List<SenderProvidedResponseData.RequestRejected> requestRejected) {
        if (requestRejected != null) {
            List<RequestRejected> result = new ArrayList<RequestRejected>(requestRejected.size());
            for (SenderProvidedResponseData.RequestRejected rejected : requestRejected) {
                RequestRejected r = new RequestRejected();
                r.rejectionReasonCode = RejectionCode.fromValue(rejected.getRejectionReasonCode().value());
                r.rejectionReasonDescription = rejected.getRejectionReasonDescription();
                result.add(r);
            }
            return result;
        }
        return Collections.emptyList();
    }

    private RequestMessage buildRequestMessage(GetRequestResponse response) {
        XmlSignature smevSignature = xmlSignatureAssembler.disassemble(
                response.getRequestMessage().getSMEVSignature().getAny());

        Request r = response.getRequestMessage().getRequest();
        byte[] normalizeData = normalizeData(r);
        if (!validateSignature(smevSignature.getSignature(), new ByteArrayInputStream(normalizeData))) {
            throw new RuntimeException("Ответ СМЭВ не прошел валидацию подписи.");
        }

        ru.codeinside.smev.v3.service.api.provider.Cancel cancel = new ru.codeinside.smev.v3.service.api.provider.Cancel();
        Cancel c = response.getRequestMessage().getCancel();
        cancel.id = c.getId();
        cancel.messageID = c.getMessageID();
        cancel.messageMetadata = buildMessageMetadata(c.getMessageMetadata());
        cancel.messageReference = buildMessageReference(c.getMessageReference(), c.getSenderInformationSystemSignature());

        ru.codeinside.smev.v3.service.api.provider.Request request = new ru.codeinside.smev.v3.service.api.provider.Request();
        request.data = buildRequestData(r, response.getRequestMessage().getAttachmentContentList());
        request.signData = normalizeData;
        request.signature = xmlSignatureAssembler.disassemble(r.getSenderInformationSystemSignature().getAny());

        RequestMessage result = new RequestMessage();
        result.cancel = cancel;
        result.request = request;

        return result;
    }

    private RequestData buildRequestData(Request request, AttachmentContentList attachmentContentList) {
        RequestData result = new RequestData();
        result.id = request.getId();
        result.replyTo = request.getReplyTo();
        result.messageMetadata = buildMessageMetadata(request.getMessageMetadata());
        result.organizationRequest = buildOrganizationRequest(
                request.getSenderProvidedRequestData(), request.getSenderInformationSystemSignature(), attachmentContentList);

        return result;
    }

    private OrganizationRequest buildOrganizationRequest(
            SenderProvidedRequestData senderProvidedRequestData,
            XMLDSigSignatureType signatureType,
            AttachmentContentList attachmentContentList) {
        OrganizationRequest result = new OrganizationRequest();
        result.data = buildOrganizationRequestData(senderProvidedRequestData, attachmentContentList);
        result.signature = xmlSignatureAssembler.disassemble(signatureType.getAny());
        result.signData = normalizeData(senderProvidedRequestData);

        return result;
    }

    private OrganizationRequestData buildOrganizationRequestData(
            SenderProvidedRequestData senderProvidedRequestData, AttachmentContentList attachmentContentList) {
        OrganizationRequestData result = new OrganizationRequestData();
        result.id = senderProvidedRequestData.getId();
        result.messageID = senderProvidedRequestData.getMessageID();
        result.metadata = senderProvidedRequestData.getBusinessProcessMetadata().getAny();
        result.testMode = senderProvidedRequestData.getTestMessage() != null;
        result.personalData = buildPersonalData(senderProvidedRequestData.getMessagePrimaryContent(), senderProvidedRequestData.getPersonalSignature());
        result.messageReferenceID = senderProvidedRequestData.getReferenceMessageID();
        result.enclosures = buildAttachments(senderProvidedRequestData.getAttachmentHeaderList(), attachmentContentList);

        return result;
    }

    private List<Enclosure> buildAttachments(AttachmentHeaderList attachmentHeaderList, AttachmentContentList attachmentContentList) {
        if (attachmentHeaderList != null && attachmentContentList != null) {
            List<AttachmentHeaderType> headers = attachmentHeaderList.getAttachmentHeader();
            List<AttachmentContentType> contentTypes = attachmentContentList.getAttachmentContent();
            List<Enclosure> result = new ArrayList<Enclosure>(headers.size());

            for (int i = 0; i < headers.size(); ++i) {
                AttachmentHeaderType header = headers.get(i);
                AttachmentContentType content = contentTypes.get(i);

                String fileName = content.getContent().getName();
                File enclosureContent = new DataContentProvider(content.getContent()).getFile();

                Enclosure enclosure = new Enclosure(fileName, fileName, enclosureContent);
                enclosure.signature = signatureAssembler.disassemble(header.getSignaturePKCS7());
                enclosure.id = header.getContentId();
                enclosure.mimeType = header.getMimeType();
            }

            return result;
        }
        return Collections.emptyList();
    }

    private PersonalData buildPersonalData(MessagePrimaryContent messagePrimaryContent, XMLDSigSignatureType signatureType) {
        PersonalData personalData = new PersonalData();
        personalData.data = messagePrimaryContent.getAny();
        personalData.signData = normalizeData(messagePrimaryContent);
        personalData.signature = xmlSignatureAssembler.disassemble(signatureType.getAny());
        return personalData;
    }

    private MessageReference buildMessageReference(
        ru.codeinside.smev.v3.transport.api.v1_2.MessageReference messageReference, XMLDSigSignatureType signatureType) {

        MessageReferenceData messageReferenceData = new MessageReferenceData();
        messageReferenceData.id = messageReference.getId();
        messageReferenceData.messageId = messageReference.getValue();

        MessageReference result = new MessageReference();
        result.signData = normalizeData(messageReference);
        result.signature = xmlSignatureAssembler.disassemble(signatureType.getAny());
        result.data = messageReferenceData;
        return result;
    }

    private boolean checkSignature(Signature signature) {
        return signature != null && signature.sign != null && signature.sign.length > 0;
    }

    /* OSGI's bind/unbind methods */

    @SuppressWarnings("unused")
    public void setGostR3410(GostR3410 gostR3410) {
        this.gostR3410 = gostR3410;
    }

    @SuppressWarnings("unused")
    public void removeGostR3410(GostR3410 gostR3410) {
        if (this.gostR3410 == gostR3410) {
            this.gostR3410 = null;
        }
    }

    @SuppressWarnings("unused")
    public void setXmlSignatureAssembler(XmlSignatureAssembler assembler) {
        this.xmlSignatureAssembler = assembler;
    }

    @SuppressWarnings("unused")
    public void removeXmlSignatureAssembler(XmlSignatureAssembler assembler) {
        if (this.xmlSignatureAssembler == assembler) {
            this.xmlSignatureAssembler = null;
        }
    }

    @SuppressWarnings("unused")
    public void setSignatureAssembler(SignatureAssembler assembler) {
        this.signatureAssembler = assembler;
    }

    @SuppressWarnings("unused")
    public void removeSignatureAssembler(SignatureAssembler assembler) {
        if (this.signatureAssembler == assembler) {
            this.signatureAssembler = null;
        }
    }

    @SuppressWarnings("unused")
    public void setXmlNormalizer(XmlNormalizer normalizer) {
        this.normalizer = normalizer;
    }

    @SuppressWarnings("unused")
    public void removeXmlNormalizer(XmlNormalizer normalizer) {
        if (this.normalizer == normalizer) {
            this.normalizer = null;
        }
    }
}
