package ru.codeinside.smev.v3.transport;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import ru.codeinside.gws.api.XmlNormalizer;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class Mocks {
    public static BundleContext mockContext(XmlNormalizer normalizerC14n) {
        ServiceReference serviceReference = mock(ServiceReference.class);

        BundleContext context = mock(BundleContext.class);
        when(context.getServiceReference(XmlNormalizer.class.getName())).thenReturn(serviceReference);
        when(context.getService(serviceReference)).thenReturn(normalizerC14n);
        return context;
    }
}
