package ru.codeinside.smev.v3.transport.v1_2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.codeinside.smev.v3.crypto.api.XmlSignature;
import ru.codeinside.smev.v3.crypto.bc.GostR3410Impl;
import ru.codeinside.smev.v3.crypto.sunpkcs7.SunSignatureAssembler;
import ru.codeinside.smev.v3.crypto.xmldsign.XmlSignatureAssemblerImpl;
import ru.codeinside.smev.v3.service.api.PersonalData;
import ru.codeinside.smev.v3.service.api.SystemMessage;
import ru.codeinside.smev.v3.service.api.consumer.ConsumerRequest;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequest;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequestData;
import ru.codeinside.smev.v3.transport.api.v1_2.*;
import ru.codeinside.smev.v3.transport.api.v1_2.Void;
import ru.codeinside.smev.v3.xml.XmlNormalizerImpl;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.UUID;

import static org.mockito.Mockito.*;

public class SmevTransportImplTest extends Assert {

  private GostR3410Impl gostR3410 = new GostR3410Impl();
  private SunSignatureAssembler signatureAssembler = new SunSignatureAssembler();
  private final XmlNormalizerImpl normalizer = new XmlNormalizerImpl();
  private final XmlSignatureAssemblerImpl xmlSignatureAssembler = new XmlSignatureAssemblerImpl();

  private SmevTransportImpl transport;
  private SMEVMessageExchangePortType mockPortType;

  @Before
  public void setUp() throws Exception {
    mockPortType = mock(SMEVMessageExchangePortType.class);

    transport = new SmevTransportImpl(mockPortType);
    transport.setGostR3410(gostR3410);
    transport.setSignatureAssembler(signatureAssembler);
    transport.setXmlNormalizer(normalizer);
    transport.setXmlSignatureAssembler(xmlSignatureAssembler);
  }

  @Test
  public void testSendRequest() throws Exception {
    Element fakePersonalData = fakePersonalData();

    ConsumerRequest consumerRequest = new ConsumerRequest();
    consumerRequest.setPersonalData(fakePersonalData);

    OrganizationRequestData personalRequest = createPersonalRequest(consumerRequest);
    personalRequest.personalData.signature.setSignature(gostR3410.sign(
            new ByteArrayInputStream(personalRequest.personalData.signData)));

    OrganizationRequest organizationRequest = createOrganizationRequest(personalRequest);
    organizationRequest.signature.setSignature(gostR3410.sign(new ByteArrayInputStream(organizationRequest.signData)));

    MessageMetadata messageMetadata = new MessageMetadata();
    messageMetadata.setId("Hello123");

    SendRequestResponse response = new SendRequestResponse();
    response.setMessageMetadata(messageMetadata);

    when(mockPortType.sendRequest(any(SendRequestRequest.class))).thenReturn(response);

    SystemMessage systemMessage = transport.sendRequest(organizationRequest, null);//TODO временно передаётся null, позже исправить
    assertEquals("Hello123", systemMessage.data.id);
  }

  @Test
  public void testSendResponse() throws Exception {

  }

  @Test
  public void testGetRequest() throws Exception {

  }

  @Test
  public void testGetResponse() throws Exception {

  }

  @Test
  public void testAck() throws Exception {

  }

  @Test
  public void testGetStatus() throws Exception {

  }

  @Test
  public void testGetIncomingQueueStatistics() throws Exception {

  }

  public OrganizationRequestData createPersonalRequest(ConsumerRequest requestData) {
    PersonalData personalData = new PersonalData();
    personalData.data = requestData.getPersonalData();
    personalData.signData = normalizeData(requestData.getPersonalData());
    personalData.signature = new XmlSignature();
    OrganizationRequestData organizationRequestData = new OrganizationRequestData();
    organizationRequestData.personalData = personalData;
    organizationRequestData.frguCode = requestData.getFrguCode();
    organizationRequestData.enclosures = requestData.getEnclosures();
    organizationRequestData.metadata = requestData.getMetadata();
    organizationRequestData.testMode = true;
    organizationRequestData.messageID = UUID.randomUUID().toString();
    organizationRequestData.id = UUID.randomUUID().toString();
    return organizationRequestData;
  }

  public OrganizationRequest createOrganizationRequest(OrganizationRequestData requestData) {
    OrganizationRequest request = new OrganizationRequest();
    request.data = requestData;
    request.signData = normalizeData(getProvidedRequestData(requestData));
    request.signature = new XmlSignature();
    return request;
  }

  private byte[] normalizeData(Object object) {
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    if (object instanceof Element) {
      normalizer.normalizeDom((Element) object, os);
    } else {
      try {
        normalizer.normalizeJaxb(object, os);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return os.toByteArray();
  }

  private SenderProvidedRequestData getProvidedRequestData(OrganizationRequestData requestData) {
    MessagePrimaryContent messagePrimaryContent = new MessagePrimaryContent();
    messagePrimaryContent.setAny(requestData.personalData.data);

    SenderProvidedRequestData senderData = new SenderProvidedRequestData();
    senderData.setMessageID(requestData.messageID);
    senderData.setMessagePrimaryContent(messagePrimaryContent);

    Element assemble = xmlSignatureAssembler.assemble(requestData.personalData.signature);
    XMLDSigSignatureType signatureType = new XMLDSigSignatureType();
    signatureType.setAny(assemble);

    senderData.setPersonalSignature(signatureType);
    if (requestData.testMode) {
      senderData.setTestMessage(new Void());
    }
    return senderData;
  }

  private Element fakePersonalData() {
    FakeData fakeData = new FakeData();
    fakeData.id = 1L;
    fakeData.text = "My very long message";

    try {
      DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      Document doc = docBuilder.newDocument();
      JAXBContext context = JAXBContext.newInstance(FakeData.class);
      Marshaller marshaller = context.createMarshaller();
      marshaller.marshal(fakeData, doc);
      return doc.getDocumentElement();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @XmlRootElement(name = "FakeData", namespace = "http://fake-data.com")
  private static class FakeData {
    @XmlElement
    public Long id;

    @XmlElement
    public String text;
  }
}