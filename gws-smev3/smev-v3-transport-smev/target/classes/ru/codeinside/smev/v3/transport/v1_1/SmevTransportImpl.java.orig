package ru.codeinside.smev.v3.transport.v1_1;

import org.w3c.dom.Element;
import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.gws.api.InfoSystem;
import ru.codeinside.gws.api.Signature;
import ru.codeinside.smev.v3.crypto.api.*;
import ru.codeinside.smev.v3.service.api.*;
import ru.codeinside.smev.v3.service.api.AsyncProcessingStatus;
import ru.codeinside.smev.v3.service.api.MessageMetadata;
import ru.codeinside.smev.v3.service.api.MessageReference;
import ru.codeinside.smev.v3.service.api.consumer.*;
import ru.codeinside.smev.v3.service.api.consumer.Response;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponse;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseData;
import ru.codeinside.smev.v3.service.api.provider.RequestData;
import ru.codeinside.smev.v3.service.api.provider.RequestMessage;
import ru.codeinside.smev.v3.transport.DataContentProvider;
import ru.codeinside.smev.v3.transport.FTP;
import ru.codeinside.smev.v3.transport.api.SmevTransport;
import ru.codeinside.smev.v3.transport.api.SmevTypesProvider;
import ru.codeinside.smev.v3.transport.api.v1_1.*;
import ru.codeinside.smev.v3.transport.api.v1_1.MessageMetadata.SupplementaryData;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import javax.xml.ws.soap.SOAPBinding;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings({"PackageAccessibility", "Duplicates"})
public class SmevTransportImpl implements SmevTransport {

    private final Logger log = Logger.getLogger(SmevTransportImpl.class.getName());
    private final Object urlMonitor = new Object();

    private SMEVMessageExchangePortType client;
    private GostR3411 gostR3411;
    private XmlNormalizer normalizer;
    private SmevTypesProvider typesProvider;
    private XmlSignatureAssembler xmlSignatureAssembler;
    private SignatureAssembler signatureAssembler;
    private FTPStorage ftpStorage;
    private GostR3410 gostR3410;

    private volatile URL url;
    private volatile boolean test;

    @SuppressWarnings("unused")
    public SmevTransportImpl() {
    }

    @SuppressWarnings("unused")
    SmevTransportImpl(SMEVMessageExchangePortType client) {
        this.client = client;
    }

    @Override
    public SystemMessage sendRequest(OrganizationRequest request, final HaunterLog consumerLog) {
        SendRequestRequest sendRequestRequest = typesProvider.sendRequestRequest(request, test);
        try {
            SendRequestResponse sendRequestResponse = getClient(consumerLog).sendRequest(sendRequestRequest);
            return buildSystemMessage(sendRequestResponse.getMessageMetadata(), sendRequestResponse.getSMEVSignature(), consumerLog);
        } catch (Exception e) {
            log.severe("Unable to send request: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public SystemMessage sendResponse(ProviderResponse request, HaunterLog providerLog) {
        SendResponseRequest sendResponseRequest = typesProvider.sendResponseRequest(request);
        try {
            SendResponseResponse sendResponseResponse = getClient(providerLog).sendResponse(sendResponseRequest);
            return buildSystemMessage(sendResponseResponse.getMessageMetadata(), sendResponseResponse.getSMEVSignature(), providerLog);
        } catch (Exception e) {
            log.severe("Unable to send request: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public RequestMessage getRequest(MessageIdentifier messageIdentifier, HaunterLog providerLog) {
        GetRequestRequest getRequestRequest = typesProvider.getRequestRequest(messageIdentifier);
        try {
            return buildRequestMessage(getClient(providerLog).getRequest(getRequestRequest), providerLog);
        } catch (Exception e) {
            log.severe("Unable to get request: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public void prepare(String url, boolean test) {
        this.test = test;
        try {
            this.url = new URL(url);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ResponseMessage getResponse(MessageIdentifier messageIdentifier, HaunterLog consumerLog) {
        GetResponseRequest request = typesProvider.getResponseRequest(messageIdentifier);
        try {
            return buildResponseMessage(getClient(consumerLog).getResponse(request), consumerLog);
        } catch (Exception e) {
            log.severe("Unable to get response: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public void ack(AcknowledgeRequest request) {
        AckRequest ack = typesProvider.ackRequest(request);
        try {
            getClient(null).ack(ack);
        } catch (Exception e) {
            log.severe("Unable to send AckRequest: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public AsyncProcessingStatus getStatus(TimestampRequest request) {
        GetStatusRequest statusRequest = typesProvider.getStatusRequest(request);
        try {
            return buildAsyncProcessingStatus(getClient(null).getStatus(statusRequest));
        } catch (Exception e) {
            log.severe("Unable to get async processing status: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<QueueStatistic> getIncomingQueueStatistics(TimestampRequest request) {
        GetIncomingQueueStatisticsRequest statisticsRequest = typesProvider.getIncomingQueueStatisticsRequest(request);
        try {
            return buildQueueStatistics(getClient(null).getIncomingQueueStatistics(statisticsRequest));
        } catch (Exception e) {
            log.severe("Unable to get incoming queue statistic: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("Duplicates")
    private RequestMessage buildRequestMessage(GetRequestResponse response, HaunterLog providerLog) {
        GetRequestResponse.RequestMessage requestMessage = response.getRequestMessage();
        if (requestMessage == null) {
            return null;
        }
        XmlSignature smevSignature = xmlSignatureAssembler.disassemble(requestMessage.getSMEVSignature().getAny());

        Request r = requestMessage.getRequest();
        ru.codeinside.smev.v3.service.api.provider.Request request = null;
        if (r != null) {
            normalizeData(r);
            /*
            валидацию пропустил, т.к. в нее надо отдавать не GetRequestResponse.getRequestMessage().getRequest()
            а парой уровней выше, весь соап конверт начиная с <Envelope/>
            */
            
//            if (!validateSignature(smevSignature.getSignature(), normalizeData(r))) {
//                throw new RuntimeException("Ответ СМЭВ не прошел валидацию подписи.");
//            }
            request = new ru.codeinside.smev.v3.service.api.provider.Request();
            request.data = buildRequestData(r, requestMessage.getAttachmentContentList());
            request.signData = normalizeData(r);
            request.signature = xmlSignatureAssembler.disassemble(r.getSenderInformationSystemSignature().getAny());
            providerLog.addData(request.data.messageMetadata);
        }

        ru.codeinside.smev.v3.service.api.provider.Cancel cancel = null;
        if (requestMessage.getCancel() != null) {
            cancel = new ru.codeinside.smev.v3.service.api.provider.Cancel();
            Cancel c = requestMessage.getCancel();
            cancel.id = c.getId();
            cancel.messageID = c.getMessageID();
            cancel.messageMetadata = buildMessageMetadata(c.getMessageMetadata());
            cancel.messageReference = buildMessageReference(c.getMessageReference(), c.getSenderInformationSystemSignature());
            providerLog.addData(cancel.messageMetadata);
        }

        RequestMessage result = new RequestMessage();
        result.cancel = cancel;
        result.request = request;

        return result;
    }

    @SuppressWarnings("Duplicates")
    private MessageReference buildMessageReference(
            ru.codeinside.smev.v3.transport.api.v1_1.MessageReference messageReference, XMLDSigSignatureType signatureType) {

        MessageReferenceData messageReferenceData = new MessageReferenceData();
        messageReferenceData.id = messageReference.getId();
        messageReferenceData.messageId = messageReference.getValue();

        MessageReference result = new MessageReference();
        result.signData = normalizeData(messageReference);
        result.signature = xmlSignatureAssembler.disassemble(signatureType.getAny());
        result.data = messageReferenceData;
        return result;
    }

    @SuppressWarnings("Duplicates")
    private RequestData buildRequestData(Request request, AttachmentContentList attachmentContentList) {
        RequestData result = new RequestData();
        result.id = request.getId();
        result.replyTo = request.getReplyTo();
        result.messageMetadata = buildMessageMetadata(request.getMessageMetadata());
        result.organizationRequest = buildOrganizationRequest(
                request.getSenderProvidedRequestData(),
                request.getSenderInformationSystemSignature(),
                attachmentContentList,
                request.getFSAttachmentsList());
        return result;
    }

    @SuppressWarnings("Duplicates")
    private OrganizationRequest buildOrganizationRequest(
            SenderProvidedRequestData senderProvidedRequestData,
            XMLDSigSignatureType signatureType,
            AttachmentContentList attachmentContentList,
            FSAttachmentsList fsAttachmentsList) {
        OrganizationRequest result = new OrganizationRequest();
        result.data = buildOrganizationRequestData(senderProvidedRequestData, attachmentContentList, fsAttachmentsList);
        result.signature = xmlSignatureAssembler.disassemble(signatureType.getAny());
        result.signData = normalizeData(senderProvidedRequestData);

        return result;
    }

    private OrganizationRequestData buildOrganizationRequestData(
            SenderProvidedRequestData senderProvidedRequestData,
            AttachmentContentList attachmentContentList,
            FSAttachmentsList fsAttachmentsList) {

        OrganizationRequestData result = new OrganizationRequestData();
        result.id = senderProvidedRequestData.getId();
        result.messageID = senderProvidedRequestData.getMessageID();

        SenderProvidedRequestData.BusinessProcessMetadata businessProcessMetadata =
                senderProvidedRequestData.getBusinessProcessMetadata();
        if (businessProcessMetadata != null && businessProcessMetadata.getAny() != null) {
            for (Object object : businessProcessMetadata.getAny())
                result.metadata.add((Element) object);
        }
        result.testMode = senderProvidedRequestData.getTestMessage() != null;
        result.personalData = buildPersonalData(senderProvidedRequestData.getMessagePrimaryContent(), senderProvidedRequestData.getPersonalSignature());
        if (fsAttachmentsList != null && fsAttachmentsList.getFSAttachment() != null) {
            result.enclosures = downloadFiles(senderProvidedRequestData.getRefAttachmentHeaderList(), fsAttachmentsList);
        } else {
            result.enclosures = buildAttachments(senderProvidedRequestData.getAttachmentHeaderList(), attachmentContentList);
        }
        return result;
    }

    @SuppressWarnings("Duplicates")
    private PersonalData buildPersonalData(MessagePrimaryContent messagePrimaryContent, XMLDSigSignatureType signatureType) {
        PersonalData personalData = new PersonalData();
        personalData.data = messagePrimaryContent.getAny();
        personalData.signData = normalizeData(messagePrimaryContent);
        if (signatureType != null) {
            personalData.signature = xmlSignatureAssembler.disassemble(signatureType.getAny());
        }
        return personalData;
    }

    @SuppressWarnings("Duplicates")
    private List<Enclosure> buildAttachments(AttachmentHeaderList attachmentHeaderList, AttachmentContentList attachmentContentList) {
        if (attachmentHeaderList != null && attachmentContentList != null) {
            List<AttachmentHeaderType> headers = attachmentHeaderList.getAttachmentHeader();
            List<AttachmentContentType> contentTypes = attachmentContentList.getAttachmentContent();
            List<Enclosure> result = new ArrayList<Enclosure>(headers.size());

            for (int i = 0; i < headers.size(); ++i) {
                AttachmentHeaderType header = headers.get(i);
                AttachmentContentType content = contentTypes.get(i);

                String fileName = content.getId();
                File enclosureContent = new DataContentProvider(content.getContent()).getFile();

                Enclosure enclosure = new Enclosure(fileName, fileName, enclosureContent);
                enclosure.id = fileName;
                enclosure.signature = signatureAssembler.disassemble(header.getSignaturePKCS7());
                enclosure.number = header.getContentId();
                enclosure.mimeType = header.getMimeType();
                result.add(enclosure);
            }

            return result;
        }
        return Collections.emptyList();
    }

    @SuppressWarnings("Duplicates")
    private ResponseMessage buildResponseMessage(GetResponseResponse getResponse, HaunterLog consumerLog) {
        if (getResponse == null || getResponse.getResponseMessage() == null) {
            return null;
        }
        XmlSignature smevSignature = xmlSignatureAssembler.disassemble(
                getResponse.getResponseMessage().getSMEVSignature().getAny());
        if (!validateSignature(smevSignature.getSignature(), normalizeData(getResponse.getResponseMessage().getResponse()))) {
            throw new RuntimeException("Ответ СМЭВ не прошел валидацию подписи.");
        }

        Response response = new Response();
        response.signature = smevSignature;
        response.data = buildResponseData(getResponse.getResponseMessage());
        consumerLog.addData(response.data.messageMetadata);

        ResponseMessage result = new ResponseMessage();
        result.response = response;

        return result;
    }

    private ResponseData buildResponseData(GetResponseResponse.ResponseMessage responseMessage) {
        ru.codeinside.smev.v3.transport.api.v1_1.Response response = responseMessage.getResponse();

        ResponseData result = new ResponseData();
        result.messageMetadata = buildMessageMetadata(response.getMessageMetadata());
        result.originalMessageId = response.getOriginalMessageId();
        result.id = response.getId();
        result.providerResponse = buildProviderResponse(
                response.getSenderProvidedResponseData(),
                response.getSenderInformationSystemSignature(),
                responseMessage.getAttachmentContentList(),
                response.getFSAttachmentsList());

        return result;
    }

    @SuppressWarnings("Duplicates")
    private ProviderResponse buildProviderResponse(
            SenderProvidedResponseData senderProvidedResponseData,
            XMLDSigSignatureType signatureType,
            AttachmentContentList attachmentContentList,
            FSAttachmentsList fsAttachmentsList) {

        ProviderResponse result = new ProviderResponse();
        result.signature = xmlSignatureAssembler.disassemble(signatureType.getAny());
        result.signData = normalizeData(senderProvidedResponseData);
        result.data = buildProviderResponseData(senderProvidedResponseData, attachmentContentList, fsAttachmentsList);

        return result;
    }

    @SuppressWarnings("Duplicates")
    private ProviderResponseData buildProviderResponseData(
            SenderProvidedResponseData senderProvidedResponseData,
            AttachmentContentList attachmentContentList,
            FSAttachmentsList fsAttachmentsList) {
        ProviderResponseData result = new ProviderResponseData();
        result.id = senderProvidedResponseData.getId();
        result.to = senderProvidedResponseData.getTo();
        result.messageID = senderProvidedResponseData.getMessageID();
        result.requestRejected = buildRequestRejected(senderProvidedResponseData.getRequestRejected());
        if (senderProvidedResponseData.getMessagePrimaryContent() != null) {
            result.personalData = buildPersonalData(
                    senderProvidedResponseData.getMessagePrimaryContent(), senderProvidedResponseData.getPersonalSignature());
        }
        if (fsAttachmentsList != null && fsAttachmentsList.getFSAttachment() != null) {
            result.enclosures = downloadFiles(senderProvidedResponseData.getRefAttachmentHeaderList(), fsAttachmentsList);
        } else {
            result.enclosures = buildAttachments(senderProvidedResponseData.getAttachmentHeaderList(), attachmentContentList);
        }
        return result;
    }

    private List<RequestRejected> buildRequestRejected(List<SenderProvidedResponseData.RequestRejected> requestRejected) {
        if (requestRejected != null) {
            List<RequestRejected> result = new ArrayList<RequestRejected>(requestRejected.size());
            for (SenderProvidedResponseData.RequestRejected rejected : requestRejected) {
                RequestRejected r = new RequestRejected();
                r.rejectionReason = rejected.getRejectionReason();
                r.rejectionReasonCode = rejected.getRejectionReasonCode();
                result.add(r);
            }
            return result;
        }
        return Collections.emptyList();
    }

    private AsyncProcessingStatus buildAsyncProcessingStatus(GetStatusResponse status) {
        SmevAsyncProcessingMessage smevAsyncProcessingMessage = status.getSmevAsyncProcessingMessage();
        XMLDSigSignatureType xmlSmevSignatureType = smevAsyncProcessingMessage.getSMEVSignature();
        XmlSignature smevSignature = xmlSignatureAssembler.disassemble(xmlSmevSignatureType.getAny());

        if (!validateSignature(smevSignature.getSignature(), normalizeData(smevAsyncProcessingMessage.getAsyncProcessingStatus()))) {
            throw new RuntimeException("Ответ из СМЭВ не прошел валидацию подписи.");
        }

        ru.codeinside.smev.v3.transport.api.v1_1.AsyncProcessingStatus asyncProcessingStatus = smevAsyncProcessingMessage.getAsyncProcessingStatus();

        AsyncProcessingStatus result = new AsyncProcessingStatus();
        result.originalMessageId = asyncProcessingStatus.getOriginalMessageId();
        result.statusDetails = asyncProcessingStatus.getStatusDetails();
        result.statusCategory = InteractionStatus.InteractionStatusType.fromValue(asyncProcessingStatus.getStatusCategory());
        return result;
    }

    private List<QueueStatistic> buildQueueStatistics(GetIncomingQueueStatisticsResponse incomingQueueStatistics) {
        if (incomingQueueStatistics.getQueueStatistics() == null) {
            return Collections.emptyList();

        }
        List<QueueStatistic> result = new ArrayList<QueueStatistic>(incomingQueueStatistics.getQueueStatistics().size());
        for (GetIncomingQueueStatisticsResponse.QueueStatistics item : incomingQueueStatistics.getQueueStatistics()) {
            QueueStatistic statistic = new QueueStatistic();
            statistic.queueName = item.getQueueName();
            statistic.pendingMessageNumber = item.getPendingMessageNumber();
            result.add(statistic);
        }

        return result;
    }

    private List<Enclosure> downloadFiles(RefAttachmentHeaderList refAttachmentHeaderList, FSAttachmentsList fsAttachmentsList) {
        List<Enclosure> result = new ArrayList<Enclosure>(refAttachmentHeaderList.getRefAttachmentHeader().size());
        FTPCredentials credentials = FTP.getFtpCredentials();
        String server = credentials.getServer();
        int port = credentials.getPort();

        for (RefAttachmentHeaderType header : refAttachmentHeaderList.getRefAttachmentHeader()) {
            FSAuthInfo authInfo = linkAuthInfo(header.getUuid(), fsAttachmentsList);
            if (authInfo != null) {
                FTPCredentials c = new FTPCredentials(server, port, authInfo.getUserName(), authInfo.getPassword());
                OutputStream content = null;
                try {
                    File tmp = File.createTempFile("ftp-", "");
                    content = new BufferedOutputStream(new FileOutputStream(tmp));
                    //ftpStorage.download(c, header.getUuid(), authInfo.getFileName(), content);
                    Enclosure e = new Enclosure(authInfo.getFileName(), authInfo.getFileName(), tmp);
                    e.id = header.getUuid();
                    e.mimeType = header.getMimeType();
                    e.number = header.getUuid();
                    e.signature = signatureAssembler.disassemble(header.getSignaturePKCS7());
                    result.add(e);
                } catch (IOException e) {
                    log.info("Не удалось загрузить вложение с FTP: " + e.getMessage());
                } finally {
                    if (content != null) {
                        try {
                            content.close();
                        } catch (IOException ignored) {
                        }
                    }
                }
            }
        }
        return result;
    }

    private FSAuthInfo linkAuthInfo(String uuid, FSAttachmentsList attachmentsList) {
        for (FSAuthInfo authInfo : attachmentsList.getFSAttachment()) {
            if (uuid.equals(authInfo.getUuid())) {
                return authInfo;
            }
        }
        return null;
    }

    @SuppressWarnings("Duplicates")
    private SMEVMessageExchangePortType getClient(final HaunterLog haunterLog) {
        if (client != null) {
            return client;
        }

        if (url == null) {
            synchronized (urlMonitor) {
                if (url == null) {
                    throw new RuntimeException("Клиент сервиса СМЭВ3 не сконфигурирован.");
                }
            }
        }

        SMEVMessageExchangeService service = new SMEVMessageExchangeService();
        SMEVMessageExchangePortType portType = service.getSMEVMessageExchangeEndpoint();
        SOAPBinding binding = (SOAPBinding) ((BindingProvider) portType).getBinding();
        Map<String, Object> requestContext = ((BindingProvider) portType).getRequestContext();
        requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url.toString());
        List<Handler> handlerChain = binding.getHandlerChain();
        handlerChain.add(new SOAPHandler<SOAPMessageContext>() {

            @Override
            public boolean handleMessage(SOAPMessageContext context) {
                try {
                    SOAPMessage soapMessage = context.getMessage();
                    String messageState = soapMessage.getSOAPBody().getFirstChild().getLocalName();
                    if (haunterLog != null) {
                        haunterLog.addOtherData(MsgDataField.messageState, messageState);
                        haunterLog.addData(messageState, MsgDataField.SOAPMessage, soapMessage);
                    }
                } catch (SOAPException e) {
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public boolean handleFault(SOAPMessageContext context) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try {
                    SOAPMessage soapMessage = context.getMessage();
                    String messageState = "BothBothResponse";
                    if (haunterLog != null) {
                        haunterLog.addData(messageState, MsgDataField.SOAPMessage, soapMessage);
                        haunterLog.addOtherData(MsgDataField.error, "Ошибка при обработке ответа от СМЭВ-3");
                    }
                    soapMessage.writeTo(bos);
                    log.info(bos.toString("UTF-8"));
                } catch (SOAPException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public void close(MessageContext context) {

            }

            @Override
            public Set<QName> getHeaders() {
                return null;
            }
        });
        binding.setHandlerChain(handlerChain);
        binding.setMTOMEnabled(true);
        return portType;
    }

    @SuppressWarnings("Duplicates")
    private SystemMessage buildSystemMessage(ru.codeinside.smev.v3.transport.api.v1_1.MessageMetadata metadata, XMLDSigSignatureType signatureType, HaunterLog consumerLog) {
        if (signatureType != null) {
            XmlSignature xmlSignature = disassembleXmlSignature(signatureType.getAny());
            byte[] signData = normalizeData(metadata);

            if (!validateSignature(xmlSignature.getSignature(), signData)) {
                try {
                    System.out.println(new String(signData, "UTF-8"));
                } catch (UnsupportedEncodingException ex) {
                    throw new RuntimeException("Ответ из СМЭВ не прошел валидацию подписи. И строка не разобралась в UTF!");
                }
                throw new RuntimeException("Ответ из СМЭВ не прошел валидацию подписи.");
                
            }
        }

        SystemMessage systemMessage = new SystemMessage();
        systemMessage.data = buildMessageMetadata(metadata);
        consumerLog.addData(systemMessage.data);
        return systemMessage;
    }

    boolean validateSignature(Signature signature, byte[] data) {
        byte[] digest = gostR3411.digest(new ByteArrayInputStream(data));
        return Arrays.equals(signature.getDigest(), digest)
                && gostR3410.validate(signature.certificate, signature.getContent(), signature.sign);
    }

    private XmlSignature disassembleXmlSignature(Element element) {
        return xmlSignatureAssembler.disassemble(element);
    }

    @SuppressWarnings("Duplicates")
    private byte[] normalizeData(Object jaxb) {
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            normalizer.normalizeJaxb(jaxb, os);
            return os.toByteArray();
        } catch (Exception e) {
            log.severe("Can't normalize data: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("Duplicates")
    private MessageMetadata buildMessageMetadata(ru.codeinside.smev.v3.transport.api.v1_1.MessageMetadata messageMetadata) {
        MessageMetadata result = new MessageMetadata();
        result.deliveryTime = fromXmlCalendarToDate(messageMetadata.getDeliveryTimestamp());
        result.destinationName = messageMetadata.getDestinationName();
        SupplementaryData supplementaryData = messageMetadata.getSupplementaryData();
        if (supplementaryData != null) {
            result.detectedContentTypeName = supplementaryData.getDetectedContentTypeName();
            if (supplementaryData.getInteractionType() != null) {
                result.interactionType = supplementaryData.getInteractionType().value();
            }
        }
        result.id = messageMetadata.getId();
        if (messageMetadata.getMessageType() != null) {
            result.messageType = MessageMetadata.MessageType.valueOf(messageMetadata.getMessageType().value());
        }
        result.recipient = buildInfoSystem(messageMetadata.getRecipient());
        result.sender = buildInfoSystem(messageMetadata.getSender());
        result.sendTime = fromXmlCalendarToDate(messageMetadata.getSendingTimestamp());
        if (messageMetadata.getStatus() != null) {
            result.interactionStatus = InteractionStatus.InteractionStatusType.fromValue(messageMetadata.getStatus().value());
        }
        return result;
    }

    private InfoSystem buildInfoSystem(ru.codeinside.smev.v3.transport.api.v1_1.MessageMetadata.Recipient recipient) {
        if (recipient == null) {
            return null;
        }
        return getInfoSystem(recipient.getHumanReadableName(), recipient.getMnemonic());
    }

    private InfoSystem buildInfoSystem(ru.codeinside.smev.v3.transport.api.v1_1.MessageMetadata.Sender sender) {
        if (sender == null) {
            return null;
        }
        return getInfoSystem(sender.getHumanReadableName(), sender.getMnemonic());
    }

    private InfoSystem getInfoSystem(String name, String code) {
        return new InfoSystem(code, name);
    }

    @SuppressWarnings("Duplicates")
    private Date fromXmlCalendarToDate(XMLGregorianCalendar xmlCalendar) {
        if (xmlCalendar == null) {
            return null;
        }

        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, xmlCalendar.getYear());
        c.set(Calendar.MONTH, xmlCalendar.getMonth());
        c.set(Calendar.DATE, xmlCalendar.getDay());
        c.set(Calendar.HOUR_OF_DAY, xmlCalendar.getHour());
        c.set(Calendar.MINUTE, xmlCalendar.getMinute());
        c.set(Calendar.SECOND, xmlCalendar.getSecond());
        return c.getTime();
    }

    /* OSGI's bind/unbind methods */

    @SuppressWarnings("unused")
    public void bindXmlSignatureAssembler(XmlSignatureAssembler assembler) {
        this.xmlSignatureAssembler = assembler;
    }

    @SuppressWarnings("unused")
    public void unbindXmlSignatureAssembler(XmlSignatureAssembler assembler) {
        if (this.xmlSignatureAssembler == assembler) {
            this.xmlSignatureAssembler = null;
        }
    }

    @SuppressWarnings("unused")
    public void unbindGostR3411(GostR3411 gostR3411) {
        if (this.gostR3411 == gostR3411) {
            this.gostR3411 = null;
        }
    }

    @SuppressWarnings("unused")
    public void bindGostR3411(GostR3411 gostR3411) {
        this.gostR3411 = gostR3411;
    }

    @SuppressWarnings("unused")
    public void bindXmlNormalizer(XmlNormalizer normalizer) {
        this.normalizer = normalizer;
    }

    @SuppressWarnings("unused")
    public void unbindXmlNormalizer(XmlNormalizer normalizer) {
        if (this.normalizer == normalizer) {
            this.normalizer = null;
        }
    }

    @SuppressWarnings("unused")
    public void unbindTypesProvider(SmevTypesProvider typesProvider) {
        if (this.typesProvider == typesProvider) {
            this.typesProvider = null;
        }
    }

    @SuppressWarnings("unused")
    public void bindTypesProvider(SmevTypesProvider typesProvider) {
        this.typesProvider = typesProvider;
    }

    @SuppressWarnings("unused")
    public void bindSignatureAssembler(SignatureAssembler assembler) {
        this.signatureAssembler = assembler;
    }

    @SuppressWarnings("unused")
    public void unbindSignatureAssembler(SignatureAssembler assembler) {
        if (this.signatureAssembler == assembler) {
            this.signatureAssembler = null;
        }
    }

    @SuppressWarnings("unused")
    public void bindFtpStorage(FTPStorage storage) {
        this.ftpStorage = storage;
    }

    @SuppressWarnings("unused")
    public void unbindFtpStorage(FTPStorage storage) {
        if (this.ftpStorage == storage) {
            this.ftpStorage = null;
        }
    }

    @SuppressWarnings("unused")
    public void bindGostR3410(GostR3410 gostR3410) {
        this.gostR3410 = gostR3410;
    }

    @SuppressWarnings("unused")
    public void unbindGostR3410(GostR3410 gostR3410) {
        if (this.gostR3410 == gostR3410) {
            this.gostR3410 = null;
        }
    }
}
