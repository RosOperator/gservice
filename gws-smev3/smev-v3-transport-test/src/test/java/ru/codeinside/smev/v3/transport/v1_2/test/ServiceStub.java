package ru.codeinside.smev.v3.transport.v1_2.test;

import ru.codeinside.smev.v3.transport.api.v1_2.*;
import ru.codeinside.smev.v3.transport.api.v1_2.Void;

import javax.jws.WebService;
import javax.xml.ws.soap.MTOM;
import java.util.PriorityQueue;
import java.util.Queue;

@MTOM(enabled = true)
@WebService(
        portName = "SMEVMessageExchangeEndpoint",
        serviceName = "SMEVMessageExchangeService",
        targetNamespace = "urn://x-artefacts-smev-gov-ru/services/message-exchange/1.2",
        endpointInterface = "ru.codeinside.smev.v3.transport.api.v1_2.SMEVMessageExchangePortType",
        wsdlLocation = "ru/codeinside/smev/v3/transport/api/v1_2/smev-message-exchange-service-1.2.wsdl")
public class ServiceStub implements SMEVMessageExchangePortType {

    private final Queue<GetResponseResponse> consumerQueue = new PriorityQueue<GetResponseResponse>(); // очередь поставщика
    private final Queue<GetRequestResponse> providerQueue = new PriorityQueue<GetRequestResponse>();   // очередь потребителя


    /**
     * Послать запрос.
     * Факт прихода запроса говорит о том, что СМЭВ удостоверился в том, что отправитель
     * имеет право на получение данных по этому типу запросов.
     * Дополнительный контроль доступа в ИС-поставщике данных запрещён.
     * Тип запроса идентифицируется полным именем (qualified name) элемента //SendRequestRequest/PrimaryContent/element().
     *
     * @param parameters
     * @return returns ru.codeinside.smev.v3.transport.v1_2.SendRequestResponse
     * @throws TransactionCodeInvalidException
     * @throws MessageIsAlreadySentException
     * @throws AttachmentContentMiscoordinationException
     * @throws RecipientIsNotFoundException
     * @throws BusinessDataTypeIsNotSupportedException
     * @throws SenderIsNotRegisteredException
     * @throws AccessDeniedException
     * @throws SMEVFailureException
     * @throws DestinationOverflowException
     * @throws SignatureVerificationFaultException
     * @throws AttachmentSizeLimitExceededException
     * @throws StaleMessageIdException
     * @throws QuoteLimitExceededException
     * @throws InvalidMessageIdFormatException
     * @throws EndOfLifeException
     * @throws InvalidContentException
     */
    @Override
    public SendRequestResponse sendRequest(SendRequestRequest parameters) throws AccessDeniedException, AttachmentContentMiscoordinationException, AttachmentSizeLimitExceededException, BusinessDataTypeIsNotSupportedException, DestinationOverflowException, EndOfLifeException, InvalidContentException, InvalidMessageIdFormatException, MessageIsAlreadySentException, QuoteLimitExceededException, RecipientIsNotFoundException, SMEVFailureException, SenderIsNotRegisteredException, SignatureVerificationFaultException, StaleMessageIdException, TransactionCodeInvalidException {
        buildRequest(parameters);
        return new SendRequestResponse();
    }

    /**
     * Дай сообщение из моей входящей очереди, если она не пуста.
     *
     * @param parameters
     * @return returns ru.codeinside.smev.v3.transport.v1_2.SendResponseResponse
     * @throws MessageIsAlreadySentException
     * @throws RecipientIsNotFoundException
     * @throws AttachmentContentMiscoordinationException
     * @throws BusinessDataTypeIsNotSupportedException
     * @throws SenderIsNotRegisteredException
     * @throws SMEVFailureException
     * @throws IncorrectResponseContentTypeException
     * @throws DestinationOverflowException
     * @throws SignatureVerificationFaultException
     * @throws AttachmentSizeLimitExceededException
     * @throws StaleMessageIdException
     * @throws QuoteLimitExceededException
     * @throws InvalidMessageIdFormatException
     * @throws InvalidContentException
     */
    @Override
    public SendResponseResponse sendResponse(SendResponseRequest parameters) throws AttachmentContentMiscoordinationException, AttachmentSizeLimitExceededException, BusinessDataTypeIsNotSupportedException, DestinationOverflowException, IncorrectResponseContentTypeException, InvalidContentException, InvalidMessageIdFormatException, MessageIsAlreadySentException, QuoteLimitExceededException, RecipientIsNotFoundException, SMEVFailureException, SenderIsNotRegisteredException, SignatureVerificationFaultException, StaleMessageIdException {
        buildResponse(parameters);
        return new SendResponseResponse();
    }

    /**
     * Дай сообщение из моей входящей очереди _запросов_, если она не пуста.
     *
     * @param parameters
     * @return returns ru.codeinside.smev.v3.transport.v1_2.GetRequestResponse
     * @throws UnknownMessageTypeException
     * @throws SenderIsNotRegisteredException
     * @throws SMEVFailureException
     * @throws SignatureVerificationFaultException
     * @throws InvalidContentException
     */
    @Override
    public GetRequestResponse getRequest(GetRequestRequest parameters) throws InvalidContentException, SMEVFailureException, SenderIsNotRegisteredException, SignatureVerificationFaultException, UnknownMessageTypeException {
        return providerQueue.poll();
    }

    /**
     * Дай сообщение из моей входящей очереди _запросов_, если она не пуста.
     *
     * @param parameters
     * @return returns ru.codeinside.smev.v3.transport.v1_2.GetStatusResponse
     * @throws UnknownMessageTypeException
     * @throws SenderIsNotRegisteredException
     * @throws SMEVFailureException
     * @throws SignatureVerificationFaultException
     * @throws InvalidContentException
     */
    @Override
    public GetStatusResponse getStatus(GetStatusRequest parameters) throws InvalidContentException, SMEVFailureException, SenderIsNotRegisteredException, SignatureVerificationFaultException, UnknownMessageTypeException {
        return null;
    }

    /**
     * Дай сообщение из моей входящей очереди _ответов_, если она не пуста.
     *
     * @param parameters
     * @return returns ru.codeinside.smev.v3.transport.v1_2.GetResponseResponse
     * @throws UnknownMessageTypeException
     * @throws SenderIsNotRegisteredException
     * @throws SMEVFailureException
     * @throws InvalidContentException
     * @throws SignatureVerificationFaultException
     */
    @Override
    public GetResponseResponse getResponse(GetResponseRequest parameters) throws InvalidContentException, SMEVFailureException, SenderIsNotRegisteredException, SignatureVerificationFaultException, UnknownMessageTypeException {
        return consumerQueue.poll();
    }

    /**
     * Подтверждение получения сообщения из очереди.
     * Должен вызваться после получения сообщения методами GetRequest или GetResponse.
     *
     * @param parameters
     * @return returns ru.codeinside.smev.v3.transport.v1_2.Void
     * @throws TargetMessageIsNotFoundException
     * @throws SenderIsNotRegisteredException
     * @throws SMEVFailureException
     * @throws InvalidContentException
     * @throws SignatureVerificationFaultException
     */
    @Override
    public Void ack(AckRequest parameters) throws InvalidContentException, SMEVFailureException, SenderIsNotRegisteredException, SignatureVerificationFaultException, TargetMessageIsNotFoundException {
        return null;
    }

    /**
     * Получение статистики входящих очередей.
     *
     * @param parameters
     * @return returns ru.codeinside.smev.v3.transport.v1_2.GetIncomingQueueStatisticsResponse
     * @throws SenderIsNotRegisteredException
     * @throws SMEVFailureException
     * @throws InvalidContentException
     * @throws SignatureVerificationFaultException
     */
    @Override
    public GetIncomingQueueStatisticsResponse getIncomingQueueStatistics(GetIncomingQueueStatisticsRequest parameters) throws InvalidContentException, SMEVFailureException, SenderIsNotRegisteredException, SignatureVerificationFaultException {
        return null;
    }

    private void buildRequest(SendRequestRequest parameters) {
        GetRequestResponse request = new GetRequestResponse(); // TODO Формировать GetRequestResponse из SendRequestRequest
        providerQueue.add(request);
    }

    private void buildResponse(SendResponseRequest parameters) {
        GetResponseResponse response = new GetResponseResponse(); //TODO Формировать GetResponseResponse из SendResponseRequest
        consumerQueue.add(response);
    }
}
