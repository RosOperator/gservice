package ru.codeinside.smev.v3.transport.v1_2.test;

import org.junit.Assert;
import org.junit.Test;
import ru.codeinside.smev.v3.transport.api.v1_2.*;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Endpoint;
import javax.xml.ws.soap.MTOMFeature;
import javax.xml.ws.soap.SOAPBinding;
import java.util.Map;

public class ServiceTest {

    @Test
    public void testCreation() throws Exception {
        SMEVMessageExchangeService serviceClient = new SMEVMessageExchangeService();
        SMEVMessageExchangePortType client = serviceClient.getSMEVMessageExchangeEndpoint();

        BindingProvider clientProvider = (BindingProvider) client;
        SOAPBinding binding = (SOAPBinding) (clientProvider).getBinding();
        binding.setMTOMEnabled(true);

        Map<String, Object> requestContext = clientProvider.getRequestContext();
        if (false) {
            requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://127.0.0.1:7777/");
        }

        Endpoint service = Endpoint.publish("http://localhost:8080/transport_1_0_1/", new ServiceStub(), new MTOMFeature());
        try {
            SendRequestResponse consumerRequest = client.sendRequest(new SendRequestRequest());
            GetRequestResponse providerRequest = client.getRequest(new GetRequestRequest());

            SendResponseResponse providerResponse = client.sendResponse(new SendResponseRequest());
            GetResponseResponse consumerResponse = client.getResponse(new GetResponseRequest());

            Assert.assertNotNull(consumerRequest);
            Assert.assertNotNull(providerRequest);
            Assert.assertNotNull(providerResponse);
            Assert.assertNotNull(consumerResponse);
        } finally {
            service.stop();
        }
    }

}