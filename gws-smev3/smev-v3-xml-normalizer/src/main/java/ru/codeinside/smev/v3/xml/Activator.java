package ru.codeinside.smev.v3.xml;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import ru.codeinside.smev.v3.crypto.api.XmlNormalizer;

public class Activator implements BundleActivator {
    private ServiceRegistration registration;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        registration = bundleContext.registerService(XmlNormalizer.class.getName(), new XmlNormalizerImpl(), null);
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        if (registration != null) {
            registration.unregister();
            registration = null;
        }
    }
}
