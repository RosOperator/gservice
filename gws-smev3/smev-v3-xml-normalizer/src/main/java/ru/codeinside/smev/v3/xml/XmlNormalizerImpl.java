package ru.codeinside.smev.v3.xml;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import ru.codeinside.smev.v3.crypto.api.XmlNormalizer;
import ru.codeinside.smev.v3.xml.normalize.Transform;
import ru.codeinside.smev.v3.xml.normalize.XMLSignatureInput;
import ru.codeinside.smev.v3.xml.normalize.exceptions.CanonicalizationException;
import ru.codeinside.smev.v3.xml.normalize.exceptions.NormalizationException;
import ru.codeinside.smev.v3.xml.normalize.exceptions.TransformationException;

import javax.annotation.Nonnull;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;

@SuppressWarnings("PackageAccessibility")
public class XmlNormalizerImpl implements XmlNormalizer {
    /**
     * @param object       jaxb объект для нормализации
     * @param outputStream исходящий поток результата
     */
    @Override
    public void normalizeJaxb(@Nonnull Object object, @Nonnull OutputStream outputStream)
            throws JAXBException, ParserConfigurationException, IOException, SAXException {
        JAXBContext context = JAXBContext.newInstance(object.getClass());
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        marshaller.marshal(object, os);

        String contentString = os.toString("UTF-8");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new StringReader(contentString)));
        Element content = document.getDocumentElement();

        normalizeDom(content, outputStream);
    }

    /**
     * @param element      элемент для нормализации
     * @param outputStream исходящий поток результата
     */
    @Override
    public void normalizeDom(@Nonnull Element element, @Nonnull OutputStream outputStream) {
        XMLSignatureInput input = new XMLSignatureInput(element);
        try {
            Transform.performTransform(input, outputStream);
        } catch (ParserConfigurationException e) {
            throw new NormalizationException(e);
        } catch (IOException e) {
            throw new NormalizationException(e);
        } catch (SAXException e) {
            throw new NormalizationException(e);
        } catch (CanonicalizationException e) {
            throw new NormalizationException(e);
        } catch (TransformationException e) {
            throw new NormalizationException(e);
        }
    }
}
