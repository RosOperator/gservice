package ru.codeinside.smev.v3.xml.normalize;

import org.xml.sax.SAXException;
import ru.codeinside.smev.v3.xml.normalize.exceptions.CanonicalizationException;
import ru.codeinside.smev.v3.xml.normalize.exceptions.TransformationException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.OutputStream;

public class Transform {
    public static XMLSignatureInput performTransform(XMLSignatureInput xmlSignatureInput, OutputStream result)
            throws ParserConfigurationException, IOException, SAXException, CanonicalizationException, TransformationException {

        xmlSignatureInput = TransformC14Exclusive.performTransform(xmlSignatureInput, null);
        xmlSignatureInput = TransformSmev.performTransform(xmlSignatureInput, result);

        return xmlSignatureInput;
    }
}
