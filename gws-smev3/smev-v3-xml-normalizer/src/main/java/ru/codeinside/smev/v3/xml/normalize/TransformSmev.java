package ru.codeinside.smev.v3.xml.normalize;

import ru.codeinside.smev.v3.xml.normalize.exceptions.TransformationException;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.Namespace;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TransformSmev {

    public static final String ALGORITHM_URN = "urn://smev-gov-ru/xmldsig/transform";
    private static final String ENCODING_UTF_8 = "UTF-8";
    private static Logger logger = Logger.getLogger(TransformSmev.class.getName());
    private static AttributeSortingComparator attributeSortingComparator = new AttributeSortingComparator();

    private static XMLInputFactory inputFactory = XMLInputFactory.newInstance();
    private static XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
    private static XMLEventFactory eventFactory = XMLEventFactory.newInstance();

    public static XMLSignatureInput performTransform(XMLSignatureInput input, OutputStream os) throws IOException, TransformationException {
        if (os == null)
            return performTransform(input);
        else {
            process(input.getOctetStream(), os);
            XMLSignatureInput result = new XMLSignatureInput((byte[]) null);
            result.setOutputStream(os);
            return result;
        }
    }

    public static XMLSignatureInput performTransform(XMLSignatureInput input) throws IOException, TransformationException {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        process(input.getOctetStream(), result);
        byte[] postTransformData = result.toByteArray();

        return new XMLSignatureInput(postTransformData);
    }

    public static void process(InputStream argSrc, OutputStream argDst) throws TransformationException {
        Stack<List<Namespace>> prefixMappingStack = new Stack<List<Namespace>>();
        int prefixCnt = 1;
        XMLEventReader src = null;
        XMLEventWriter dst = null;
        try {
            src = inputFactory.createXMLEventReader(argSrc, ENCODING_UTF_8);
            dst = outputFactory.createXMLEventWriter(argDst, ENCODING_UTF_8);
            XMLEventFactory factory = eventFactory;

            while (src.hasNext()) {
                XMLEvent event = src.nextEvent();

                if (event.isCharacters()) {
                    String data = event.asCharacters().getData();
                    // Отсекаем whitespace symbols.
                    if (!data.trim().isEmpty()) {
                        dst.add(event);
                    }
                    continue;
                } else if (event.isStartElement()) {
                    List<Namespace> myPrefixMappings = new LinkedList<Namespace>();
                    prefixMappingStack.push(myPrefixMappings);

                    // Обработка элемента: NS prefix rewriting.
                    // N.B. Элементы в unqualified form не поддерживаются.
                    StartElement srcEvent = (StartElement) event;
                    String nsURI = srcEvent.getName().getNamespaceURI();
                    String prefix = findPrefix(nsURI, prefixMappingStack);

                    if (prefix == null) {
                        prefix = "ns" + String.valueOf(prefixCnt++);
                        myPrefixMappings.add(factory.createNamespace(prefix, nsURI));
                    }
                    StartElement dstEvent = factory.createStartElement(prefix, nsURI, srcEvent.getName().getLocalPart());
                    dst.add(dstEvent);

                    // == Обработка атрибутов. Два шага: отсортировать, промэпить namespace URI. ==
                    Iterator<Attribute> srcAttributeIterator = srcEvent.getAttributes();
                    // Положим атрибуты в list, чтобы их можно было отсортировать.
                    List<Attribute> srcAttributeList = new LinkedList<Attribute>();
                    while (srcAttributeIterator.hasNext()) {
                        srcAttributeList.add(srcAttributeIterator.next());
                    }
                    // Сортировка атрибутов по алфавиту.
                    Collections.sort(srcAttributeList, attributeSortingComparator);

                    // Обработка префиксов. Аналогична обработке префиксов элементов,
                    // за исключением того, что у атрибут может не иметь namespace.
                    List<Attribute> dstAttributeList = new LinkedList<Attribute>();
                    for (Attribute srcAttribute : srcAttributeList) {
                        String attributeNsURI = srcAttribute.getName().getNamespaceURI();
                        String attributeLocalName = srcAttribute.getName().getLocalPart();
                        String value = srcAttribute.getValue();
                        String attributePrefix = null;
                        Attribute dstAttribute = null;
                        if (attributeNsURI != null && !"".equals(attributeNsURI)) {
                            attributePrefix = findPrefix(attributeNsURI, prefixMappingStack);
                            if (attributePrefix == null) {
                                attributePrefix = "ns" + String.valueOf(prefixCnt++);
                                myPrefixMappings.add(factory.createNamespace(attributePrefix, attributeNsURI));
                            }
                            dstAttribute = factory.createAttribute(attributePrefix, attributeNsURI, attributeLocalName, value);
                        } else {
                            dstAttribute = factory.createAttribute(attributeLocalName, value);
                        }
                        dstAttributeList.add(dstAttribute);
                    }

                    // Высести namespace prefix mappings для текущего элемента.
                    // Их порядок детерминирован, т.к. перед мэппингом атрибуты были отсортированы.
                    // Поэтому дополнительной сотрировки здесь не нужно.
                    for (Namespace mapping : myPrefixMappings) {
                        dst.add(mapping);
                    }

                    // Вывести атрибуты.
                    // N.B. Мы не выводим атрибуты сразу вместе с элементом, используя метод
                    // XMLEventFactory.createStartElement(prefix, nsURI, localName, List<Namespace>, List<Attribute>),
                    // потому что при использовании этого метода порядок атрибутов в выходном документе
                    // меняется произвольным образом.
                    for (Attribute attr : dstAttributeList) {
                        dst.add(attr);
                    }

                    continue;
                } else if (event.isEndElement()) {
                    // Гарантируем, что empty tags запишутся в форме <a></a>, а не в форме <a/>.
                    dst.add(eventFactory.createSpace(""));

                    // NS prefix rewriting
                    EndElement srcEvent = (EndElement) event;
                    String nsURI = srcEvent.getName().getNamespaceURI();
                    String prefix = findPrefix(nsURI, prefixMappingStack);
                    if (prefix == null) {
                        throw new TransformationException("EndElement: prefix mapping is not found for namespace " + nsURI);
                    }

                    EndElement dstEvent = eventFactory.createEndElement(prefix, nsURI, srcEvent.getName().getLocalPart());
                    dst.add(dstEvent);

                    prefixMappingStack.pop();
                    continue;
                } else if (event.isAttribute()) {
                    // Атрибуты обрабатываются в событии startElement.
                    continue;
                }

                // Остальные события (processing instructions, start document, etc.) опускаем.
            }
        } catch (XMLStreamException e) {
            Object exArgs[] = {e.getMessage()};
            throw new TransformationException(
                    "Can not perform transformation " + ALGORITHM_URN, exArgs, e
            );
        } finally {
            if (src != null) {
                try {
                    src.close();
                } catch (XMLStreamException e) {
                    logger.log(Level.WARNING, "Can not close XMLEventReader", e);
                }
            }
            if (dst != null) {
                try {
                    dst.close();
                } catch (XMLStreamException e) {
                    logger.log(Level.WARNING, "Can not close XMLEventWriter", e);
                }
            }
            try {
                argSrc.close();
            } catch (IOException e) {
                logger.log(Level.WARNING, "Can not close input stream.", e);
            }
            if (argDst != null) {
                try {
                    argDst.close();
                } catch (IOException e) {
                    logger.log(Level.WARNING, "Can not close output stream.", e);
                }
            }
        }
    }

    private static String findPrefix(String argNamespaceURI, Stack<List<Namespace>> argMappingStack) {
        if (argNamespaceURI == null) {
            throw new IllegalArgumentException("No namespace элементы не поддерживаются.");
        }

        for (List<Namespace> elementMappingList : argMappingStack) {
            for (Namespace mapping : elementMappingList) {
                if (argNamespaceURI.equals(mapping.getNamespaceURI())) {
                    return mapping.getPrefix();
                }
            }
        }
        return null;
    }

    final static class AttributeSortingComparator implements Comparator<Attribute> {
        @Override
        public int compare(Attribute x, Attribute y) {
            String xNS = x.getName().getNamespaceURI();
            String xLocal = x.getName().getLocalPart();
            String yNS = y.getName().getNamespaceURI();
            String yLocal = y.getName().getLocalPart();

            // Оба атрибута - unqualified.
            if (empty(xNS) && empty(yNS)) {
                return xLocal.compareTo(yLocal);
            }

            // Оба атрибута - qualified.
            if (!empty(xNS) && !empty(yNS)) {
                // Сначала сравниваем namespaces.
                int nsComparisonResult = xNS.compareTo(yNS);
                if (nsComparisonResult != 0) {
                    return nsComparisonResult;
                } else {
                    // Если равны - local names.
                    return xLocal.compareTo(yLocal);
                }
            }

            // Один - qualified, второй - unqualified.
            if (empty(xNS)) {
                return 1;
            } else {
                return -1;
            }
        }

        private boolean empty(String arg) {
            return arg == null || "".equals(arg);
        }
    }

    final static class DebugOutputStream extends OutputStream {
        private ByteArrayOutputStream collector = new ByteArrayOutputStream();
        private OutputStream wrappedStream;

        public DebugOutputStream(OutputStream arg) {
            wrappedStream = arg;
        }

        public byte[] getCollectedData() {
            try {
                collector.flush();
            } catch (IOException e) {
            }
            return collector.toByteArray();
        }

        @Override
        public void write(int b) throws IOException {
            collector.write(b);
            wrappedStream.write(b);
        }

        @Override
        public void close() throws IOException {
            collector.close();
            wrappedStream.close();
            super.close();
        }

        @Override
        public void flush() throws IOException {
            collector.flush();
            wrappedStream.flush();
        }

    }
}
