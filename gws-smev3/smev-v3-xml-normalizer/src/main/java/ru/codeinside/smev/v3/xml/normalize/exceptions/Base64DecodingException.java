package ru.codeinside.smev.v3.xml.normalize.exceptions;

public class Base64DecodingException extends XMLSecurityException {
    public Base64DecodingException(String message) {
        super(message);
    }
}
