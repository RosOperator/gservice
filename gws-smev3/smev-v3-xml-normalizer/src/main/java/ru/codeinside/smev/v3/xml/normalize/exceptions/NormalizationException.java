package ru.codeinside.smev.v3.xml.normalize.exceptions;

public class NormalizationException extends RuntimeException {
    public NormalizationException(String message) {
        super(message);
    }

    public NormalizationException(Throwable cause) {
        super(cause);
    }
}