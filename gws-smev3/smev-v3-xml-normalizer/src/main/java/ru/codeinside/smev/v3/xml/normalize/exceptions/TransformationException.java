package ru.codeinside.smev.v3.xml.normalize.exceptions;

public class TransformationException extends XMLSecurityException {
    public TransformationException(String message) {
        super(message);
    }

    public TransformationException(String msgID, Object exArgs[], Exception originalException) {
        super(msgID, exArgs, originalException);
    }
}
