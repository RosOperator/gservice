package ru.codeinside.smev.v3.xml;


import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import ru.codeinside.smev.v3.crypto.api.XmlNormalizer;
import ru.codeinside.smev.v3.xml.data.JaxbObject;
import ru.codeinside.smev.v3.xml.data.JaxbObjectData;
import ru.codeinside.smev.v3.xml.normalize.TransformC14Exclusive;
import ru.codeinside.smev.v3.xml.normalize.TransformSmev;
import ru.codeinside.smev.v3.xml.normalize.XMLSignatureInput;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;

public class XmlNormalizerTest {

    @Test
    public void normalizeDom() throws Exception {
        String contentString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<ns2:SenderProvidedRequestData xmlns:ns2=\"urn://x-artefacts-smev-gov-ru/services/message-exchange/types/1.0\" Id=\"SIGNED_BY_CONSUMER\">\n" +
                " <MessagePrimaryContent xmlns=\"urn://x-artefacts-smev-gov-ru/services/message-exchange/types/basic/1.0\">\n" +
                "  <SomeRequest:SomeRequest xmlns:SomeRequest=\"urn://x-artifacts-it-ru/vs/smev/test/test-business-data/1.0\">\n" +
                "   <x xmlns=\"urn://x-artifacts-it-ru/vs/smev/test/test-business-data/1.0\">qweqwe</x>\n" +
                "    <!--<executions>-->" +
                "     <!--<execution>-->" +
                "      <!--<goals>-->" +
                "       <!--<goal>jar</goal>-->" +
                "      <!--</goals>-->" +
                "     <!--</execution>-->" +
                "    <!--</executions>-->" +
                "  </SomeRequest:SomeRequest>\n" +
                " </MessagePrimaryContent>\n" +
                "</ns2:SenderProvidedRequestData>";

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new StringReader(contentString)));
        Element content = document.getDocumentElement();

        ByteArrayOutputStream result = new ByteArrayOutputStream();

        XmlNormalizer normalizer = new XmlNormalizerImpl();
        normalizer.normalizeDom(content, result);

        String normalized = "<ns1:SenderProvidedRequestData xmlns:ns1=\"" +
                "urn://x-artefacts-smev-gov-ru/services/message-exchange/types/1.0\" Id=\"SIGNED_BY_CONSUMER\">" +
                "<ns2:MessagePrimaryContent " +
                "xmlns:ns2=\"urn://x-artefacts-smev-gov-ru/services/message-exchange/types/basic/1.0\">" +
                "<ns3:SomeRequest " +
                "xmlns:ns3=\"urn://x-artifacts-it-ru/vs/smev/test/test-business-data/1.0\">" +
                "<ns3:x>qweqwe</ns3:x>" +
                "</ns3:SomeRequest></ns2:MessagePrimaryContent></ns1:SenderProvidedRequestData>";
        assertEquals(normalized, result.toString("UTF-8"));
    }

    @Test
    public void normalizeJaxb() throws Exception {
        JaxbObject jaxbObject = new JaxbObject();
        jaxbObject.addMessage(new JaxbObjectData("RU", "Русский текст"));
        jaxbObject.addMessage(new JaxbObjectData("EN", "English text"));

        ByteArrayOutputStream result = new ByteArrayOutputStream();

        XmlNormalizer normalizer = new XmlNormalizerImpl();
        normalizer.normalizeJaxb(jaxbObject, result);
        String normalized = "<ns1:test_xml xmlns:ns1=\"\">" +
                "<ns1:test_message lang=\"RU\">Русский текст</ns1:test_message>" +
                "<ns1:test_message lang=\"EN\">English text</ns1:test_message>" +
                "</ns1:test_xml>";
        assertEquals(normalized, result.toString("UTF-8"));
    }

    // TODO нужен пример для теста
    @Test
    public void canonicalization() throws Exception {
        String contentString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<ns1:EchoPlease xmlns:ns1=\"urn://x-artefacts-smev-gov-ru/services/echo/1.0\">" +
                "<ns1:MyYell>Это - русский текст</ns1:MyYell>" +
                "<!--<executions>-->" +
                "<!--<execution>-->" +
                "<!--<goals>-->" +
                "<!--<goal>jar</goal>-->" +
                "<!--</goals>-->" +
                "<!--</execution>-->" +
                "<!--</executions>-->" +
                "</ns1:EchoPlease>";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new StringReader(contentString)));
        Element content = document.getDocumentElement();

        XMLSignatureInput input = new XMLSignatureInput(content);

        ByteArrayOutputStream result = new ByteArrayOutputStream();

        TransformC14Exclusive.performTransform(input, result);

        String canonicalizedString = "<ns1:EchoPlease xmlns:ns1=\"urn://x-artefacts-smev-gov-ru/services/echo/1.0\">" +
                "<ns1:MyYell>Это - русский текст</ns1:MyYell>" +
                "</ns1:EchoPlease>";

        assertEquals(canonicalizedString, result.toString("UTF-8"));
    }


    /**
     * Методические рекомендации по взаимодействию в СМЭВ3rev.0.8.0.docx
     * 7.1 Приложение 1: Результат трансформации urn://smev-gov-ru/xmldsig/transform
     *
     * @throws Exception
     */
    @Test
    public void transformSmev() throws Exception {
        String contentString = "<ns2:SenderProvidedRequestData xmlns:ns2=\"urn://x-artefacts-smev-gov-ru/services/message-exchange/types/1.0\" Id=\"SIGNED_BY_CONSUMER\">\n" +
                " <MessagePrimaryContent xmlns=\"urn://x-artefacts-smev-gov-ru/services/message-exchange/types/basic/1.0\">\n" +
                "  <SomeRequest:SomeRequest xmlns:SomeRequest=\"urn://x-artifacts-it-ru/vs/smev/test/test-business-data/1.0\">\n" +
                "   <x xmlns=\"urn://x-artifacts-it-ru/vs/smev/test/test-business-data/1.0\">qweqwe</x>\n" +
                "  </SomeRequest:SomeRequest>\n" +
                " </MessagePrimaryContent>\n" +
                "</ns2:SenderProvidedRequestData>";

        byte[] bytes = contentString.getBytes();
        XMLSignatureInput input = new XMLSignatureInput(bytes);
        ByteArrayOutputStream result = new ByteArrayOutputStream();

        TransformSmev.performTransform(input, result);

        String normalized = "<ns1:SenderProvidedRequestData xmlns:ns1=\"" +
                "urn://x-artefacts-smev-gov-ru/services/message-exchange/types/1.0\" Id=\"SIGNED_BY_CONSUMER\">" +
                "<ns2:MessagePrimaryContent " +
                "xmlns:ns2=\"urn://x-artefacts-smev-gov-ru/services/message-exchange/types/basic/1.0\">" +
                "<ns3:SomeRequest " +
                "xmlns:ns3=\"urn://x-artifacts-it-ru/vs/smev/test/test-business-data/1.0\">" +
                "<ns3:x>qweqwe</ns3:x>" +
                "</ns3:SomeRequest></ns2:MessagePrimaryContent></ns1:SenderProvidedRequestData>";
        assertEquals(normalized, result.toString("UTF-8"));
    }

    @Test
    public void testRules_1_2_6() throws ParserConfigurationException, IOException, SAXException {
        String input = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<!-- Тестирование правил 1, 2, 6: \n" +
                "\t- XML declaration выше, этот комментарий, и следующая за ним processing instruction должны быть вырезаны;\n" +
                "\t- Переводы строки должны быть удалены;\n" +
                "\t- Namespace prefixes заменяются на автоматически сгенерированные.\n" +
                "-->\n" +
                "<?xml-stylesheet type=\"text/xsl\" href=\"style.xsl\"?>\n" +
                "\n" +
                "<elementOne xmlns=\"http://test/1\">\n" +
                "\t<qwe:elementTwo xmlns:qwe=\"http://test/2\">asd</qwe:elementTwo>  \n" +
                "</elementOne>";

        String expected = "<ns1:elementOne xmlns:ns1=\"http://test/1\">" +
                "<ns2:elementTwo xmlns:ns2=\"http://test/2\">asd</ns2:elementTwo></ns1:elementOne>";

        assertNormalize(input, expected);
    }

    @Test
    public void testRules_4_5() throws ParserConfigurationException, SAXException, IOException {
        String input = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<!-- \n" +
                "\tВсё то же, что в test case 1, плюс правила 4 и 5:\n" +
                "\t- Удалить namespace prefix, которые на текущем уровне объявляются, но не используются.\n" +
                "\t- Проверить, что namespace текущего элемента объявлен либо выше по дереву, либо в текущем элементе. Если не объявлен, объявить в текущем элементе\n" +
                "-->\n" +
                "<?xml-stylesheet type=\"text/xsl\" href=\"style.xsl\"?>\n" +
                "\n" +
                "<elementOne xmlns=\"http://test/1\" xmlns:qwe=\"http://test/2\" xmlns:asd=\"http://test/3\">\n" +
                "\t<qwe:elementTwo>\n" +
                "\t\t<asd:elementThree>\n" +
                "\t\t\t<!-- Проверка обработки default namespace. -->\n" +
                "\t\t\t<elementFour> z x c </elementFour>     \n" +
                "\t\t\t<!-- Тестирование ситуации, когда для одного namespace объявляется несколько префиксов во вложенных элементах. -->\n" +
                "\t\t\t<qqq:elementFive xmlns:qqq=\"http://test/2\"> w w w </qqq:elementFive>\n" +
                "\t\t</asd:elementThree>\n" +
                "\t\t<!-- Ситуация, когда prefix был объявлен выше, чем должно быть в нормальной форме, \n" +
                "\t\tпри нормализации переносится ниже, и это приводит к генерации нескольких префиксов  \n" +
                "\t\tдля одного namespace в sibling элементах. -->\n" +
                "\t\t<asd:elementSix>eee</asd:elementSix>\n" +
                "\t</qwe:elementTwo>  \n" +
                "</elementOne>";
        String expected = "<ns1:elementOne xmlns:ns1=\"http://test/1\"><ns2:elementTwo xmlns:ns2=\"http://test/2\">" +
                "<ns3:elementThree xmlns:ns3=\"http://test/3\"><ns1:elementFour> z x c </ns1:elementFour>" +
                "<ns2:elementFive> w w w </ns2:elementFive></ns3:elementThree>" +
                "<ns4:elementSix xmlns:ns4=\"http://test/3\">eee</ns4:elementSix></ns2:elementTwo></ns1:elementOne>";
        assertNormalize(input, expected);
    }

    @Test
    public void testRules_3_7_8() throws ParserConfigurationException, SAXException, IOException {
        String input = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<!-- \n" +
                "\tВсё то же, что в test case 1, плюс правила 3, 7 и 8:\n" +
                "\t- Атрибуты должны быть отсортированы в алфавитном порядке: сначала по namespace URI (если атрибут - в qualified form), затем – по local name. \n" +
                "\t\tАтрибуты в unqualified form после сортировки идут после атрибутов в qualified form.\n" +
                "\t- Объявления namespace prefix должны находиться перед атрибутами. Объявления префиксов должны быть отсортированы в порядке объявления, а именно:\n" +
                "\t\ta.\tПервым объявляется префикс пространства имен элемента, если он не был объявлен выше по дереву.\n" +
                "\t\tb.\tДальше объявляются префиксы пространств имен атрибутов, если они требуются. \n" +
                "\t\t\tПорядок этих объявлений соответствует порядку атрибутов, отсортированных в алфавитном порядке (см. п.5).\n" +
                "-->\n" +
                "<?xml-stylesheet type=\"text/xsl\" href=\"style.xsl\"?>\n" +
                "\n" +
                "<elementOne xmlns=\"http://test/1\" xmlns:qwe=\"http://test/2\" xmlns:asd=\"http://test/3\">\n" +
                "\t<qwe:elementTwo>\n" +
                "\t\t<asd:elementThree xmlns:wer=\"http://test/a\" xmlns:zxc=\"http://test/0\" wer:attZ=\"zzz\" attB=\"bbb\" attA=\"aaa\" zxc:attC=\"ccc\" asd:attD=\"ddd\" asd:attE=\"eee\" qwe:attF=\"fff\"/>\n" +
                "\t</qwe:elementTwo>  \n" +
                "</elementOne>";
        String expected = "<ns1:elementOne xmlns:ns1=\"http://test/1\"><ns2:elementTwo xmlns:ns2=\"http://test/2\">" +
                "<ns3:elementThree xmlns:ns3=\"http://test/3\" xmlns:ns4=\"http://test/0\" xmlns:ns5=\"http://test/a\" " +
                "ns4:attC=\"ccc\" ns2:attF=\"fff\" ns3:attD=\"ddd\" ns3:attE=\"eee\" ns5:attZ=\"zzz\" attA=\"aaa\" " +
                "attB=\"bbb\"></ns3:elementThree></ns2:elementTwo></ns1:elementOne>";
        assertNormalize(input, expected);
    }

    private void assertNormalize(String input, String expected) throws IOException, ParserConfigurationException, SAXException {
        XmlNormalizer impl = new XmlNormalizerImpl();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);

        DocumentBuilder documentBuilder = factory.newDocumentBuilder();
        Document document = documentBuilder.parse(new InputSource(new StringReader(input)));

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        impl.normalizeDom(document.getDocumentElement(), bos);

        assertEquals(expected, bos.toString("utf-8"));
    }
}
