package ru.codeinside.smev.v3.xml.data;

import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="test_xml")
public class JaxbObject {
    private List<JaxbObjectData> list = new ArrayList<JaxbObjectData>();

    public void addMessage(JaxbObjectData msg){
        this.list.add(msg);
    }

    @XmlElementRef
    public List<JaxbObjectData> getList() {
        return list;
    }

    public void setList(List<JaxbObjectData> list) {
        this.list = list;
    }
}
