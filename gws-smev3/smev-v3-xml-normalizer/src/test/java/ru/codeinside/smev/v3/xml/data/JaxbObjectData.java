package ru.codeinside.smev.v3.xml.data;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name="test_message")
public class JaxbObjectData {
    private String language = "EN";
    private String message = "Test Message";

    public JaxbObjectData() {
    }

    public JaxbObjectData(String lang, String message) {
        this.language = lang;
        this.message = message;
    }

    @XmlValue
    public String getMessage() {
        return message;
    }

    public void setMessage(String m) {
        this.message = m;
    }

    @XmlAttribute(name="lang")
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}

