/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.codeinside.sign;

import java.io.BufferedReader;
import java.io.File;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPMessage;
import javax.xml.bind.DatatypeConverter;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.stream.StreamSource;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JOptionPane;

import ru.codeinside.gses.cert.X509;



/**
 *
 * @author Alex
 * УСТАРЕЛ ДО РЕЛИЗА, ПРОБНАЯ ВЕРСИЯ, НЕ ИСПОЛЬЗУЕТСЯ
 * .NET заменили на JS и браузерплагин
 */
public class CSPHandler {
    
    CSPHandler() {   
        if (System.getProperty("os.name").contains("Windows"))
        {   
            initWrapper();
        }
    }
    
    public void finalize() {
        //TODO unload dlls
        //JOptionPane.showMessageDialog(null, "finalizing", "CSP", JOptionPane.INFORMATION_MESSAGE);
    }
    
    private String tempSavePath;   
    
    //закладка руками, при ребилде адаптера, главное чтобы отличалась от предидущей
    private static final String ASSEBLY_DATE = "11.09.2015"; 
    ///////////////////////////////////////////////////////////////////////////////
    
    private static File INI;
    
    public static boolean isValid;
    
    final public static int EPOV = 0;
    
    final public static int EPSP = 1;    
    
    public static final String OV_SIGN = "_SoapBodySignatureField";
    
    public static final String SP_SIGN = "AppDataSignatureField";
    
    
    /*
    * NATIVES
    */
    private native String signWithCSP(String message, int sigtype);
    
    private native String getSignForData(String message, String sdn, String tag, String ns);
    
    private native String signByQName(String message, String elem, String ns);
    
    private native String getAliases();
    
    private native String getB64ByAlias(String alias);
    /*
    * SYSTEM
    */
        
    private static boolean checkValid() {
        try
        {   String rkp = System.getProperty("java.io.tmpdir").replace("Temp", "RKIS2");  
            
            File rkisFolder = new File(rkp.toString());            
            if (!rkisFolder.exists()) {
                Boolean success = (rkisFolder).mkdirs();                
                if (success) {              
                    CSPHandler.loadFile("RKIS2.ini",rkp.toString(),true);
                    updateDlls();
                } else throw new RuntimeException("Невозможно создать папку "
                            +System.getProperty("java.io.tmpdir").replace("Temp", "RKIS2"));
            }
            
            INI = new File(System.getProperty("java.io.tmpdir").replace("Temp", "RKIS2")+"/RKIS2.ini");
            
            Properties props = new Properties();
            props.load(new FileInputStream(INI));
            
            Boolean fl = Boolean.valueOf(props.getProperty("FIRST_LAUNCH"));
            if (!fl) {                
                return props.getProperty("DATE_ASSEMBLY").equals(ASSEBLY_DATE);
            } else return false;          
        
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }   
    
    private static final String REGQUERY_UTIL = "reg query ";
    private static final String REGSTR_TOKEN = "REG_SZ";
    private static final String DESKTOP_FOLDER_CMD = REGQUERY_UTIL 
        + "\"HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\" 
        + "Explorer\\Shell Folders\" /v DESKTOP";

  
    public static String getCurrentUserDesktopPath() {
        try {
            Process process = Runtime.getRuntime().exec(DESKTOP_FOLDER_CMD);
            InputStreamReader input = new InputStreamReader(process.getInputStream());
            StringBuilder sb=new StringBuilder();
            BufferedReader br = new BufferedReader(input);
            String read = br.readLine();
            while(read != null) {
                sb.append(read);
                read =br.readLine();

            }
            String result = sb.toString();
            int p = result.indexOf(REGSTR_TOKEN);

            if (p == -1) return null;
            return result.substring(p + REGSTR_TOKEN.length()).trim();
        }
        catch (Exception e) {
            return null;
        }
    }
    
    public void showRegistration() {  
        try
            {   
                updateDlls();
                
                loadFile("register.cmd",getCurrentUserDesktopPath(),false);
                File file = new File(getCurrentUserDesktopPath()+"\\register.cmd");
                BufferedReader reader = new BufferedReader(new FileReader(file));
                
                Properties props =  new Properties();
                props.load(new FileInputStream(INI)); 
                
                String line = "", cmd = "";                
                while((line = reader.readLine()) != null)
                {
                    if (line.startsWith("SET RKPTH="))
                        cmd += "SET RKPTH=\""+INI.getCanonicalPath().replace("RKIS2.ini","")+ "\r\n";
                    else if (line.startsWith("SET DATE_OLD="))
                        cmd += "SET DATE_OLD=DATE_ASSEMBLY = "+props.getProperty("DATE_ASSEMBLY")+ "\r\n";
                    else if (line.startsWith("SET DATE_NEW="))
                        cmd += "SET DATE_NEW=DATE_ASSEMBLY = "+ASSEBLY_DATE+ "\r\n";
                    else cmd += line + "\r\n";
                }
                reader.close();            
                
                FileWriter writer = new FileWriter(getCurrentUserDesktopPath()+"\\register.cmd");
                writer.write(cmd);
                writer.close();    
            
                JOptionPane.showMessageDialog(null, 
                    "Необходима регистрация адаптера РКИС 2.0 для КриптоПро CSP. \n" +
                    "Для этого закройте браузер и запустите файл \"register.cmd\" \n" +
                    "находящийся на вашем рабочем столе, от имени администратора. \n" +        
                    //"находящийся в \""+System.getProperty("user.home")+"\\Desktop\" от имени администратора, \n" +
                    "При отсутствии прав обратитесь к вашему системному администратору.", "Изменения конфигурации: ", 
                    JOptionPane.INFORMATION_MESSAGE);
                
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка: ", JOptionPane.INFORMATION_MESSAGE);
            throw new RuntimeException(e);
        }    
    }    
    
    private static void updateDlls() {
        String appdataPath = System.getProperty("java.io.tmpdir").replace("Temp", "RKIS2"); 
        CSPHandler.loadFile("SiuUtils.SignerBase.dll",appdataPath,false);
        CSPHandler.loadFile("SiuUtils.JavaAdapter.dll",appdataPath,false);            
        CSPHandler.loadFile("SiuUtils.ClientSignerWrapper.dll",appdataPath,false);
    }
    
    private static void initWrapper() {
        try
        {   if (checkValid()) {  
                isValid = true;
                System.load(System.getProperty("java.io.tmpdir").replace("Temp", "RKIS2")+"SiuUtils.ClientSignerWrapper.dll");
            } else isValid = false; 
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getStackTrace(), "Ошибка: ", JOptionPane.INFORMATION_MESSAGE);
            throw new RuntimeException(e);
        }
    }    
     
    private static void loadFile(String FileName, String Dest, boolean firstTime) {
        try {                
                URL res = CSPHandler.class.getResource("/"+FileName);
                InputStream is = res.openStream();
            
                File file = new File(Dest,FileName);    
                
                if (!(file.exists() && firstTime)) {
                
                    if (file.exists()) file.delete(); 
                    FileOutputStream fos = new FileOutputStream(file);

                    byte[] array = new byte[1024];
                    for(int i=is.read(array);
                        i!=-1;
                        i=is.read(array)
                    ) {
                        fos.write(array,0,i);
                    }

                    fos.close();
                    is.close();
                    }                   
            }
            catch(Throwable e)
            {
                e.getMessage();
            }    
    }    
    /*
    * SIGNING
    */        
    byte[] getSignature(byte[] inputData, X509Certificate certificate, String spTag, String spNs, String blockId) throws FileNotFoundException {    
        
        //JOptionPane.showMessageDialog(null, spTag + " : " + spNs, blockId, JOptionPane.INFORMATION_MESSAGE);
        String signature;                
        if (blockId.equals(SP_SIGN) || blockId.equals(OV_SIGN)){
            String xml = new String(inputData,Charset.forName("UTF-8"));        
            xml = xml.replace("�?", "И");//глюк с киррилицей байтами и утф

            signature = this.getSignForData(encodeCyr(xml),encodeCyr(certificate.getSubjectDN().getName()),
                    encodeCyr(spTag), encodeCyr(spNs));
        } else {
            
//            byte[] enc;
//            if (blockId.endsWith(".xml")) {
//                String xml = new String(inputData.getBytes(),Charset.forName("UTF-8"));        
//                xml = xml.replace("�?", "И");
//                enc = xml.getBytes(Charset.forName("UTF-8"));
//            } else enc = inputData.getBytes();  
            
            String b64enclosure = DatatypeConverter.printBase64Binary(inputData);  
            
            signature = this.getSignForData(b64enclosure,encodeCyr(certificate.getSubjectDN().getName()),
                    blockId, "http://enclosure/");
        }
            
        return DatatypeConverter.parseBase64Binary(signature);
        
    }
    
    void getCspCerts(List<Cert> certs) throws FileNotFoundException, CertificateException {     

        String inp = decodeUniCyr(getAliases());
        //JOptionPane.showMessageDialog(null, inp, "gotaliases: ", JOptionPane.INFORMATION_MESSAGE);
        
        String[] als = inp.split(";");
        for( String al : als ) {
            
            String certBytes = decodeUniCyr(getB64ByAlias(encodeCyr(al)));
            byte[] encoded = DatatypeConverter.parseBase64Binary(certBytes);
            try {
                X509Certificate cert = X509.decode(encoded);
                certs.add(new Cert("CryptoProCSP", al, cert));
            } catch (CertificateException e) {
                JOptionPane.showMessageDialog(null, e.getMessage()+"\n", "CERT EXC: ", JOptionPane.INFORMATION_MESSAGE);
                throw e;
            }
        }
    }
    
    public SOAPMessage signSoapMessage(SOAPMessage inp) throws SOAPException, IOException {	
        //initDlls();
        
    	ByteArrayOutputStream param = new ByteArrayOutputStream();
        inp.writeTo(param);              
            
        /*
        String signedXml = this.SignWithCSP(encodeCyr(param.toString()), EPOV);        
        signedXml = new String(decodeUniCyr(signedXml).getBytes(),Charset.forName("UTF-8"));        
        signedXml = this.SignWithCSP(encodeCyr(signedXml), EPSP);  
        signedXml = decodeUniCyr(signedXml); */

        String signedXml = new String(param.toString().getBytes(),Charset.forName("UTF-8"));
        
        signedXml = this.signWithCSP(encodeCyr(signedXml), EPSP);        
        signedXml = decodeUniCyr(signedXml);        
        signedXml = this.signWithCSP(encodeCyr(signedXml), EPOV);  
        signedXml = decodeUniCyr(signedXml);
       
        SOAPMessage out = MessageFactory.newInstance().createMessage(
                new MimeHeaders(), 
                new ByteArrayInputStream(signedXml.getBytes(Charset.forName("UTF-8")))
        );
	    
        return out;
    }
    
    public String signElement(String inp, String elname, String namesp) throws UnsupportedEncodingException, FileNotFoundException {	        
        //initDlls();
        
        try
        {        
            String signedXml = new String(inp.getBytes(),Charset.forName("UTF-8"));
            
            signedXml = this.signByQName(encodeCyr(signedXml), elname, namesp);        
            signedXml = decodeUniCyr(signedXml);   
            return signedXml;
        } catch (Exception e) {
            return e.getMessage();
        }           
    }
    /*
    * UTILS
    */
    private String encodeCyr(String inp) throws FileNotFoundException{
        StringBuilder converter = new StringBuilder();
        
        for (char c : inp.toCharArray()) {
        if (c >= 128)
            converter.append("\\u").append(String.format("%04X", (int) c));
        else
            converter.append(c);
        }
        
        //PrintStream pout = new PrintStream(new FileOutputStream(tempSavePath.replace(".xml", ".ucoded.xml")));
        //pout.print(converter.toString());
        //pout.close();
        return converter.toString();
    }
    
    private String decodeUniCyr(String inp) throws FileNotFoundException {
        String out = inp; 
        
        Pattern pattern = Pattern.compile("\\\\u[a-fA-F0-9]{4}");
        
        Matcher matcher = pattern.matcher(inp);
        
        while(matcher.find()) {
            String code = inp.substring(matcher.start(),matcher.end());     
            char sym = (char)Integer.parseInt(code.substring(2), 16);
            out = out.replace(code, Character.toString(sym));           
        }            
        
        //PrintStream pout = new PrintStream(new FileOutputStream(tempSavePath.replace(".xml", ".decoded.xml")));
        //pout.print(out); 
        //pout.close();        
        return out;
    }
    
    public SOAPMessage getFromFile() throws SOAPException, FileNotFoundException, IOException {
        
        SOAPMessage out = MessageFactory.newInstance().createMessage();
        
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Xml files", "xml");
        chooser.setFileFilter(filter);
        
        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            
            String filename = chooser.getSelectedFile().getAbsolutePath();
            tempSavePath = chooser.getSelectedFile().getAbsolutePath();
            System.out.println(filename);
            SOAPPart soapPart = out.getSOAPPart();
            soapPart.setContent(new StreamSource(new FileInputStream(filename)));
            out.saveChanges();

        }
        return out;
    }
    
    public void saveNearOpened(SOAPMessage inp) throws SOAPException, IOException {
        if (tempSavePath != null) {
            
            ByteArrayOutputStream resultText = new ByteArrayOutputStream();
            inp.writeTo(resultText);
            
            PrintStream out = new PrintStream(new FileOutputStream(tempSavePath.replace(".xml", ".final.xml")));
            out.print(resultText.toString());      
            out.close();
        }
    }

    
}
