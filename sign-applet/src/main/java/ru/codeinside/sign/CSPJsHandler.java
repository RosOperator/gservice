/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.codeinside.sign;

import java.applet.Applet;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.xml.bind.DatatypeConverter;
import netscape.javascript.JSObject;
import ru.codeinside.gses.cert.X509;

/**
 *
 * @author Alex
 */
public class CSPJsHandler {
    
    final private static long MAX_JS_WAIT_TIME = 10000;

    final Applet applet;
    
    static JSObject cman;
    
    public static boolean isValid;
    
    public static final String OV_SIGN = "_SoapBodySignatureField";
    
    public static final String SP_SIGN = "AppDataSignatureField";

    CSPJsHandler( Applet applet) {
        this.applet = applet;
        try {
            //this.applet.wait(100);
            
            initCMan();
            //JOptionPane.showMessageDialog(null, "inited getting is installed", "init: ", JOptionPane.ERROR_MESSAGE); 
            isValid = (Boolean) jsCallSync("IsInstalled", null);           
        } catch (Exception e) {
            throw new RuntimeException("Инициализация криптоменеджера не удалась. Возможно включен режим совместимости IE либо в браузере заблокированы плагины.");
        }        
    }
    
    byte[] getSignature(byte[] inputData, X509Certificate certificate, String sTag, String sNs, String blockId) {    
        /*TODO:
        Все подписание в js устарело, не надо этих плясок с иксемелями, из ядра берем уже готовый SignedInfo
        И просто для него рассчитываем подпись, обработку кастомных тегов, трансформации канонизации, выбор  СП или ОВ
        все это уже есть, на стороне сервера, все закладки вроде overrideData не нужны!!!*/
        String signature;         
        
        String hexsn = certificate.getSerialNumber().toString(16);
        
        if ( (hexsn.length() % 2) != 0)
        {
            hexsn = "0".concat(hexsn);
        }      
        
        hexsn = hexsn.toUpperCase();
        
        if (blockId.equals(OV_SIGN)) {
            signature = (String) jsCallSync(
                    "GetSignatureBySI",
                    new Object[] {
                        DatatypeConverter.printBase64Binary(inputData),
                        DatatypeConverter.printBase64Binary(hexsn.getBytes())
                    }
            );
        } else if (blockId.equals(SP_SIGN) ){
            
            signature = (String) jsCallSync(
                    "GetSignatureForData",
                    new Object[] {
                        //DatatypeConverter.printBase64Binary(xml.getBytes(Charset.forName("UTF-8"))), 
                        DatatypeConverter.printBase64Binary(inputData),
                        DatatypeConverter.printBase64Binary(hexsn.getBytes()),
                        DatatypeConverter.printBase64Binary(sTag.getBytes()), 
                        DatatypeConverter.printBase64Binary(sNs.getBytes())
                    }
            );
        } else {        
            signature = (String) jsCallSync(
                    "GetSignatureForData", 
                    new Object[] {
                        DatatypeConverter.printBase64Binary(inputData),
                        DatatypeConverter.printBase64Binary(hexsn.getBytes()),
                        DatatypeConverter.printBase64Binary(blockId.getBytes()), 
                        DatatypeConverter.printBase64Binary("http://enclosure/".getBytes())
                    }
            );
        }
        
        return DatatypeConverter.parseBase64Binary(signature);
        
    }
    
    void getCspCerts(List<Cert> certs) throws CertificateException {     
                
        String got = (String) jsCallSync("GetCertificates", null);
            
        String[] als = got.split("FOUNDCERT:");

        for( String al : als ) {
            String b64raw = (String) jsCallSync("GetRawCert", new Object[] {getThumbprint(al)});
            
            byte[] encoded = DatatypeConverter.parseBase64Binary(b64raw);
            try {
                X509Certificate cert = X509.decode(encoded);
                certs.add(new Cert("CSPBPlugin", al, cert));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ошибка сертификата", "Ошибка: ", JOptionPane.ERROR_MESSAGE);           
                
                throw new CertificateException(e);
            }
        }

        
    }
    
    public void showRegistration() {  
        jsCallSync("ShowRegistration", null);       
    } 
    
    private String getThumbprint(String inp) {
        String out = "thumbprint not found";
        
        Matcher matcher = Pattern.compile("\\((.*?)\\)").matcher(inp);
        while(matcher.find()) {
           out = matcher.group(1);          
        }
        
        return out;
    }
    
    private void initCMan() throws RuntimeException {
        try
        {
            JsCryptoSetter t = new JsCryptoSetter(applet);
            t.start();
            t.join(MAX_JS_WAIT_TIME);
        } catch (InterruptedException e) {
            JOptionPane.showMessageDialog(null, "Выполнение превысило время ожидания", "Ошибка: ", JOptionPane.ERROR_MESSAGE);
            throw new RuntimeException("JS execution timed out", e);
        }
               
    }

    private Object jsCallSync(String method, Object[] parameters) throws RuntimeException {
        try
        {
            JsCryptoCaller t = new JsCryptoCaller(applet, method, parameters);
            t.start();
            t.join();      
            return t.result;
        } catch (InterruptedException e) {
            JOptionPane.showMessageDialog(null, "Выполнение прервано", "Ошибка: ", JOptionPane.ERROR_MESSAGE);            
            throw new RuntimeException("JS execution aborted", e);
        }
    }

}
