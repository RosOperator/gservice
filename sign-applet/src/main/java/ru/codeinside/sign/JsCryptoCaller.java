/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.codeinside.sign;

import java.applet.Applet;
import netscape.javascript.JSException;
import netscape.javascript.JSObject;

/**
 *
 * @author Alex
 */
public class JsCryptoCaller extends Thread {
    
    final String method;
    final Applet applet;
    final Object[] params;
    public Object result = null;

    JsCryptoCaller(Applet applet, String method, Object[] parameters) {
        this.applet = applet;
        this.method = method; 
        this.params = parameters;
    }
    
    @Override
    public void run(){
        JSObject cmanager;
        int counter = 0;
        try {
            Boolean ready = (Boolean) JSObject.getWindow(applet).getMember("cpmanready");
            while (!ready) {
                try {
                    if (counter > 10000) this.interrupt();
                    counter = counter + 100;
                    sleep(100);
                    ready = (Boolean) JSObject.getWindow(applet).getMember("cpmanready");
                    }
                    catch (InterruptedException e) {
                        System.err.print(" IExcept: " + e.getMessage());
                        return;
                    }
                }
            counter = 0;
            cmanager = (JSObject) JSObject.getWindow(applet).getMember("cpman");
            Boolean busy = ((Boolean) cmanager.eval("cpman('isBusy',{})"));
            while (busy) {
            try {
                if (counter > 10000) this.interrupt();
                sleep(100);
                busy = ((Boolean) cmanager.eval("cpman('isBusy',{})"));
                }
                catch (InterruptedException e) {
                    System.err.print(" IExcept: " + e.getMessage());
                    return;
                }
            }
            processNext(cmanager);            
           
        } catch (JSException e) {   
            System.err.print(" JSExcept: " + e.getMessage());
            this.interrupt();
        }  
    }    
    
    private void processNext(JSObject cmanager) throws JSException{
        try {            
            result = cmanager.call(method, params);              
        } catch (Exception e) {
            System.err.print(" direct fail: " + e.getMessage());
            String pstr = "";
            int i = 0;
            if (params != null) {
                for (Object p : params) {
                    i++;                    
                    pstr = pstr + "par" + Integer.toString(i) + ": '" + ((String) p) + "', ";
                }
                pstr = pstr.substring(0, pstr.lastIndexOf(","));                
                if (pstr.equals("null")) {
                    pstr = "";
                }
            }
            String command = "cpman('" + method + "', {"+pstr+"})"; 
            result = cmanager.eval(command);
            
        } 
    }
}
