/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.codeinside.sign;

import java.applet.Applet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import netscape.javascript.JSException;
import netscape.javascript.JSObject;

/**
 *
 * @author Alex
 */
public class JsCryptoSetter extends Thread {

    final Applet applet;

    JsCryptoSetter(Applet applet) {
        this.applet = applet;
    }
    
    @Override
    public void run() {        
        try { 
            JSObject wnd = (JSObject) JSObject.getWindow(applet);//

            
            String js = 
                "var isie; var ua = window.navigator.userAgent; " +
                "if (ua.indexOf(\"MSIE \") > 0 || !!navigator.userAgent.match(/Trident.*rv\\:11\\./)) " +
                "{ isie = true; } else { isie = false; }";
            wnd.eval(js);            
            wnd.eval("var cpman;");
            wnd.eval("var cpmanready = false;");
            js =
                "function injectC14() { " +
                "    var c14 = document.createElement('script'); " +
                "    c14.type = 'text/javascript'; " +
                "    c14.defer = true; " +
                "    c14.onload = function(){ " +
                "       injectCp(); " +
                "    }; " +
                "    c14.src = '/web-client/scripts/C14NTransformation.js'; " +
                "    document.getElementsByTagName('head')[0].appendChild(c14); " +
                "}; " +
                "function injectCore() { " +
                "    var core = document.createElement('script'); " +
                "    core.type = 'text/javascript'; " +
                "    core.defer = true; " +
                "    core.onload = function(){ " +
                "       injectC14(); " +
                "    }; " +
                "    core.src = '/web-client/scripts/core.js'; " +
                "    document.getElementsByTagName('head')[0].appendChild(core); " +
                "}; " +
                "function injectJq(callback) { " +       
                "    var jq = document.createElement('script'); " +
                "    jq.type = 'text/javascript'; " +
                "    jq.defer = true; " +
                "    jq.onload = function(){ " +
                "       injectCore(); " +
                "    }; " +
                "    jq.src = '/web-client/scripts/jquery-2.1.4.js'; " +
                "    document.getElementsByTagName('head')[0].appendChild(jq); " +
                "}; " +
                "function injectCp() { " +        
                "    var cp = document.createElement('script'); " +
                "    cp.type = 'text/javascript'; " +
                "    cp.defer = true; " +
                "    cp.onload = function(){ " +
                "       cpman = $.fn.cryptoManager; " +
                "       cpmanready = true; " +
                "    }; " +
                "    cp.src = '/web-client/scripts/cryptomanager.js'; " +
                "    document.getElementsByTagName('head')[0].appendChild(cp); " +
                "}; " +
                "injectJq();";
            ///web-client/scripts/
            wnd.eval(js);
            
            //wnd.setMember("cpman", "$.fn.cryptoManager");
            //wnd.eval("var cpman = $.fn.cryptoManager;");     

        } catch (JSException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "JSExcept: ", JOptionPane.INFORMATION_MESSAGE);            
            System.err.print(" error: " + e.getMessage());
        } 
    }
    
}
