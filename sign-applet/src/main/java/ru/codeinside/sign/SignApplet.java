/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2013, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.sign;

import javax.xml.bind.DatatypeConverter;
import java.applet.Applet;
import java.awt.*;
import java.io.IOException;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.LogManager;

final public class SignApplet extends Applet {

  private Signer signer;
  private CertConsumer consumer;
  private Vaadin vaadin;
  public static CSPJsHandler cspjs;

  @Override
  public void init() {

      cspjs = new CSPJsHandler(this);
      
    try {
      LogManager.getLogManager().readConfiguration(getClass().getResourceAsStream("/logging.properties"));
    } catch (IOException e) {
      // skip;
    }

    AccessController.doPrivileged(new CheckServiceAction(
        "javax.xml.datatype.DatatypeFactory",
        "com.sun.org.apache.xerces.internal.jaxp.datatype.DatatypeFactoryImpl"
    ));

    AccessController.doPrivileged(new CheckServiceAction(
        "javax.xml.parsers.DocumentBuilderFactory",
        "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl"
    ));

    AccessController.doPrivileged(new CheckServiceAction(
        "javax.xml.transform.TransformerFactory",
        "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl"
    ));


    boolean debug = Boolean.valueOf(getParameter("appDebug"));
    String pid = getParameter("paintableId");
    String mode = getParameter("mode");

    int count = 0;
    String certSerialNumber;
    Set<Long> lockedCerts = new HashSet<Long>();
    while ((certSerialNumber = getParameter("lockedCert" + count)) != null) {
      lockedCerts.add(Long.valueOf(certSerialNumber));
      count++;
    }

    int maxAttempts;
    try {
      maxAttempts = Integer.valueOf(getParameter("maxAttempts")) >= 0 ? Integer.valueOf(getParameter("maxAttempts")) : 5;
    } catch (NumberFormatException e) {
      maxAttempts = 5;
    }
    setLayout(new BorderLayout(2, 2));

    vaadin = new JsVaadin(debug, this, pid);
    if ("binding".equalsIgnoreCase(mode)) {
      consumer = new Binder(vaadin, this, getParameter("fio"), getParameter("orgName"), maxAttempts, lockedCerts);
    } else if ("rebind".equalsIgnoreCase(mode)) {
      byte[] x509 = DatatypeConverter.parseBase64Binary(getParameter("x509"));
      consumer = new Rebinder(vaadin, this, x509, getParameter("fio"), getParameter("orgName"), maxAttempts, lockedCerts);
    } else if ("sign".equalsIgnoreCase(mode)) {
      byte[] x509 = DatatypeConverter.parseBase64Binary(getParameter("x509"));
      signer = new Signer(vaadin, this, x509, maxAttempts, lockedCerts);
      consumer = signer;
    } else {
      signer = new Signer(vaadin, this, maxAttempts, lockedCerts);
      consumer = signer;
    }
    EventQueue.invokeLater(new CertLoading(consumer));
  }

  @SuppressWarnings("unused") // JS API
  public void execute(final String command) {
    if ("detach".equals(command)) {
      if (vaadin != null) {
        vaadin.setUpdatesEnabled(false);
      }
    }
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        doExecute(command, new String[0]);
      }
    });
  }

  @SuppressWarnings("unused") // JS API
  public void execute(final String command, final String[] params) {
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        doExecute(command, params);
      }
    });
  }

  void doExecute(String command, String[] params) {
    if ("close".equals(command)) {
      removeAll();
      invalidate();
      repaint();
    } else if ("block".equals(command)) {
      int current = Integer.parseInt(params[0]);
      int total = Integer.parseInt(params[1]);
      signer.block(current, total, params[2], params[3], params[4]);
    } else if ("chunk".equals(command)) {
      int current = Integer.parseInt(params[0]);
      int total = Integer.parseInt(params[1]);
      byte[] bytes = DatatypeConverter.parseBase64Binary(params[2]);
      signer.chunk(current, total, bytes);
    } else if ("maxProgress".equals(command)) {
      signer.setMaxProgress(Integer.parseInt(params[0]));
    }
  }

  static class CheckServiceAction implements PrivilegedAction<Object> {
    private final String cfg;
    private final String test;

    public CheckServiceAction(String cfg, String test) {
      this.cfg = cfg;
      this.test = test;
    }

    @Override
    public Object run() {
      try {
        Class.forName(test, false, SignApplet.class.getClassLoader());
        System.setProperty(cfg, test);
      } catch (Exception e) {

      }
      return null;
    }
  }
}