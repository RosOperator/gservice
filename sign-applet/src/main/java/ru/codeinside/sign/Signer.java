/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2013, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.sign;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Set;

import static javax.xml.bind.DatatypeConverter.printBase64Binary;

final class Signer implements CertConsumer {

  final private Vaadin vaadin;
  final private Panel ui;
  final Filter filter;
  final int maxAttempts;
  final Set<Long> lockedCerts;

  private PrivateKey privateKey;
  private Signature signature;
  private X509Certificate currentcert;

  private boolean cspShedule;
  
  private static byte[] input;

  private String currentTag;
  private String currentNs;
  private String currentBId;

  private final Label label = new Label("Запуск...");
  private int currentBlock;
  private int blocksCount;
  private int currentChunk;
  private int maxProgress;
  private int currentProgress;
  private boolean isReady = false;

  Signer(Vaadin vaadin, Panel ui, byte[] x509, int maxAttempts, Set<Long> lockedCerts) {
    this.vaadin = vaadin;
    this.ui = ui;
    this.maxAttempts = maxAttempts;
    this.lockedCerts = lockedCerts;
    filter = new EqualsFilter(x509);
  }

  Signer(Vaadin vaadin, Panel ui, int maxAttempts, Set<Long> lockedCerts) {
    this.vaadin = vaadin;
    this.ui = ui;
    this.maxAttempts = maxAttempts;
    this.lockedCerts = lockedCerts;
    filter = new AcceptAll();
  }

  @Override
  public void ready(final String name, PrivateKey privateKey, X509Certificate certificate) {
    if (privateKey != null) {
		cspShedule = false;        
		this.privateKey = privateKey;
		isReady = true;
	} else {
            isReady = true;
            cspShedule = true;
	}

    currentcert = certificate;
    input = null;
    currentBlock = 0;
    blocksCount = 0;
    currentChunk = 0;
    currentProgress = 0;

    ui.removeAll();
    ui.add(new Label(name), BorderLayout.PAGE_START);
    ui.add(label, BorderLayout.CENTER);

    Button prev = new Button("Отменить");
    Panel panel = new Panel(new BorderLayout(2, 2));
    panel.add(prev, BorderLayout.LINE_START);
    ui.add(panel, BorderLayout.PAGE_END);
    prev.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        loading();
      }
    });
    ui.validate();
    ui.repaint();

    notifyReady(certificate);
  }

//  @Override
//  public void wrongPassword(long certSerialNumber) {
//    vaadin.updateVariable("wrongPassword", String.valueOf(certSerialNumber));
//    refresh();
//  }

  @Override
  public void loading() {
    vaadin.updateVariable("state", "loading");

    isReady = false;
    ui.removeAll();
    ui.add(new Label("Загрузка сертификатов..."), BorderLayout.LINE_START);
    Label label = new Label("");
    ui.add(label, BorderLayout.CENTER);
    ui.validate();
    ui.repaint();
    new Thread(new CertDetector(this, ui, label)).start();
//    new Thread(new CertDetector(this, ui, label, maxAttempts, lockedCerts)).start();
  }

  @Override
  public void refresh() {
    ui.validate();
    ui.repaint();
  }

  @Override
  public void noJcp() {
    ui.removeAll();
    ui.add(new Label("Не установлено программное обеспечение КриптоПро, либо отсутствуют действительные сертификаты(ГОСТ Р 34.11/34.10-2001)."), BorderLayout.LINE_START);
    refresh();
    vaadin.updateVariable("state", "noJcp");
  }

  @Override
  public Filter getFilter() {
    return filter;
  }

  @Override
  public String getActionText() {
    return "Подписать";
  }

  @Override
  public String getSelectionLabel() {
    return "Текущий сертификат:";
  }

  @Override
  public void setMaxProgress(int maxProgress) {
    this.maxProgress = maxProgress;
  }

  public void block(int num, int total, String tag, String ns, String blockId) {
    currentTag = tag;
    currentNs = ns;
    currentBId = blockId;
    if (!isReady) {
      return;
    }

    currentBlock = num;
    blocksCount = total;
    currentChunk = 0;
	if (!cspShedule) {
		try {
		signature = Signature.getInstance("GOST3411withGOST3410EL");
		signature.initSign(privateKey);
		notifyBlock(currentBlock);
		} catch (NoSuchAlgorithmException e) {
		fail(e);
		} catch (InvalidKeyException e) {
		fail(e);
	}
	} else {
        input = null;
        notifyBlock(currentBlock);
    }    
  }


  public void chunk(final int num, final int total, final byte[] bytes) {
    if (!isReady) {
      return;
    }

    currentProgress++;
    currentChunk = num;
    if (!cspShedule) {
        try {
          signature.update(bytes);
          if (num < total) {
            notifyChunk(currentChunk);
          } else {
            final byte[] signed = signature.sign();
            signature = null;
            notifySign(signed);
          }
        } catch (SignatureException e) {
          fail(e);
        } 
    } else {
        try {
            if (input == null) {
                input = bytes;
            } else {
                byte[] tmp = new byte[input.length + bytes.length];
                System.arraycopy(input, 0, tmp, 0, input.length);
                System.arraycopy(bytes, 0, tmp, input.length, bytes.length); 
                input = tmp;
            }

            if (num < total) {
              notifyChunk(currentChunk);
            } else {
              if (SignApplet.cspjs.isValid) {
                  byte[] signed = SignApplet.cspjs.getSignature(input, currentcert, currentTag, currentNs, currentBId);   
                  //byte[] signed = SignApplet.cspjs.getSignatureBySI(input, currentcert);  
                  if (!(new String(signed)).equals("cancel")) {
                    notifySign(signed);
                  }
              } else {
                  SignApplet.cspjs.showRegistration();
                  fail(new RuntimeException("Bad installation of browser plugin!"));
              }
            }        
        } catch (RuntimeException e) {
            fail(e);
        } 

    }
  };

  private void fail(final Exception e) {
    label.setText("Ошибка: " + e.getMessage());
    refresh();

    final StringWriter w = new StringWriter();
    e.printStackTrace(new PrintWriter(w));
    vaadin.updateVariable("fail", w.toString());
  }

  private void notifyBlock(int i) {
    vaadin.updateVariable("block", i);
  }

  private void notifyChunk(int i) {
    vaadin.updateVariable("chunk", i);

    float progress = 100f * currentProgress / maxProgress;
    label.setText(((int) progress) + "%");
  }

  private void notifySign(final byte[] b) {
    vaadin.updateVariable("sign", printBase64Binary(b));

    float complete = 100f * currentBlock / blocksCount;
    label.setText(((int) complete) + "%");
    refresh();
  }

  private void notifyReady(final X509Certificate c) {
    try {
      vaadin.updateVariable("cert", printBase64Binary(c.getEncoded()));
      label.setText("Получение данных с СИУ для подписания...");
      refresh();
    } catch (CertificateEncodingException e) {
      fail(e);
    }
  }

}
