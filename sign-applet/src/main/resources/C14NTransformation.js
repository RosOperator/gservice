var C14NTransformation = function() {
	this.escapeEntities = {
		"&": "&amp;",
		"\"": "&quot;",
		"<": "&lt;",
		">": "&gt;",
		"\t": "&#x9;",
		"\n": "&#xA;",
		"\r": "&#xD;"
	};
};
C14NTransformation.prototype =
{
	Transform: function (stringXml, includeComments) {
		var includeComments = includeComments || false;

		var node = jQuery.parseXML(stringXml);

		var transformedXml = this._processInner([], "", !!includeComments, node);

		return transformedXml;
	},

	_processInner: function (prefixesInScope, defaultNamespace, includeComments, node) {
		if (node.nodeType === 3) {
			return (node.ownerDocument === node.parentNode) ? this._escapeTextEntities(node.data.trim()) : this._escapeTextEntities(node.data);
		}
		if (node.nodeType === 7) {
			return this._renderProcessingInstruction(node);
		}
		if (node.nodeType === 8) {
			return includeComments ? this._renderComment(node) : "";
		}
		if (node.nodeType === 10) {
			return "";
		}

		prefixesInScope = prefixesInScope.slice();

		var ns = this._renderNamespace(prefixesInScope, defaultNamespace, node);

		var newChildNodes = [];
		$.map(node.childNodes, function (item) { newChildNodes.push(item); });
		newChildNodes = $.map(newChildNodes, $.proxy(function (e) { return this._processInner(prefixesInScope, ns.newDefaultNamespace, includeComments, e); }, this));

		return [node.tagName ? "<" + node.tagName + ns.rendered + this._renderAttributes(node) + ">" : "", newChildNodes.join(""), node.tagName ? "</" + node.tagName + ">" : ""].join("");
	},

	_renderNamespace: function (prefixesInScope, defaultNamespace, node) {
		var res = "";
		var newDefaultNamespace = defaultNamespace;
		var nsListToRender = [];
		var currentNamespace = node.namespaceURI || "";
		if (node.prefix) {
			if (prefixesInScope.indexOf(node.prefix) === -1) {
				nsListToRender.push({
					prefix: node.prefix,
					namespaceURI: node.namespaceURI
				});
				prefixesInScope.push(node.prefix);
			}
		} else if (defaultNamespace !== currentNamespace) {
			newDefaultNamespace = currentNamespace;
			res += " xmlns=\"" + this._escapeAttributeEntities(newDefaultNamespace) + "\"";
		}
		if (node.attributes) {
			for (var i = 0; i < node.attributes.length; i++) {
				var attr = node.attributes[i];
				if (attr.prefix && prefixesInScope.indexOf(attr.prefix) === -1 && attr.prefix !== "xmlns") {
					nsListToRender.push({
						prefix: attr.prefix,
						namespaceURI: attr.namespaceURI
					});
					prefixesInScope.push(attr.prefix);
				}
			}
		}
		nsListToRender.sort(this._compareNamespaces);
		nsListToRender = $.map(nsListToRender, $.proxy(function (item, a) {
			var p = nsListToRender[a];
			res += " xmlns:" + p.prefix + "=\"" + this._escapeAttributeEntities(p.namespaceURI) + "\"";
		}, this))

		return {
			rendered: res,
			newDefaultNamespace: newDefaultNamespace
		};
	},

	_compareNamespaces: function (a, b) {
		var attr1 = a.prefix + a.namespaceURI;
		var attr2 = b.prefix + b.namespaceURI;
		if (attr1 === attr2) {
			return 0;
		}
		return attr1.localeCompare(attr2);
	},

	_renderAttributes: function (node) {

		var newNodeAttributes = [];
		$.map(node.attributes, function (item) { newNodeAttributes.push(item); });

		newNodeAttributes = newNodeAttributes.filter(function (e) {
			return e.name.indexOf("xmlns") !== 0;
		});
		newNodeAttributes = newNodeAttributes.sort(this._compareAttributes);
		newNodeAttributes = $.map(newNodeAttributes, $.proxy(function (e) {
			return " " + e.name + "=\"" + this._escapeAttributeEntities(e.value) + "\"";
		}, this));

		return newNodeAttributes.join("");
	},

	_compareAttributes: function (a, b) {
		if (!a.prefix && b.prefix) {
			return -1;
		}
		if (!b.prefix && a.prefix) {
			return 1;
		}
		return a.name.localeCompare(b.name);
	},

	_renderComment: function (node) {
		var isOutsideDocument = (node.ownerDocument === node.parentNode);
		var isBeforeDocument = null;
		var isAfterDocument = null;
		if (isOutsideDocument) {
			var nextNode = node, previousNode = node;
			while (nextNode !== null) {
				if (nextNode === node.ownerDocument.documentElement) {
					isBeforeDocument = true;
					break;
				}
				nextNode = nextNode.nextSibling;
			}
			while (previousNode !== null) {
				if (previousNode === node.ownerDocument.documentElement) {
					isAfterDocument = true;
					break;
				}
				previousNode = previousNode.previousSibling;
			}
		}
		return (isAfterDocument ? "\n" : "") + "<!--" + this._escapeTextEntities(node.data) + "-->" + (isBeforeDocument ? "\n" : "");
	},

	_renderProcessingInstruction: function (node) {
		if (node.tagName === "xml") {
			return "";
		}
		var isOutsideDocument = (node.ownerDocument === node.parentNode);
		var isBeforeDocument = null;
		var isAfterDocument = null;
		if (isOutsideDocument) {
			var nextNode = node, previousNode = node;
			while (nextNode !== null) {
				if (nextNode === node.ownerDocument.documentElement) {
					isBeforeDocument = true;
					break;
				}
				nextNode = nextNode.nextSibling;
			}
			while (previousNode !== null) {
				if (previousNode === node.ownerDocument.documentElement) {
					isAfterDocument = true;
					break;
				}
				previousNode = previousNode.previousSibling;
			}
		}
		return (isAfterDocument ? "\n" : "") + "<?" + node.tagName + (node.data ? " " + this._escapeTextEntities(node.data) : "") + "?>" + (isBeforeDocument ? "\n" : "");
	},

	_escapeAttributeEntities: function (string) {
		return string.replace(/([\&<"\t\n\r])/g, $.proxy(function (character) {
			return this.escapeEntities[character];
		}, this));
	},
	_escapeTextEntities: function (string) {
		return string.replace(/([\&<>\r])/g, $.proxy(function (character) {
			return this.escapeEntities[character];
		}, this));
	}

};

if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function (searchElement /*, fromIndex */) {
		"use strict";
		if (this == null) {
			throw new TypeError();
		}
		var t = Object(this);
		var len = t.length >>> 0;

		if (len === 0) {
			return -1;
		}
		var n = 0;
		if (arguments.length > 1) {
			n = Number(arguments[1]);
			if (n != n) { // shortcut for verifying if it's NaN
				n = 0;
			} else if (n != 0 && n != Infinity && n != -Infinity) {
				n = (n > 0 || -1) * Math.floor(Math.abs(n));
			}
		}
		if (n >= len) {
			return -1;
		}
		var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
		for (; k < len; k++) {
			if (k in t && t[k] === searchElement) {
				return k;
			}
		}
		return -1;
	}
}

if (!Array.prototype.filter) {
	Array.prototype.filter = function (fun /*, thisp*/) {
		"use strict";

		if (this == null)
			throw new TypeError();

		var t = Object(this);
		var len = t.length >>> 0;
		if (typeof fun != "function")
			throw new TypeError();

		var res = [];
		var thisp = arguments[1];
		for (var i = 0; i < len; i++) {
			if (i in t) {
				var val = t[i]; // in case fun mutates this
				if (fun.call(thisp, val, i, t))
					res.push(val);
			}
		}

		return res;
	};
}
