@echo off

  
:: DEFS

  
	SET PATH=C:\workspace\Rkis3\sign-applet\src\main\keystore
	SET KT=C:\JDKs\jdk1.8.0_45\bin\keytool



:: _________________________________________________________

%KT% -importkeystore -srckeystore "%PATH%\er76.122015.p12" -srcstoretype pkcs12 -destkeystore "%PATH%\er76.122015.keystore" -deststoretype JKS -deststorepass 1234567890 -srcalias "gbu yaroslavl region electronic region's comodo ca limited id" -destalias er76

