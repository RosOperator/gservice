(function( $, undefined ) {

    var busy = false;

    var methods = {   
        
        isBusy: function () {
            
            return busy;
        },
        

        GetRawCert: function (options) {
            if (!options) return null;

            var certificate = null;

            var pStore = methods._createPluginObject("CAPICOM.store");

            try {
                pStore.Open();
            } catch (e) {
                alert("Ошибка при открытии хранилища сертификатов: " + e.message);
                return null;
            }

            try {
                var foundCertificates = pStore.Certificates.Find(0, options.par1)
                if (foundCertificates.Count == 0) {
                    alert("Сертификат с отпечатком " + options.par1 + " не найден.");
                } else {
                    certificate = foundCertificates.Item(1);
                }
            } catch (e) {
                alert("Ошибка при перечислении сертификатов: " + e.message);
            }

            pStore.Close();

            return methods._getCertificateBinary(certificate);
        },

        GetCertificates: function () {
            var certificates = "";
            var today = new Date();
            var name;
            var pStore = methods._createPluginObject("CAPICOM.Store");

            try {
                pStore.Open();
            } catch (e) {
                alert("Ошибка при открытии хранилища сертификатов: " + e.message);
                return;
            }

            for (var i = 1; i <= pStore.Certificates.Count; i++) {
                try {
                    var certificate = pStore.Certificates.Item(i);

                    var certificateInfoMatch = [];

                    if (certificate.SubjectName.match(new RegExp("SN=([^,]+)", "i")))
                        certificateInfoMatch[1] = certificate.SubjectName.match(new RegExp("SN=([^,]+)", "i"))[1]
                                    + " " + certificate.SubjectName.match(new RegExp("G=([^,]+)", "i"))[1];

                    if (certificate.SubjectName.match(new RegExp("O=([^,]+)", "i")))
                        certificateInfoMatch[2] = certificate.SubjectName.match(new RegExp("O=([^,]+)", "i"))[1];

                    var certificateDateTo = new Date(certificate.ValidToDate);

                    if ((certificateDateTo.getTime() < today.getTime()) ||
                        ((certificate.PrivateKey.ProviderName.indexOf("GOST R 34.10") == -1))) {
                        continue;
                    }

                    if (certificateInfoMatch[1] || certificateInfoMatch[2]) {
                        name = (!!certificateInfoMatch[1] ? certificateInfoMatch[1] + ', ' : "")
                            + (!!certificateInfoMatch[2] ? certificateInfoMatch[2] + ', ' : "")
                                + dateFormat(certificateDateTo, "dd.mm.yyyy") + ' (' + certificate.Thumbprint + ')';
                    } else {
                        name = certificate.SubjectName + ', ' + dateFormat(certificateDateTo, "dd.mm.yyyy");
                    }
                    if (certificates.length == 0) {
                        certificates = name;
                    }
                    else {
                        certificates = certificates + "FOUNDCERT:" + name;
                    }
                } catch (ex) {
                    alert("Ошибка при перечислении сертификатов: " + ex.message);
                    break;
                }
            }

            pStore.Close();

            return certificates;
        },

        IsInstalled: function () {
            try {
                var obj = methods._createPluginObject("CAdESCOM.CPSigner");
                if (!obj)
                    return false;
            }
            catch (exception) {
                return false;
            }
            return true;
        },

        ShowRegistration: function () {
            alert("Не установлен КриптоПро CSP, КриптоПро браузер-плагин, или отсутствуют действительные сертификаты(ГОСТ Р 34.11/34.10-2001)");
        },

        GetSignatureForData: function (options) {
            
            if (!options) return null;
            var certName = methods._utf8Decode(methods._base64Decode(options.par2));
            var tagToSign = methods._utf8Decode(methods._base64Decode(options.par3));
            var nsToSign = methods._utf8Decode(methods._base64Decode(options.par4));
            if (nsToSign == "http://enclosure/") {
                var data = options.par1;
            } else {
                data = methods._utf8Decode(methods._base64Decode(options.par1));
            }

            var selectedCert = methods._getCertByName(certName);
            if (!selectedCert) return null;

            if (tagToSign == "Body") {

                return methods._getOV(data, selectedCert);

            } else if (nsToSign == "http://enclosure/")
            {
                return methods._getSPEnclosure(data, selectedCert);

            } else if (nsToSign =="http://nowhere/" && tagToSign == "nothing")
            {
                return methods._getSPAppData("<AppData>" + data + "</AppData>", selectedCert);// Id=\"AppData\"

            } else

                return methods._getSPCustom(data, tagToSign, nsToSign, selectedCert);
        },

        _getOV: function (data,selectedCert) {

            busy = true;
            try {
                if (!selectedCert || !data) {
                    return null;
                }

                data = data.replace(/[\n\r]/g, '');
                data = data.replace(/>\s*/g, '>');
                data = data.replace(/\s*</g, '<');
                var xml = $.parseXML(data);

                var header = xml.getElementsByTagNameNS("http://schemas.xmlsoap.org/soap/envelope/","Header")[0];
                var security = xml.getElementsByTagNameNS("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security")[0];
                header.removeChild(security);

                var body = xml.getElementsByTagNameNS("http://schemas.xmlsoap.org/soap/envelope/", "Body")[0];
                
                if (!body) return null;

                var id;
                if (!body.getAttributeNS(
                    "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd", "Id"
                    )) {
                        body.setAttributeNS("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd",
                            "Id", "body");
                        id = "body";
                    
                } else id = body.getAttributeNS(
                    "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd", "Id"
                    )

                dataToSign = new XMLSerializer().serializeToString(body);

                var digestValue = methods._computeHashBase64(dataToSign, true);

                var signedInfoRaw = methods._getSignedInfoSignatureRawForHeader(id, digestValue);

                var signatureValue = methods._computeRawSignature(signedInfoRaw, selectedCert, true);

                return signatureValue;

            } finally {
                busy = false;
            }
        },

        _getSPEnclosure: function (data, selectedCert) {
            busy = true;
            try {
                if (!selectedCert || !data) {
                    return null;
                }
                
                var signedData = null;

                var pSigner = methods._createPluginObject("CAdESCOM.CPSigner");
                pSigner.Certificate = selectedCert;

                var pSignedData = methods._createPluginObject("CAdESCOM.CadesSignedData");
                pSignedData.ContentEncoding = 1;
                pSignedData.Content = data;

                signedData = pSignedData.SignCades(pSigner, 1, true);
                signedData = signedData.replace(/(?:\\[rn]|[\r\n]+)+/g, "");

                return signedData;

            } finally {
                busy = false;
            }
        },

        _getSPAppData: function (data, selectedCert) {
            busy = true;
            try {
                data = data.replace(/[\n\r]/g, '');
                data = data.replace(/[\n\r]/g, '');
                data = data.replace(/>\s*/g, '>');
                data = data.replace(/\s*</g, '<');
                var xml = $.parseXML(data);
                var part = xml.getElementsByTagName("AppData")[0];
                if (!part) return null;

                var id;
                if (!part.attributes.getNamedItem("Id")) {
                    part.setAttribute("Id", "AppData");
                    id = "AppData";
                } else id = part.attributes.getNamedItem("Id").value;
                
                dataToSign = new XMLSerializer().serializeToString(part);

                var binarySecurityToken = methods._getCertificateBinary(selectedCert);

                var digestValue = methods._computeHashBase64(dataToSign, true);

                var signedInfoRaw = methods._getSignedInfoSignatureRawForAppData(id, digestValue);

                var signatureValue = methods._computeRawSignature(signedInfoRaw, selectedCert, true);

                return signatureValue;
                
            } finally {
                busy = false;
            }
        },

        _getSPCustom: function (data, tagToSign, nsToSign, selectedCert) {
            busy = true;
            try {
                data = data.replace(/[\n\r]/g, '');
                data = data.replace(/>\s*/g, '>');
                data = data.replace(/\s*</g, '<');
                var xml = $.parseXML(data);
                var part = xml.getElementsByTagNameNS(nsToSign, tagToSign)[0];
                if (!part) return null;

                var id;
                if (!part.attributes.getNamedItem("Id")) {
                    part.setAttribute("Id", tagToSign);
                    id = tagToSign;
                } else id = part.attributes.getNamedItem("Id").value;
                
                dataToSign = new XMLSerializer().serializeToString(part);

                var binarySecurityToken = methods._getCertificateBinary(selectedCert);

                var digestValue = methods._computeHashBase64(dataToSign, true);

                var signedInfoRaw = methods._getSignedInfoSignatureRawForAppData(id, digestValue);

                var signatureValue = methods._computeRawSignature(signedInfoRaw, selectedCert, true);

                return signatureValue;
                
            } finally {
                busy = false;
            }
        },

        _computeRawSignature: function (data, pCertificate, canonize) {
            busy = true;
            try {
                if (!data) return null;

                if (canonize) {
                    data = methods._canonizeC14N(data);
                }

                data = methods._utf8Encode(data);
                data = methods._base64Encode(data);

                var hashedData = methods._createPluginObject("CAdESCOM.HashedData");
                hashedData.DataEncoding = 0x01;
                hashedData.Hash(data);
                var signedData;
                var rawSignature = methods._createPluginObject("CAdESCOM.RawSignature");
                try {
                    signedData = rawSignature.SignHash(hashedData, pCertificate);
                    signedData = methods._hexToBin(signedData);
                    signedData = methods._reverseString(signedData);
                    signedData = methods._base64Encode(signedData);

                    return signedData;
                } catch (exception) {
                    if ((exception.description == "Действие было отменено пользователем.") ||
                        (exception.description == "Операция была отменена пользователем."))
                        return methods._base64Encode("cancel");
                }

                
            } finally {
                busy = false;
            }
        },

        _getSignedInfoSignatureRawForAppData: function (signTargetId, digestValue) {
            busy = true;
            try {
                var stringSignTargetId = signTargetId ? '#' + signTargetId : '';
                var signedInfo =
                '<SignedInfo xmlns="http://www.w3.org/2000/09/xmldsig#">' +
                    '<CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#" />' +
                    '<SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411" />' +
                    '<Reference URI="' + stringSignTargetId + '">' +
                        '<Transforms>' +
                            '<Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" />' +
                            '<Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#" />' +
                        '</Transforms>' +
                        '<DigestMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#gostr3411" />' +
                        '<DigestValue>' + digestValue + '</DigestValue>' +
                    '</Reference>' +
                '</SignedInfo>';
                return signedInfo;
            } finally {
                busy = false;
            }
        },

        _getSignedInfoSignatureRawForHeader: function (signTargetId, digestValue) {
            busy = true;
            try {
                var stringSignTargetId = signTargetId ? '#' + signTargetId : '';
                var signedInfo =
                '<SignedInfo xmlns="http://www.w3.org/2000/09/xmldsig#">' +
                    '<CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#" />' +
                    '<SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411" />' +
                    '<Reference URI="' + stringSignTargetId + '">' +
                        '<Transforms>' +
                            '<Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#" />' +
                        '</Transforms>' +
                        '<DigestMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#gostr3411" />' +
                        '<DigestValue>' + digestValue + '</DigestValue>' +
                    '</Reference>' +
                '</SignedInfo>';
                return signedInfo;
            } finally {
                busy = false;
            }
            
        },

        _computeHash: function (data, canonize) {
            busy = true;
            try {
                if (!data) return null;

                if (canonize) {
                    data = methods._canonizeC14N(data);
                }

                data = methods._utf8Encode(data);
                data = methods._base64Encode(data);

                var hashedData = methods._createPluginObject("CAdESCOM.HashedData");
                hashedData.DataEncoding = 0x01;
                hashedData.Hash(data);
                return hashedData.Value;
            } finally {
                busy = false;
            }
        },

        _computeHashBase64: function (data, canonize) {
            busy = true;
            try {
                if (!data) return null;

                var hash = methods._computeHash(data, canonize);
                hash = methods._hexToBin(hash);
                hash = methods._base64Encode(hash);

                return hash;
            } finally {
                busy = false;
            }
        },

        _getCertByName: function (SubjectName) {
            busy = true;
            try {
                if (!SubjectName) return null;

                var certificate = null;
                var today = new Date();

                var pStore = methods._createPluginObject("CAPICOM.Store");

                try {
                    pStore.Open();
                } catch (e) {
                    alert("Ошибка при открытии хранилища сертификатов: " + e.message);
                    return;
                }

                for (var i = 1; i <= pStore.Certificates.Count; i++) {
                    try {
                        var cert = pStore.Certificates.Item(i);

                        var certificateDateTo = new Date(cert.ValidToDate);

                        if (!(certificateDateTo.getTime() < today.getTime()) &&
                                ((cert.PrivateKey.ProviderName.indexOf("GOST R 34.10") != -1)) &&
                                    (methods._getCertCNO(cert.SubjectName) == methods._getCertCNO(SubjectName))) {

                            certificate = cert;
                        }
                        
                    } catch (ex) {
                        alert("Ошибка при перечислении сертификатов: " + ex.message);
                        break;
                    }
                }

                pStore.Close();

                return certificate;
            } finally {
                busy = false;
            }
        },

        _reverseString: function (str) {
            busy = true;
            try {
                var newStr = '';
                for (var i = str.length - 1; i >= 0; i--) {
                    newStr += str.charAt(i);
                }
                return newStr;
            } finally {
                busy = false;
            }
        },

        _getCertCNO: function (SN) {
            busy = true;
            try {
                var cn, o;
                var inp = SN.split(',');
                inp.forEach(function (item, i, inp) {
                    if (item.toString().indexOf("CN=") != -1) cn = item.replace(/[\\/" ,]/g, '');
                    if (item.toString().indexOf("O=") != -1) o = item.replace(/[\\/" ,]/g, '');
                });

                return cn + ", " + o;
            } finally {
                busy = false;
            }
        },

        _createPluginObject: function (objectName) {
            busy = true;
            try {
                var ua = window.navigator.userAgent;
                var msie = ua.indexOf("MSIE ");
                if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                    return new ActiveXObject(objectName);
                } else {
                    var cadesobject = document.getElementById('cadesplugin');
                    if (cadesobject == null) {
                        var mimetype = navigator.mimeTypes["application/x-cades"];
                        if (mimetype) {
                            var plugin = mimetype.enabledPlugin;
                            if (plugin) {
                                $('body').append("<object id=\"cadesplugin\" type=\"application/x-cades\" class=\"hiddenObject\"></object>");
                            }
                        }
                        var cadesobject = document.getElementById('cadesplugin');
                    }
                    return cadesobject.CreateObject(objectName);
                }
            } finally {
                busy = false;
            }
        },

        _getCertificateBinary: function (certificate) {
            busy = true;
            try {
                var binarySecurityToken = certificate.Export(0);
                binarySecurityToken = binarySecurityToken.replace(/[\r\n]/g, '');
                return binarySecurityToken;
            } finally {
                busy = false;
            }
        },

        _base64KeyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

        _base64Encode: function (input) {
            busy = true;
            try {
                var output = "";
                var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
                var i = 0;

                while (i < input.length) {
                    chr1 = input.charCodeAt(i++);
                    chr2 = input.charCodeAt(i++);
                    chr3 = input.charCodeAt(i++);

                    enc1 = chr1 >> 2;
                    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                    enc4 = chr3 & 63;

                    if (isNaN(chr2)) {
                        enc3 = enc4 = 64;
                    } else if (isNaN(chr3)) {
                        enc4 = 64;
                    }

                    output = output +
                    this._base64KeyStr.charAt(enc1) + this._base64KeyStr.charAt(enc2) +
                    this._base64KeyStr.charAt(enc3) + this._base64KeyStr.charAt(enc4);

                }

                return output;
            } finally {
                busy = false;
            }
        },

        _base64Decode: function (input) {
            busy = true;
            try {
                var output = "";
                var chr1, chr2, chr3;
                var enc1, enc2, enc3, enc4;
                var i = 0;

                input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

                while (i < input.length) {
                    enc1 = this._base64KeyStr.indexOf(input.charAt(i++));
                    enc2 = this._base64KeyStr.indexOf(input.charAt(i++));
                    enc3 = this._base64KeyStr.indexOf(input.charAt(i++));
                    enc4 = this._base64KeyStr.indexOf(input.charAt(i++));

                    chr1 = (enc1 << 2) | (enc2 >> 4);
                    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                    chr3 = ((enc3 & 3) << 6) | enc4;

                    output = output + String.fromCharCode(chr1);

                    if (enc3 != 64) {
                        output = output + String.fromCharCode(chr2);
                    }
                    if (enc4 != 64) {
                        output = output + String.fromCharCode(chr3);
                    }
                }

                return output;
            } finally {
                busy = false;
            }
        },

        _utf8Encode: function (string) {
            busy = true;
            try {
                string = string.replace(/\r\n/g, "\n");
                var utftext = "";

                for (var n = 0; n < string.length; n++) {
                    var c = string.charCodeAt(n);

                    if (c < 128) {
                        utftext += String.fromCharCode(c);
                    }
                    else if ((c > 127) && (c < 2048)) {
                        utftext += String.fromCharCode((c >> 6) | 192);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }
                    else {
                        utftext += String.fromCharCode((c >> 12) | 224);
                        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }
                }

                return utftext;
            } finally {
                busy = false;
            }
        },

        _utf8Decode: function (utftext) {
            busy = true;
            try {
                var string = "";
                var i = 0;
                var c = c1 = c2 = 0;

                while (i < utftext.length) {
                    c = utftext.charCodeAt(i);

                    if (c < 128) {
                        string += String.fromCharCode(c);
                        i++;
                    }
                    else if ((c > 191) && (c < 224)) {
                        c2 = utftext.charCodeAt(i + 1);
                        string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                        i += 2;
                    }
                    else {
                        c2 = utftext.charCodeAt(i + 1);
                        c3 = utftext.charCodeAt(i + 2);
                        string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                        i += 3;
                    }
                }

                return string;
            } finally {
                busy = false;
            }
        },

        _hexToBin: function (hex) {
            busy = true;
            try {
                var bytes = [];
                for (var i = 0; i < hex.length - 1; i += 2) {
                    bytes.push(parseInt(hex.substr(i, 2), 16));
                }
                return String.fromCharCode.apply(String, bytes);
            } finally {
                busy = false;
            }
        },

        _binToHex: function (bin) {
            busy = true;
            try {
                var i = 0, l = bin.length, chr, hex = '';

                for (i; i < l; ++i) {
                    chr = bin.charCodeAt(i).toString(16);

                    hex += chr.length < 2 ? '0' + chr : chr;
                }

                return hex;
            } finally {
                busy = false;
            }
        },

        _canonizeC14N: function (xmlDocument) {
            busy = true;
            try {
                var c14n = new C14NTransformation();
                var result = c14n.Transform(xmlDocument);
                return result;
            } finally {
                busy = false;
            }
        }

    };

    $.fn.cryptoManager = function(mHandle, options) {
        return methods[mHandle].apply(
            this, [
                (undefined == options)? {} : options
            ]
        );
    };

})(jQuery);