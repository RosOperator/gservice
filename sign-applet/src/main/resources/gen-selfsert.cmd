@echo off

  
:: DEFS

  
	SET PATH=C:\workspace\csp-applet\src\main\keystore
	SET KT=C:\JDKs\jdk1.8.0_45\bin\keytool



:: _________________________________________________________



%KT% -genkey -alias er76 -keystore "%PATH%\er76.keystore" -storepass 1234567890 -keypass 1234567890 -dname "CN=er76, OU=GSES DEV TEAM, O=ru.er76, L=yaroslavl, ST=Russia, C=RU"


%KT% -selfcert -alias er76 -keystore "%PATH%\er76.keystore" -storepass 1234567890 -keypass 1234567890

%KT% -importkeystore -srckeystore "%PATH%\er76.keystore" -destkeystore "%PATH%\er76.p12" -srcstoretype JKS -deststoretype PKCS12 -deststorepass 1234567890 -srcalias er76 -destalias er76