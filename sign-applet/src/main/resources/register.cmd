@ECHO OFF
SET RKPTH=
SET DATE_OLD=
SET DATE_NEW=

if exist C:\Windows\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe (
    SET RA=C:\Windows\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe
) else (
    if exist C:\Windows\Microsoft.NET\Framework\v3.5\RegAsm.exe (
        SET RA=C:\Windows\Microsoft.NET\Framework\v3.5\RegAsm.exe
    ) else (
        ECHO ON
        echo ".net not found"
    )
)
ECHO ON

%RA% %RKPTH%\SiuUtils.JavaAdapter.dll" /tlb:%RKPTH%SiuUtils.JavaAdapter.tlb" /codebase
pause
@echo off
if %errorlevel% neq 0 exit /b %errorlevel% 
setlocal disableDelayedExpansion


set if1=%RKPTH%RKIS2.ini"
set of1=%RKPTH%RKIS2.new.ini"
set "_strFind=FIRST_LAUNCH = true"
set "_strInsert=FIRST_LAUNCH = false"


>%of1% (
  for /f "usebackq delims=" %%A in (%if1%) do (
    if "%%A" equ "%_strFind%" (echo %_strInsert%) else (echo %%A)
  )
)

set if2=%RKPTH%RKIS2.new.ini"
set of2=%RKPTH%RKIS2.ini"
set "_strFind=%DATE_OLD%"
set "_strInsert=%DATE_NEW%"


>%of2% (
  for /f "usebackq delims=" %%A in (%if2%) do (
    if "%%A" equ "%_strFind%" (echo %_strInsert%) else (echo %%A)
  )
)

del %RKPTH%RKIS2.new.ini"
@echo on
@echo "ini updated"
pause