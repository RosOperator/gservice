:: Unregister
@ECHO OFF
	SET PATH=%temp%

if exist C:\Windows\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe (
    SET RA=C:\Windows\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe
) else (
    if exist C:\Windows\Microsoft.NET\Framework\v3.5\RegAsm.exe (
    	SET RA=C:\Windows\Microsoft.NET\Framework\v3.5\RegAsm.exe
	) else (
    echo can not unreg, .net not found
	)
)
ECHO ON
%RA4% %PATH%\SiuUtils.JavaAdapter.dll /tlb:SiuUtils.JavaAdapter.tlb  /u
 
echo enter to close command tool batch file
pause