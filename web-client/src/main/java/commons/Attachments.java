package commons;


import java.io.*;
import java.util.UUID;

public final class Attachments {

    private static final File BASE_DIR = new File(System.getProperty("com.sun.aas.instanceRoot"), "files");
    private static final File BUFFER_DIR = new File(BASE_DIR, "buffers");
    private static final File ATTACHMENT_DIR = new File(BASE_DIR, "attachments");

    static {
        if (!BUFFER_DIR.exists()) {
            BUFFER_DIR.mkdirs();
        }
        if (!ATTACHMENT_DIR.exists()) {
            ATTACHMENT_DIR.mkdirs();
        }
    }

    private Attachments() {
    }

    public static File storeBytesBuffer(File tmpFile) {
        File file = new File(BUFFER_DIR, UUID.randomUUID().toString());
        try {
            return copyTo(new FileInputStream(tmpFile), file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static File storeAttachmentContent(InputStream content) {
        File file = new File(ATTACHMENT_DIR, UUID.randomUUID().toString());
        return copyTo(content, file);
    }

    private static File copyTo(InputStream fromContent, File toFile) {
        InputStream in = null;
        OutputStream out = null;
        try {
            if (!toFile.createNewFile()) {
                throw new RuntimeException("Не удалось создать файл: " + toFile.getAbsolutePath());
            }
            in = new BufferedInputStream(fromContent);
            out = new BufferedOutputStream(new FileOutputStream(toFile));
            Streams.copy(in, out);
            out.flush();
            return toFile;
        } catch (IOException e) {
            throw new RuntimeException("Не удалось скопировать временный файл: " + e.getMessage(), e);
        } finally {
            Streams.close(in, out);
        }
    }
}
