package commons;

import java.io.InputStream;

public final class StreamHolder {
    private InputStream is;

    public InputStream getStream() {
        return is;
    }

    public void update(InputStream is) {
        release();
        this.is = is;
    }

    public void release() {
        Streams.close(is);
        is = null;
    }
}
