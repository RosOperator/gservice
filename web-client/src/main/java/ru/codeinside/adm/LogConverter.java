/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2014, MPL CodeInside http://codeinside.ru
 */
package ru.codeinside.adm;

import commons.Streams;
import java.io.BufferedOutputStream;
import java.io.EOFException;

import org.codehaus.jackson.map.ObjectMapper;
import ru.codeinside.adm.database.ExternalGlue;
import ru.codeinside.adm.database.HttpLog;
import ru.codeinside.adm.database.SmevLog;
import ru.codeinside.adm.database.SoapPacket;
import ru.codeinside.gses.API;
import ru.codeinside.gses.webui.osgi.LogCustomizer;
import ru.codeinside.gws.log.format.Metadata;
import ru.codeinside.gws.log.format.Pack;

import javax.ejb.DependsOn;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static javax.ejb.TransactionAttributeType.REQUIRES_NEW;

@TransactionManagement
@Singleton
@Lock(LockType.READ)
@DependsOn("BaseBean")
public class LogConverter {

    @PersistenceContext(unitName = "myPU")
    EntityManager em;

    final Logger logger = Logger.getLogger(getClass().getName());
    String dirPath;
    Storage storage;

    public static boolean logConverterDebugModeEnabled = false;

    private String logStoragePath;

    // test support
    public interface Storage {

        String getLazyDirPath();

        SmevLog findLogEntry(String marker);

        void store(SmevLog smevLog);
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    @TransactionAttribute(REQUIRES_NEW)
    public boolean logToBd() {
        if (storage == null) {
            storage = new Storage() {
                @Override
                public String getLazyDirPath() {
                    return LogCustomizer.getStoragePath();
                }

                @Override
                public SmevLog findLogEntry(String marker) {
                    List<SmevLog> rs = em.
                            createQuery("select l from SmevLog l where l.marker = :marker", SmevLog.class)
                            .setMaxResults(1)
                            .setParameter("marker", marker)
                            .getResultList();
                    return rs.isEmpty() ? null : rs.get(0);
                }

                @Override
                public void store(SmevLog smevLog) {
                    em.persist(smevLog);
                }
            };
        }

        String pathInfo = getDirPath();
        if (isEmpty(pathInfo)) {
            return false;
        }

        final File logPackage = listLowDirs(new File(pathInfo));
        if (logPackage == null) {
            return false;
        }
        try {
            SmevLog log = getOepLog(logPackage.getName());
            File[] logFiles = logPackage.listFiles();
            if (logFiles != null) {
                for (File logFile : logFiles) {
                    String name = logFile.getName();
                    
                    if (Metadata.HTTP_RECEIVE.equals(name)) {
                        log.setReceiveHttp(new HttpLog(Streams.toBytes(logFile)));

                    } else if (Metadata.HTTP_SEND.equals(name)) {
                        log.setSendHttp(new HttpLog(Streams.toBytes(logFile)));

                    } else if (Metadata.METADATA.equals(name)) {
                        ObjectMapper objectMapper = new ObjectMapper();
                        Metadata metadata = null;
                        
                        try {
                        	metadata = objectMapper.readValue(logFile, Metadata.class);
                        } catch (EOFException e) {
                        	logger.log(Level.WARNING, "Cannot parsing log: " + logFile.getPath());
						}
                        
                        if (metadata != null) {
                        	log.setClient(metadata.client);
                            log.setError(metadata.error);
                            log.setLogDate(metadata.date);
                            log.setComponent(limitLength(metadata.componentName, 255));
                            if (metadata.bid != null) {
                                log.setBidId(metadata.bid);
                            }
                            if (metadata.send != null) {
                                log.setSendPacket(getSoapPacket(metadata.send));
                            }
                            if (metadata.receive != null) {
                                log.setReceivePacket(getSoapPacket(metadata.receive));
                            }
                        }
                        
                        // Р Р† РЎРѓРЎС“РЎвЂ°Р Р…Р С•РЎРѓРЎвЂљР С‘ Р Р…Р ВµРЎвЂљ processInstanceId
                    }
                    deleteFile(logFile);
                }
                if (log.getBidId() == null && !log.isClient()) {
                    ExternalGlue glue = getExternalGlue(log);
                    if (glue != null) {
                        log.setBidId(glue.getId());
                    }
                }
                if (isEmpty(log.getInfoSystem())) {
                    String infoSystem = null;
                    if (log.isClient()) {
                        SoapPacket sendPacket = log.getSendPacket();
                        if (sendPacket != null) {
                            infoSystem = sendPacket.getRecipient();
                        }
                    } else {
                        SoapPacket receivePacket = log.getReceivePacket();
                        if (receivePacket != null) {
                            infoSystem = receivePacket.getSender();
                        }
                    }
                    log.setInfoSystem(infoSystem);
                }
                
                if (log.getComponent() != null) {
                	storage.store(log);
                }
            }
            deleteFile(logPackage);
        } catch (Exception e) {
            logger.log(Level.WARNING, "failure", e);
            return false;
        }
        return true;
    }

    private boolean checkLogSettings() {
        logStoragePath = AdminServiceProvider.get().getSystemProperty(API.EXTERNAL_LOG_PATH);

        if (logStoragePath == null) {
            System.out.println("[LOG CLEANER] External log storage path is not set. Please, set path in Admin panel. Exit...");
            return false;
        }

        if (logStoragePath.isEmpty()) {
            System.out.println("[LOG CLEANER] External log storage path is not set. Please, set path in Admin panel. Exit...");
            return false;
        }

        return true;
    }

    private boolean isFileExists(String filePath) {
        File f = new File(filePath);
        if (f.isFile()) {
            return true;
        }
//        if (f.exists() && !f.isDirectory()) {
//            return true;
//        }
        
        return false;
    }

    
    @TransactionAttribute(REQUIRES_NEW)
    public boolean logToZip() {

        Date edgeDate = calcEdge();

        if (!checkLogSettings()) {
            return false;
        }

        logStoragePath = AdminServiceProvider.get().getSystemProperty(API.EXTERNAL_LOG_PATH);

        String lastLogId = AdminServiceProvider.get().getSystemProperty(API.LAST_LOG_ID);
        if (lastLogId == null) {
            lastLogId = "0";
        }

        Long lastLogIdLong = Long.parseLong(lastLogId);

        Number count = em.createQuery("select count(o) from SmevLog o where o.id > :lastLogId and (o.logDate is null or o.logDate < :date) and (o.sendHttp.data is not null or o.receiveHttp.data is not null)", Number.class)
                .setParameter("date", edgeDate)
                .setParameter("lastLogId", lastLogIdLong)
                .getSingleResult();

        if (count == null || count.intValue() == 0) {
            System.out.println("[LOG CLEANER] No available logs for cleaning. Exit...");
            return false;
        }

        System.out.println("[LOG CLEANER] Found " + Integer.toString(count.intValue()) + " logs for cleaning. Processing...");

        ZipOutputStream zip = null;
        try {
            for (int i = 0; i < count.intValue(); i++) {
                writeDebugLog("[LOG CLEANER][DEBUGMODE] 1. Fetching SmevLog database record... Log increment = " + Integer.toString(i));
                
                List<SmevLog> logs = em.createQuery("select o from SmevLog o where o.id > :lastLogId and (o.logDate is null or o.logDate < :date) and (o.sendHttp.data is not null or o.receiveHttp.data is not null) order by o.id", SmevLog.class)
                        .setParameter("date", edgeDate)
                        .setParameter("lastLogId", lastLogIdLong)
                        .setMaxResults(1)
                        .getResultList();

                if (logs.isEmpty()) {
                    System.out.println("[LOG CLEANER] Finished cleaning logs. Processed " + Integer.toString(i) + " records");
                    return false;
                }

                for (SmevLog log : logs) {
                    writeDebugLog("[LOG CLEANER][DEBUGMODE] 2. Start processing log (bid=" + log.getBidId() + ")");
                    writeDebugLog("[LOG CLEANER][DEBUGMODE] 3. Log id = " + Long.toString(log.getId()));
                    
                    lastLogIdLong = log.getId();
                    AdminServiceProvider.get().saveSystemProperty(API.LAST_LOG_ID, Long.toString(lastLogIdLong));

                    final long packageTime = log.getLogDate() == null ? log.getDate().getTime() : log.getLogDate().getTime();
                    final String packagePath;

                    Long bidLong = log.getBidId();
                    
                    String zipFilePath = prepareZipFileName(log.getLogDate(), bidLong);
                    if (isFileExists(zipFilePath)) {
                        System.out.println("[LOG CLEANER][DEBUGMODE] Existing file detected: " + zipFilePath + ". Passing...");
                        continue;
                    }

                    writeDebugLog("[LOG CLEANER][DEBUGMODE] 4. Creating zip container...");
                    zip = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(createZipFileName(log.getLogDate(), bidLong))));

                    writeDebugLog("[LOG CLEANER][DEBUGMODE] 5. Zip container was successfully created");

                    packagePath = "/";

                    if (log.getSendHttp() != null && log.getSendHttp().getData() != null) {
                        writeDebugLog("[LOG CLEANER][DEBUGMODE] 6. Put send_http log into zip container...");

                        ZipEntry httpSend = new ZipEntry(packagePath + Metadata.HTTP_SEND);
                        httpSend.setTime(packageTime);
                        zip.putNextEntry(httpSend);
                        zip.write(log.getSendHttp().getData());
                        zip.closeEntry();
                    }

                    if (log.getReceiveHttp() != null && log.getReceiveHttp().getData() != null) {
                        writeDebugLog("[LOG CLEANER][DEBUGMODE] 7. Put receive_http log into zip container...");

                        ZipEntry httpReceive = new ZipEntry(packagePath + Metadata.HTTP_RECEIVE);
                        httpReceive.setTime(packageTime);
                        zip.putNextEntry(httpReceive);
                        zip.write(log.getReceiveHttp().getData());
                        zip.closeEntry();
                    }

                    ObjectMapper objectMapper = new ObjectMapper();
                    Metadata metadata = new Metadata();
                    metadata.client = log.isClient();
                    metadata.error = log.getError();
                    metadata.date = log.getLogDate();
                    metadata.componentName = log.getComponent();

                    Long bidId = log.getBidId();
                    if (bidId != null) {
                        metadata.bid = bidId;

                        writeDebugLog("[LOG CLEANER][DEBUGMODE] 8. Fetching processInstanceId record...");

                        List<String> ids = em.createQuery("select b.processInstanceId from Bid b where b.id = :id", String.class)
                                .setParameter("id", bidId).getResultList();
                        if (!ids.isEmpty()) {
                            metadata.processInstanceId = ids.get(0);
                        }
                    }
                    metadata.send = getPack(log.getSendPacket());
                    metadata.receive = getPack(log.getReceivePacket());

                    writeDebugLog("[LOG CLEANER][DEBUGMODE] 9. Put metadata into zip container...");

                    ZipEntry logMetadata = new ZipEntry(packagePath + Metadata.METADATA);
                    logMetadata.setTime(packageTime);
                    zip.putNextEntry(logMetadata);
                    zip.write(objectMapper.writeValueAsBytes(metadata));
                    zip.closeEntry();

                    writeDebugLog("[LOG CLEANER][DEBUGMODE] 10. Closing zip container...");
                    zip.close();

                    writeDebugLog("[LOG CLEANER][DEBUGMODE] 11. Set null values to DB record...");

                    HttpLog receiveLog = log.getReceiveHttp();
                    HttpLog sendLog = log.getSendHttp();

                    if (receiveLog != null) {
                        receiveLog.setData(null);
                    }

                    if (sendLog != null) {
                        sendLog.setData(null);
                    }

                    log.setReceiveHttp(receiveLog);
                    log.setSendHttp(sendLog);

//                    lastLogIdLong = log.getId();
//                    AdminServiceProvider.get().saveSystemProperty(API.LAST_LOG_ID, Long.toString(log.getId()));

                    em.persist(log);
                    em.flush();
                    em.clear();
                }
            }

//            AdminServiceProvider.get().saveSystemProperty(API.LAST_LOG_ID, Long.toString(lastLogIdLong));

        } catch (IOException e) {
            logger.log(Level.WARNING, "[LOG CLEANER] I/O error", e);
        } finally {
            Streams.close(zip);
        }

        System.out.println("[LOG CLEANER] Finished cleaning logs. Processed " + Integer.toString(count.intValue()) + " records");

        return true;
    }

    private void writeDebugLog(String debugMsg) {
        if (logConverterDebugModeEnabled) {
            System.out.println(debugMsg);
        }
    }

    // ---- internals ----
    private SmevLog getOepLog(String marker) {
        SmevLog entry = storage.findLogEntry(marker);
        if (entry == null) {
            entry = new SmevLog();
            entry.setDate(new Date());
            entry.setMarker(marker);
        }
        return entry;
    }

    private String getDirPath() {
        if (dirPath == null) {
            dirPath = storage.getLazyDirPath();
        }
        return dirPath;
    }

    private ExternalGlue getExternalGlue(SmevLog log) {
        ExternalGlue externalGlue = getExternalGlue(log.getReceivePacket());
        if (externalGlue != null) {
            return externalGlue;
        }
        return getExternalGlue(log.getSendPacket());
    }

    private ExternalGlue getExternalGlue(SoapPacket soapPacket) {
        if (em == null || soapPacket == null) {
            return null;
        }
        String[] idRefs = {soapPacket.getOriginRequestIdRef(), soapPacket.getRequestIdRef()};
        for (String idRef : idRefs) {
            if (!isEmpty(idRef)) {
                List resultList = em.createQuery("select g from ExternalGlue g where g.requestIdRef = :requestIdRef")
                        .setParameter("requestIdRef", idRef).getResultList();
                if (!resultList.isEmpty()) {
                    return (ExternalGlue) resultList.get(0);
                }
            }
        }
        return null;
    }

    private SoapPacket getSoapPacket(Pack packet) {
        SoapPacket result = new SoapPacket();
        result.setSender(packet.sender);
        result.setRecipient(packet.recipient);
        result.setOriginator(packet.originator);
        result.setService(packet.serviceName);
        result.setTypeCode(packet.typeCode);
        result.setStatus(packet.status);
        result.setDate(packet.date);
        result.setRequestIdRef(packet.requestIdRef);
        result.setOriginRequestIdRef(packet.originRequestIdRef);
        result.setServiceCode(packet.serviceCode);
        result.setCaseNumber(packet.caseNumber);
        result.setExchangeType(packet.exchangeType);
        return result;
    }

    private Pack getPack(SoapPacket packet) {
        if (packet == null) {
            return null;
        }
        Pack result = new Pack();
        result.sender = packet.getSender();
        result.recipient = packet.getRecipient();
        result.originator = packet.getOriginator();
        result.serviceName = packet.getService();
        result.typeCode = packet.getTypeCode();
        result.status = packet.getStatus();
        result.date = packet.getDate();
        result.requestIdRef = packet.getRequestIdRef();
        result.originRequestIdRef = packet.getOriginRequestIdRef();
        result.serviceCode = packet.getServiceCode();
        result.caseNumber = packet.getCaseNumber();
        result.exchangeType = packet.getExchangeType();
        return result;
    }

    private String getZipPath() {
        String instanceRoot = System.getProperty("com.sun.aas.instanceRoot");
        File file;
        if (instanceRoot != null) {
            file = new File(new File(new File(instanceRoot), "logs"), "zip");
        } else {
            file = new File("/var/", "zip");
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }

    private boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    private File listLowDirs(File root) {
        File[] files = root.listFiles();
        if (files != null) {
            Arrays.sort(files);
            for (File file : files) {
                if (file.isDirectory()) {
                    File file1 = listLowDirs(file);
                    if (file1 != null) {
                        return file1;
                    }
                } else {
                    return root;
                }
            }
        }
        return null;
    }

    private void setInfoSystem(SmevLog log, String sender) {
        if (isEmpty(log.getInfoSystem()) && !isEmpty(sender)) {
            log.setInfoSystem(sender);
        }
    }

    private void deleteFile(File file) {
        if (!file.delete()) {
            logger.info("can't delete " + file);
        }
    }

    private String limitLength(String src, int maxLen) {
        if (src == null || src.length() <= maxLen) {
            return src;
        }
        return src.substring(0, maxLen);
    }

    private Date calcEdge() {
        int depth = -1;
        String depthConfig = AdminServiceProvider.get().getSystemProperty(API.LOG_DEPTH);
        if (depthConfig != null) {
            try {
                depth = Integer.parseInt(depthConfig);
            } catch (NumberFormatException e) {
            }
        }
        if (depth <= 0) {
            depth = API.DEFAULT_LOG_DEPTH;
        }
        Calendar cal = Calendar.getInstance(getTimeZone());
        cal.add(Calendar.DATE, -depth);
        return cal.getTime();
    }

    private TimeZone getTimeZone() {
        return TimeZone.getTimeZone("Europe/Moscow");
    }
    
    
    private String prepareZipFileName(Date edge, Long bid) {
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMdd-HHmmssSSS");

        String bidString;

        if (bid == null) {
            bidString = "NOBID";
        } else {
            bidString = Long.toString(bid);
        }

        return logStoragePath + "/" + dateTimeFormat.format(edge) + "_" + bidString + ".zip";
    }

    private File createZipFileName(Date edge, Long bid) {
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMdd-HHmmssSSS");
        SimpleDateFormat pathFormat = new SimpleDateFormat("yyyy/MM/dd");

        String filePath = logStoragePath + "/" + pathFormat.format(edge);
        String bidString;

        if (bid == null) {
            bidString = "NOBID";
        } else {
            bidString = Long.toString(bid);
        }

        String fileName = dateTimeFormat.format(edge) + "_" + bidString + ".zip";

        File zipFile = new File(filePath + "/" + fileName);

        writeDebugLog("[LOG CLEANER][DEBUGMODE] Creating filename " + filePath + "/" + fileName);

        if (!zipFile.exists()) {
            zipFile.getParentFile().mkdirs();
        }

        return zipFile;
    }

}
