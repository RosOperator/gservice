package ru.codeinside.adm.database;

import ru.codeinside.log.Logger;

import javax.persistence.*;

@Entity
@EntityListeners(Logger.class)
@Table(name = "anticipants")
public class Anticipant {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)//TODO NB
    private Long id;

    @Column(name = "consumer_name", nullable = false)
    private String consumerName;

    @Column(name = "bid_id", nullable = false)
    private Long bidId;

    @Column(name = "process_instance_id", nullable = false)
    private String processInstanceId;

    @Column(name = "message_id", nullable = false)
    private String messageId;

    public Anticipant() {}

    public Anticipant(String consumerName, Long bidId, String processInstanceId, String messageId) {
        this.consumerName = consumerName;
        this.bidId = bidId;
        this.processInstanceId = processInstanceId;
        this.messageId = messageId;
    }

    public Long getId() {
        return id;
    }

    public Long getBidId() {
        return bidId;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public String getConsumerName() {
        return consumerName;
    }

    public String getMessageId() {
        return messageId;
    }
}
