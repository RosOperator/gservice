package ru.codeinside.adm.database;

import org.activiti.engine.impl.ServiceImpl;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.variable.EntityManagerSession;

import javax.persistence.*;

@SuppressWarnings("PackageAccessibility")
@Entity
@Table(name = "file_content")
@SequenceGenerator(name = "file_content_id_seq", sequenceName = "file_content_id_seq", allocationSize = 1)
public class FileContent {
    @Id
    @GeneratedValue(generator = "file_content_id_seq")
    private Long id;

    @Column(nullable = false)
    private String path;

    /**
     * Внешний ключ на act_hi_attachment.id_
     */
    @Column(nullable = false, name = "attachment_id", length = 64)
    private String attachmentId;

    public FileContent() {
    }

    public FileContent(String path, String attachmentId) {
        this.path = path;
        this.attachmentId = attachmentId;
    }

    public static FileContent findByAttachmentId(final CommandContext context, final String attachmentId) {
        return new FindByAttachmentIdCmd(attachmentId).execute(context);
    }

    public static FileContent findByAttachmentId(final ServiceImpl service, final String attachmentId) {
        return service.getCommandExecutor().execute(new FindByAttachmentIdCmd(attachmentId));
    }

    //
    // Getters and setters
    //

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    //
    // Private classes
    //

    private static class FindByAttachmentIdCmd implements Command<FileContent> {

        private final String attId;

        private FindByAttachmentIdCmd(final String attId) {
            this.attId = attId;
        }

        @Override
        public FileContent execute(CommandContext commandContext) {
            EntityManager em = commandContext.getSession(EntityManagerSession.class).getEntityManager();
            return em
                    .createQuery("select f from FileContent f where f.attachmentId = :attachmentId", FileContent.class)
                    .setParameter("attachmentId", attId)
                    .getSingleResult();
        }
    }
}
