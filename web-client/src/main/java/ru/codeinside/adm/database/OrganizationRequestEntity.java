package ru.codeinside.adm.database;


import org.eclipse.persistence.annotations.CascadeOnDelete;
import org.w3c.dom.Element;
import ru.codeinside.gses.beans.Converter;
import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.gws.api.ExchangeContext;
import ru.codeinside.gws.api.Signature;
import ru.codeinside.log.Logger;
import ru.codeinside.smev.v3.crypto.api.XmlSignature;
import ru.codeinside.smev.v3.service.api.PersonalData;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequest;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequestData;

import javax.persistence.*;
import java.io.ByteArrayInputStream;
import java.util.*;

@SuppressWarnings("PackageAccessibility")
@Entity
@EntityListeners(Logger.class)
@Table(name = "org_request")
@SequenceGenerator(name = "org_request_seq", sequenceName = "org_request_id_seq", allocationSize = 1)
public class OrganizationRequestEntity {

    public OrganizationRequestEntity() {
        //для JPA
    }

    public OrganizationRequestEntity(OrganizationRequest request) {
        organizationSignData = request.signData;
        metadata = Converter.getByteArrayListFromElementList(request.data.metadata);

        if (request.signature != null) {
            organizationElementReference = request.signature.getElementReference();
            organizationSignature = request.signature.getSignature().sign;
            organizationDigest = request.signature.getSignature().getDigest();
            organizationCertificate = Converter.X509CertificateToByteArray(request.signature.getSignature().certificate);
        }

        messageId = request.data.messageID;
        ovMessageId = request.data.id;
        messageReferenceID = request.data.messageReferenceID;
        testMode = request.data.testMode;
        frguCode = request.data.frguCode;

        personalData = Converter.getByteArrayFromElement(request.data.personalData.data);
        personalSignData = request.data.personalData.signData;

        if (request.data.personalData.signature != null) {
            personalElementReference = request.data.personalData.signature.getElementReference();
            personalSignature = request.data.personalData.signature.getSignature().sign;
            personalDigest = request.data.personalData.signature.getSignature().getDigest();
            personalCertificate = Converter.X509CertificateToByteArray(request.data.personalData.signature.getSignature().certificate);
        }
    }

    @Id
    @GeneratedValue(generator = "org_request_seq")
    private Long id;

    public Long getId() {
        return id;
    }

    @Column(name = "org_sign_data")
    public byte[] organizationSignData;


    //OrganizationRequest XmlSignature
    @Column(name = "org_element_reference")
    public String organizationElementReference;

    @Column(name = "org_signature")
    public byte[] organizationSignature;

    @Column(name = "org_digest")
    public byte[] organizationDigest;

    @Column(name = "org_certificate")
    public byte[] organizationCertificate;

    //OrganizationRequestData
    @Column(name = "message_id")
    public String messageId;

    @OneToMany(mappedBy = "request", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<EnclosureEntity> enclosures;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "org_request_metadata", joinColumns = @JoinColumn(name = "org_request_id"))
    @CascadeOnDelete
    @Column(name = "org_metadata")
    public List<byte[]> metadata;

    @Column(name = "ov_message_id")
    public String ovMessageId;

    @Column(name = "message_reference_id")
    public String messageReferenceID;

    //PersonalData
    @Column(name = "personal_data")
    public byte[] personalData;

    @Column(name = "personal_sign_data")
    public byte[] personalSignData;

    //PersonalData XmlSignature
    @Column(name = "personal_element_reference")
    public String personalElementReference;

    @Column(name = "frgu_code")
    public String frguCode;

    @Column(name = "personal_signature")
    public byte[] personalSignature;

    @Column(name = "personal_digest")
    public byte[] personalDigest;

    @Column(name = "personal_certificate")
    public byte[] personalCertificate;

    @Column(name = "test_mode", nullable = false)
    public boolean testMode;

    public Set<EnclosureEntity> getEnclosures() {
        if (enclosures == null) {
            enclosures = new LinkedHashSet<EnclosureEntity>();
        }
        return enclosures;
    }

    public OrganizationRequest createOrganizationRequest(ExchangeContext context) {
        OrganizationRequest request = new OrganizationRequest();
        request.data = createOrganizationRequestData(context);
        request.signData = organizationSignData;

        if (organizationSignData != null) {
            Signature signature = new Signature(
                    Converter.byteArrayToX509Certificate(organizationCertificate),
                    new ByteArrayInputStream(organizationSignData),
                    organizationSignature,
                    organizationDigest,
                    true);

            XmlSignature organizationSignature = new XmlSignature(signature);
            organizationSignature.setElementReference(organizationElementReference);
            request.signature = organizationSignature;
        }
        return request;
    }

    private OrganizationRequestData createOrganizationRequestData(ExchangeContext context) {
        OrganizationRequestData organizationRequestData = new OrganizationRequestData();
        organizationRequestData.id = ovMessageId;
        organizationRequestData.messageID = messageId;
        organizationRequestData.messageReferenceID = messageReferenceID;
        organizationRequestData.testMode = testMode;

        organizationRequestData.personalData = createPersonalData();
        organizationRequestData.frguCode = frguCode;

        organizationRequestData.enclosures = extractEnclosures(context);
        organizationRequestData.metadata = createMetadata(metadata);

        return organizationRequestData;
    }

    private List<Enclosure> extractEnclosures(ExchangeContext context) {
        List<Enclosure> enclosures = new LinkedList<Enclosure>();
        for (String name : context.getVariableNames()) {
            if (context.isEnclosure(name)) {
                enclosures.add(context.getEnclosure(name));
            }
        }
        return enclosures;
    }

    private List<Element> createMetadata(List<byte[]> metadata) {
        List<Element> result = new ArrayList<Element>();
        for (byte[] data : metadata) {
            result.add(Converter.byteArrayToElement(data));
        }
        return result;
    }

    private PersonalData createPersonalData() {
        PersonalData personalData = new PersonalData();
        personalData.data = Converter.byteArrayToElement(this.personalData);
        personalData.signData = personalSignData;

        if (personalSignData != null) {
            Signature signature = new Signature(
                    Converter.byteArrayToX509Certificate(personalCertificate),
                    new ByteArrayInputStream(personalSignData),
                    personalSignature,
                    personalDigest,
                    true);

            XmlSignature personalSignature = new XmlSignature(signature);
            personalSignature.setElementReference(personalElementReference);
            personalData.signature = personalSignature;
        }
        return personalData;
    }
}
