/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2013, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.adm.database;

import org.eclipse.persistence.annotations.CascadeOnDelete;
import ru.codeinside.log.Logger;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "procedure_print_form")
@EntityListeners(Logger.class)
public class ProcedurePrintForm implements Serializable {

	private static final long serialVersionUID = 6583425270460724689L;

	private Double version;

	@Id
    @GeneratedValue(generator = "printformid_seq")
    @SequenceGenerator(name = "printformid_seq", sequenceName = "printformid_seq")
	private String printFormId;

	private String fileName;

	@JoinColumn(nullable = false)
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Procedure procedure;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCreated = new Date();

    @CascadeOnDelete
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "bytes")
    private BytesBuffer bytes;

	@ManyToOne(fetch = FetchType.LAZY)
	private Employee creator;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getPrintFormId() {
        return printFormId;
    }

    public void setPrintFormId(String printFormId) {
        this.printFormId = printFormId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Procedure getProcedure() {
        return procedure;
    }

    public void setProcedure(Procedure procedure) {
        this.procedure = procedure;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Employee getCreator() {
        return creator;
    }

    public void setCreator(Employee creator) {
        this.creator = creator;
    }

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }

    public BytesBuffer getBytes() {
        return bytes;
    }

    public void setBytes(BytesBuffer bytes) {
        this.bytes = bytes;
    }
}
