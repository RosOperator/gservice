package ru.codeinside.adm.database;

import org.eclipse.persistence.annotations.CascadeOnDelete;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.gses.beans.Converter;
import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.gws.api.Signature;
import ru.codeinside.log.Logger;
import ru.codeinside.smev.v3.crypto.api.XmlSignature;
import ru.codeinside.smev.v3.service.api.PersonalData;
import ru.codeinside.smev.v3.service.api.RejectionCode;
import ru.codeinside.smev.v3.service.api.RequestRejected;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponse;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseData;

import javax.persistence.*;
import java.io.ByteArrayInputStream;
import java.util.*;

@SuppressWarnings("PackageAccessibility")
@Entity
@EntityListeners(Logger.class)
@Table(name = "provider_response")
@SequenceGenerator(name = "provider_response_seq", sequenceName = "provider_response_id_seq", allocationSize = 1)
public class ProviderResponseEntity {

    public ProviderResponseEntity() {
        //для JPA
    }

    public ProviderResponseEntity(ProviderResponse response) {
        providerSignData = response.signData;

        if (response.signature != null) {
            providerElementReference = response.signature.getElementReference();
            providerSignature = response.signature.getSignature().sign;
            providerDigest = response.signature.getSignature().getDigest();
            providerCertificate = Converter.X509CertificateToByteArray(response.signature.getSignature().certificate);
        }

        messageId = response.data.messageID;
        to = response.data.to;
        requestId = response.data.id;

        requestRejected = parseRequestRejected(response.data.requestRejected);

        personalData = Converter.getByteArrayFromElement(response.data.personalData.data);
        personalSignData = response.data.personalData.signData;

        if (response.data.personalData.signature != null) {
            personalElementReference = response.data.personalData.signature.getElementReference();
            personalSignature = response.data.personalData.signature.getSignature().sign;
            personalDigest = response.data.personalData.signature.getSignature().getDigest();
            personalCertificate = Converter.X509CertificateToByteArray(response.data.personalData.signature.getSignature().certificate);
        }

        List<Enclosure> enclosures = response.data.enclosures;
        if (enclosures != null && !enclosures.isEmpty()) {
            for (Enclosure enclosure : enclosures) {
                EnclosureEntity enclosureEntity = new EnclosureEntity(this, enclosure.id, enclosure.code, enclosure.fileName,
                        enclosure.code, enclosure.number, enclosure.zipPath);
                getEnclosures().add(enclosureEntity);
            };
        }
    }

    @Id
    @GeneratedValue(generator = "org_request_seq")
    private Long id;

    public Long getId() {
        return id;
    }

    @Column(name = "provider_sign_data")
    public byte[] providerSignData;


    //ProviderResponse XmlSignature
    @Column(name = "provider_element_reference")
    public String providerElementReference;

    @Column(name = "provider_signature")
    public byte[] providerSignature;

    @Column(name = "provider_digest")
    public byte[] providerDigest;

    @Column(name = "provider_certificate")
    public byte[] providerCertificate;

    //ProviderResponseData
    @Column(name = "message_id")
    public String messageId;

    @Column(name = "reply_to")
    public String to;

    @OneToMany(mappedBy = "providerResponse", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<EnclosureEntity> enclosures;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "request_rejected", joinColumns = {
            @JoinColumn(name = "provider_response_id")
    })
    @MapKeyColumn(name = "rejection_reason_code")
    @Column(name = "rejection_reason")
    @CascadeOnDelete
    private Map<String, String> requestRejected;

    //PersonalData
    @Column(name = "personal_data")
    public byte[] personalData;

    @Column(name = "personal_sign_data")
    public byte[] personalSignData;

    @Column(name = "request_id")
    public String requestId;

    //PersonalData XmlSignature
    @Column(name = "personal_element_reference")
    public String personalElementReference;

    @Column(name = "personal_signature")
    public byte[] personalSignature;

    @Column(name = "personal_digest")
    public byte[] personalDigest;

    @Column(name = "personal_certificate")
    public byte[] personalCertificate;

    public Set<EnclosureEntity> getEnclosures() {
        if (enclosures == null) {
            enclosures = new LinkedHashSet<EnclosureEntity>();
        }
        return enclosures;
    }

    public ProviderResponse createProviderResponse(String processInstanceId) {
        ProviderResponse response = new ProviderResponse();
        response.data = createProviderResponseData(processInstanceId);
        response.signData = providerSignData;

        if (providerSignData != null) {
            Signature signature = new Signature(
                    Converter.byteArrayToX509Certificate(providerCertificate),
                    new ByteArrayInputStream(providerSignData),
                    providerSignature,
                    providerDigest,
                    true);

            XmlSignature organizationSignature = new XmlSignature(signature);
            organizationSignature.setElementReference(providerElementReference);
            response.signature = organizationSignature;
        }

        return response;
    }

    private ProviderResponseData createProviderResponseData(String processInstanceId) {
        ProviderResponseData providerResponseData = new ProviderResponseData();
        providerResponseData.messageID = messageId;
        providerResponseData.to = to;
        providerResponseData.id = requestId;

        providerResponseData.personalData = createPersonalData();
        providerResponseData.requestRejected = createRequestRejected();
        providerResponseData.enclosures = AdminServiceProvider.get().getEnclosures(enclosures, processInstanceId);

        return providerResponseData;
    }

    private PersonalData createPersonalData() {
        PersonalData personalData = new PersonalData();
        personalData.data = Converter.byteArrayToElement(this.personalData);
        personalData.signData = personalSignData;

        if (personalSignData != null) {
            Signature signature = new Signature(
                    Converter.byteArrayToX509Certificate(personalCertificate),
                    new ByteArrayInputStream(personalSignData),
                    personalSignature,
                    personalDigest,
                    true);
            XmlSignature personalSignature = new XmlSignature(signature);
            personalSignature.setElementReference(personalElementReference);
            personalData.signature = personalSignature;
        }

        return personalData;
    }

    private Map<String, String> parseRequestRejected(List<RequestRejected> requestRejected) {
        Map<String, String> result = new HashMap<String, String>();
        for (RequestRejected rejected : requestRejected) {
            result.put(rejected.rejectionReasonCode.value(), rejected.rejectionReasonDescription);
        }
        return result;
    }

    private List<RequestRejected> createRequestRejected() {
        List<RequestRejected> result = new ArrayList<RequestRejected>();
        for (Map.Entry<String, String> entry : requestRejected.entrySet()) {
            RequestRejected rejected = new RequestRejected();
            rejected.rejectionReasonCode = RejectionCode.valueOf(entry.getKey());
            rejected.rejectionReasonDescription = entry.getValue();
            result.add(rejected);
        }
        return result;
    }
}
