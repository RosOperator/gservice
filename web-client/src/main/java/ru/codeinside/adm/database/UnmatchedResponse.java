package ru.codeinside.adm.database;

import ru.codeinside.smev.v3.service.api.consumer.ResponseData;

import javax.persistence.*;
import java.util.Date;

@SuppressWarnings("PackageAccessibility")
@Entity
@Table(name = "unmatched_response")
public class UnmatchedResponse {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_date", nullable = false)
    private Date dateCreated;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_date", nullable = false)
    private Date dateUpdate;

    @Column(name = "count")
    private Long count;

    @Column(name = "message_id", nullable = false)
    private String messageId;

    @Column(name = "original_message_id", nullable = false)
    private String originalMessageId;

    @Column(name = "content_type_name")
    private String contentTypeName;

    public UnmatchedResponse() {
    }

    public UnmatchedResponse(ResponseData data) {
        this.messageId = data.providerResponse.data.messageID;
        this.originalMessageId = data.originalMessageId;
        this.contentTypeName = data.messageMetadata.detectedContentTypeName;
        this.dateCreated = new Date();
        this.dateUpdate = new Date();
        this.count = 1L;
    }


    public String getOriginalMessageId() {
        return originalMessageId;
    }

    public Long getCount() {
        return count;
    }

    public String getContentTypeName() {
        return contentTypeName;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public Long getId() {
        return id;
    }

    public void update(){
        this.count++;
        this.dateUpdate = new Date();
    }

}
