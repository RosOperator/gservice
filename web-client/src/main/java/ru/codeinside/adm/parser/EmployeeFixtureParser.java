/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2013, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.adm.parser;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.adm.database.Role;
import ru.codeinside.log.ImportLogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang.StringUtils.defaultIfEmpty;
import static org.apache.commons.lang.StringUtils.trimToNull;

/**
 * Разбор файла фикстур с данными о сотрудниках и организициях
 */
public class EmployeeFixtureParser {
  private final Splitter groupSplitter;
  private final LinkedList<Row> stack;
  private final Pattern snilsPattern = Pattern.compile("\\d{11}");
  private final Pattern mobilePhonePattern = Pattern.compile("\\d{10}");

  public EmployeeFixtureParser() {
    groupSplitter = Splitter.on(',').trimResults().omitEmptyStrings();
    stack = new LinkedList<Row>();
  }

  public void loadFixtures(InputStream is, EmployeeFixtureParser.PersistenceCallback callback) throws IOException {

    final Splitter propertySplitter = Splitter.on(';');
    final BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
    String line;
    int lineNumber = 0;
    ImportLogger importLogger = new ImportLogger();
    while ((line = reader.readLine()) != null) {
      lineNumber++;
      int level = startIndex(line);
      if (line.startsWith("#") || level < 0) {
        continue;
      }
      final ArrayList<String> props = Lists.newArrayList(propertySplitter.split(line.substring(level)));
      final String name = StringUtils.trimToNull(props.get(0));
      if (name == null) {
        if (props.size() == 1) {
          continue;
        }
          importLogger.log(false, "Пропущено имя (строка:" + lineNumber + ")", null, null);
      } else if (name.length() >= 255) {
          importLogger.log(false, "Длина имени не должна превышать 255 символов (строка:" + lineNumber + ")", null, null);
      }
      final boolean isOrg = props.size() < 11;
      Row parent = getParentRow(level);
      if (isOrg) {
          if(props.size() < 11) {
              String inn = StringUtils.trimToNull(props.get(1));
              String ogrn = StringUtils.trimToNull(props.get(2));
              String mnemonics = StringUtils.trimToNull(props.get(3));
              String informSystemName = StringUtils.trimToNull(props.get(4));
              String phone = StringUtils.trimToNull(props.get(5));
              String email = StringUtils.trimToNull(props.get(6));
              String address = StringUtils.trimToNull(props.get(7));
              String fioHead = StringUtils.trimToNull(props.get(8));
              long orgId = callback.onOrganizationComplete(name, parent != null ? parent.id : null, inn, ogrn,
                      mnemonics, "", informSystemName, phone, email, address, fioHead);
              stack.addLast(new Row(level, orgId));
          } else {
              importLogger.log(false, "Недостаточно данных для создания организации (строка:" + lineNumber + ")", null, null);
          }
      }
      if (!isOrg && parent != null) {
          if(props.size() >= 11) {
              String username = StringUtils.trimToNull(props.get(1));
              if (username == null) {
                  importLogger.log(false, "Пропущен логин (строка:" + lineNumber + ")", null, null);
              }
              String pwd = defaultIfEmpty(trimToNull(props.get(2)), null);
              String snils = "";
              String snilsValue = defaultIfEmpty(trimToNull(props.get(3)), null);
              if(snilsValue != null) {
                  Matcher snilsMatcher = snilsPattern.matcher(snilsValue);
                  if (snilsMatcher.matches() && AdminServiceProvider.get().isUniqueSnils(name, snilsValue)) {
                      snils = snilsValue.replaceAll("\\D+", "");
                  }
              }
              String email = StringUtils.trimToNull(props.get(4));
              String workPhone = StringUtils.trimToNull(props.get(5));
              String mobilePhone = "";
              String mobilePhoneValue = defaultIfEmpty(trimToNull(props.get(6)), null);
              if(mobilePhoneValue != null) {
                  Matcher mobilePhoneMatcher = mobilePhonePattern.matcher(mobilePhoneValue);
                  if (mobilePhoneMatcher.matches()) {
                      mobilePhone = mobilePhoneValue.replaceAll("\\D+", "");
                  }
              }
              String position = StringUtils.trimToNull(props.get(7));
              Boolean smsInforming = Boolean.valueOf(props.get(8));
              Boolean emailInforming = Boolean.valueOf(props.get(9));
              Set<Role> roles = parseRoles(groupSplitter, lineNumber, props.get(10));
              Set<String> groups = parseGroups(props, 11);
              callback.onUserComplete(username, pwd, name, snils, email, workPhone, mobilePhone, position, smsInforming,
                      emailInforming, parent.id, roles, groups);
          } else {
              importLogger.log(false, "Недостаточно данных для создания пользователя (строка:" + lineNumber + ")", null, null);
          }
      }
      if (!isOrg && parent == null) {
          importLogger.log(false, "Пользователь без организации (строка:" + lineNumber + ")", null, null);
      }
    }
  }

  private Row getParentRow(int level) {
    Row parent = stack.isEmpty() ? null : stack.getLast();
    while (!stack.isEmpty()) {
      if (parent != null && (parent.level < level)) {
        break;
      }
      stack.removeLast();
      parent = stack.isEmpty() ? null : stack.getLast();
    }
    return parent;
  }

  private Set<String> parseGroups(ArrayList<String> props, int propertyGroupIndex) {
    final Set<String> groups;
    if (props.size() > propertyGroupIndex) {
      groups = Sets.newTreeSet(groupSplitter.split(props.get(propertyGroupIndex)));
    } else {
      groups = Collections.emptySet();
    }
    return groups;
  }

  private Set<Role> parseRoles(Splitter groupSplitter, int lineNumber, String rolesString) {
    Set<String> roleNames = Sets.newTreeSet(groupSplitter.split(rolesString));
    Set<Role> roles = EnumSet.noneOf(Role.class);
    for (String roleName : roleNames) {
      try {
        roles.add(Role.valueOf(roleName));
      } catch (Exception e) {
          ImportLogger importLogger = new ImportLogger();
          importLogger.log(false, "\"Неизвестная роль (строка:" + lineNumber + "): " + roleName, null, null);
      }
    }

    return roles;
  }

  private int startIndex(String line) {
    int N = line.length();
    for (int i = 0; i < N; i++) {
      char c = line.charAt(i);
      if (c != ' ') {
        return i;
      }
    }
    return -1;
  }

  public interface PersistenceCallback {
    Long onOrganizationComplete(String orgName, Long ownerId, String inn, String ogrn, String mnemonics, String frguCode,
                                String informSystemName, String phone, String email, String address, String fioHeade);

    void onUserComplete(String login, String pwd, String name, String snils, String email, String workPhone,
                        String mobilePhone, String position, Boolean smsInforming, Boolean emailInforming, long orgId,
                        Set<Role> roles, Set<String> groups);
  }

  final static class Row {
    final int level;
    final long id;

    Row(int level, long id) {
      this.level = level;
      this.id = id;
    }
  }


}

