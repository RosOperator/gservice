/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2013, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.adm.ui;

import com.vaadin.data.validator.EmailValidator;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.adm.database.Organization;

import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.activiti.engine.impl.identity.Authentication;
import ru.codeinside.gses.vaadin.NumericField;

public class ButtonCreateOrganization extends VerticalLayout {

    final Logger logger = Logger.getLogger(getClass().getName());

	private static final long serialVersionUID = 1L;
	private static final String NAME = "Наименование организации";
    private static final String INN = "ИНН";
    private static final String OGRN = "ОГРН";
    private static final String MNEMONICS = "Мнемоника";
    private static final String FRGU_CODE = "Код ФРГУ";
    private static final String INFORM_SYSTEM_NAME = "Наименование ведомственной информационной системы";
    private static final String PHONE = "Телефон";
    private static final String EMAIL = "E-mail";
    private static final String ADDRESS = "Адрес";
    private static final String FIO_HEAD = "ФИО руководителя";
    private static final String DATE_REGISTRATION = "Дата регистрации";
	private TreeTable treetable;

	public ButtonCreateOrganization(final TreeTable treetable) {
		this.treetable = treetable;
		setSpacing(true);
		setMargin(false, true, true, false);
		showButtonCreateOrganization();
	}

	private void showButtonCreateOrganization() {
		removeAllComponents();

		final Button createOrg = new Button("Создание организации", new Button.ClickListener() {

			private static final long serialVersionUID = 1L;

			public void buttonClick(ClickEvent event) {
				removeAllComponents();
				final Form form = new Form();
				form.addField(NAME, new TextField(NAME));
				form.getField(NAME).setRequired(true);
				form.getField(NAME).setRequiredError("Введите название организации");
				form.getField(NAME).addValidator(
						new StringLengthValidator("Название организации должно быть не более 255 символов", 1, 255,
								false));
				form.getField(NAME).setWidth("500px");

                form.addField(INN, new NumericField(INN));
                form.getField(INN).setRequired(true);
                form.getField(INN).setRequiredError("Введите ИНН организации");
                form.getField(INN).addValidator(
                        new StringLengthValidator("ИНН организации должно быть не более 10 символов", 1, 10, false));
                form.getField(INN).setWidth("500px");

                form.addField(OGRN, new NumericField(OGRN));
                form.getField(OGRN).setRequired(true);
                form.getField(OGRN).setRequiredError("Введите ОГРН организации");
                form.getField(OGRN).addValidator(
                        new StringLengthValidator("ОГРН организации должно быть не более 15 символов", 1, 15, false));
                form.getField(OGRN).setWidth("500px");

                form.addField(MNEMONICS, new TextField(MNEMONICS));
                form.getField(MNEMONICS).setRequired(true);
                form.getField(MNEMONICS).setRequiredError("Введите мнемонику организации");
                form.getField(MNEMONICS).addValidator(
                        new StringLengthValidator("Мнемоника организации должна быть не более 9 символов", 1, 9,
                                false));
                form.getField(MNEMONICS).setWidth("500px");

                form.addField(FRGU_CODE, new TextField(FRGU_CODE));
                form.getField(FRGU_CODE).setRequired(true);
                form.getField(FRGU_CODE).setRequiredError("Введите код ФРГУ организации");
                form.getField(FRGU_CODE).addValidator(
                        new StringLengthValidator("Код ФРГУ организации должен быть не более 20 символов", 1, 20,
                                false));
                form.getField(FRGU_CODE).setWidth("500px");

                form.addField(INFORM_SYSTEM_NAME, new TextField(INFORM_SYSTEM_NAME));
                form.getField(INFORM_SYSTEM_NAME).setRequired(true);
                form.getField(INFORM_SYSTEM_NAME).setRequiredError("Введите наименование ведомственной информационной системы");
                form.getField(INFORM_SYSTEM_NAME).addValidator(
                        new StringLengthValidator("Наименование ведомственной информационной системы должно быть не более 255 символов",
                                1, 255, false));
                form.getField(INFORM_SYSTEM_NAME).setWidth("500px");

                form.addField(PHONE, new NumericField(PHONE));
                form.getField(PHONE).setRequired(false);
                form.getField(PHONE).addValidator(
                        new StringLengthValidator("Телефон организации должен быть не более 64 символов", 1, 64, false));
                form.getField(PHONE).setWidth("500px");

                form.addField(EMAIL, new TextField(EMAIL));
                form.getField(EMAIL).setRequired(false);
                form.getField(EMAIL).addValidator( new EmailValidator("Введите правильный e-mail организации"));
                form.getField(EMAIL).addValidator(
                        new StringLengthValidator("E-mail организации должен быть не более 255 символов", 1, 255,
                                false));
                form.getField(EMAIL).setWidth("500px");

                form.addField(ADDRESS, new TextField(ADDRESS));
                form.getField(ADDRESS).setRequired(false);
                form.getField(ADDRESS).addValidator(
                        new StringLengthValidator("Адрес организации должна быть не более 255 символов", 1, 255,
                                false));
                form.getField(ADDRESS).setWidth("500px");

                form.addField(FIO_HEAD, new TextField(FIO_HEAD));
                form.getField(FIO_HEAD).setRequired(false);
                form.getField(FIO_HEAD).setRequiredError("Введите ФИО руководителя организации");
                form.getField(FIO_HEAD).addValidator(
                        new StringLengthValidator("ФИО руководителя организации должна быть не более 255 символов", 1,
                                255, false));
                form.getField(FIO_HEAD).setWidth("500px");

                DateField registrationDate = new DateField(DATE_REGISTRATION);
                registrationDate.setResolution(DateField.RESOLUTION_DAY);
                form.addField(DATE_REGISTRATION, registrationDate);
                form.getField(DATE_REGISTRATION).setRequired(false);

				addComponent(form);
				Button create = new Button("Создать", new Button.ClickListener() {
					private static final long serialVersionUID = 1L;

					public void buttonClick(ClickEvent event) {
						try {
							form.commit();
							String name = form.getField(NAME).getValue().toString();
                            String inn = form.getField(INN).getValue().toString();
                            String ogrn = form.getField(OGRN).getValue().toString();
                            String mnemonics = form.getField(MNEMONICS).getValue().toString();
                            String frguCode = form.getField(FRGU_CODE).getValue().toString();
                            String informSystemName = form.getField(INFORM_SYSTEM_NAME).getValue().toString();
                            String phone = form.getField(PHONE).getValue().toString();
                            String email = form.getField(EMAIL).getValue().toString();
                            String address = form.getField(ADDRESS).getValue().toString();
                            String fioHead = form.getField(FIO_HEAD).getValue().toString();
                            Date dateRegistration = (Date) form.getField(DATE_REGISTRATION).getValue();
							Organization org = AdminServiceProvider.get().createOrganization(name,
									getApplication().getUser().toString(), null, inn, ogrn, mnemonics, frguCode,
                                    informSystemName, phone, email, address,fioHead,dateRegistration);
							treetable.addItem(new Object[]{org.getName()}, org.getId());
							treetable.setChildrenAllowed(org.getId(), false);
                            treetable.setValue(org.getId());
							treetable.requestRepaint();
							showButtonCreateOrganization();
                            String login = Authentication.getAuthenticatedUserId();
                            logger.log(Level.INFO, "Пользователь " + login + " создал организацию " + name + " ИНН " + inn);
							getWindow().showNotification("Организация " + name + " создана");
						} catch (Exception e) {

						}
					}
				});
				final Button cancel = new Button("Отменить", new Button.ClickListener() {

					private static final long serialVersionUID = 1L;

					public void buttonClick(ClickEvent event) {
						showButtonCreateOrganization();
					}
				});
				HorizontalLayout buttons = new HorizontalLayout();
				buttons.setSpacing(true);
				buttons.addComponent(create);
				buttons.addComponent(cancel);
				form.getFooter().addComponent(buttons);
			}
		});
		addComponent(createOrg);
	}
}
