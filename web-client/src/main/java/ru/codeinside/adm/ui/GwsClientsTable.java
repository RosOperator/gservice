/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2014, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.adm.ui;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.IndexedContainer;
import org.tepi.filtertable.FilterTable;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.adm.database.InfoSystemService;
import ru.codeinside.gses.webui.gws.TRefProvider;
import ru.codeinside.gws.api.Revision;

import java.util.Arrays;
import java.util.List;

final class GwsClientsTable extends FilterTable {


  GwsClientSink sink;

  final IndexedContainer container;

  boolean selectionMode;

  GwsClientsTable() {
    super("Зарегистрированные модули");
    container = new IndexedContainer();
    addContainerProperty();
    List<InfoSystemService> serviceList = AdminServiceProvider.get().getAllInfoSystemService();
    for (InfoSystemService service : serviceList){
      addServiceToContainer(service);
    }
    setFilterBarVisible(true);
    setContainerDataSource(container);
    setVisibleColumns(new Object[]{"id", "sname", "sversion", "infoSystem.code", "source.code", "address", "revision", "name", "available", "logEnabled"});
    setFilterDecorator(new FilterDecorator_());
    setFilterGenerator(new FilterGenerator_(Arrays.asList("id"), Arrays.asList("available", "logEnabled")));
    setImmediate(true);
    setSizeFull();
    setPageLength(0);
    setColumnHeaders(new String[]{
      "Id", "Имя", "Вер.", "Код системы", "Источник", "Адрес", "Рев.", "Описание", "Доступен", "Журнал"
    });
    setSelectable(true);
    setColumnExpandRatio("id", 0.01f);
    setColumnExpandRatio("infoSystem.code", 0.1f);
    setColumnExpandRatio("address", 0.1f);
    setColumnExpandRatio("available", 0.05f);

    addGeneratedColumn("available", new YesColumnGenerator());
    addGeneratedColumn("logEnabled", new YesColumnGenerator());


    addListener(new ValueChangeListener() {
      @Override
      public void valueChange(Property.ValueChangeEvent event) {
        if (selectionMode) {
          return;
        }
        Object itemId = event.getProperty().getValue();
        Item item = itemId == null ? null : getItem(itemId);
        if (item != null) {
          String name = (String) item.getItemProperty("sname").getValue();
          String version = (String) item.getItemProperty("sversion").getValue();
          if (sink != null) {
            Long id = (Long) item.getItemProperty("id").getValue();
            String infoSys = (String) item.getItemProperty("infoSystem.code").getValue();
            String source = (String) item.getItemProperty("source.code").getValue();
            String url = (String) item.getItemProperty("address").getValue();
            Revision revision = Revision.valueOf((String) item.getItemProperty("revision").getValue());
            String description = (String) item.getItemProperty("name").getValue();
            Boolean available = item.getItemProperty("available") == null ? null : (Boolean) item.getItemProperty("available").getValue();
            Boolean logEnabled = item.getItemProperty("logEnabled") == null ? null : (Boolean) item.getItemProperty("logEnabled").getValue();

            Integer smevVersion = 0;
            if (AdminServiceProvider.get().getClientRefByNameAndVersion(name, version) != null) {
              smevVersion = 2;
            }
            if (TRefProvider.getConsumerRefRegistry().getConsumerByName(name) != null) {
              smevVersion = 3;
            }
            sink.selectClient(id, revision, url, name, version, infoSys, source, description, available, logEnabled, smevVersion);
          }
        }
      }
    });
  }

  void setSink(GwsClientSink sink) {
    this.sink = sink;
  }

  boolean setCurrent(String name, String version, boolean mode) {
    selectionMode = mode;
    try {
      if (name != null && version != null) {
        Container container = getContainerDataSource();
        for (Object itemId : container.getItemIds()) {
          Object sname = container.getContainerProperty(itemId, "sname").getValue();
          Object sversion = container.getContainerProperty(itemId, "sversion").getValue();
          if (name.equals(sname) && version.equals(sversion)) {
            setValue(itemId);
            return true;
          }
        }
      }
      setValue(null);
    } finally {
      selectionMode = false;
    }
    return false;
  }

  private void addContainerProperty() {
    container.addContainerProperty("id", Long.class, 0);
    container.addContainerProperty("sname", String.class, "");
    container.addContainerProperty("sversion", String.class, "");
    container.addContainerProperty("infoSystem.code", String.class, "");
    container.addContainerProperty("source.code", String.class, "");
    container.addContainerProperty("address", String.class, "");
    container.addContainerProperty("revision", String.class, "");
    container.addContainerProperty("name", String.class, "");
    container.addContainerProperty("available", Boolean.class, null);
    container.addContainerProperty("logEnabled", Boolean.class, null);
  }

  private void addServiceToContainer(InfoSystemService service){
    Object id = container.addItem();
    container.getContainerProperty(id,"id").setValue(service.getId());
    container.getContainerProperty(id,"sname").setValue(service.getSname());
    container.getContainerProperty(id,"sversion").setValue(service.getSversion());
    container.getContainerProperty(id,"infoSystem.code").setValue(service.getInfoSystem().getCode());
    container.getContainerProperty(id,"source.code").setValue(service.getSource() == null ? "" : service.getSource().getCode());
    container.getContainerProperty(id,"address").setValue(service.getAddress());
    container.getContainerProperty(id,"revision").setValue(service.getRevision());
    container.getContainerProperty(id,"name").setValue(service.getName());
    container.getContainerProperty(id,"available").setValue(service.isAvailable());
    container.getContainerProperty(id,"logEnabled").setValue(service.isLogEnabled());
  }

  public void refreshItems(){
    container.removeAllItems();
    List<InfoSystemService> serviceList = AdminServiceProvider.get().getAllInfoSystemService();
    for (InfoSystemService service : serviceList){
      addServiceToContainer(service);
    }
    container.sort(new String[]{"id"}, new boolean[]{true});
  }
}
