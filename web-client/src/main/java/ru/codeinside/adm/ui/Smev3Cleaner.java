package ru.codeinside.adm.ui;

import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.MouseEvents;
import com.vaadin.ui.*;
import org.osgi.framework.ServiceReference;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.adm.database.Anticipant;
import ru.codeinside.adm.database.UnmatchedResponse;
import ru.codeinside.gses.beans.Smev3;
import ru.codeinside.gses.webui.osgi.Activator;
import ru.codeinside.smev.v3.service.api.AcknowledgeRequest;
import ru.codeinside.smev.v3.service.api.Smev;
import ru.codeinside.smev.v3.transport.api.SmevTransport;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Smev3Cleaner extends Panel {
    private IndexedContainer responseContainer;
    private IndexedContainer requestContainer;
    final private Window popupWindow = new Window();

    public Smev3Cleaner() {
        init();
    }

    private void init() {
        setCaption("Чистка очереди СМЭВ3");
        popupWindow.setWidth(200, UNITS_PIXELS);
        popupWindow.setModal(true);
        popupWindow.setClosable(false);
        popupWindow.setDraggable(false);
        popupWindow.setResizable(false);

        Table responseTable = new Table("Не принятые ответы");
        Table requestTable = new Table("Запросы ожидающие ответа");
        Button refreshButton = new Button("Обновить");
        refreshButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                refreshContainers();
            }
        });

        responseContainer = new IndexedContainer();
        requestContainer = new IndexedContainer();
        initRequestContainer();
        initResponseContainer();
        requestTable.setContainerDataSource(requestContainer);
        responseTable.setContainerDataSource(responseContainer);

        requestTable.setColumnHeaders(new String[]{"id", "original Message Id", "Система потребитель", "Bid Id", "process Instance Id", "Удалить"});
        responseTable.setColumnHeaders(new String[]{"id", "Дата создания", "Дата обновления", "Количество", "Виды сведений", "Origin Message Id","Message Id", "Подтвердить", "Удалить"});
        responseTable.setVisibleColumns(new Object[]{"id", "dateCreate", "dateUpdate", "count", "contentTypeName", "originalMessageId", "accept", "cancel"});
        requestTable.setSizeFull();
        responseTable.setSizeFull();

        HorizontalLayout tables = new HorizontalLayout();
        tables.addComponent(responseTable);
        tables.addComponent(requestTable);
        tables.setExpandRatio(responseTable, 0.6f);
        tables.setExpandRatio(requestTable, 0.4f);

        tables.setHeight("100%");
        tables.setWidth("100%");

        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.addComponent(refreshButton);
        mainLayout.addComponent(tables);
        mainLayout.setSizeFull();
        mainLayout.setExpandRatio(refreshButton, 0.01f);
        mainLayout.setExpandRatio(tables, 0.99f);

        this.getContent().setSizeFull();
        this.addComponent(mainLayout);
        this.setSizeFull();

        this.addListener(new MouseEvents.ClickListener() {
            @Override
            public void click(MouseEvents.ClickEvent event) {
                refreshContainers();
            }
        });
    }

    private void ackResponse(Object itemId) {
        String messageId = (String) responseContainer.getContainerProperty(itemId, "messageId").getValue();
        boolean isOk = true;
        if (messageId != null && !messageId.isEmpty()) {
            isOk = sendAck(messageId);
            deleteUnmatchedFromDb(messageId);
        }

        if (isOk) {
            this.getWindow().showNotification("Все хорошо.", Window.Notification.TYPE_HUMANIZED_MESSAGE);
        }
    }

    private void ackRequest(Object itemId) {
        String originalMessageId = (String) requestContainer.getContainerProperty(itemId, "originMessageId").getValue();
        if (originalMessageId != null && !originalMessageId.isEmpty()) {
            deleteAnticipantFromDb(originalMessageId);
        }
    }

    private void deleteAnticipantFromDb(String originalMessageId) {
        Anticipant anticipant = AdminServiceProvider.get().findAnticipantByMessageId(originalMessageId);
        if (anticipant != null) {
            AdminServiceProvider.get().eliminateConsumerFromAnticipants(anticipant.getId());
        }
    }

    private void deleteUnmatchedFromDb(String messageId) {
        UnmatchedResponse response = AdminServiceProvider.get().findUnmatchedByMessageId(messageId);
        if (response != null) {
            AdminServiceProvider.get().eliminateResponseFromUnmatched(response.getId());
        }
    }

    private void deleteUnmatchedFromDb(Object itemId) {
        String messageId = (String) responseContainer.getItem(itemId).getItemProperty("messageId").getValue();
        deleteUnmatchedFromDb(messageId);
    }

    private boolean sendAck(String messageId) {
        AcknowledgeRequest acknowledgeRequest = getAck(messageId);
        if (acknowledgeRequest == null) {
            return false;
        }

        SmevTransport transport = getTransport();
        if (transport == null) {
            return false;
        }

        try {
            transport.ack(acknowledgeRequest);
        } catch (Exception e) {
            this.getWindow().showNotification("Ошибка", e.getMessage(), Window.Notification.TYPE_ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    private SmevTransport getTransport() {
        String filter = "(component.name=smev-transport-" + Smev3.SMEV3_SERVICE_VERSION + ")";

        SmevTransport transport = null;
        ServiceReference[] references = null;
        try {
            references = Activator.getContext().getServiceReferences(SmevTransport.class.getName(), filter);
            if (references == null || references.length == 0) {
                this.getWindow().showNotification("Ошибка", "Отсутствует сервис SmevTransport", Window.Notification.TYPE_ERROR_MESSAGE);
                return null;
            }
            transport = (SmevTransport) Activator.getContext().getService(references[0]);
        } catch (Exception e) {
            this.getWindow().showNotification("Ошибка", e.getMessage(), Window.Notification.TYPE_ERROR_MESSAGE);
            return null;
        } finally {
            if (references != null) {
                Activator.getContext().ungetService(references[0]);
            }
        }

        return transport;
    }

    private AcknowledgeRequest getAck(String messageId) {
        ServiceReference smevReference = null;
        try {
            smevReference = Activator.getContext().getServiceReference(Smev.class.getName());
            if (smevReference == null) {
                this.getWindow().showNotification("Ошибка", "Отсутствует сервис Smev", Window.Notification.TYPE_ERROR_MESSAGE);
                return null;
            }

            Smev smev = (Smev) Activator.getContext().getService(smevReference);
            if (smev == null) {
                this.getWindow().showNotification("Ошибка", "Отсутствует инстанс сервиса Smev", Window.Notification.TYPE_ERROR_MESSAGE);
                return null;
            }
            return smev.ackRequest(messageId, true);
        } catch (Exception e) {
            this.getWindow().showNotification("Ошибка", e.getMessage(), Window.Notification.TYPE_ERROR_MESSAGE);
            return null;
        } finally {
            if (smevReference != null) {
                Activator.getContext().ungetService(smevReference);
            }
        }
    }

    private void refreshContainers() {
        requestContainer.removeAllItems();
        fillRequestContainer();
        responseContainer.removeAllItems();
        fillResponseContainer();
    }

    private void initResponseContainer() {
        responseContainer.addContainerProperty("id", Long.class, 0);
        responseContainer.addContainerProperty("dateCreate", String.class, "");
        responseContainer.addContainerProperty("dateUpdate", String.class, "");
        responseContainer.addContainerProperty("count", Long.class, 0);
        responseContainer.addContainerProperty("contentTypeName", String.class, "");
        responseContainer.addContainerProperty("originalMessageId", String.class, "");
        responseContainer.addContainerProperty("messageId", String.class, "");
        responseContainer.addContainerProperty("accept", Button.class, "");
        responseContainer.addContainerProperty("cancel", Button.class, "");
        fillResponseContainer();
    }

    private void initRequestContainer() {
        requestContainer.addContainerProperty("id", Long.class, 0);
        requestContainer.addContainerProperty("originMessageId", String.class, "");
        requestContainer.addContainerProperty("consumerName", String.class, "");
        requestContainer.addContainerProperty("bidId", String.class, "");
        requestContainer.addContainerProperty("processInstanceId", String.class, "");
        requestContainer.addContainerProperty("cancel", Button.class, "");
        fillRequestContainer();
    }

    private void fillResponseContainer() {
        Date date = new Date();
        date.setMinutes(date.getMinutes() - 14);
        List<UnmatchedResponse> responses = AdminServiceProvider.get().getAllUnmatchedResponse();
        for (UnmatchedResponse response : responses) {
            final Object id = responseContainer.addItem();
            responseContainer.getContainerProperty(id, "id").setValue(response.getId());
            responseContainer.getContainerProperty(id, "dateCreate").setValue(formatDate(response.getDateCreated()));
            responseContainer.getContainerProperty(id, "dateUpdate").setValue(formatDate(response.getDateUpdate()));
            responseContainer.getContainerProperty(id, "count").setValue(response.getCount());
            responseContainer.getContainerProperty(id, "contentTypeName").setValue(response.getContentTypeName());
            responseContainer.getContainerProperty(id, "originalMessageId").setValue(response.getOriginalMessageId());
            responseContainer.getContainerProperty(id, "messageId").setValue(response.getMessageId());

            Button accept = new Button("Подтвердить");
            accept.addListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    openPopupWindow("Подтвердить получение ответа?", new Button.ClickListener() {
                        @Override
                        public void buttonClick(Button.ClickEvent event) {
                            ackResponse(id);
                            closePopupWindow();
                        }
                    });
                }
            });
            if (response.getDateUpdate().after(date)) {
                accept.setEnabled(true);
            } else {
                accept.setEnabled(false);
            }
            responseContainer.getContainerProperty(id, "accept").setValue(accept);

            Button cancel = new Button("Удалить");
            cancel.addListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    openPopupWindow("Удалить запись из базы данныйх?", new Button.ClickListener() {
                        @Override
                        public void buttonClick(Button.ClickEvent event) {
                            deleteUnmatchedFromDb(id);
                            closePopupWindow();
                        }
                    });
                }
            });
            responseContainer.getContainerProperty(id, "cancel").setValue(cancel);
        }
        responseContainer.sort(new String[]{"dateUpdate"}, new boolean[]{false});
    }

    private void fillRequestContainer() {
        List<Anticipant> anticipants = AdminServiceProvider.get().getAllAnticipant();
        for (Anticipant anticipant : anticipants) {
            final Object id = requestContainer.addItem();
            requestContainer.getContainerProperty(id, "id").setValue(anticipant.getId());
            requestContainer.getContainerProperty(id, "processInstanceId").setValue(anticipant.getProcessInstanceId());
            requestContainer.getContainerProperty(id, "originMessageId").setValue(anticipant.getMessageId());
            requestContainer.getContainerProperty(id, "consumerName").setValue(anticipant.getConsumerName());
            requestContainer.getContainerProperty(id, "bidId").setValue(anticipant.getBidId());
            Button cancel = new Button("Удалить");
            cancel.addListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    openPopupWindow("Удалить запись из базы данных?",
                            new Button.ClickListener() {
                                @Override
                                public void buttonClick(Button.ClickEvent event) {
                                    ackRequest(id);
                                    closePopupWindow();
                                }
                            });
                }
            });
            requestContainer.getContainerProperty(id, "cancel").setValue(cancel);
        }
    }

    private void openPopupWindow(String message, Button.ClickListener clickListener) {
        Button accept = new Button("Да");
        Button cancel = new Button("Нет");
        accept.setSizeFull();
        cancel.setSizeFull();

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(accept);
        horizontalLayout.addComponent(cancel);
        horizontalLayout.setSizeFull();


        popupWindow.addComponent(new Label(message));
        popupWindow.addComponent(horizontalLayout);

        getApplication().getMainWindow().addWindow(popupWindow);
        accept.addListener(clickListener);

        cancel.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                closePopupWindow();
            }
        });

    }

    private void closePopupWindow() {
        popupWindow.removeAllComponents();
        getApplication().getMainWindow().removeWindow(popupWindow);
        refreshContainers();
    }

    private String formatDate(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }
}
