package ru.codeinside.auth;

import com.google.common.collect.Sets;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.adm.database.Employee;
import ru.codeinside.adm.database.Role;
import ru.codeinside.filter.AuthorizationFilter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import java.util.Set;
import java.util.logging.Logger;

@WebServlet(urlPatterns = {"/authServlet"})
public class AuthServlet extends HttpServlet {
  private Logger log = Logger.getLogger(AuthServlet.class.getName());

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      standardAuthorization(req, resp);
  }

  private void standardAuthorization(HttpServletRequest req, HttpServletResponse resp) {
    String login = req.getParameter("username");
    String password = req.getParameter("password");
    Employee employee = AdminServiceProvider.get().findEmployeeByLogin(login);
    if (employee != null && employee.checkPassword(password) && !employee.isLocked()) {
      setPrincipal(req, employee);
      sendRedirect(resp, "/web-client/");
    } else {
      sendForward(req, resp, "/loginError.jsp", "loginError");
    }
  }

  protected void sendRedirect(HttpServletResponse resp, String url) {
    try {
      resp.sendRedirect(url);
    } catch (IOException e) {
      log.severe("Не удалось сделать редирект на " + url + ": " + e.getMessage());
    }
  }

  protected void sendForward(HttpServletRequest req, HttpServletResponse resp, String url, String errorMessage) {
    req.setAttribute("error", errorMessage);
    try {
      req.getRequestDispatcher(url).forward(req, resp);
    } catch (ServletException e) {
      log.severe("Не удалось перейти на на " + url + ": " + e.getMessage());
      e.printStackTrace();
    } catch (IOException e) {
      log.severe("Не удалось перейти на на " + url + ": " + e.getMessage());
      e.printStackTrace();
    }
  }

  protected void setPrincipal(HttpServletRequest req, Employee employee) {
    String login = employee.getLogin();
    Set<String> roles = Sets.newHashSet();
    for (Role role : employee.getRoles()) {
      roles.add(role.name());
    }
    HasRolePrincipal principal = new UserPrincipal(login, roles);
    req.getSession().setAttribute(AuthorizationFilter.SESSION_ATTR_USER_PRINCIPAL, principal);
  }
}
