package ru.codeinside.auth;

import org.osgi.framework.ServiceReference;
import ru.codeinside.gses.webui.osgi.Activator;
import ru.codeinside.gws.api.EsiaManager;

import java.util.logging.Logger;

public class EsiaManagerImpl {

    private static Logger log = Logger.getLogger(EsiaManagerImpl.class.getName());
    private volatile static EsiaManager instance;

    private EsiaManagerImpl() {}

    public static  EsiaManager getInstance() {
        if (instance==null) {
            synchronized (EsiaManagerImpl.class) {
                if (instance == null) {
                    try{
                        ServiceReference esiaReference = Activator.getContext().getServiceReference(EsiaManager.class.getName());
                        instance = esiaReference == null ? null : (EsiaManager) Activator.getContext().getService(esiaReference);
                    } catch (Exception e) {
                        log.severe(e.getMessage());
                    }

                }
            }
        }
        return instance;
    }
}
