package ru.codeinside.auth;

import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.adm.database.Employee;

import ru.codeinside.gses.API;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceException;
import java.io.IOException;
import java.util.logging.Logger;

@WebServlet(urlPatterns = {"/esiaServlet"})
public class EsiaServlet extends AuthServlet {

    private Logger log = Logger.getLogger(EsiaServlet.class.getName());

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (EsiaManagerImpl.getInstance() == null) {
            sendForward(request, response, "/loginError.jsp", "esiaError");
        } else {
            if (EsiaManagerImpl.getInstance().isValidEsiaSession(request.getSession().getId())) {
                String snils =  EsiaManagerImpl.getInstance().getSnilsAttribute(request.getSession().getId());
                if (snils != null && !snils.isEmpty()) {
                    authorizeBySnils(request, response, snils);
                } else {
                    sendForward(request, response, "/loginError.jsp", "esiaError");
                }
            } else {
                Cookie ssocookie = new Cookie("SSOSESSIONID", request.getSession().getId());
                ssocookie.setPath("/");
                response.addCookie(ssocookie);

                String serviceAddress = AdminServiceProvider.get().getSystemProperty(API.ESIA_SERVICE_ADDRESS);
                sendRedirect(response, serviceAddress + "/login/?id=" + request.getSession().getId());
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (EsiaManagerImpl.getInstance() == null) {
            sendForward(request, response, "/loginError.jsp", "esiaError");
        } else if (EsiaManagerImpl.getInstance().isValidEsiaSession(request.getSession().getId())) {
            String snils =  EsiaManagerImpl.getInstance().getSnilsAttribute(request.getSession().getId());
            if (snils != null && !snils.isEmpty()) {
                authorizeBySnils(request, response, snils);
            } else {
                sendForward(request, response, "/loginError.jsp", "esiaError");
            }
        } else {
            sendRedirect(response, "/web-client/");
        }
    }

    @Override
    protected void sendForward(HttpServletRequest request, HttpServletResponse response,
                               String url, String errorMessage) {

        if (EsiaManagerImpl.getInstance() != null) {
            EsiaManagerImpl.getInstance().destroyEsiaSession(request.getSession().getId());
        }
        super.sendForward(request, response, url, errorMessage);
    }

    private void authorizeBySnils(HttpServletRequest request, HttpServletResponse response, String snils) {
        String baseSnils = snils.replaceAll("\\D+", "");

        try {
            Employee user = AdminServiceProvider.get().findEmployeeBySnils(baseSnils);
            if (user != null) {
                setPrincipal(request, user);
                request.getSession().setAttribute("isEsiaAuth", "true");
                response.setStatus(HttpServletResponse.SC_OK);
                sendRedirect(response, "/web-client/");
            } else {
                sendForward(request, response, "/loginError.jsp", "snilsNotFound");
            }
        } catch (IllegalStateException e) {
            log.severe(e.getMessage());
            sendForward(request, response, "/loginError.jsp", "snilsNotFound");

        } catch (WebServiceException e) {
            log.severe(e.getMessage());
            sendForward(request, response, "/loginError.jsp", "esiaError");
        }
    }
}
