/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2013, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.gses.activiti;

import com.vaadin.ui.Form;
import commons.StreamHolder;
import org.osgi.framework.ServiceReference;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.gses.API;
import ru.codeinside.gses.activiti.forms.FormID;
import ru.codeinside.gses.activiti.forms.Signatures;
import ru.codeinside.gses.beans.Converter;
import ru.codeinside.gses.cert.NameParts;
import ru.codeinside.gses.cert.X509;
import ru.codeinside.gses.webui.*;
import ru.codeinside.gses.webui.components.sign.SignApplet;
import ru.codeinside.gses.webui.components.sign.SignAppletListener;
import ru.codeinside.gses.webui.form.DataAccumulator;
import ru.codeinside.gses.webui.form.FormOvSignatureSeq;
import ru.codeinside.gses.webui.form.FormSpSignatureSeq;
import ru.codeinside.gses.webui.form.ProtocolUtils;
import ru.codeinside.gses.webui.osgi.Activator;
import ru.codeinside.gses.webui.osgi.OSGiUtils;
import ru.codeinside.gws.api.*;
import ru.codeinside.smev.v3.service.api.ContentProvider;
import ru.codeinside.smev.v3.service.api.SignedData;
import ru.codeinside.smev.v3.service.api.consumer.ConsumerRequestBuilder;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequest;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponse;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseBuilder;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

@SuppressWarnings("PackageAccessibility")
public class SignatureProtocol implements SignAppletListener {
  private static final long serialVersionUID = 1L;
  private static final int MAX_CHUNK_SIZE = 100 * 1024;  // 100 Kb

  final private FormID formID;
  final private String fieldId;
  final private String lockId = "lockField";
  final private String hintId = "hintField";
  final private String timeHintId = "timeHintField";
  final private String[] ids;
  final private ContentProvider[] blocks;
  final private boolean[] files;
  final private byte[][] signs;
  final private String caption;
  final private Form form;
  final private DataAccumulator dataAccumulator;
  final private QName spProps;
  final private Boolean spAfterContent;

  private StreamHolder currentBlockHolder = new StreamHolder();
  private int currentBlockPos;

  public SignatureProtocol(FormID formID, String fieldId, String caption, ContentProvider[] blocks, boolean[] files, String[] ids,
          Form form, DataAccumulator dataAccumulator, QName spProps, Boolean spAfterContent) {
    this.formID = formID;
    this.fieldId = fieldId;
    this.caption = caption;
    this.form = form;
    this.blocks = blocks;
    this.files = files;
    this.ids = ids;
    this.dataAccumulator = dataAccumulator;
    this.spProps = spProps;
    this.spAfterContent = spAfterContent;
    signs = new byte[blocks.length][];
  }

  public SignatureProtocol(FormID formID, String fieldId, String caption, ContentProvider[] blocks, boolean[] files, String[] ids, Form form) {
    this(formID, fieldId, caption, blocks, files, ids, form, null, null, null);
  }

  @Override
  public void onLoading(SignApplet signApplet) {
    form.removeItemProperty(hintId);
    form.removeItemProperty(timeHintId);

    signApplet.setMaxProgress(calculateMaxProgress());
  }

  @Override
  public void onNoJcp(SignApplet signApplet) {
    form.removeItemProperty(fieldId);
    ReadOnly field = new ReadOnly("Не установлен КриптоПро CSP либо КриптоПро браузер-плагин. Обратитесь к системному администратору.", false);
    field.setCaption(caption);
    form.addField(fieldId, field);
  }

  @Override
  public void onCert(SignApplet signApplet, X509Certificate certificate) {
    form.removeItemProperty(hintId);
    form.removeItemProperty(timeHintId);
    boolean ok = false;
    String errorClause = null;
    try {
      boolean link = AdminServiceProvider.getBoolProperty(CertificateVerifier.LINK_CERTIFICATE);
      String login = Flash.login();
      if (link) {        
        byte[] x509 = AdminServiceProvider.get().withEmployee(login, new CertificateReader());
        ok = Arrays.equals(x509, certificate.getEncoded());
      } else {
        ok = true;
      }
      CertificateVerifyClientProvider.getInstance().verifyCertificate(certificate);
    } catch (CertificateEncodingException e) {
    } catch (CertificateInvalid err) {
      errorClause = err.getMessage();
      ok = false;
    }
    if (ok) {      
      //signApplet.block(1, blocks.length);
      signApplet.block(1, blocks.length, spProps.getLocalPart(), spProps.getNamespaceURI(), ids[0]);     
      
    } else {
      form.removeItemProperty(fieldId);
      NameParts subject = X509.getSubjectParts(certificate);
      String fieldValue = (errorClause == null) ? "Сертификат " + subject.getShortName() + " отклонён" : errorClause;
      ReadOnly field = new ReadOnly(fieldValue, false);
      field.setCaption(caption);
      form.addField(fieldId, field);
    }
  }

  @Override
  public void onBlockAck(SignApplet signApplet, int i) {
    logger().fine("AckBlock:" + i);
    if (1 <= i && i <= blocks.length) {
      currentBlockPos = i - 1;
      ContentProvider block = blocks[currentBlockPos];
      currentBlockHolder.update(block.getContent());
      int chunks = chunksCount(block);
      signApplet.chunk(1, chunks, makeChunk(currentBlockHolder.getStream()));
    }
  }

  @Override
  public void onChunkAck(SignApplet signApplet, int i) {
    logger().fine("AckChunk:" + i);
    int chunks = chunksCount(blocks[currentBlockPos]);
    signApplet.chunk(i + 1, chunks, makeChunk(currentBlockHolder.getStream()));
  }

  //TODO рефакторинг
  @Override
  public void onSign(SignApplet signApplet, byte[] sign) {
    final int i = signApplet.getBlockAck();
    currentBlockHolder.release();
    logger().fine("done block:" + i);
    if (1 <= i && i <= blocks.length) {
      signs[i - 1] = sign;
      if (i < blocks.length) {        
        //signApplet.block(i + 1, blocks.length);
        signApplet.block(i + 1, blocks.length, spProps.getLocalPart(), spProps.getNamespaceURI(), ids[i]);
      } else {
        form.removeItemProperty(fieldId);
        NameParts subjectParts = X509.getSubjectParts(signApplet.getCertificate());
        FormSignaturesField field2 = new FormSignaturesField(subjectParts.getShortName(), 
                new Signatures(formID, signApplet.getCertificate(), ids, files, signs));
        boolean isSpSign = ids.length > 0 && ids[0].equals(FormSpSignatureSeq.SP_SIGN);
        boolean isOvSign = ids.length > 0 && ids[0].equals(FormOvSignatureSeq.OV_SIGN);
        boolean isClientRequest = dataAccumulator.getServiceName() != null;
        boolean isServerResponse = dataAccumulator.getRequestType() != null;

        if (isSpSign) {
          Signatures spSignatures = (Signatures) field2.getValue();
          dataAccumulator.setSpSignatures(spSignatures);
          boolean isSmev3 = dataAccumulator.isSmev3();

          if (isClientRequest) {
            List<Enclosure> enclosures = isSmev3
                ? dataAccumulator.getOrganizationRequestData().enclosures
                : toList(dataAccumulator.getClientRequest().enclosures);
            linkSignaturesWithEnclosures(signApplet, spSignatures, enclosures);
            if (isSmev3) {
              setSmev3Signature(dataAccumulator.getOrganizationRequestData().personalData, spSignatures);
            } else if (dataAccumulator.getClientRequest().signRequired) {
              dataAccumulator.getClientRequest().appData = injectSignatureToAppData(spSignatures, dataAccumulator.getClientRequest().appData);
            }
          } else if (isServerResponse) {
            List<Enclosure> enclosures = isSmev3
                ? dataAccumulator.getProviderResponseData().enclosures
                : dataAccumulator.getServerResponse().attachmens;
            linkSignaturesWithEnclosures(signApplet, spSignatures, enclosures);
            if (isSmev3) {
              setSmev3Signature(dataAccumulator.getProviderResponseData().personalData, spSignatures);
            } else if (dataAccumulator.getServerResponse().signRequired) {
              dataAccumulator.getServerResponse().appData = injectSignatureToAppData(spSignatures, dataAccumulator.getServerResponse().appData);
            }
          }

          // если нет следующего шага, формируем сообщение и пишем запрос (или ответ для поставщиков) в базу
          if (!dataAccumulator.isNeedOv()) {
            if (isSmev3) {
              //TODO Дернуть билдер и подпись OrganizationRequest или ProviderResponse
              if (dataAccumulator.getOrganizationRequestData() != null) {
                buildOrganizationRequest();
              } else if (dataAccumulator.getProviderResponseData() != null) {
                buildProviderResponse();
              }
            } else {
            if (dataAccumulator.getClientRequest() != null) {
              buildClientRequest();
            } else if (dataAccumulator.getServerResponse() != null) {  
              buildServerResponse();
            }
          }
          }

        } else if (isOvSign) {
          Signatures ovSignatures = (Signatures) field2.getValue();
          dataAccumulator.setOvSignatures(ovSignatures);
          boolean isSmev3 = dataAccumulator.isSmev3();

          if (isSmev3) {
            if (isClientRequest) {
              setSmev3Signature(dataAccumulator.getOrganizationRequest(), ovSignatures);
            } else {
              setSmev3Signature(dataAccumulator.getProviderResponse(), ovSignatures);
            }
          } else {
          injectSignatureToSoapHeader(dataAccumulator.getSoapMessage(), ovSignatures);
          
            if (isClientRequest) {
            dataAccumulator.getClientRequest().requestMessage =  ProtocolUtils.getBytesFromSoapMessage(dataAccumulator.getSoapMessage());
            } else if (isServerResponse) {
            dataAccumulator.getServerResponse().responseMessage = ProtocolUtils.getBytesFromSoapMessage(dataAccumulator.getSoapMessage());
          } else {
            throw new IllegalStateException("Ошибка в маршруте");
          }
          }

        } else {
          Signatures signatures = field2.getValue() instanceof Signatures ? (Signatures) field2.getValue() : null;
          dataAccumulator.setSignatures(signatures);
        }
        
        field2.setCaption(caption);
        field2.setRequired(true);
        form.addField(fieldId, field2);
      }
    }
  }

  private String nodeToString(Node node) throws TransformerException {
    StringWriter sw = new StringWriter();
    Transformer t = TransformerFactory.newInstance().newTransformer();
    t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
    t.transform(new DOMSource(node), new StreamResult(sw)); 
    return sw.toString();
  }

  @Override
  protected void finalize() throws Throwable {
    currentBlockHolder.release();
    currentBlockHolder = null;

    super.finalize();
  }

  private List<Enclosure> toList(Enclosure[] enclosures) {
    if (enclosures != null) {
      return Arrays.asList(enclosures);
    }
    return null;
  }

  /**
   * Связать полученные подписи с вложениями
   *
   * @param signApplet аплет подписания
   * @param spSignatures полученные сигнатуры
   */
  private void linkSignaturesWithEnclosures(SignApplet signApplet, Signatures spSignatures, List<Enclosure> enclosures) {
    
    if (enclosures != null) {
      for (Enclosure e : enclosures) {
        String propId = e.code != null && !e.code.isEmpty() ? e.code : e.fileName;
        int signPos = spSignatures.findSign(propId);
        
        if (signPos > -1) {
          e.signature = new Signature(signApplet.getCertificate(), e.getContent(),
                  spSignatures.signs[signPos], getDigest(e.getContent()), true);
        }
      }
    }
  }

  private String injectSignatureToAppData(Signatures spSignatures, String appData) {
    if (appData == null) {
      return null;
    }

    ServiceReference serviceReference = null;
    try {
      serviceReference = Activator.getContext().getServiceReference(XmlSignatureInjector.class.getName());
      XmlSignatureInjector injector = (XmlSignatureInjector) Activator.getContext().getService(serviceReference);

      CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
      InputStream in = new ByteArrayInputStream(spSignatures.certificate);
      X509Certificate cert = (X509Certificate) certFactory.generateCertificate(in);
      byte[] signatureValue = spSignatures.signs[0];
      
      byte[] content;
      if (spProps.getLocalPart().equals("nothing") && spProps.getNamespaceURI().equals("http://nowhere/")) {
         content = appData.getBytes(Charset.forName("UTF-8"));
      } else {        
          DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
          dbf.setNamespaceAware(true);
          Element ad =  dbf.newDocumentBuilder()
            .parse(new ByteArrayInputStream(appData.getBytes()))
            .getDocumentElement();
          content = nodeToString(ad.getElementsByTagNameNS(
                  spProps.getNamespaceURI(),
                  spProps.getLocalPart()
          ).item(0)).getBytes(Charset.forName("UTF-8"));          
      }
      
      byte[] digest = getDigest(new ByteArrayInputStream(content));

      Signature signature = new Signature(cert, content, signatureValue, digest, true);
      WrappedAppData wrappedAppData = new WrappedAppData(appData, signature, spAfterContent);
      return injector.injectSpToAppData(wrappedAppData);
    } catch (CertificateException e) {
      throw new RuntimeException("Injection signature to AppData error" + e.getMessage());
    } catch (ParserConfigurationException e) {
        throw new RuntimeException("Injection signature to AppData error" + e.getMessage());
      } catch (SAXException e) {
          throw new RuntimeException("Injection signature to AppData error" + e.getMessage());
      } catch (IOException e) {
          throw new RuntimeException("Injection signature to AppData error" + e.getMessage());
      } catch (TransformerException e) {
          throw new RuntimeException("Injection signature to AppData error" + e.getMessage());
      } finally {
      if (serviceReference != null) {
        Activator.getContext().ungetService(serviceReference);
      }
    }
  }

  private void injectSignatureToSoapHeader(SOAPMessage soapMessage, Signatures ovSignatures) {
    ServiceReference serviceReference = null;
    try {
      serviceReference = Activator.getContext().getServiceReference(XmlSignatureInjector.class.getName());
      XmlSignatureInjector injector = (XmlSignatureInjector) Activator.getContext().getService(serviceReference);

      CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
      InputStream in = new ByteArrayInputStream(ovSignatures.certificate);
      X509Certificate cert = (X509Certificate) certFactory.generateCertificate(in);
      InputStream content = blocks[0].getContent();
      content.reset();
      byte[] signatureValue = ovSignatures.signs[0];
      byte[] digest = getDigest(content);

      content.reset();
      Signature signature = new Signature(cert, content, signatureValue, digest, true);

      injector.injectOvToSoapHeader(soapMessage, signature);
    } catch (CertificateException e) {
      throw new RuntimeException("Injection signature to SoapHeader error");
    } catch (IOException e) {
      throw new RuntimeException("Не удалось вставить подписиь в заголовок сообщения: " + e.getMessage(), e);
    } finally {
      if (serviceReference != null) {
        Activator.getContext().ungetService(serviceReference);
      }
    }
  }

  private byte[] getDigest(InputStream signedContent) {
    ServiceReference cryptoProviderReference = null;
    try {
      cryptoProviderReference = Activator.getContext().getServiceReference(CryptoProvider.class.getName());
      if (cryptoProviderReference == null) {
        throw new IllegalStateException("Ошибка получения DigestValue: сервис CryptoProvider недоступен");
      }

      CryptoProvider cryptoProvider = ProtocolUtils.getService(cryptoProviderReference, CryptoProvider.class);
      return cryptoProvider.digest(signedContent);
    } finally {
      if (cryptoProviderReference != null) {
        Activator.getContext().ungetService(cryptoProviderReference);
      }
    }
  }

  private void setSmev3Signature(SignedData<?> signedData, Signatures signatures) {
    if (signedData.signature == null) {
      logger().info("Блок данных не был подготовлен для подписи. Вставить подпись нельзя.");
      return;
    }
    Signature preparedSign = signedData.signature.getSignature();
    Signature signature = new Signature(
            Converter.byteArrayToX509Certificate(signatures.certificate),
            new ByteArrayInputStream(signedData.signData),
            signatures.signs[0],
            preparedSign.getDigest(),
            true);
    signedData.signature.setSignature(signature);
  }

  private void buildClientRequest() {
    ServiceReference reference = null;
    ServiceReference cryptoReference = null;
    try {
      reference = ProtocolUtils.getServiceReference(dataAccumulator.getServiceName(), Client.class);
      final Client client = ProtocolUtils.getService(reference, Client.class);

      cryptoReference = Activator.getContext().getServiceReference(CryptoProvider.class.getName());
      final CryptoProvider crypto = (CryptoProvider) Activator.getContext().getService(cryptoReference);

      ClientProtocol clientProtocol = ProtocolUtils.getClientProtocol(client);

      SOAPMessage message = clientProtocol.createMessage(client.getWsdlUrl(),
          dataAccumulator.getClientRequest(), null, null);
      dataAccumulator.setSoapMessage(message);
      crypto.sign(message);
      dataAccumulator.getClientRequest().requestMessage = ProtocolUtils.getBytesFromSoapMessage(message);
    } catch (Exception e) {
      e.printStackTrace();
      throw new IllegalStateException("Ошибка получения подготовительных данных: " + e.getMessage(), e);
    } finally {
      if (reference != null) {
        Activator.getContext().ungetService(reference);
      }
      if (cryptoReference != null) {
        Activator.getContext().ungetService(cryptoReference);
      }
    }
  }

  private void buildServerResponse() {
    ServiceReference reference = null;
    ServiceReference cryptoReference = null;
    try {
      String serviceName = ProtocolUtils.getServerName(dataAccumulator.getTaskId());
      reference = ProtocolUtils.getServiceReference(serviceName, Server.class);
      final Server server = ProtocolUtils.getService(reference, Server.class);

      cryptoReference = Activator.getContext().getServiceReference(CryptoProvider.class.getName());
      final CryptoProvider crypto = (CryptoProvider) Activator.getContext().getService(cryptoReference);

      List<Object> serviceInfo = ProtocolUtils.getQNameAndServicePort(server);
      QName qName = (QName) serviceInfo.get(0);
      ServiceDefinition.Port port = (ServiceDefinition.Port) serviceInfo.get(1);

      ServerProtocol serverProtocol = ProtocolUtils.getServerProtocol(server);

      SOAPMessage message = serverProtocol.createMessage(
          dataAccumulator.getServerResponse(),
          qName, //Service
          port,  //Port
          null,  //Log
          null
      );
      dataAccumulator.setSoapMessage(message);
      crypto.sign(message);
      dataAccumulator.getServerResponse().responseMessage = ProtocolUtils.getBytesFromSoapMessage(message);
    } catch (RuntimeException e) {
      e.printStackTrace();
      throw new IllegalStateException("Ошибка получения подготовительных данных: " + e.getMessage(), e);
    } finally {
      if (reference != null) {
        Activator.getContext().ungetService(reference);
      }
      if (cryptoReference != null) {
        Activator.getContext().ungetService(cryptoReference);
      }
    }
  }

  private void buildOrganizationRequest() {
    try {
      ConsumerRequestBuilder requestBuilder = OSGiUtils.service(ConsumerRequestBuilder.class);
      boolean prod = AdminServiceProvider.getBoolProperty(API.PRODUCTION_MODE);
      final OrganizationRequest organizationRequest =
              requestBuilder.createOrganizationRequest(dataAccumulator.getOrganizationRequestData(), !prod);
      dataAccumulator.setOrganizationRequest(organizationRequest);
    } catch(Exception e) {
      throw new RuntimeException(e);
    }
  }

  private void buildProviderResponse() {
    try{
      ProviderResponseBuilder responseBuilder = OSGiUtils.service(ProviderResponseBuilder.class);
      final ProviderResponse providerResponse = responseBuilder.createProviderResponse(dataAccumulator.getProviderResponseData());
      dataAccumulator.setProviderResponse(providerResponse);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private Logger logger() {
    return Logger.getLogger(getClass().getName());
  }

  private int chunksCount(ContentProvider block) {
    return (int) Math.ceil(block.size() / (float) MAX_CHUNK_SIZE);
  }

  private byte[] makeChunk(InputStream block) {
    byte[] buffer = new byte[MAX_CHUNK_SIZE];
    try {
      int count = block.read(buffer, 0, buffer.length);
      if (count == MAX_CHUNK_SIZE) {
        return buffer;
      } else if (count != -1) {
        return Arrays.copyOfRange(buffer, 0, count);
      } else {
        return new byte[0];
      }
    } catch (IOException e) {
      logger().info("Ошибка формирования блока данных для подписи: " + e.getMessage());
      return new byte[0];
    }
  }

  private int calculateMaxProgress() {
    int total = 0;
    for (ContentProvider block : blocks) {
      total += chunksCount(block);
    }
    return total;
  }
}
