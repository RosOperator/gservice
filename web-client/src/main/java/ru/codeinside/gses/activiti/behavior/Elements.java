package ru.codeinside.gses.activiti.behavior;

import org.activiti.engine.impl.util.xml.Element;
import org.activiti.engine.impl.util.xml.Parse;

/**
 * Вспомогательные функции для получеия блоков BPMN
 */
public final class Elements {

    public static Element findElement(String id, String tag, Parse parse) {
        Element rootElement = parse.getRootElement();
        return findElement(id, tag, rootElement);
    }

    private static Element findElement(String id, String tag, Element element) {
        for (Element child : element.elements()) {
            if (tag.equals(child.getTagName()) && id.equals(child.attribute("id"))) {
                return child;
            }
            Element deep = findElement(id, tag, child);
            if (deep != null) {
                return deep;
            }
        }
        return null;
    }
}
