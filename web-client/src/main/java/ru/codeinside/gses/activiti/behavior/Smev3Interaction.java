package ru.codeinside.gses.activiti.behavior;

import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.el.FixedValue;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.impl.jobexecutor.TimerDeclarationImpl;
import org.activiti.engine.impl.jobexecutor.TimerDeclarationType;
import org.activiti.engine.impl.jobexecutor.TimerExecuteNestedActivityJobHandler;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.TimerEntity;
import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;
import org.activiti.engine.impl.variable.EntityManagerSession;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.adm.database.Employee;
import ru.codeinside.adm.database.SmevRequestType;
import ru.codeinside.adm.database.SmevTask;
import ru.codeinside.adm.database.SmevTaskStrategy;
import ru.codeinside.gses.beans.Smev3;

import javax.persistence.EntityManager;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@SuppressWarnings("PackageAccessibility")
public class Smev3Interaction {
    private static Logger logger = Logger.getLogger(Smev3Interaction.class.getName());
    private static final int DELAY = 30;

    private final ActivityExecution execution;
    private final EntityManager em;

    public Smev3Interaction(ActivityExecution execution) {
        this.execution = execution;
        this.em = Context.getCommandContext().getSession(EntityManagerSession.class).getEntityManager();
    }

    public void execute() {
        SmevTask task = findSmevTask();
        if (Boolean.TRUE.equals(execution.getVariable(Smev3.RESPONSE_READY))) {
            PvmTransition active = execution.getActivity().getOutgoingTransitions().get(0);
            execution.take(active);
            em.remove(task);
            em.flush();
        } else {
            scheduleNextTime(execution);
            task.setLastChange(new Date());
            em.persist(task);
            em.flush();
        }
    }

    private void scheduleNextTime(ActivityExecution execution) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, DELAY);
        FixedValue nextRun = new FixedValue(calendar.getTime());
        logger.fine("schedule check response at " + nextRun.getExpressionText());
        TimerDeclarationImpl timerDeclaration = new TimerDeclarationImpl(
                nextRun, TimerDeclarationType.DATE, TimerExecuteNestedActivityJobHandler.TYPE);
        timerDeclaration.setRetries(1);
        TimerEntity timer = timerDeclaration.prepareTimerEntity((ExecutionEntity) execution);
        timer.setJobHandlerConfiguration(execution.getCurrentActivityId());
        timer.setExclusive(true);
        Context.getCommandContext().getJobManager().schedule(timer);
    }

    private SmevTask findSmevTask() {
        List<SmevTask> tasks = em.createQuery("select t from SmevTask t where " +
                "t.taskId=:taskId and t.executionId=:executionId and t.processInstanceId=:processId", SmevTask.class)
                .setParameter("taskId", execution.getCurrentActivityId())
                .setParameter("executionId", execution.getId())
                .setParameter("processId", execution.getProcessInstanceId())
                .getResultList();
        if (tasks.size() > 0) {
            return tasks.get(0);
        }
        return createNewTask();
    }

    private SmevTask createNewTask() {
        SmevTask task = new SmevTask();
        task.setTaskId(execution.getCurrentActivityId());
        task.setExecutionId(execution.getId());
        task.setProcessInstanceId(execution.getProcessInstanceId());
        task.setErrorMaxCount(0);
        task.setErrorDelay(0);
        task.setPingMaxCount(0);
        task.setPingDelay(0);
        task.setConsumer(String.valueOf(execution.getVariable(Smev3.CONSUMER_NAME_KEY)));
        task.setStrategy(SmevTaskStrategy.REQUEST);
        task.setRequestType(SmevRequestType.REQUEST);
        task.setGroups(Collections.<String>emptySet());
        task.setBid(AdminServiceProvider.get().getBidByProcessInstanceId(execution.getProcessInstanceId()));
        String login = Authentication.getAuthenticatedUserId();
        if (login != null) {
            task.setEmployee(em.find(Employee.class, login));
        }
        return task;
    }
}
