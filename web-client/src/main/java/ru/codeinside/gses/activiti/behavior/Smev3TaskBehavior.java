package ru.codeinside.gses.activiti.behavior;

import org.activiti.engine.impl.bpmn.behavior.TaskActivityBehavior;
import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.util.xml.Parse;

import java.util.List;

/**
 * Сервис-таск для ожидания ответа из СМЭВ3
 */
public final class Smev3TaskBehavior extends TaskActivityBehavior implements TransitionValidator {

    @Override
    public void validateTransitions(ActivityImpl activity, Parse parse) {
        List<PvmTransition> outgoingTransitions = activity.getOutgoingTransitions();
        if (outgoingTransitions.size() != 1) {
            parse.addError(String.format("Для блока СМЭВ3 {%s} должен быть только один выход.", activity.getId()),
                    Elements.findElement(activity.getId(), "serviceTask", parse));
        }
    }

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        new Smev3Interaction(execution).execute();
    }
}
