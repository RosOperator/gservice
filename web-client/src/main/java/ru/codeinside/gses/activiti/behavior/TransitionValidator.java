package ru.codeinside.gses.activiti.behavior;

import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.util.xml.Parse;

/**
 * Интерфес валдидирования переходов
 */
public interface TransitionValidator {
    void validateTransitions(ActivityImpl activity, Parse parse);
}
