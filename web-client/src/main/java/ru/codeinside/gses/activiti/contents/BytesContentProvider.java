package ru.codeinside.gses.activiti.contents;

import com.google.common.base.Preconditions;
import ru.codeinside.smev.v3.service.api.ContentProvider;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class BytesContentProvider implements ContentProvider {
    private final byte[] content;
    private final int size;

    public BytesContentProvider(final byte[] content) {
        Preconditions.checkNotNull(content);
        this.content = content;
        this.size = content.length;
    }

    @Override
    public InputStream getContent() {
        return new ByteArrayInputStream(content);
    }

    @Override
    public long size() {
        return size;
    }
}
