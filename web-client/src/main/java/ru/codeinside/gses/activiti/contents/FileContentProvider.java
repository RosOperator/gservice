package ru.codeinside.gses.activiti.contents;

import ru.codeinside.smev.v3.service.api.ContentProvider;

import java.io.*;

public class FileContentProvider implements ContentProvider {
    private final File file;
    private final long size;

    public FileContentProvider(final File file, final long size) {
        this.file = file;
        this.size = size;
    }

    public FileContentProvider(final File file) {
        this(file, file.length());
    }

    public FileContentProvider(final String path) {
        this(new File(path));
    }

    @Override
    public InputStream getContent() {
        try {
            return new BufferedInputStream(new FileInputStream(this.file));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public long size() {
        return this.size;
    }
}
