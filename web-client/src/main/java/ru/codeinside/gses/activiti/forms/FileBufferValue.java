/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2014, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.gses.activiti.forms;

import ru.codeinside.gses.activiti.FileValue;
import ru.codeinside.gses.activiti.contents.BytesContentProvider;
import ru.codeinside.gses.activiti.contents.FileContentProvider;
import ru.codeinside.gses.beans.ExecutorServiceProvider;
import ru.codeinside.smev.v3.service.api.ContentProvider;

import java.io.File;

final public class FileBufferValue implements FileValue {

  private final String fileName;
  private final Integer contentId;
  private String mimeType;

  public FileBufferValue(String fileName, String mimeType, Integer contentId) {
    this.fileName = fileName;
    this.mimeType = mimeType;
    this.contentId = contentId;
  }

  @Override
  public String getFileName() {
    return fileName;
  }

  @Override
  public String getMimeType() {
    return mimeType;
  }

  @Override
  public ContentProvider getContent() {
    if (contentId == null) {
      return new BytesContentProvider(new byte[0]);
    }

    File file = getFile();
    if (file == null) {
      return new BytesContentProvider(new byte[0]);
    }
    return new FileContentProvider(file, file.length());
  }

  @Override
  public File getFile() {
    return ExecutorServiceProvider.get().getFile(contentId);
  }

  @Override
  public String toString() {
    return fileName;
  }
}
