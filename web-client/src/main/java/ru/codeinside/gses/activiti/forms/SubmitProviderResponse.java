package ru.codeinside.gses.activiti.forms;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.impl.ServiceImpl;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.impl.variable.EntityManagerSession;
import ru.codeinside.adm.database.ProviderResponseEntity;
import ru.codeinside.gses.service.F1;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponse;

import javax.persistence.EntityManager;

public final class SubmitProviderResponse implements F1<Long, ProviderResponse> {
  @Override
  public Long apply(ProcessEngine engine, ProviderResponse providerResponse) {
    CommandExecutor commandExecutor = ((ServiceImpl) engine.getManagementService()).getCommandExecutor();
    return commandExecutor.execute(new SaveCommand(providerResponse));
  }

  @SuppressWarnings("PackageAccessibility")
  private final class SaveCommand implements Command<Long>{
    private final ProviderResponse providerResponse;

    public SaveCommand(ProviderResponse providerResponse) {
      this.providerResponse = providerResponse;
    }

    @Override
    public Long execute(CommandContext commandContext) {
      EntityManager em = commandContext.getSession(EntityManagerSession.class).getEntityManager();
      ProviderResponseEntity entity = new ProviderResponseEntity(providerResponse);
      em.persist(entity);
      return entity.getId();
    }
  }
}
