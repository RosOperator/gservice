/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2013, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.gses.activiti.ftarchive;

import com.vaadin.ui.Field;
import ru.codeinside.gses.activiti.ReadOnly;
import ru.codeinside.gses.activiti.SimpleField;
import ru.codeinside.gses.activiti.forms.api.definitions.PropertyNode;
import ru.codeinside.gses.activiti.forms.types.FieldType;

public class AttachmentExternalFFT implements FieldType<String> {

  private static final long serialVersionUID = 1L;


  private Field getField(String name, String value, boolean writable) {
    if (writable) {
      return new AttachmentExternalField(name, false);
    }
    if (value == null || value.isEmpty()) {
      return new ReadOnly(null);
    }
    return new SimpleField(new AttachmentExternalField(name, true).createDownloadLink(value), name);
  }

  @Override
  public Field createField(String taskId, String fieldId, String name, String value, PropertyNode node, boolean archive) {
    Field field = getField(name, value, node.isFieldWritable() && !archive);
    FieldHelper.setCommonFieldProperty(field, node.isFieldWritable() && !archive, name, node.isFieldRequired());
    return field;
  }
}
