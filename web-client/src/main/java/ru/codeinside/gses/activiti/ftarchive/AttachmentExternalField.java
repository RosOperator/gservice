/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2013, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.gses.activiti.ftarchive;

import com.vaadin.terminal.DownloadStream;
import com.vaadin.terminal.Sizeable;
import com.vaadin.terminal.StreamResource;
import com.vaadin.terminal.gwt.server.WebBrowser;
import com.vaadin.ui.*;
import commons.Streams;
import ru.codeinside.gses.activiti.FileValue;
import ru.codeinside.gses.activiti.ReadOnly;
import ru.codeinside.gses.beans.export.ExternalFileManager;
import ru.codeinside.gses.vaadin.customfield.CustomField;
import ru.codeinside.gses.webui.Flash;

import javax.mail.internet.MimeUtility;
import java.io.*;
import java.net.URI;

final public class AttachmentExternalField extends CustomField implements Serializable, Upload.Receiver {

    private static final long serialVersionUID = 1L;
    final Upload upload;
    final HorizontalLayout layout = new HorizontalLayout();
    final ProgressIndicator indicator = new ProgressIndicator();
    final Label sizeInfo = new Label();
    final ExternalFileManager fileManager = new ExternalFileManager();

    Component oldValue;
    File tmpFile;
    Button removeAttachmentButton;
    final boolean signature;

    public AttachmentExternalField(final String name, boolean signature) {
        this.signature = signature;
        upload = new Upload(null, this);
        if (!signature) {
            upload.setButtonCaption("Выбрать файл");
            upload.setImmediate(true);
            upload.addListener(new Upload.StartedListener() {
                private static final long serialVersionUID = 1L;

                @Override
                public void uploadStarted(Upload.StartedEvent event) {
                    indicator.setValue(0f);
                    indicator.setVisible(true);
                    sizeInfo.setValue("0/" + event.getContentLength());
                }
            });
            upload.addListener(new Upload.ProgressListener() {
                private static final long serialVersionUID = 1L;

                public void updateProgress(long readBytes, long contentLength) {
                    indicator.setValue((float) readBytes / (float) contentLength);
                    sizeInfo.setValue("" + readBytes + "/" + contentLength);
                }

            });
            upload.addListener(new Upload.SucceededListener() {
                private static final long serialVersionUID = 1L;

                @Override
                public void uploadSucceeded(Upload.SucceededEvent event) {
                    removeOldValue();
                    String fileName = event.getFilename();
                    String fileType = event.getMIMEType();
                    FileValue value = new TmpFileValue(fileName, fileType, tmpFile);

                    byte[] content = getBytesFromFileValue(value);

                    if (content == null) {
                        getWindow().showNotification("Ошибка при загрузке файла " + fileName, Window.Notification.TYPE_HUMANIZED_MESSAGE);
                    } else {
                        String externalLink = fileManager.putFile(
                                new ExternalFileManager.ExternalFile(value.getMimeType(), value.getFileName(), content)
                        );
                        if (externalLink == null) {
                            getWindow().showNotification("Ошибка при сохранении файла " + fileName, Window.Notification.TYPE_HUMANIZED_MESSAGE);
                        } else {
                            setValue(externalLink);
                            setDownloadLink(createDownloadLink(value.getMimeType(), value.getFileName(), content));
                        }
                    }
                    resetUpload();
                }
            });

            upload.addListener(new Upload.FailedListener() {
                private static final long serialVersionUID = 1L;

                @Override
                public void uploadFailed(Upload.FailedEvent event) {
                    if (tmpFile != null) {
                        tmpFile.delete();
                        tmpFile = null;
                    }
                    getWindow().showNotification("Ошибка при загрузке " + name, Window.Notification.TYPE_HUMANIZED_MESSAGE);
                }
            });

            upload.addListener(new Upload.FinishedListener() {
                private static final long serialVersionUID = 1L;

                @Override
                public void uploadFinished(Upload.FinishedEvent event) {
                    indicator.setVisible(false);
                    sizeInfo.setValue(null);
                }
            });
            indicator.setVisible(false);
            indicator.setWidth(100, Sizeable.UNITS_PIXELS);

            layout.setSpacing(true);
            layout.addComponent(indicator);
            layout.addComponent(sizeInfo);
        }

        setCaption(name);
        setValidationVisible(true);
        setRequiredError("Выберите файл!");

        initRemoveAttachmentButton();

        if (!signature) {
            layout.addComponent(upload);
            layout.addComponent(removeAttachmentButton);
        }
        setCompositionRoot(layout);
    }

    void setDownloadLink(Component link) {
        oldValue = link;
        layout.addComponent(oldValue);
        if (!signature) {
            removeAttachmentButton.setVisible(true);
            upload.setVisible(false);
        }
    }

    private void initRemoveAttachmentButton() {
        if (!signature) {
            removeAttachmentButton = new Button("Удалить вложение");
            removeAttachmentButton.setVisible(false);
            removeAttachmentButton.addListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    removeOldValue();
                    removeAttachmentButton.setVisible(false);
                    upload.setVisible(true);
                    if (getValue() != null) {
                        fileManager.deleteFile(getValue().toString());
                    }
                    setValue(null, true);
                    resetUpload();
                }
            });
        }
    }

    void removeOldValue() {
        if (oldValue != null) {
            layout.removeComponent(oldValue);
            oldValue = null;
        }
    }

    @Override
    public Class<String> getType() {
        return String.class;
    }

    public OutputStream receiveUpload(String name, String type) {
        try {
            tmpFile = Streams.createTempFile("upload", ".tmp");
            return new FileOutputStream(tmpFile);
        } catch (IOException e) {
            throw new RuntimeException("On upload " + name, e);
        }
    }

    @Override
    public void discard() throws SourceException {
        super.discard();
        resetUpload();
    }

    private void resetUpload() {
        sizeInfo.setValue(null);
        tmpFile = null;
    }

    private byte[] getBytesFromFileValue(final FileValue fileValue) {
        InputStream stream = fileValue.getContent().getContent();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            final byte[] buffer = new byte[1024];
            int read;
            while ((read = stream.read(buffer)) != -1) {
                bos.write(buffer, 0, read);
            }
            return bos.toByteArray();

        } catch (IOException e) {
            return null;
        } finally {
            try {
                stream.close();
                bos.close();
            } catch (IOException ignored) {
            }
        }
    }

    Component createDownloadLink(final String fileType, final String fileName, final byte[] content) {
        final Link result = new Link();
        result.setCaption(fileName);
        result.setTargetName("_top");
        result.setImmediate(true);
        result.setDescription("Скачать");
        StreamResource.StreamSource streamSource = new StreamResource.StreamSource() {
            public InputStream getStream() {
                return new ByteArrayInputStream(content);
            }
        };
        StreamResource resource = new StreamResource(streamSource, fileName, Flash.app()) {
            public DownloadStream getStream() {
                final StreamSource ss = getStreamSource();
                if (ss == null) {
                    return null;
                }
                final DownloadStream ds = new DownloadStream(ss.getStream(), getMIMEType(), getFilename());
                ds.setCacheTime(0);
                try {
                    WebBrowser browser = (WebBrowser) result.getWindow().getTerminal();
                    if (browser.isIE()) {
                        URI uri = new URI(null, null, fileName, null);
                        ds.setParameter("Content-Disposition", "attachment; filename=" + uri.toASCIIString());
                    } else {
                        ds.setParameter("Content-Disposition", "attachment; filename=\"" + MimeUtility.encodeWord(fileName, "utf-8", "Q") + "\"");
                    }
                } catch (Exception e) {
                    ds.setParameter("Content-Disposition", "attachment; filename=" + fileName);
                }
                return ds;
            }
        };
        resource.setMIMEType(fileType);
        result.setResource(resource);
        return result;
    }

    Component createDownloadLink(final String externalLink) {
        ru.codeinside.gws.api.ExternalFileManager.ExternalFile externalFile = fileManager.getFile(externalLink);
        if (externalFile == null) {
            return new ReadOnly("Ошибка при загрузке файла вложения. Обратитесь в службу технической поддержки.", false);
        }
        return createDownloadLink(externalFile.type, externalFile.name, externalFile.content);
    }
}