/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2014, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.gses.activiti.ftarchive;

import commons.Streams;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.ServiceImpl;
import org.activiti.engine.task.Attachment;
import ru.codeinside.adm.database.FileContent;
import ru.codeinside.gses.activiti.FileValue;
import ru.codeinside.gses.activiti.contents.BytesContentProvider;
import ru.codeinside.gses.activiti.contents.FileContentProvider;
import ru.codeinside.gses.service.F0;
import ru.codeinside.gses.service.Fn;
import ru.codeinside.smev.v3.service.api.ContentProvider;

import java.io.File;
import java.io.IOException;

/**
 * Ссылка на существующие вложение.
 */
@SuppressWarnings("PackageAccessibility")
final public class AttachmentFileValue implements FileValue {

    private final Attachment attachment;

    public AttachmentFileValue(Attachment attachment) {
        this.attachment = attachment;
    }

    @Override
    public String getFileName() {
        return attachment.getName();
    }

    @Override
    public String getMimeType() {
        return attachment.getType();
    }


    @Override
    public ContentProvider getContent() {
        return Fn.withEngine(new F0<ContentProvider>() {
            @Override
            public ContentProvider apply(ProcessEngine engine) {
                TaskService taskService = engine.getTaskService();
                FileContent fileContent = FileContent.findByAttachmentId((ServiceImpl) taskService, attachment.getId());
                if (fileContent != null) {
                    return new FileContentProvider(fileContent.getPath());
                } else {
                    try {
                        byte[] payload = Streams.toBytes(taskService.getAttachmentContent(attachment.getId()));
                        return new BytesContentProvider(payload);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        });
    }

    @Override
    public File getFile() {
        return Fn.withEngine(new F0<File>() {
            @Override
            public File apply(ProcessEngine engine) {
                TaskService taskService = engine.getTaskService();
                FileContent fileContent = FileContent.findByAttachmentId((ServiceImpl) taskService, attachment.getId());
                if (fileContent != null) {
                    return new File(fileContent.getPath());
                }
                return null;
            }
        });
    }

    public Attachment getAttachment() {
        return attachment;
    }

    @Override
    public String toString() {
        return attachment.getName();
    }
}
