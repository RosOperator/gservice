package ru.codeinside.gses.beans;

import ru.codeinside.adm.database.SmevChain;
import ru.codeinside.gses.beans.filevalues.BaseRequestContext;
import ru.codeinside.gws.api.DeclarerContext;
import ru.codeinside.smev.v3.service.api.MessageMetadata;
import ru.codeinside.smev.v3.service.api.ProviderRequestContext;
import ru.codeinside.smev.v3.service.api.provider.RequestMessage;

import java.util.List;
import java.util.Map;

/**
 * Контекст обработки запроса для потребителй СМЭВ3
 */
public class ActivitiProviderRequestContext extends BaseRequestContext implements ProviderRequestContext {

    private final RequestMessage message;

    public ActivitiProviderRequestContext(RequestMessage message, String componentName) {
        super(componentName, getSmevChain(message));
        this.message = message;
    }

    @Override
    public RequestMessage getRequestMessage() {
        return this.message;
    }

    @Override
    public String getBid() {
        return super.getBid();
    }

    @Override
    public List<String> getBids() {
        return super.getBids();
    }

    @Override
    public DeclarerContext getDeclarerContext(long procedureCode) {
        return super.getDeclarerContext(procedureCode);
    }

    @Override
    public void updateReceiptContext(Map<String, Object> values) {
        super.updateReceiptContext(values);
    }

    private static SmevChain getSmevChain(RequestMessage message) {
        String originRequestRefId;
        String requestRefId;
        MessageMetadata metadata;
        if (message.request != null) {
            originRequestRefId = message.request.data.replyTo;
            requestRefId = message.request.data.organizationRequest.data.messageID;
            metadata = message.request.data.messageMetadata;
        } else {
            originRequestRefId = message.cancel.messageReference.data.messageId;
            requestRefId = message.cancel.messageID;
            metadata = message.cancel.messageMetadata;
        }

        return new SmevChain(true, null, originRequestRefId, metadata.sender, requestRefId, metadata.recipient);
    }
}
