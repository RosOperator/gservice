package ru.codeinside.gses.beans;

import javax.ejb.DependsOn;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@SuppressWarnings("PackageAccessibility")
@Singleton
@Lock(LockType.READ)
@DependsOn("BaseBean")
public class AnticipantsProvider {
    private final static int MAX_COUNT = 5;

    @PersistenceContext(unitName = "myPU")
    EntityManager em;

    public List<String> distinctConsumers() {
        return em.createQuery("select distinct a.consumerName from Anticipant a", String.class)
                .setMaxResults(MAX_COUNT)
                .getResultList();
    }
}
