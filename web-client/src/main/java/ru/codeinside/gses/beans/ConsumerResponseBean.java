package ru.codeinside.gses.beans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.*;
import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Logger;

@SuppressWarnings({"PackageAccessibility", "unused"})
@DependsOn("BaseBean")
@Lock(LockType.READ)
@Singleton
@Startup
public class ConsumerResponseBean {
    private static final int DELTA = 20;  // 20 sec
    private static final int MINIMUM_DELAY = 60; // 60 sec
    private static final int MAXIMUM_DELAY = 300;  // 5 min

    private static final Logger logger = Logger.getLogger(ConsumerResponseBean.class.getName());
    private ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

    private int scheduleDelay = MINIMUM_DELAY;

    @Inject
    Smev3 smev3;

    @Inject
    AnticipantsProvider anticipantsProvider;

    @PostConstruct
    public void init() {
        startScheduler();
    }

    @PreDestroy
    public void shutdown() {
        scheduler.shutdown();
        logger.info("Завершение опрашивателей ответов СМЭВ3");
    }

    private void startScheduler() {
        logger.info("Активация опрашивателей ответов СМЭВ3");
        scheduler.schedule(new ResponsePuller(), scheduleDelay, TimeUnit.SECONDS);
    }

    private int updateScheduleDelay(boolean success) {
        scheduleDelay = success ? MINIMUM_DELAY : (scheduleDelay + DELTA);
        if (scheduleDelay > MAXIMUM_DELAY) {
            scheduleDelay = MAXIMUM_DELAY;
        }
        return scheduleDelay;
    }

    private class ResponsePuller implements Runnable {

        @Override
        public void run() {
            final List<String> consumers = anticipantsProvider.distinctConsumers();
            final int consumersCount = consumers.size();
            if (consumersCount < 1) {
                if (!scheduler.isShutdown()) {
                    scheduler.schedule(this, MINIMUM_DELAY, TimeUnit.SECONDS);
                }
                return;
            }

            ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
            boolean isSmevCalled = false;
            for (String consumer : consumers) {
                if (Thread.currentThread().isInterrupted()) {
                    break;
                }
                Future<Smev3Result> futureResult;
                if (!isSmevCalled) {
                    futureResult = executor.submit(new ProcessResponse(consumer));
                } else {
                    futureResult = executor.schedule(new ProcessResponse(consumer), scheduleDelay, TimeUnit.SECONDS);
                }
                logger.info("Получение ответа для потребителя " + consumer);

                try {
                    Smev3Result result = futureResult.get();
                    if (result != Smev3Result.NONE) {
                        isSmevCalled = true;
                        updateScheduleDelay(result == Smev3Result.RESULT_WAS_GOT);
                    }
                } catch (InterruptedException e) {
                    logger.info("При ожидании ответа возникло прерывание потока: " + e.getMessage());
                } catch (ExecutionException e) {
                    logger.info("Ошибка выполнения потока обработки ответа: " + e.getMessage());
                }
            }
            executor.shutdown();
            try {
                logger.info("Ожидаем завершения обработки ответов");
                boolean isOk = executor.awaitTermination(50, TimeUnit.SECONDS);
                logger.info("Ожидание завершилось со статусом '" + (isOk ? "нормально" : "таймаут") + "'");
            } catch (InterruptedException e) {
                logger.info("Was interrupted while awaiting termination: " + e.getMessage());
            }

            if (!scheduler.isShutdown()) {
                scheduler.schedule(this, scheduleDelay, TimeUnit.SECONDS);
            }
        }
    }

    private class ProcessResponse implements Callable<Smev3Result> {
        private final String consumer;

        public ProcessResponse(String consumer) {
            this.consumer = consumer;
        }

        @Override
        public Smev3Result call() throws Exception {
            if (Thread.currentThread().isInterrupted()) {
                logger.info("Поток для " + consumer + " был прерван");
                return Smev3Result.NONE;
            }
            return smev3.getResponse(consumer);
        }
    }
}
