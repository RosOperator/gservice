package ru.codeinside.gses.beans;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

public class Converter {
    private Converter() {
        throw new UnsupportedOperationException("Static methods only");
    }

    public static byte[] getByteArrayFromElement(Element element) {
        if (element == null) {
            return null;
        }
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            StreamResult streamResult = new StreamResult(new StringWriter());
            transformer.transform(new DOMSource(element), streamResult);
            String stringResult = streamResult.getWriter().toString();
            return stringResult.getBytes(Charset.forName("UTF-8"));
        } catch (TransformerException e) {
            throw new IllegalStateException(e);
        }
    }

    public static List<byte[]> getByteArrayListFromElementList(List<Element> elements) {
        if (elements == null || elements.isEmpty()) {
            return null;
        }
        List<byte[]> result = new ArrayList<byte[]>();
        for (Element element : elements) {
            result.add(getByteArrayFromElement(element));
        }
        return result;
    }

    public static Element byteArrayToElement(byte[] personalData) {
        if (personalData == null || personalData.length == 0) {
            return null;
        }
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(new ByteArrayInputStream(personalData));
            return document.getDocumentElement();
        } catch (ParserConfigurationException e) {
            throw new IllegalStateException("Не удалось получить элемент", e);
        } catch (SAXException e) {
            throw new IllegalStateException("Не удалось получить элемент", e);
        } catch (IOException e) {
            throw new IllegalStateException("Не удалось получить элемент", e);
        }
    }

    public static X509Certificate byteArrayToX509Certificate(byte[] personalCertificate) {
        if (personalCertificate == null || personalCertificate.length == 0) {
            return null;
        }
        try {
            CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
            InputStream in = new ByteArrayInputStream(personalCertificate);
            return (X509Certificate) certFactory.generateCertificate(in);
        } catch (CertificateException e) {
            throw new IllegalStateException("Не удалось получить сертификат", e);
        }
    }

    public static byte[] X509CertificateToByteArray(X509Certificate certificate) {
        try {
            if (certificate == null) {
                return null;
            }
            return certificate.getEncoded();
        } catch (CertificateEncodingException e) {
            throw new IllegalStateException("Не удалось преобразовтаь сертификат в байты.", e);
        }
    }
}
