package ru.codeinside.gses.beans;

import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

@SessionScoped
public class EnclosureSizeChecker implements Serializable {
    private final long MAX_SIZE = 1024 * 1024 * 1024;  // 1Gb

    private long totalSize = 0;

    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public boolean increaseTotalSize(long size) {
        if (size > MAX_SIZE || size < 0) {
            return false;
        }
        if ((totalSize + size) > MAX_SIZE) {
            return false;
        }
        totalSize += size;
        return true;
    }

    public void decreaseTotalSize(long size) {
        if (totalSize < size) {
            totalSize = 0;
        }
        this.totalSize -= size;
    }
}
