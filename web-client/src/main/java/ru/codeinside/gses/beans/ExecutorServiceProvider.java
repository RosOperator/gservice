package ru.codeinside.gses.beans;

import ru.codeinside.gses.service.ExecutorService;
import ru.codeinside.gses.service.impl.ExecutorServiceImpl;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.*;
import javax.inject.Inject;

@SuppressWarnings("PackageAccessibility")
@Singleton
@Startup
@DependsOn("BaseBean")
@Lock(LockType.READ)
public class ExecutorServiceProvider {

    transient static ExecutorService instance;

    @Inject
    ExecutorService executorService;

    @PostConstruct
    public void setUp() {
        synchronized (ExecutorServiceImpl.class) {
            if (instance == null) {
                instance = executorService;
            }
        }
    }

    @PreDestroy
    public void tearDown() {
        synchronized (ExecutorServiceImpl.class) {
            if (instance == executorService) {
                instance = null;
            }
        }
    }

    public static ExecutorService get() {
        ExecutorService result = instance;
        if (result == null) {
            throw new IllegalStateException("Сервис Исполнителя не зарегистрирован.");
        }
        return result;
    }
}
