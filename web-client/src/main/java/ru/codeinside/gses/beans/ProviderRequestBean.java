package ru.codeinside.gses.beans;

import ru.codeinside.gses.webui.gws.ProviderRefRegistry;
import ru.codeinside.gses.webui.gws.TRef;
import ru.codeinside.smev.v3.service.api.provider.Provider;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 * Бин для опроса заявок
 */
@SuppressWarnings({"PackageAccessibility", "unused"})
@DependsOn("BaseBean")
@Singleton
@Startup
public class ProviderRequestBean {
    private static final int DELTA = 20;  // 20 sec
    private static final int MINIMUM_DELAY = 60; // 60 sec
    private static final int MAXIMUM_DELAY = 300;  // 5 min

    private final Logger logger = Logger.getLogger(ProviderRequestBean.class.getName());

    private int scheduleDelay = MINIMUM_DELAY;

    @Inject
    ProviderRefRegistry providerRegistry;

    @Inject
    Smev3 smev3;

    private ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

    @PostConstruct
    public void init() {
        startScheduler();
    }

    @PreDestroy
    public void shutdown() {
        scheduler.shutdown();
        logger.info("Завершение опрашивателей запросов СМЭВ3");
    }

    private void startScheduler() {
        logger.info("Активация опрашивателей запросов СМЭВ3");
        scheduler.schedule(new RequestPuller(), scheduleDelay, TimeUnit.SECONDS);
    }

    private int updateScheduleDelay(boolean success) {
        scheduleDelay = success ? MINIMUM_DELAY : (scheduleDelay + DELTA);
        if (scheduleDelay > MAXIMUM_DELAY) {
            scheduleDelay = MAXIMUM_DELAY;
        }
        return scheduleDelay;
    }

    // Inner Classes

    private class RequestPuller implements Runnable {
        private final float scale = 0.5f;
        private ScheduledExecutorService executor;

        @Override
        public void run() {
            if (providerRegistry.getProviderRefs().size()  == 0) {
                logger.info("Поставщики отсутствуют");
                if (!scheduler.isShutdown()) {
                    scheduler.schedule(this, MINIMUM_DELAY, TimeUnit.SECONDS);
                }
                return;
            }

            executor = Executors.newSingleThreadScheduledExecutor();
            boolean isSmevCalled = false;
            for (TRef<Provider> tRef : providerRegistry.getProviderRefs()) {
                if (Thread.currentThread().isInterrupted()) {
                    break;
                }

                Future<Smev3Result> futureResult;
                if (!isSmevCalled) {
                    futureResult = executor.submit(new ProcessRequest(tRef.getName()));
                } else {
                    futureResult = executor.schedule(new ProcessRequest(tRef.getName()), scheduleDelay, TimeUnit.SECONDS);
                }
                logger.info("Получение заявок для поставщика " + tRef.getName());

                try {
                    Smev3Result result = futureResult.get();
                    if (result != Smev3Result.NONE) {
                        isSmevCalled = true;
                        updateScheduleDelay(result == Smev3Result.RESULT_WAS_GOT);
                    }
                } catch (InterruptedException e) {
                    logger.info("При ожидании запроса возникло прерывание потока: " + e.getMessage());
                } catch (ExecutionException e) {
                    logger.info("Ошибка выполнения потока обработки запроса: " + e.getMessage());
                }
            }

            executor.shutdown();
            try {
                logger.info("Ожидаем завершения обработки заявок");
                boolean isOk = executor.awaitTermination(50, TimeUnit.SECONDS);
                logger.info("Ожидание завершилось со статусом '" + (isOk ? "нормально" : "таймаут") + "'");
            } catch (InterruptedException e) {
                logger.info("Was interrupted while awaiting termination: " + e.getMessage());
            }

            if (!scheduler.isShutdown()) {
                scheduler.schedule(this, scheduleDelay, TimeUnit.SECONDS);
            }
        }
    }

    private class ProcessRequest implements Callable<Smev3Result> {
        private final String provider;

        public ProcessRequest(String providerName) {
            this.provider = providerName;
        }

        @Override
        public Smev3Result call() {
            if (Thread.currentThread().isInterrupted()) {
                logger.info("Поток для " + provider + " был прерван");
                return Smev3Result.NONE;
            }
            return smev3.getRequest(provider);
        }
    }
}
