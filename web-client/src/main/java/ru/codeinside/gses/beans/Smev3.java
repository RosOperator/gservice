package ru.codeinside.gses.beans;

import com.google.common.base.Preconditions;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.impl.ServiceImpl;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.apache.commons.lang.StringUtils;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import ru.codeinside.adm.AdminService;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.adm.database.Anticipant;
import ru.codeinside.adm.database.OrganizationRequestEntity;
import ru.codeinside.adm.database.ProviderResponseEntity;
import ru.codeinside.gses.API;
import ru.codeinside.gses.service.ActivitiService;
import ru.codeinside.gses.service.F0;
import ru.codeinside.gses.webui.form.ProtocolUtils;
import ru.codeinside.gses.webui.gws.TRef;
import ru.codeinside.gses.webui.gws.TRefProvider;
import ru.codeinside.gses.webui.osgi.Activator;
import ru.codeinside.gses.webui.osgi.LogCustomizer;
import ru.codeinside.gws.api.ExchangeContext;
import ru.codeinside.gws.api.InfoSystem;
import ru.codeinside.gws.api.Packet;
import ru.codeinside.gws.api.ReceiptContext;
import ru.codeinside.smev.v3.service.api.*;
import ru.codeinside.smev.v3.service.api.consumer.*;
import ru.codeinside.smev.v3.service.api.provider.*;
import ru.codeinside.smev.v3.transport.api.SmevTransport;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Бин для вызова методов СМЗВv.3
 */
@SuppressWarnings("PackageAccessibility")
@Named("smev3")
@Singleton
public class Smev3 {

  public static final String SMEV3_TEST_SERVICE = "http://85.143.127.133:7500/ws";
  public static final String ORGANISATION_REQUEST_KEY = "ORGANISATION_REQUEST_ID";
  public static final String PROVIDER_RESPONSE_KEY = "PROVIDER_RESPONSE_ID";
  public static final String CONSUMER_NAME_KEY = "CONSUMER_NAME";
  public static final String RESPONSE_READY = "isProcessed";
  public static final String SMEV3_SERVICE_VERSION = "1_1";

  private final Logger logger = Logger.getLogger(Smev3.class.getName());

  final Boolean isLogEnabled = true;
  final Boolean isErrorLogEnabled = true;

  @Inject
  AdminService adminService;

  @Inject
  ActivitiService activitiService;

  @SuppressWarnings("unused")
  public void sendRequest(DelegateExecution execution, String consumerName) {
    HaunterLog consumerLog = Preconditions.checkNotNull(LogCustomizer.createHaunterLog(isLogEnabled, isErrorLogEnabled));
    String processInstanceId = Preconditions.checkNotNull(execution.getProcessInstanceId());
    Long bidId = adminService.getBidByProcessInstanceId(processInstanceId).getId();

    consumerLog.addOtherData(MsgDataField.bid, bidId);
    consumerLog.addOtherData(MsgDataField.client, true);
    consumerLog.addOtherData(MsgDataField.processInstanceId, processInstanceId);
    consumerLog.addOtherData(MsgDataField.componentName, consumerName);
    consumerLog.addOtherData(MsgDataField.date, new Date());

    ServiceReference ref = getSmevTransportReference();
    InfoSystem recipient = null;
    InfoSystem sender = null;
    long anticipantId = 0;
    try {
      SmevTransport smevTransport = ProtocolUtils.getService(ref, SmevTransport.class);
      ExchangeContext context = new ActivitiExchangeContext(execution);
      recipient = (InfoSystem) context.getVariable("recipient");
      sender = (InfoSystem) context.getVariable("sender");
      OrganizationRequest request = createOrganizationRequest(context, consumerName);
      anticipantId = adminService.turnConsumerIntoAnticipant(
              new Anticipant(consumerName, bidId, processInstanceId, request.data.messageID));
      execution.setVariable(RESPONSE_READY, false);
      execution.setVariable(CONSUMER_NAME_KEY, consumerName);

      consumerLog.addSendData(MsgDataField.serviceName, consumerName);
      consumerLog.addSendData(MsgDataField.status, Packet.Status.REQUEST.name());
      consumerLog.addSendData(MsgDataField.requestIdRef, request.data.messageID);
      consumerLog.addSendData(MsgDataField.originRequestIdRef, request.data.messageID);
      consumerLog.addSendData(MsgDataField.date, new Date());

      prepareTransport(smevTransport);
      smevTransport.sendRequest(request, consumerLog);
      consumerLog.createLog();
    } catch (Exception e) {
      MessageMetadata messageMetadata = new MessageMetadata();
      messageMetadata.recipient = recipient == null ? new InfoSystem("", "") : recipient;
      messageMetadata.sender = sender == null ? new InfoSystem("", "") : sender;
      messageMetadata.sendTime = new Date();
      consumerLog.addData(messageMetadata);
      consumerLog.createLog();

      if (anticipantId != 0) {
        adminService.eliminateConsumerFromAnticipants(anticipantId);
      }
      throw new RuntimeException(e);
    }
    finally {
      consumerLog.close();
      if (ref != null) {
        Activator.getContext().ungetService(ref);
      }
    }
  }

  @SuppressWarnings("unused")
  public Smev3Result getResponse(final String consumerName) {
    final ServiceReference transportReference = getSmevTransportReference();
    ServiceReference smevReference = Activator.getContext().getServiceReference(
            ru.codeinside.smev.v3.service.api.Smev.class.getName());
    if (smevReference == null) {
      logger.info("Не удалось получить ссылку на сервис Smev");
      return Smev3Result.NONE;
    }
    HaunterLog consumerLog = Preconditions.checkNotNull(LogCustomizer.createHaunterLog(isLogEnabled, isErrorLogEnabled));

    try {
      ru.codeinside.smev.v3.service.api.Smev smev = ProtocolUtils.getService(smevReference, ru.codeinside.smev.v3.service.api.Smev.class);
      SmevTransport smevTransport = ProtocolUtils.getService(transportReference, SmevTransport.class);

      final Consumer consumer = getConsumerByName(consumerName);
      MessageIdentifier messageIdentifier = smev.getResponse(consumer.getInformationType());

      if (messageIdentifier == null) {
        logger.info("Не удалось получить идентификтор сообщений для " + consumerName);
        return Smev3Result.NONE;
      }

      prepareTransport(smevTransport);
      final ResponseMessage response = smevTransport.getResponse(messageIdentifier, consumerLog);

      if (response == null) {
        logger.info("Входящяя очередь ответов для потребителя " + consumerName + " пуста");
        return Smev3Result.RESULT_WASNT_GOT;
      }

      String messageId = response.response.data.providerResponse.data.messageID;
      String originalMessageId = response.response.data.originalMessageId;
      Anticipant anticipant = adminService.findAnticipantByMessageId(originalMessageId);
      if (anticipant == null) {
        adminService.turnResponseIntoUnmatched(response.response.data);
        logger.info("Не удалось сопоставить запрос с ответом " + originalMessageId);
        return Smev3Result.RESULT_WAS_GOT;
      }

      final String processInstanceId = anticipant.getProcessInstanceId();
      final F0<Boolean> getMessageIdFn = new F0<Boolean>() {
        @Override
        public Boolean apply(ProcessEngine processEngine) {
          return ((ServiceImpl) processEngine.getRuntimeService()).getCommandExecutor().execute(
                  new Command<Boolean>() {
                    @Override
                    public Boolean execute(CommandContext commandContext) {
                      DelegateExecution delegateExecution = commandContext.getExecutionManager().findExecutionById(processInstanceId);

                      ProviderResponseData responseData = response.response.data.providerResponse.data;

                      if (responseData.asyncProcessingStatus != null) {
                        switch (responseData.asyncProcessingStatus.statusCategory) {
                          case REQUEST_IS_QUEUED:
                          case REQUEST_IS_ACCEPTED_BY_SMEV:
                          case UNDER_PROCESSING:
                            delegateExecution.setVariable(RESPONSE_READY, false);
                            return false;
                        }
                      }

                      String messageId = consumer.processResponse(response.response, new ActivitiExchangeContext(delegateExecution));
                      if (messageId != null && responseData.requestStatus == null) {
                        delegateExecution.setVariable(RESPONSE_READY, true);
                        return true;
                      }
                      return false;
                    }
                  });
        }
      };

      Boolean isFinished = activitiService.withEngine(getMessageIdFn);

      consumerLog.addOtherData(MsgDataField.client, true);
      consumerLog.addOtherData(MsgDataField.bid, anticipant.getBidId());
      consumerLog.addOtherData(MsgDataField.processInstanceId, processInstanceId);
      consumerLog.addOtherData(MsgDataField.componentName, consumerName);
      consumerLog.addOtherData(MsgDataField.date, new Date());

      consumerLog.addSendData(MsgDataField.serviceName, consumerName);
      consumerLog.addSendData(MsgDataField.status, Packet.Status.RESULT.name());
      consumerLog.addSendData(MsgDataField.requestIdRef, messageId);
      consumerLog.addSendData(MsgDataField.originRequestIdRef, originalMessageId);
      consumerLog.addSendData(MsgDataField.date, new Date());
      consumerLog.addReceiveData(MsgDataField.requestIdRef, messageId);
      consumerLog.addReceiveData(MsgDataField.originRequestIdRef, originalMessageId);

      AcknowledgeRequest acknowledgeRequest = smev.ackRequest(messageId, true);
      smevTransport.ack(acknowledgeRequest);

      if (isFinished){
        adminService.eliminateConsumerFromAnticipants(anticipant.getId());
      }

      consumerLog.createLog();
      return Smev3Result.RESULT_WAS_GOT;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    finally {
      MessageMetadata messageMetadata = new MessageMetadata();
      messageMetadata.recipient = new InfoSystem("", "");
      messageMetadata.sender = new InfoSystem("", "");;
      messageMetadata.sendTime = new Date();
      consumerLog.addData(messageMetadata);
      consumerLog.createLog();
      consumerLog.close();
      Activator.getContext().ungetService(smevReference);
      Activator.getContext().ungetService(transportReference);
    }
  }

  public Smev3Result getRequest(String providerName) {
    HaunterLog providerLog = Preconditions.checkNotNull(LogCustomizer.createHaunterLog(isLogEnabled, isErrorLogEnabled));
    ServiceReference transportReference = getSmevTransportReference();
    ServiceReference smevReference = Activator.getContext().getServiceReference(
            ru.codeinside.smev.v3.service.api.Smev.class.getName());
    if (smevReference == null) {
      logger.info("Не удалось получить ссылку на сервис Smev");
      return Smev3Result.NONE;
    }

    try {
      ru.codeinside.smev.v3.service.api.Smev smev = ProtocolUtils.getService(smevReference, ru.codeinside.smev.v3.service.api.Smev.class);
      SmevTransport smevTransport = ProtocolUtils.getService(transportReference, SmevTransport.class);

      Provider provider = getProviderByName(providerName);
      MessageIdentifier messageIdentifier = smev.getRequest(provider.getInformationType());

      if (messageIdentifier == null) {
        logger.info("Не удалось получить идентификтор сообщений для " + providerName);
        return Smev3Result.NONE;
      }

      prepareTransport(smevTransport);
      RequestMessage response = smevTransport.getRequest(messageIdentifier, providerLog);

      if (response == null) {
        logger.info("Входящяя очередь запросов для поставщика " + providerName + " пуста");
        return Smev3Result.RESULT_WASNT_GOT;
      }

      ActivitiProviderRequestContext requestContext = new ActivitiProviderRequestContext(response, providerName);

      String messageId = requestContext.getRequestMessage().request != null
              ? requestContext.getRequestMessage().request.data.organizationRequest.data.messageID
              : requestContext.getRequestMessage().cancel.messageID;
      try{
        messageId = provider.processRequest(requestContext);
        if (messageId == null) {
          logger.info("Поставщик " + providerName + " не смог обработать запрос");
          return Smev3Result.RESULT_WAS_GOT;
        }
      } catch (ProviderRequestException exception){
        AcknowledgeRequest acknowledgeRequest = smev.ackRequest(messageId, true);
        smevTransport.ack(acknowledgeRequest);

        ProviderResponse providerResponse = createRejectProviderResponse(requestContext.getRequestMessage().request, exception.getRequestRejected());
        smevTransport.sendResponse(providerResponse, providerLog);

        return Smev3Result.RESULT_WAS_GOT;
      }
      final String bidId = requestContext.getBid();
      providerLog.addOtherData(MsgDataField.client, false);
      providerLog.addOtherData(MsgDataField.bid, Long.valueOf(bidId));
      providerLog.addOtherData(MsgDataField.processInstanceId, adminService.getBid(bidId).getProcessInstanceId());
      providerLog.addOtherData(MsgDataField.componentName, providerName);
      providerLog.addOtherData(MsgDataField.date, new Date());
      providerLog.addSendData(MsgDataField.requestIdRef, messageId);
      providerLog.addSendData(MsgDataField.originRequestIdRef, response.request.data.replyTo);
      providerLog.addSendData(MsgDataField.serviceName, providerName);
      providerLog.addSendData(MsgDataField.status, Packet.Status.REQUEST.name());
      providerLog.addSendData(MsgDataField.date, new Date());
      providerLog.addReceiveData(MsgDataField.originRequestIdRef, response.request.data.replyTo);
      providerLog.addReceiveData(MsgDataField.requestIdRef, messageId);

      AcknowledgeRequest acknowledgeRequest = smev.ackRequest(messageId, true);
      smevTransport.ack(acknowledgeRequest);

      providerLog.createLog();
      return Smev3Result.RESULT_WAS_GOT;
    } catch (Exception e) {
      MessageMetadata messageMetadata = new MessageMetadata();
      messageMetadata.recipient = new InfoSystem("", "");
      messageMetadata.sender = new InfoSystem("", "");
      messageMetadata.sendTime = new Date();
      providerLog.addData(messageMetadata);
      providerLog.createLog();
      throw new RuntimeException(e);
    }
    finally {
        providerLog.close();
        Activator.getContext().ungetService(smevReference);
        Activator.getContext().ungetService(transportReference);
    }
  }

  @SuppressWarnings("unused")
  public void sendResponse(DelegateExecution execution, String providerName) {
    HaunterLog providerLog = Preconditions.checkNotNull(LogCustomizer.createHaunterLog(isLogEnabled, isErrorLogEnabled));
    ServiceReference ref = getSmevTransportReference();
    String processInstanceId = execution.getProcessInstanceId();
    InfoSystem recipient = null;
    InfoSystem sender = null;
    try {
      SmevTransport smevTransport = ProtocolUtils.getService(ref, SmevTransport.class);
      Long bidId = adminService.getBidByProcessInstanceId(processInstanceId).getId();
      ActivitiReceiptContext context = new ActivitiReceiptContext(execution, bidId);
      recipient = (InfoSystem) context.getVariable("recipient");
      sender = (InfoSystem) context.getVariable("sender");
      providerLog.addOtherData(MsgDataField.bid, bidId);
      providerLog.addOtherData(MsgDataField.client, false);
      providerLog.addOtherData(MsgDataField.processInstanceId, processInstanceId);
      providerLog.addOtherData(MsgDataField.componentName, providerName);
      providerLog.addOtherData(MsgDataField.date, new Date());
      Provider provider = getProviderByName(providerName);

      ProviderResponse response = createProviderResponse(context, processInstanceId, provider);
      providerLog.addSendData(MsgDataField.serviceName, providerName);
      providerLog.addSendData(MsgDataField.status, Packet.Status.RESULT.name());
      providerLog.addSendData(MsgDataField.requestIdRef, response.data.messageID);
      providerLog.addSendData(MsgDataField.originRequestIdRef, response.data.to);
      providerLog.addSendData(MsgDataField.date, new Date());

      prepareTransport(smevTransport);
      smevTransport.sendResponse(response, providerLog);

      providerLog.createLog();
    } catch (Exception e) {
      MessageMetadata messageMetadata = new MessageMetadata();
      messageMetadata.recipient = recipient;
      messageMetadata.sender = sender;
      messageMetadata.sendTime = new Date();
      providerLog.addData(messageMetadata);
      providerLog.createLog();
      throw new RuntimeException(e);
    } finally {
      providerLog.close();
      Activator.getContext().ungetService(ref);
    }
  }

  private Consumer getConsumerByName(String consumerName) {
    TRef<Consumer> consumerRef = TRefProvider.getConsumerRefRegistry().getConsumerByName(consumerName);
    if (consumerRef == null) {
      throw new IllegalStateException("Не удалось получить ссылку на сервис потребителя " + consumerName);
    }

    Consumer consumer = consumerRef.getRef();

    if (consumer == null) {
      throw new IllegalStateException("Потребитель " + consumerName + " не найден");
    }

    return consumer;
  }

  private Provider getProviderByName(String providerName) {
    TRef<Provider> providerRef = TRefProvider.getProviderRefRegistry().getProviderByName(providerName);
    if (providerRef == null) {
      throw new IllegalStateException("Не удалось получить ссылку на сервис поставщика " + providerName);
    }

    Provider provider = providerRef.getRef();

    if (provider == null) {
      throw new IllegalStateException("Поставщик " + providerName + " не найден");
    }

    return provider;
  }

  private OrganizationRequest createOrganizationRequest(ExchangeContext context, String consumerName) {
    Long requestEntityId;
    requestEntityId = (Long) context.getVariable(ORGANISATION_REQUEST_KEY);
    if (requestEntityId == null) {
      if (consumerName == null || consumerName.isEmpty()) {
        throw new IllegalStateException("Отсутствует имя потребителя");
      }
      return createRequest(consumerName, context);
    }

    OrganizationRequestEntity requestEntity = AdminServiceProvider.get().getOrganizationRequestEntity(requestEntityId);
    if (requestEntity == null) {
      throw new IllegalStateException("Не найден запрос пользователя");
    }

    return requestEntity.createOrganizationRequest(context);
  }

  private OrganizationRequest createRequest(String consumerName, ExchangeContext context){
    Consumer consumer = getConsumerByName(consumerName);
    ConsumerRequest consumerRequest = consumer.createRequest(context);
    ServiceReference requestBuilderReference = Activator.getContext().getServiceReference(ConsumerRequestBuilder.class.getName());
    try {
      ConsumerRequestBuilder requestBuilder = (ConsumerRequestBuilder) Activator.getContext().getService(requestBuilderReference);
      OrganizationRequestData organizationRequestData = requestBuilder.createPersonalRequest(consumerRequest);
      boolean prod = AdminServiceProvider.getBoolProperty(API.PRODUCTION_MODE);
      return requestBuilder.createOrganizationRequest(organizationRequestData, !prod);
    } finally {
      if (requestBuilderReference != null){
        Activator.getContext().ungetService(requestBuilderReference);
      }
    }
  }

  private ProviderResponse createRejectProviderResponse(Request request, RequestRejected requestRejected){
    final ServiceReference responseBuilderReference = Activator.getContext().getServiceReference(ProviderResponseBuilder.class.getName());
    if (responseBuilderReference == null) {
      throw new IllegalStateException("Сервис формирования запросов поставщика недоступен.");
    }
    try {
      final Object responseBuilderPossibleNull = Activator.getContext().getService(responseBuilderReference);
      if (responseBuilderPossibleNull == null) {
        throw new IllegalStateException("Не удалось получить сервис построения ответа.");
      } else {
        final ProviderResponseBuilder responseBuilder = (ProviderResponseBuilder) responseBuilderPossibleNull;
        ProviderResponseData providerResponseData = new ProviderResponseData();
        providerResponseData.to = request.data.replyTo;
        providerResponseData.messageID = request.data.organizationRequest.data.messageID;
        providerResponseData.id = request.data.id;
        providerResponseData.requestRejected = new ArrayList<RequestRejected>();
        providerResponseData.requestRejected.add(requestRejected);
        return responseBuilder.createProviderResponse(providerResponseData);
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    } finally {
      Activator.getContext().ungetService(responseBuilderReference);
    }
  }

  private ProviderResponse createProviderResponse(final ReceiptContext context, final String processInstanceId, final Provider provider) {
    final Object responseEntityId = context.getVariableByFullName(PROVIDER_RESPONSE_KEY);
    if (responseEntityId != null) {
      final ProviderResponseEntity providerResponseEntity = AdminServiceProvider.get().getProviderResponseEntity((Long)responseEntityId);
      if (providerResponseEntity == null) {
        throw new IllegalStateException("Запрашиваемый объект не найден в БД.");
      } else {
        return providerResponseEntity.createProviderResponse(processInstanceId);
      }
    } else {
      final ServiceReference responseBuilderReference = Activator.getContext().getServiceReference(ProviderResponseBuilder.class.getName());
      if (responseBuilderReference == null) {
        throw new IllegalStateException("Сервис формирования запросов поставщика недоступен.");
      }
      try {
        final Object responseBuilderPossibleNull = Activator.getContext().getService(responseBuilderReference);
        if (responseBuilderPossibleNull == null) {
          throw new IllegalStateException("Не удалось получить сервис построения ответа.");
        } else {
          final ProviderResponseBuilder responseBuilder = (ProviderResponseBuilder) responseBuilderPossibleNull;
          final ProvidedResponse providedResponse = provider.createResponse(context);
          final ProviderResponseData providerResponseData = responseBuilder.createPersonalResponse(providedResponse);
          return responseBuilder.createProviderResponse(providerResponseData);
        }
      } catch (Exception e) {
        throw new RuntimeException(e);
      } finally {
        Activator.getContext().ungetService(responseBuilderReference);
      }
    }
  }

  private ServiceReference getSmevTransportReference() {
    String filter = "(component.name=smev-transport-" + SMEV3_SERVICE_VERSION + ")";
    ServiceReference[] refs = null;
    try {
      refs = Activator.getContext().getServiceReferences(SmevTransport.class.getName(), filter);
    } catch (InvalidSyntaxException ignored) { }
    if (refs == null || refs.length == 0) {
      throw new IllegalStateException("Не удалось получить ссылку на сервис SmevTransport версии " +
              SMEV3_SERVICE_VERSION);
    }
    return refs[0];
  }

  /**
   *  Вызывать перед каждым обращением к СМЭВ3
   */
  private void prepareTransport(SmevTransport transport) {
    boolean prod = Boolean.parseBoolean(adminService.getSystemProperty(API.PRODUCTION_MODE));
    String url;
    if (!prod) {
      url = adminService.getSystemProperty(API.SMEV3_TEST_SERVICE);
      url = StringUtils.defaultIfEmpty(url, SMEV3_TEST_SERVICE);
    } else {
      url = adminService.getSystemProperty(API.SMEV3_PROD_SERVICE);
    }
    transport.prepare(url, !prod);
  }
}
