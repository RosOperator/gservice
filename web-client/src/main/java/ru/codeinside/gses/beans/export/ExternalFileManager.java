package ru.codeinside.gses.beans.export;

import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.gses.API;

import javax.activation.MimetypesFileTypeMap;
import java.io.*;
import java.security.MessageDigest;
import java.util.Calendar;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

final public class ExternalFileManager implements ru.codeinside.gws.api.ExternalFileManager {

    @Override
    public String putFile(ExternalFile externalFile) {
        String fileDestination = AdminServiceProvider.get().getSystemProperty(API.EXTERNAL_FILE_PATH);
        try {
            if (fileDestination == null || fileDestination.isEmpty()) {
                return null;
            }
            String externalLink = generateExternalLink();
            File createdFile = new File(fileDestination.replaceAll("/$", "") + File.separator + externalLink);
            createdFile.getParentFile().mkdirs();
            if (!createdFile.createNewFile()) {
                return null;
            }
            ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(createdFile)));
            try {
                ZipEntry entry = new ZipEntry(externalFile.name);
                zos.putNextEntry(entry);
                zos.write(externalFile.content);
                zos.closeEntry();
                return externalLink;
            } finally {
                zos.close();
            }
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public ExternalFile getFile(String externalLink) {
        String fileDestination = AdminServiceProvider.get().getSystemProperty(API.EXTERNAL_FILE_PATH);
        try {
            if (fileDestination == null || fileDestination.isEmpty()) {
                return null;
            }
            File savedFile = new File(fileDestination.replaceAll("/$", "") + File.separator + externalLink);
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(savedFile)));
            try {
                ZipEntry ze;
                while ((ze = zis.getNextEntry()) != null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    int count;
                    while ((count = zis.read(buffer)) != -1) {
                        baos.write(buffer, 0, count);
                    }
                    String filename = ze.getName();
                    byte[] bytes = baos.toByteArray();
                    String mimeType = MimetypesFileTypeMap.getDefaultFileTypeMap().getContentType(filename);
                    return new ExternalFile(mimeType, filename, bytes);
                }
            } finally {
                zis.close();
            }
        } catch (IOException e) {
            return null;
        }
        return null;
    }

    @Override
    public void deleteFile(String externalLink) {
        String fileDestination = AdminServiceProvider.get().getSystemProperty(API.EXTERNAL_FILE_PATH);
        if (fileDestination != null && !fileDestination.isEmpty()) {
            File savedFile = new File(fileDestination.replaceAll("/$", "") + File.separator + externalLink);
            if (savedFile.exists()) {
                savedFile.delete();
            }
        }
    }

    @Override
    public String getFileChecksum(String externalLink) {
        String fileDestination = AdminServiceProvider.get().getSystemProperty(API.EXTERNAL_FILE_PATH);
        if (fileDestination != null && !fileDestination.isEmpty()) {
            File savedFile = new File(fileDestination.replaceAll("/$", "") + File.separator + externalLink);
            if (savedFile.exists()) {
                return getSha1Checksum(savedFile);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private String getSha1Checksum(File file) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA1");
            FileInputStream fis = new FileInputStream(file);

            byte[] data = new byte[1024];
            int read = 0;
            while ((read = fis.read(data)) != -1) {
                sha1.update(data, 0, read);
            };
            byte[] hashBytes = sha1.digest();

            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < hashBytes.length; i++) {
                sb.append(Integer.toString((hashBytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            fis.close();
            return sb.toString();

        } catch (Exception e) {
            return null;
        }
    }

    private String generateExternalLink() {
        synchronized (ExternalFileManager.class) {
            Calendar now = Calendar.getInstance();
            int year = now.get(Calendar.YEAR);
            int month = now.get(Calendar.MONTH) + 1;
            return String.format("%d/%d/%s.zip", year, month, UUID.randomUUID());
        }
    }
}