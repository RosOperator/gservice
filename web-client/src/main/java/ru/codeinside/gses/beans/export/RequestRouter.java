package ru.codeinside.gses.beans.export;

import ru.codeinside.adm.AdminServiceProvider;

import java.util.Set;

final public class RequestRouter implements ru.codeinside.gws.api.RequestRouter {

    private final String DEFAULT_OKTMO = "78000000";

    @Override
    public String getRoutedOktmo(String prefix, String oktmo) {
        return isNeedCheck(oktmo) ? findRoutedOktmo(AdminServiceProvider.get().getEmpGroupNames(), prefix, normalizeOktmo(oktmo, 8)) : DEFAULT_OKTMO;
    }

    private boolean isNeedCheck(String oktmo) {
        return oktmo != null && oktmo.length() > 2;
    }

    private String normalizeOktmo(String oktmo, int length) {
        if (oktmo.length() > length) {
            return oktmo.substring(0, length);
        } else {
            return oktmo + new String(new char[length - oktmo.length()]).replace('\0', '0');
        }
    }

    private String findRoutedOktmo(Set<String> groups, String groupPrefix, String oktmo) {
        if (groups.contains(groupPrefix + oktmo)) {
            return oktmo;
        } else {
            return oktmo.endsWith("000") ? DEFAULT_OKTMO : findRoutedOktmo(groups, groupPrefix, oktmo.substring(0, 5) + "000");
        }
    }
}