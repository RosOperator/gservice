package ru.codeinside.gses.beans.filevalues;


import commons.Exceptions;
import ru.codeinside.adm.AdminService;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.adm.database.Bid;
import ru.codeinside.adm.database.ProcedureProcessDefinition;
import ru.codeinside.adm.database.SmevChain;
import ru.codeinside.gses.beans.ActivitiDeclarerContext;
import ru.codeinside.gses.service.DeclarantService;
import ru.codeinside.gses.service.DeclarantServiceProvider;
import ru.codeinside.gses.webui.Configurator;
import ru.codeinside.gws.api.DeclarerContext;
import ru.codeinside.gws.api.ServerException;

import javax.ejb.EJBException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@SuppressWarnings("PackageAccessibility")
public abstract class BaseRequestContext {

    protected final String componentName;
    protected final SmevChain smevChain;
    protected final AtomicLong gid;

    public BaseRequestContext(String componentName, SmevChain smevChain) {
        this.componentName = componentName;
        this.smevChain = smevChain;
        long id = declarantService().getGlueIdByRequestIdRef(smevChain.originRequestIdRef);
        gid = new AtomicLong(id);
    }

    protected List<String> getBids() {
        long activeGid = gid.get();
        if (activeGid == 0L) {
            return Collections.emptyList();
        }
        try {
            return declarantService().getBids(activeGid);
        } catch (EJBException e) {
            throw Exceptions.convertToApi(e);
        }
    }

    protected DeclarerContext getDeclarerContext(long procedureCode) {
        try {
            ProcedureProcessDefinition active = adminService().getProcedureProcessDefinitionByProcedureCode(procedureCode);
            if (active == null) {
                throw new ServerException("Не найдено процедруы с кодом '" + procedureCode + "'");
            }
            return new ActivitiDeclarerContext(smevChain, gid, active.getProcessDefinitionId(), componentName);
        } catch (EJBException e) {
            throw Exceptions.convertToApi(e);
        }
    }

    protected String getBid() {
        long activeGid = gid.get();
        if (activeGid == 0L) {
            return null;
        }
        return Long.toString(activeGid);
    }

    protected void updateReceiptContext(Map<String, Object> values) {
        if (values == null) {
            return;
        }

        Bid bid = adminService().getBid(gid.toString());
        if (bid == null) {
            return;
        }

        declarantService().updateContext(Configurator.get(), bid.getProcessInstanceId(), values);
    }

    // Internals

    protected DeclarantService declarantService() {
        return DeclarantServiceProvider.forApi();
    }

    protected AdminService adminService() {
        return AdminServiceProvider.forApi();
    }
}
