
package ru.codeinside.gses.manager.printform;

import com.vaadin.Application;
import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.terminal.DownloadStream;
import com.vaadin.terminal.StreamResource;
import com.vaadin.terminal.StreamResource.StreamSource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Window;
import ru.codeinside.adm.database.Employee;
import ru.codeinside.adm.database.ProcedurePrintForm;
import ru.codeinside.adm.ui.LazyLoadingContainer2;
import ru.codeinside.gses.manager.ManagerService;
import ru.codeinside.gses.webui.components.ContentWindowChanger;
import ru.codeinside.gses.webui.components.PrintFormShowUi;
import ru.codeinside.gses.webui.containers.LazyLoadingContainer;
import ru.codeinside.gses.webui.containers.LazyLoadingQuery;
import ru.codeinside.gses.webui.utils.Components;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class PrintFormQuery implements LazyLoadingQuery {

    private static final long serialVersionUID = 1L;

    final SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
    DecimalFormat df = new DecimalFormat("##.00");

    String[] sortProps = {};
    boolean[] sortAsc = {};

    private final String procedureId;
    private final LazyLoadingContainer2 proceduresContainer;

    public PrintFormQuery(String procedureId, LazyLoadingContainer2 proceduresContainer) {
        this.procedureId = procedureId;
        this.proceduresContainer = proceduresContainer;
    }

    @Override
    public int size() {
        return ManagerService.get().getPrintFormCountByProcedureId(procedureId);
    }

    @Override
    public List<Item> loadItems(int start, int count) {
        ArrayList<Item> items = new ArrayList<Item>();
        for (ProcedurePrintForm procedure : ManagerService.get().getPrintFormsByProcedureId(
                procedureId, start, count, sortProps, sortAsc)) {
            items.add(createItem(procedure));
        }
        return items;
    }

    @Override
    public Item loadSingleResult(String id) {
        final ProcedurePrintForm procedure = ManagerService.get().getPrintForm(id);
        return createItem(procedure);
    }

    PropertysetItem createItem(final ProcedurePrintForm p) {
        PropertysetItem item = new PropertysetItem();

        ClickListener listener = new ClickListener() {

            private static final long serialVersionUID = -8900212370037948964L;

            @Override
            public void buttonClick(ClickEvent event) {
                Window mainWin = event.getButton().getApplication().getMainWindow();

                String caption = "Версия " + df.format(p.getVersion());
                Window win = Components.createWindow(mainWin, caption);
                win.center();
                ContentWindowChanger changer = new ContentWindowChanger(win);
                PrintFormShowUi putComponent = new PrintFormShowUi(p, changer);
                changer.set(putComponent, caption);
            }

        };
        ObjectProperty<Component> versionProperty = Components.buttonProperty(df.format(p.getVersion()), listener);
        item.addItemProperty("version", versionProperty);

        item.addItemProperty("date", Components.stringProperty(formatter.format(p.getDateCreated())));
        Employee creator = p.getCreator();
        item.addItemProperty("user", Components.stringProperty(creator==null? null: creator.getLogin()));

        Button b = new Button("Выгрузить");

        b.addListener(new ClickListener() {

            private static final long serialVersionUID = 1362078893395574138L;

            @Override
            public void buttonClick(ClickEvent event) {
                StreamSource streamSource = new StreamSource() {

                    private static final long serialVersionUID = 456334952891565471L;

                    public InputStream getStream() {
                        return new ByteArrayInputStream(p.getBytes().getBytes());
                    }
                };
                final Application application = event.getButton().getApplication();
                StreamResource resource = new StreamResource(streamSource, p.getFileName(), application) {
                    private static final long serialVersionUID = -3869546661105572851L;
                    public DownloadStream getStream() {
                        final StreamSource ss = getStreamSource();
                        if (ss == null) {
                            return null;
                        }
                        final DownloadStream ds = new DownloadStream(ss.getStream(), getMIMEType(), getFilename());
                        ds.setBufferSize(getBufferSize());
                        ds.setCacheTime(getCacheTime());
                        ds.setParameter("Content-Disposition", "attachment; filename=" + getFilename());
                        return ds;
                    }
                };
                Window window = event.getButton().getWindow();
                window.open(resource);
            }
        });
        item.addItemProperty("getPrintForm", new ObjectProperty<Component>(b));
        return item;
    }

    private static void closeWindow(final Window thisWindow, final Window window) {
        thisWindow.removeWindow(window);
        if (thisWindow.getParent() != null) {
            thisWindow.getParent().removeWindow(window);
        }
    }

    private LazyLoadingContainer2 paramLazyLoadingContainer;

    @Override
    public void setLazyLoadingContainer(LazyLoadingContainer paramLazyLoadingContainer) {
        this.paramLazyLoadingContainer = (LazyLoadingContainer2) paramLazyLoadingContainer;
    }

    @Override
    public void setSorting(Object[] propertyIds, boolean[] ascending) {
        String[] props = new String[propertyIds.length];
        for (int i = 0; i < propertyIds.length; i++) {
            props[i] = propertyIds[i].toString();
        }
        sortProps = props;
        sortAsc = ascending;
    }

}
