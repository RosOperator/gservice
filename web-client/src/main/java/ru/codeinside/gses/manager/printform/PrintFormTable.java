

package ru.codeinside.gses.manager.printform;

import com.vaadin.data.Container;
import com.vaadin.data.Container.ItemSetChangeEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import ru.codeinside.adm.ui.LazyLoadingContainer2;

public class PrintFormTable extends VerticalLayout {

	private static final long serialVersionUID = -3060552897820352218L;

	private static final String[] NAMES = new String[] { "Версия", "Изменен", "Пользователь", " "};

	private LazyLoadingContainer2 container;

	public LazyLoadingContainer2 getTableContainer(){
		return container;
	}

	public PrintFormTable(String procedureId, LazyLoadingContainer2 proceduresContainer) {
		final Table listAp = new Table();
		Container.ItemSetChangeListener listener = new Container.ItemSetChangeListener() {
						
			private static final long serialVersionUID = 1391138329800139622L;

			@Override
			public void containerItemSetChange(ItemSetChangeEvent event) {
				listAp.refreshRowCache();				
			}
		};
		LazyLoadingContainer2 newDataSource = new LazyLoadingContainer2(new PrintFormQuery(procedureId, proceduresContainer));
		newDataSource.addListener(listener);
		container = newDataSource;
		
		listAp.setContainerDataSource(newDataSource);
		listAp.addContainerProperty("version", Component.class, "");
		listAp.addContainerProperty("date", String.class, "");
		listAp.addContainerProperty("user", String.class, "");
		listAp.addContainerProperty("getPrintForm", Component.class, "");
		listAp.sort(new String[]{"version"}, new boolean[]{false});
		listAp.setHeight("220px");
		listAp.setWidth("100%");
		listAp.setSortDisabled(true);
		listAp.setColumnHeaders(NAMES);
		listAp.setColumnExpandRatio("version", 0.08f);
		listAp.setColumnExpandRatio("date", 0.1f);
		listAp.setColumnExpandRatio("user", 0.1f);
        listAp.setColumnExpandRatio("getPrintForm", 0.1f);

		addComponent(listAp);
	}

}
