/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2013, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.gses.webui.actions.deployment;

import com.google.common.collect.Lists;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.Notification;
import ru.codeinside.adm.ui.LazyLoadingContainer2;
import ru.codeinside.gses.manager.ManagerService;

import java.util.List;

public class JasperDeploymentSucceededListener implements com.vaadin.ui.Upload.SucceededListener {

    private static final long serialVersionUID = 8870696618119836644L;
    private final DeploymentUploadReceiver receiver;
    private final String procedureId;
    private List<LazyLoadingContainer2> loadingContainers = Lists.newArrayList();

    public void addLoadingContainer(LazyLoadingContainer2 loadingContainer) {
        this.loadingContainers.add(loadingContainer);
    }

    public JasperDeploymentSucceededListener(DeploymentUploadReceiver receiver, String procedureId) {
        this.receiver = receiver;
        this.procedureId = procedureId;
    }

    @Override
    public void uploadSucceeded(SucceededEvent event) {

        String user = event.getUpload().getApplication().getUser().toString();
        ManagerService.get().createPrintForm(receiver.fileName, receiver.getBytes(), user, procedureId);

        event.getUpload().setData(null);
        for (LazyLoadingContainer2 loadingContainer : loadingContainers) {
            loadingContainer.fireItemSetChange();
        }
        showMessage(event,
                "Новая версия",
                "Загружен шаблон печатной формы",
                Notification.TYPE_HUMANIZED_MESSAGE);
    }

    private void showMessage(SucceededEvent event, String title, String message, int type) {
        Window window = currentWindow(event);
        if (window != null) {
            window.showNotification(title, message, type);
        }
    }

    private Window currentWindow(SucceededEvent event) {
        return event.getUpload().getWindow();
    }

}