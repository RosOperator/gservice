package ru.codeinside.gses.webui.components;

import com.vaadin.terminal.Sizeable;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.Reindeer;
import ru.codeinside.adm.database.ProcedurePrintForm;
import ru.codeinside.gses.webui.components.api.Changer;

import java.io.UnsupportedEncodingException;

public class PrintFormShowUi extends CustomComponent {

    private static final long serialVersionUID = -2168823678756493358L;
    private final Changer changer;
    private final ProcedurePrintForm procedurePrintForm;


    public PrintFormShowUi(ProcedurePrintForm procedurePrintForm, Changer changer) {
        this.changer = changer;
        this.procedurePrintForm = procedurePrintForm;
        setCompositionRoot(buildMainLayout());
        setWidth(1100, Sizeable.UNITS_PIXELS);
        setHeight(600, Sizeable.UNITS_PIXELS);
    }

    private Component buildMainLayout() {
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();
        layout.setSpacing(true);
        layout.setMargin(true);

        Label label = new Label();
        String name = this.procedurePrintForm.getFileName();
        label.setCaption(name);
        label.setStyleName(Reindeer.LABEL_H2);

        Table table = new Table();
        table.setSizeFull();
        table.setImmediate(true);
        table.setSelectable(true);
        table.setSortDisabled(true);
        table.setPageLength(0);
        table.setSelectable(false);
        table.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);
        table.addContainerProperty("printFormContent", String.class, null);
        String jasper;
        try {
            jasper = new String(this.procedurePrintForm.getBytes().getBytes(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            jasper = "";
        }
        table.addItem(new Object[]{jasper}, 1);

        layout.addComponent(label);
        layout.setExpandRatio(label, 1);

        layout.addComponent(table);
        layout.setExpandRatio(table, 40);

        return layout;
    }
}
