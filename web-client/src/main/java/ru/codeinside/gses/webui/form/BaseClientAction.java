package ru.codeinside.gses.webui.form;

import ru.codeinside.gses.webui.gws.TRef;
import ru.codeinside.gses.webui.gws.TRefProvider;
import ru.codeinside.gses.webui.wizard.ResultTransition;
import ru.codeinside.gses.webui.wizard.TransitionAction;
import ru.codeinside.gws.api.Client;
import ru.codeinside.smev.v3.service.api.consumer.Consumer;

public abstract class BaseClientAction implements TransitionAction {

  protected final DataAccumulator accumulator;

  public BaseClientAction(DataAccumulator accumulator) {
    this.accumulator = accumulator;
  }

  @Override
  public ResultTransition doIt() throws IllegalStateException {
    try {
      final String serviceName = accumulator.getServiceName();

      TRef<Client> clientRef = TRefProvider.getClientRefRegistry().getClientByName(serviceName);
      TRef<Consumer> consumerRef;
      if (clientRef == null) {
        consumerRef = TRefProvider.getConsumerRefRegistry().getConsumerByName(serviceName);
        if (consumerRef == null) {
          throw new IllegalStateException("Клиент " + serviceName + " недоступен");
        } else {
          accumulator.setSmev3(true);
          return smev3request(consumerRef.getRef());
        }
      }

      accumulator.setSmev3(false);
      return smev2request(clientRef.getRef());
    } catch (Exception e) {
      throw new IllegalStateException("Ошибка получения подготовительных данных: " +  e.toString(), e);
    }
  }

  protected abstract ResultTransition smev2request(Client client);

  protected abstract ResultTransition smev3request(Consumer consumer);
}
