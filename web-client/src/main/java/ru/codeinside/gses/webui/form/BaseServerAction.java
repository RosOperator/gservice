package ru.codeinside.gses.webui.form;

import ru.codeinside.gses.webui.gws.TRef;
import ru.codeinside.gses.webui.gws.TRefProvider;
import ru.codeinside.gses.webui.wizard.ResultTransition;
import ru.codeinside.gses.webui.wizard.TransitionAction;
import ru.codeinside.gws.api.Server;
import ru.codeinside.smev.v3.service.api.provider.Provider;

public abstract class BaseServerAction implements TransitionAction {
  protected final DataAccumulator accumulator;

  public BaseServerAction(DataAccumulator accumulator) {
    this.accumulator = accumulator;
  }

  @Override
  public ResultTransition doIt() throws IllegalStateException {
    if (accumulator.getTaskId() == null) {
      throw new IllegalStateException("Отсутствует taskId");
    }
    String serverName = ProtocolUtils.getServerName(accumulator.getTaskId());
    TRef<Server> serviceRef = TRefProvider.getServiceRefRegistry().getServerByName(serverName);

    TRef<Provider> providerRef;
    if (serviceRef == null) {
      providerRef = TRefProvider.getProviderRefRegistry().getProviderByName(serverName);
      if (providerRef == null) {
        throw new IllegalStateException("Поставщик " + serverName + " недоступен");
      } else {
        accumulator.setSmev3(true);
        return smev3response(providerRef.getRef());
      }
    }
    accumulator.setSmev3(false);
    return smev2response(serviceRef.getRef());
  }

  protected abstract ResultTransition smev2response(Server server);

  protected abstract ResultTransition smev3response(Provider provider);
}
