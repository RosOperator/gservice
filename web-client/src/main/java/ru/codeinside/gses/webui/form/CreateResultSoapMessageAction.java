package ru.codeinside.gses.webui.form;

import org.osgi.framework.ServiceReference;
import ru.codeinside.gses.service.Fn;
import ru.codeinside.gses.webui.osgi.Activator;
import ru.codeinside.gses.webui.wizard.ResultTransition;
import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.gws.api.Server;
import ru.codeinside.gws.api.ServerProtocol;
import ru.codeinside.gws.api.ServerResponse;
import ru.codeinside.gws.api.ServiceDefinition;
import ru.codeinside.smev.v3.service.api.provider.Provider;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponse;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseBuilder;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseData;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.List;

public class CreateResultSoapMessageAction extends BaseServerAction {

  public CreateResultSoapMessageAction(DataAccumulator accumulator) {
    super(accumulator);
  }

  @Override
  protected ResultTransition smev2response(Server server) {
    try {

      //если отсутствовал шаг подписания AppData, дёргаем поставщика и получаем response
      if (accumulator.getServerResponse() == null) {
        serverResponse(server);
      }

      validateState();

      List<Object> serviceInfo = ProtocolUtils.getQNameAndServicePort(server);
      QName qName = (QName) serviceInfo.get(0);
      ServiceDefinition.Port port = (ServiceDefinition.Port) serviceInfo.get(1);

      ServerProtocol serverProtocol = ProtocolUtils.getServerProtocol(server);
      ByteArrayOutputStream normalizedSignedInfo = new ByteArrayOutputStream();

      SOAPMessage message = serverProtocol.createMessage(
          accumulator.getServerResponse(),
          qName, //Service
          port,  //Port
          null,  //Log
          normalizedSignedInfo
      );

      accumulator.setSoapMessage(message);

      return new ResultTransition(new SignData(normalizedSignedInfo.toByteArray(), Collections.<Enclosure>emptyList()));
    } catch (RuntimeException e) {
      e.printStackTrace();
      throw new IllegalStateException("Ошибка получения подготовительных данных: " + e.getMessage(), e);
    }
  }

  @Override
  protected ResultTransition smev3response(Provider provider) {
    if (accumulator.getProviderResponseData() == null) {
      ProviderResponseData providerResponseData = Fn.withEngine(new GetRequestAppDataAction.GetProviderResponse(),  accumulator, provider);
      accumulator.setProviderResponseData(providerResponseData);
    }

    ServiceReference responseBuilderReference = Activator.getContext().getServiceReference(ProviderResponseBuilder.class.getName());
    if (responseBuilderReference == null) {
      throw new IllegalStateException("Сервис формирования запросов поставщика недоступен.");
    }
    try {
      ProviderResponseBuilder responseBuilder = (ProviderResponseBuilder) Activator.getContext().getService(responseBuilderReference);
      if (responseBuilder == null) {
        throw new IllegalStateException("Не удалось получить сервис построения ответа.");
      }
      ProviderResponse providerResponse = responseBuilder.createProviderResponse(accumulator.getProviderResponseData());
      accumulator.setProviderResponse(providerResponse);
      return new ResultTransition(new SignData(providerResponse.signData, providerResponse.data.enclosures));
    } finally {
      Activator.getContext().ungetService(responseBuilderReference);
    }
  }

  private void validateState() {
    if (accumulator.getServerResponse() == null) {
      throw new IllegalStateException("Отсутствуют данные сервиса.");
    }
  }

  private void serverResponse(Server server) {
    ServerResponse response = Fn.withEngine(new GetRequestAppDataAction.GetServerResponse(), accumulator, server);
    accumulator.setServerResponse(response);
  }
}
