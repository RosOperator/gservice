package ru.codeinside.gses.webui.form;

import org.osgi.framework.ServiceReference;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.gses.API;
import ru.codeinside.gses.service.Fn;
import ru.codeinside.gses.webui.osgi.Activator;
import ru.codeinside.gses.webui.wizard.ResultTransition;
import ru.codeinside.gws.api.Client;
import ru.codeinside.gws.api.ClientProtocol;
import ru.codeinside.gws.api.ClientRequest;
import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.smev.v3.service.api.consumer.Consumer;
import ru.codeinside.smev.v3.service.api.consumer.ConsumerRequestBuilder;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequest;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequestData;

import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayOutputStream;
import java.util.Collections;

/**
 * Действие для получения SOAP-сообещения
 */
@SuppressWarnings("PackageAccessibility")
public class CreateSoapMessageAction extends BaseClientAction {

  public CreateSoapMessageAction(DataAccumulator accumulator) {
    super(accumulator);
  }

  @Override
  protected ResultTransition smev2request(Client client) {
    //если отсутствовал шаг подписания AppData, дёргаем потребителя и получаем request
    if (accumulator.getClientRequest() == null) {
      clientRequest(client);
    }

    validateState();

    ClientProtocol clientProtocol = ProtocolUtils.getClientProtocol(client);
    ByteArrayOutputStream normalizedSignedInfo = new ByteArrayOutputStream();

    SOAPMessage message = clientProtocol.createMessage(client.getWsdlUrl(),
        accumulator.getClientRequest(), null, normalizedSignedInfo);
    accumulator.setSoapMessage(message);

    return new ResultTransition(new SignData(normalizedSignedInfo.toByteArray(), Collections.<Enclosure>emptyList()));
  }

  private void clientRequest(Client client) {
    ClientRequest request = Fn.withEngine(new GetAppDataAction.GetClientRequest(), client, accumulator);
    accumulator.setClientRequest(request);
  }

  private void validateState() {
    if (accumulator.getClientRequest() == null) {
      throw new IllegalStateException("Отсутствуют данные клиента.");
    }
  }

  @Override
  protected ResultTransition smev3request(Consumer consumer) {
    if (accumulator.getOrganizationRequestData() == null) {
      OrganizationRequestData organizationRequestData = Fn.withEngine(new GetAppDataAction.GetConsumerRequest(), consumer, accumulator);
      accumulator.setOrganizationRequestData(organizationRequestData);
    }

    ServiceReference requestBuilderReference = Activator.getContext().getServiceReference(ConsumerRequestBuilder.class.getName());
    if (requestBuilderReference == null) {
      throw new IllegalStateException("Севрис формирования запросов потребителя недоступен.");
    }

    try {
      ConsumerRequestBuilder requestBuilder = (ConsumerRequestBuilder) Activator.getContext().getService(requestBuilderReference);
      if (requestBuilder == null) {
        throw new IllegalStateException("Не удалось получить сервис построения запроса.");
      }

      boolean prod = AdminServiceProvider.getBoolProperty(API.PRODUCTION_MODE);
      OrganizationRequest organizationRequest =
              requestBuilder.createOrganizationRequest(accumulator.getOrganizationRequestData(), !prod);
      accumulator.setOrganizationRequest(organizationRequest);
      return new ResultTransition(new SignData(organizationRequest.signData, organizationRequest.data.enclosures));
    } finally {
      Activator.getContext().ungetService(requestBuilderReference);
    }
  }
}
