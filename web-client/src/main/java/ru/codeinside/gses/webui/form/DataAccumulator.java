package ru.codeinside.gses.webui.form;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.xml.soap.SOAPException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import org.osgi.framework.ServiceReference;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import ru.codeinside.gses.webui.osgi.Activator;
import ru.codeinside.gws.api.XmlNormalizer;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.vaadin.ui.Form;
import ru.codeinside.gses.activiti.forms.Signatures;
import ru.codeinside.gses.activiti.forms.api.definitions.PropertyTree;
import ru.codeinside.gws.api.ClientRequest;
import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.gws.api.ExchangeContext;
import ru.codeinside.gws.api.ServerResponse;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequest;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequestData;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponse;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseData;

import javax.xml.soap.SOAPMessage;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Накапливает данные в результат выполнения шагов мастера. Используется пока как черновое решение.
 */
public class DataAccumulator implements Serializable {
  private boolean needOv;
  private String serviceName;
  private String requestType;
  private String responseMessage;
  private String taskId;

  private ClientRequest clientRequest;
  private ServerResponse serverResponse;
  private PropertyTree propertyTree;
  private List<Form> forms = Lists.newLinkedList();
  private Map<Enclosure, String[]> usedEnclosures;
  private List<TmpAttachment> attachments;
  private Signatures spSignatures;
  private Signatures ovSignatures;
  private Signatures signatures;

  private SOAPMessage soapMessage;

  private String virginAppData;
  private String virginSoapMessage;

  private boolean isSmev3;

  /**
   * Хранит временный контекст который формируется для обращения с потребителю
   */
  private ExchangeContext tempContext;

  private OrganizationRequestData organizationRequestData;
  private OrganizationRequest organizationRequest;
  private ProviderResponseData providerResponseData;
  private ProviderResponse providerResponse;

  public ClientRequest getClientRequest() {
    return clientRequest;
  }

  public void setClientRequest(ClientRequest clientRequest) {
    this.clientRequest = clientRequest;
    if (clientRequest != null) {
      setVirginAppData(clientRequest.appData);
    } else {
      setVirginAppData(null);
    }
  }

  public String getTaskId() {
    return taskId;
  }

  public void setTaskId(String taskId) {
    this.taskId = taskId;
  }

  public Signatures getSpSignatures() {
    return spSignatures;
  }

  public void setSpSignatures(Signatures spSignatures) {
    this.spSignatures = spSignatures;
  }

  public Signatures getOvSignatures() {
    return ovSignatures;
  }

  public void setOvSignatures(Signatures ovSignatures) {
    this.ovSignatures = ovSignatures;
  }

  public String getServiceName() {
    return serviceName;
  }

  public void setServiceName(String serviceName) {
    this.serviceName = serviceName;
  }

  public boolean isNeedOv() {
    return needOv;
  }

  public void setNeedOv(boolean needOv) {
    this.needOv = needOv;
  }
  
    
    public String getSoapMessageString() {
        //List<SOAPMessage> cp = new ArrayList<SOAPMessage>(soapMessage);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            soapMessage.writeTo(out);
        } catch (SOAPException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String(out.toByteArray(),Charset.forName("UTF-8"));
    }
    
    public String getNormalizedSoapMessageString() {
        ByteArrayOutputStream normalizedBody = new ByteArrayOutputStream();
        
        final ServiceReference normalizerReference = Activator.getContext().getServiceReference(XmlNormalizer.class.getName());
        try {
            XmlNormalizer normalizer = (XmlNormalizer) Activator.getContext().getService(normalizerReference);
            if (normalizer == null) {
              throw new IllegalStateException("Сервис нормализации не доступен.");
            }
            
            normalizer.normalize(soapMessage.getSOAPBody(), normalizedBody);
        } catch (SOAPException ex) {          
          throw new IllegalStateException("Проблема при нормализации SOAPBody.");
        } finally {          
            Activator.getContext().ungetService(normalizerReference);
        }
        return new String(normalizedBody.toByteArray(),Charset.forName("UTF-8"));
    }
    
    public String getSoapMessageDigestString() {
        
        String digest = "";
        try {
            Node dEl = toDocument(soapMessage).getElementsByTagNameNS("http://www.w3.org/2000/09/xmldsig#", "DigestValue").item(0);
            digest = dEl.getTextContent();
        } catch (Exception ex) {          
            throw new IllegalStateException("Проблема при получении дайджеста SOAPBody.");
        } 
        if (digest.equals("")) throw new IllegalStateException("Проблема при получении дайджеста SOAPBody.");
        return digest;
    }
    
    public Document toDocument(SOAPMessage soapMsg) 
                   throws TransformerConfigurationException, TransformerException, SOAPException, IOException {
        Source src = soapMsg.getSOAPPart().getContent();
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        DOMResult result = new DOMResult();
        transformer.transform(src, result);
        return (Document)result.getNode();
    }
  
    public String getRawAppDataString() {
        //ревертуем аппдату до иксемеля
        String ad;
        try {
            ad = this.getClientRequest().appData;
        } catch (NullPointerException e) {
            ad = this.getServerResponse().appData;
        } 
          
        if (ad == null || ad.isEmpty() || ad.equals(" ")) {
            return null;
        } else {
            ad = ad.replace("<AppData Id=\"AppData\">", "");
            ad = ad.replace("</AppData>", "");
            return ad;
        }     
    }
  
  public SOAPMessage getSoapMessage() {
    return soapMessage;
  }

  public void setSoapMessage(SOAPMessage soapMessage) {
    this.soapMessage = soapMessage;
    setVirginSoapMessage(soapMessage);
  }

  public String getRequestType() {
    return requestType;
  }

  public void setRequestType(String requestType) {
    this.requestType = requestType;
  }

  public ServerResponse getServerResponse() {
    return serverResponse;
  }

  public void setServerResponse(ServerResponse serverResponse) {
    this.serverResponse = serverResponse;
    if (serverResponse != null) {
      setVirginAppData(serverResponse.appData);
    } else {
      setVirginAppData(null);
    }
  }

  public PropertyTree getPropertyTree() {
    return propertyTree;
  }

  public void setPropertyTree(PropertyTree propertyTree) {
    this.propertyTree = propertyTree;
  }

  public List<Form> getForms() {
    return ImmutableList.copyOf(forms);
  }

  public void addForm(Form form) {
    forms.add(form);
  }

  public Map<Enclosure, String[]> getUsedEnclosures() {
    return usedEnclosures;
  }

  public void setUsedEnclosures(Map<Enclosure, String[]> usedEnclosures) {
    this.usedEnclosures = usedEnclosures;
  }

  public List<TmpAttachment> getAttachments() {
    return attachments;
  }

  public TmpAttachment getAttachment(String name) {
    if (name == null || attachments == null) {
      return null;
    }

    for (TmpAttachment attachment : attachments) {
      if (name.equals(attachment.getId())) {
        return attachment;
      }
    }

    return null;
  }

  public void setAttachment(TmpAttachment attachment) {
    if (this.attachments == null) {
      this.attachments = new ArrayList<TmpAttachment>();
    }
    this.attachments.add(attachment);
  }

  public Signatures getSignatures() {
    return signatures;
  }

  public void setSignatures(Signatures signatures) {
    this.signatures = signatures;
  }

  public String getResponseMessage() {
    return responseMessage;
  }

  public void setResponseMessage(String responseMessage) {
    this.responseMessage = responseMessage;
  }

  public void setTempContext(ExchangeContext context) {
    this.tempContext = context;
  }

  public ExchangeContext getTempContext() {
    return tempContext;
  }

  private void setVirginSoapMessage(SOAPMessage soapMessage) {
    if (soapMessage != null) {
      this.virginSoapMessage = new String(ProtocolUtils.getBytesFromSoapMessage(soapMessage), Charset.forName("UTF-8"));
    } else {
      this.virginSoapMessage = null;
    }
  }

  public String getVirginSoapMessage() {
    return virginSoapMessage;
  }

  private void setVirginAppData(String appData) {
    this.virginAppData = appData;
  }

  public String getVirginAppData() {
    return virginAppData;
  }

  public boolean isSmev3() {
    return isSmev3;
  }

  public void setSmev3(boolean smev3) {
    isSmev3 = smev3;
  }

  public void setOrganizationRequestData(OrganizationRequestData organizationRequestData) {
    this.organizationRequestData = organizationRequestData;
  }

  public OrganizationRequestData getOrganizationRequestData() {
    return organizationRequestData;
  }

  public void setOrganizationRequest(OrganizationRequest organizationRequest) {
    this.organizationRequest = organizationRequest;
  }

  public OrganizationRequest getOrganizationRequest() {
    return organizationRequest;
  }

  public void setProviderResponseData(ProviderResponseData providerResponseData) {
    this.providerResponseData = providerResponseData;
  }

  public ProviderResponseData getProviderResponseData() {
    return providerResponseData;
  }

  public void setProviderResponse(ProviderResponse providerResponse) {
    this.providerResponse = providerResponse;
  }

  public ProviderResponse getProviderResponse() {
    return providerResponse;
  }
}
