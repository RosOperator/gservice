/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2013, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.gses.webui.form;

import ru.codeinside.gses.activiti.FileValue;
import ru.codeinside.gses.activiti.contents.FileContentProvider;
import ru.codeinside.smev.v3.service.api.ContentProvider;

import java.io.File;

final class EFileValue implements FileValue {
  final String name;
  final String mime;
  final File file;

  public EFileValue(String name, String mime, File file) {
    this.name = name;
    this.mime = mime;
    this.file = file;
  }

  @Override
  public String getFileName() {
    return name;
  }

  @Override
  public String getMimeType() {
    return mime;
  }

  @Override
  public ContentProvider getContent() {
    return new FileContentProvider(file, file.length());
  }

  @Override
  public File getFile() {
    return this.file;
  }

  @Override
  public String toString() {
    return name;
  }
}
