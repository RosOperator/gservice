package ru.codeinside.gses.webui.form;

import com.vaadin.ui.Form;
import ru.codeinside.gses.activiti.FileValue;
import ru.codeinside.gses.activiti.ReadOnly;
import ru.codeinside.gses.activiti.SignatureProtocol;
import ru.codeinside.gses.activiti.VariableToBytes;
import ru.codeinside.gses.activiti.contents.BytesContentProvider;
import ru.codeinside.gses.activiti.contents.FileContentProvider;
import ru.codeinside.gses.activiti.forms.FormID;
import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.smev.v3.service.api.ContentProvider;

import java.nio.charset.Charset;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.xml.namespace.QName;


public class FormUtils {
  private FormUtils() {
    throw new UnsupportedOperationException("Static methods only");
  }

  static void addSignedDataToForm(Form form, SignData signData, String propertyId) {
    byte[] signedData = signData.getData() != null ? signData.getData() : new byte[0];
    final ReadOnly txt = new ReadOnly(new String(signedData, Charset.forName("UTF-8")));
    txt.setCaption("Подписываемые данные");
    txt.addStyleName("light");
    txt.setVisible(false);
    form.addField(propertyId, txt);

    for (Enclosure e : getEnclosuresWithoutSign(signData.getEnclosures())) {
      String fieldId = e.code != null && !e.code.isEmpty() ? e.code : e.fileName;
      ReadOnly field = new ReadOnly(fieldId);
        field.setVisible(false);
        form.addField(fieldId, field);
    }
  }

  static void addSignatureFieldToForm(
    Form form, FormID formId, SignData signData, List<FormField> previousFields, 
          String fieldId, DataAccumulator dataAccumulator, QName spProps, Boolean spAfterContent) {
    List<Enclosure> enclosures = getEnclosuresWithoutSign(signData.getEnclosures());
    List<FormField> attachmentsFromForm = getAttachmentsFromForm(previousFields, signData.getEnclosures());
    int enclosureSize = enclosures.size();
    int formAttachmentsSize = attachmentsFromForm.size();
    boolean[] files = new boolean[enclosureSize + formAttachmentsSize + 1];
    files[0] = false;
    
    //String[] ids = new String[enclosureSize + 1];
    String[] ids = new String[enclosureSize + formAttachmentsSize + 1];
    ids[0] = fieldId;

    ContentProvider[] blocks = new ContentProvider[enclosureSize + formAttachmentsSize + 1];
    blocks[0] = new BytesContentProvider(signData.getData() != null ? signData.getData() : new byte[0]);

    for (int i = 0; i < enclosureSize; ++i) {
      Enclosure e = enclosures.get(i);

      files[i + 1] = true;
      ids[i + 1] = e.code != null && !e.code.isEmpty() ? e.code : e.fileName;
      blocks[i + 1] = new FileContentProvider(e.getFile(), e.size());
    }

    for (int i = 0; i < formAttachmentsSize; ++i) {
      FormField field = attachmentsFromForm.get(i);

      files[i + enclosureSize + 1] = true;
      ids[i + enclosureSize + 1] = field.getPropId();
      blocks[i + enclosureSize + 1] = VariableToBytes.toBytes(field.getValue());
    }

    
    FormSignatureField signatureField = new FormSignatureField(
            new SignatureProtocol(
                    formId, 
                    FormSignatureSeq.SIGNATURE,
                    FormSignatureSeq.SIGNATURE, 
                    blocks, 
                    files, 
                    ids, 
                    form, 
                    dataAccumulator,
                    spProps,
                    spAfterContent)
    );

    signatureField.setCaption(FormSignatureSeq.SIGNATURE);
    signatureField.setRequired(true);

    form.addField(FormSignatureSeq.SIGNATURE, signatureField);
  }

  private static List<FormField> getAttachmentsFromForm(List<FormField> previousFields, List<Enclosure> enclosures) {
    if (previousFields != null) {
      List<FormField> result = new LinkedList<FormField>();
      for (FormField field : previousFields) {
        if (field.getValue() instanceof FileValue) {
          if (enclosures != null) {
            // не добавяле поле с формы если оно было преобразовано во вложение
            boolean exclude = false;
            for (Enclosure enclosure : enclosures) {
              if (field.getPropId().equals(enclosure.code)) {
                exclude = true;
              }
            }
            if (!exclude) {
              result.add(field);
            }
          } else {
            result.add(field);
          }
        }
      }
      return result;
    }
    return Collections.emptyList();
  }

  private static List<Enclosure> getEnclosuresWithoutSign(List<Enclosure> enclosures) {
    List<Enclosure> withoutSign = new LinkedList<Enclosure>();
    for (Enclosure e : enclosures) {
      if (e.signature == null) {
        withoutSign.add(e);
      }
    }
    return withoutSign;
  }
}
