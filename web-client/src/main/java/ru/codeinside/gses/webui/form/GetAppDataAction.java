package ru.codeinside.gses.webui.form;

import com.google.common.collect.Maps;
import com.vaadin.ui.Form;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.VariableScope;
import org.activiti.engine.impl.ServiceImpl;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.osgi.framework.ServiceReference;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.gses.activiti.forms.FormID;
import ru.codeinside.gses.activiti.forms.Signatures;
import ru.codeinside.gses.activiti.forms.SubmitFormCmd;
import ru.codeinside.gses.activiti.forms.SubmitFormDataCmd;
import ru.codeinside.gses.beans.ActivitiExchangeContext;
import ru.codeinside.gses.beans.StartFormExchangeContext;
import ru.codeinside.gses.service.F2;
import ru.codeinside.gses.service.Fn;
import ru.codeinside.gses.webui.form.api.FieldValuesSource;
import ru.codeinside.gses.webui.osgi.Activator;
import ru.codeinside.gses.webui.wizard.ResultTransition;
import ru.codeinside.gws.api.Client;
import ru.codeinside.gws.api.ClientRequest;
import ru.codeinside.gws.api.CryptoProvider;
import ru.codeinside.gws.api.ExchangeContext;
import ru.codeinside.gws.api.XmlNormalizer;
import ru.codeinside.gws.api.XmlSignatureInjector;
import ru.codeinside.smev.v3.service.api.consumer.Consumer;
import ru.codeinside.smev.v3.service.api.consumer.ConsumerRequest;
import ru.codeinside.smev.v3.service.api.consumer.ConsumerRequestBuilder;
import ru.codeinside.smev.v3.service.api.consumer.OrganizationRequestData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetAppDataAction extends BaseClientAction {

  public GetAppDataAction(final DataAccumulator dataAccumulator) {
   super(dataAccumulator);
  }

  @Override
  protected ResultTransition smev3request(Consumer consumer) {
    OrganizationRequestData organizationRequestData = Fn.withEngine(new GetConsumerRequest(), consumer, accumulator);
    accumulator.setOrganizationRequestData(organizationRequestData);
    return new ResultTransition(
        new SignData(organizationRequestData.personalData.signData, organizationRequestData.enclosures));
  }

  @Override
  protected ResultTransition smev2request(Client client) {
    final ClientRequest request = Fn.withEngine(new GetClientRequest(), client, accumulator);
    accumulator.setClientRequest(request);
    return prepareRequest(request);
  }

  private ResultTransition prepareRequest(final ClientRequest request) {
    try {
      final ServiceReference normalizerReference = Activator.getContext().getServiceReference(XmlNormalizer.class.getName());
      final ServiceReference cryptoReference = Activator.getContext().getServiceReference(CryptoProvider.class.getName());
      final ServiceReference injectorReference = Activator.getContext().getServiceReference(XmlSignatureInjector.class.getName());
      byte[] signedInfoBytes = null;
      try {
        XmlNormalizer normalizer = (XmlNormalizer) Activator.getContext().getService(normalizerReference);
        if (normalizer == null) {
          throw new IllegalStateException("Сервис нормализации не доступен.");
        }
        CryptoProvider cryptoProvider = (CryptoProvider) Activator.getContext().getService(cryptoReference);
        if (cryptoProvider == null) {
          throw new IllegalStateException("Сервис криптографии не доступен.");
        }
        XmlSignatureInjector injector = (XmlSignatureInjector) Activator.getContext().getService(injectorReference);
        if (injector == null) {
          throw new IllegalStateException("Сервис внедрения подписи не доступен.");
        }
        boolean isSignatureLast = accumulator.getPropertyTree().isAppDataSignatureBlockLast();
        signedInfoBytes = injector.prepareAppData(request, isSignatureLast, normalizer, cryptoProvider);
      } finally {
        Activator.getContext().ungetService(normalizerReference);
        Activator.getContext().ungetService(cryptoReference);
        Activator.getContext().ungetService(injectorReference);
      }
      return new ResultTransition(new SignData(signedInfoBytes, request.enclosures));
    } catch (Exception e) {
      e.printStackTrace();
      throw new IllegalStateException("Ошибка получения подготовительных данных: " + e.getCause().getMessage(), e);
    }
  }

  private static void temporarySubmitContext(CommandExecutor commandExecutor, final DataAccumulator accumulator) {
    List<Form> forms = accumulator.getForms();
    if (accumulator.getTaskId() != null && forms.size() > 0) {
      Map<String, Object> properties = ((FieldValuesSource) forms.get(0)).getFieldValues();
      commandExecutor.execute(
          new SubmitFormCmd(FormID.byTaskId(accumulator.getTaskId()), properties, null, false, accumulator));
    }
  }

  private static Map<String, Object> getFieldValues(final DataAccumulator accumulator) {
    List<Form> forms = accumulator.getForms();
    if (forms.size() > 0) {
      return ((FieldValuesSource) forms.get(0)).getFieldValues();
    }
    return Maps.newHashMap();
  }

  private static ExchangeContext getExchangeContext(CommandContext commandContext, final DataAccumulator accumulator) {
    String taskId = accumulator.getTaskId();
    ExchangeContext context;
    if (taskId != null) {
      final String processInstanceId = AdminServiceProvider.get().getBidByTask(taskId).getProcessInstanceId();

      DelegateExecution execution = Context.getCommandContext()
          .getExecutionManager()
          .findExecutionById(processInstanceId);
      context = new ActivitiExchangeContext(execution);
    } else {

      VariableScope variableScope = new StartEventVariableScope();
      Map<SignatureType, Signatures> signatures = null;
      if (accumulator.getSignatures() != null) {
        signatures = new HashMap<SignatureType, Signatures>();
        signatures.put(SignatureType.FIELDS, accumulator.getSignatures());
      }

      new SubmitFormDataCmd(
          accumulator.getPropertyTree(),
          variableScope,
          getFieldValues(accumulator),
          signatures,
          new StartEventAttachmentConverter(accumulator),
          accumulator).execute(commandContext);

      context = new StartFormExchangeContext(variableScope, accumulator);
      accumulator.setTempContext(context);
    }

    ProtocolUtils.writeInfoSystemsToContext(accumulator.getServiceName(), context);
    return context;
  }

  // -------------
  // Inner Classes
  // -------------

  public static final class GetClientRequest implements F2<ClientRequest, Client, DataAccumulator> {
    @Override
    public ClientRequest apply(ProcessEngine engine, Client client, DataAccumulator accumulator) {
      CommandExecutor commandExecutor = ((ServiceImpl) engine.getFormService()).getCommandExecutor();
      temporarySubmitContext(commandExecutor, accumulator);
      return commandExecutor.execute(new GetClientRequestCmd(client, accumulator));
    }
  }

  private static final class GetClientRequestCmd implements Command<ClientRequest> {
    private final Client client;
    private final DataAccumulator accumulator;

    GetClientRequestCmd(Client client, DataAccumulator accumulator) {
      this.client = client;
      this.accumulator = accumulator;
    }

    @Override
    public ClientRequest execute(CommandContext commandContext) {
      ExchangeContext context = getExchangeContext(commandContext, accumulator);
      ClientRequest request = client.createClientRequest(context);
      ProtocolUtils.fillRequestPacket(request, accumulator.getServiceName());

      return request;
    }
  }

  public static final class GetConsumerRequest implements F2<OrganizationRequestData, Consumer, DataAccumulator> {

    @Override
    public OrganizationRequestData apply(ProcessEngine engine, Consumer consumer, DataAccumulator accumulator) {
      CommandExecutor commandExecutor = ((ServiceImpl) engine.getFormService()).getCommandExecutor();
      temporarySubmitContext(commandExecutor, accumulator);
      return commandExecutor.execute(new GetOrganizationRequestDataCmd(consumer, accumulator));
    }
  }

  private static final class GetOrganizationRequestDataCmd implements Command<OrganizationRequestData> {

    private final Consumer consumer;
    private final DataAccumulator accumulator;

    public GetOrganizationRequestDataCmd(Consumer consumer, DataAccumulator accumulator) {
      this.consumer = consumer;
      this.accumulator = accumulator;
    }

    @Override
    public OrganizationRequestData execute(CommandContext commandContext) {
      ExchangeContext context = getExchangeContext(commandContext, accumulator);
      ConsumerRequest consumerRequest = consumer.createRequest(context);
      ServiceReference requestBuilderReference = Activator.getContext().getServiceReference(ConsumerRequestBuilder.class.getName());

      if (requestBuilderReference == null) {
        throw new IllegalStateException("Севрис формирования запросов потребителя недоступен.");
      }

      try {
        ConsumerRequestBuilder requestBuilder = (ConsumerRequestBuilder) Activator.getContext().getService(requestBuilderReference);
        if (requestBuilder == null) {
          throw new IllegalStateException("Не удалось получить сервис построения запроса.");
        }

        return requestBuilder.createPersonalRequest(consumerRequest);
      } finally {
        Activator.getContext().ungetService(requestBuilderReference);
      }
    }
  }

}
