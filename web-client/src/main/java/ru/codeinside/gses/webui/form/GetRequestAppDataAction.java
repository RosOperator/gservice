package ru.codeinside.gses.webui.form;

import com.vaadin.ui.Form;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.impl.ServiceImpl;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.osgi.framework.ServiceReference;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.adm.database.Bid;
import ru.codeinside.adm.database.ExternalGlue;
import ru.codeinside.gses.activiti.forms.FormID;
import ru.codeinside.gses.activiti.forms.SubmitFormCmd;
import ru.codeinside.gses.beans.ActivitiReceiptContext;
import ru.codeinside.gses.service.F2;
import ru.codeinside.gses.service.Fn;
import ru.codeinside.gses.webui.Flash;
import ru.codeinside.gses.webui.form.api.FieldValuesSource;
import ru.codeinside.gses.webui.osgi.Activator;
import ru.codeinside.gses.webui.wizard.ResultTransition;
import ru.codeinside.gws.api.CryptoProvider;
import ru.codeinside.gws.api.InfoSystem;
import ru.codeinside.gws.api.Packet;
import ru.codeinside.gws.api.Server;
import ru.codeinside.gws.api.ServerResponse;
import ru.codeinside.gws.api.XmlNormalizer;
import ru.codeinside.gws.api.XmlSignatureInjector;
import ru.codeinside.smev.v3.service.api.provider.ProvidedResponse;
import ru.codeinside.smev.v3.service.api.provider.Provider;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseBuilder;
import ru.codeinside.smev.v3.service.api.provider.ProviderResponseData;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class GetRequestAppDataAction extends BaseServerAction {

  public GetRequestAppDataAction(DataAccumulator accumulator) {
    super(accumulator);
  }

  @Override
  protected ResultTransition smev3response(Provider provider) {
    ProviderResponseData response = Fn.withEngine(new GetProviderResponse(), accumulator, provider);
    accumulator.setProviderResponseData(response);
    return new ResultTransition(new SignData(response.personalData.signData, response.enclosures));
  }

  @Override
  protected ResultTransition smev2response(Server server) {
    ServerResponse response = Fn.withEngine(new GetServerResponse(), accumulator, server);
    accumulator.setServerResponse(response);
    return prepareResponse(response);
  }

  private static void temporarySubmitContext(CommandExecutor commandExecutor, DataAccumulator accumulator) {
    List<Form> forms = accumulator.getForms();
    if (accumulator.getTaskId() != null && forms.size() > 0) {
      Map<String, Object> properties = ((FieldValuesSource) forms.get(0)).getFieldValues();
      commandExecutor.execute(
          new SubmitFormCmd(FormID.byTaskId(accumulator.getTaskId()), properties, null, false, accumulator));
    }
  }

  private static ActivitiReceiptContext getActivityReceiptContext(CommandContext commandContext, DataAccumulator accumulator) {
    String taskId = accumulator.getTaskId();
    if (taskId != null) {
      final Bid bid = AdminServiceProvider.get().getBidByTask(taskId);
      DelegateExecution execution = commandContext.getExecutionManager().findExecutionById(bid.getProcessInstanceId());
      return new ActivitiReceiptContext(execution, bid.getId());
    } else {
      throw new IllegalStateException("Task is null");
    }
  }

  private ResultTransition prepareResponse(ServerResponse response) {
    try {
      final ServiceReference normalizerReference = Activator.getContext().getServiceReference(XmlNormalizer.class.getName());
      final ServiceReference cryptoReference = Activator.getContext().getServiceReference(CryptoProvider.class.getName());
      final ServiceReference injectorReference = Activator.getContext().getServiceReference(XmlSignatureInjector.class.getName());
      byte[] signedInfoBytes = null;
      try {
        XmlNormalizer normalizer = (XmlNormalizer) Activator.getContext().getService(normalizerReference);
        if (normalizer == null) {
          throw new IllegalStateException("Сервис нормализации не доступен.");
        }
        CryptoProvider cryptoProvider = (CryptoProvider) Activator.getContext().getService(cryptoReference);
        if (cryptoProvider == null) {
          throw new IllegalStateException("Сервис криптографии не доступен.");
        }
        XmlSignatureInjector injector = (XmlSignatureInjector) Activator.getContext().getService(injectorReference);
        if (injector == null) {
          throw new IllegalStateException("Сервис внедрения подписи не доступен.");
        }
        boolean isSignatureLast = accumulator.getPropertyTree().isAppDataSignatureBlockLast();
        signedInfoBytes = injector.prepareAppData(response, isSignatureLast, normalizer, cryptoProvider);
      } finally {
        Activator.getContext().ungetService(normalizerReference);
        Activator.getContext().ungetService(cryptoReference);
        Activator.getContext().ungetService(injectorReference);
      }
      return new ResultTransition(new SignData(signedInfoBytes, response.attachmens));
    } catch (Exception e) {
      e.printStackTrace();
      throw new IllegalStateException("Ошибка получения подготовительных данных: " + e.getCause().getMessage(), e);
    }
  }

  final static class GetServerResponse implements F2<ServerResponse, DataAccumulator, Server> {
    @Override
    public ServerResponse apply(ProcessEngine engine, DataAccumulator accumulator, Server server) {
      CommandExecutor commandExecutor = ((ServiceImpl) engine.getFormService()).getCommandExecutor();
      temporarySubmitContext(commandExecutor, accumulator);
      return commandExecutor.execute(new GetServerResponseCmd(accumulator, server));
    }
  }

  final private static class GetServerResponseCmd implements Command<ServerResponse> {
    private final DataAccumulator accumulator;
    private final Server server;

    GetServerResponseCmd(DataAccumulator accumulator, Server server) {
      this.accumulator = accumulator;
      this.server = server;
    }

    @Override
    public ServerResponse execute(CommandContext commandContext) {
      ActivitiReceiptContext context = getActivityReceiptContext(commandContext, accumulator);
      String requestType = accumulator.getRequestType();
      ServerResponse response;
      String responseMessage = accumulator.getResponseMessage();
      if ("result".equals(requestType)) {
        if (responseMessage == null || responseMessage.isEmpty()) {
          responseMessage = "Исполнено";
        }
        response = server.processResult(responseMessage, context);
      } else if ("status".equals(requestType)) {
        if (responseMessage == null || responseMessage.isEmpty()) {
          responseMessage = "Статус";
        }
        response = server.processStatus(responseMessage, context);
      } else {
        throw new IllegalStateException("Неправильный тип запроса");
      }
      accumulator.setUsedEnclosures(context.getUsedEnclosures());
      fillResponsePacket(response.packet);
      return response;
    }

    private void fillResponsePacket(Packet packet) {
      Bid bid = AdminServiceProvider.get().getBidByTask(accumulator.getTaskId());

      ExternalGlue glue = bid.getGlue();

      ru.codeinside.adm.database.InfoSystem sender;
      ru.codeinside.adm.database.InfoSystem originator;
      ru.codeinside.adm.database.InfoSystem recipient;
      if (glue != null &&
          (sender = glue.getSender()) != null &&
          (recipient = glue.getRecipient()) != null) {
        packet.sender = new InfoSystem(recipient.getCode(), recipient.getName());
        packet.recipient = new InfoSystem(sender.getCode(), sender.getName());
		if ((originator = glue.getOrigin()) != null) {
		  packet.originator = new InfoSystem(originator.getCode(), originator.getName());
        }
      } else {
        throw new IllegalStateException("Нет связи с внешней услугой");
      }

      packet.originRequestIdRef = glue.getRequestIdRef();
      packet.requestIdRef = UUID.randomUUID().toString();

      if (packet.date == null) {
        packet.date = new Date();
      }
    }
  }

  final public static class GetProviderResponse implements F2<ProviderResponseData, DataAccumulator, Provider> {

    @Override
    public ProviderResponseData apply(ProcessEngine engine, DataAccumulator accumulator, Provider provider) {
      CommandExecutor commandExecutor = ((ServiceImpl) engine.getFormService()).getCommandExecutor();
      temporarySubmitContext(commandExecutor, accumulator);
      return commandExecutor.execute(new GetProviderResponseCmd(accumulator, provider));
    }
  }

  final private static class GetProviderResponseCmd implements Command<ProviderResponseData> {

    private final DataAccumulator accumulator;
    private final Provider provider;

    public GetProviderResponseCmd(DataAccumulator accumulator, Provider provider) {
      this.accumulator = accumulator;
      this.provider = provider;
    }

    @Override
    public ProviderResponseData execute(CommandContext commandContext) {
      ActivitiReceiptContext context = getActivityReceiptContext(commandContext, accumulator);
      ProvidedResponse response = provider.createResponse(context);

      ServiceReference responseBuilderReference = Activator.getContext().getServiceReference(ProviderResponseBuilder.class.getName());
      if (responseBuilderReference == null) {
        throw new IllegalStateException("Севрис формирования запросов поставщика недоступен.");
      }

      try {
        ProviderResponseBuilder responseBuilder = (ProviderResponseBuilder) Activator.getContext().getService(responseBuilderReference);
        if (responseBuilder == null) {
          throw new IllegalStateException("Не удалось получить сервис построения запроса.");
        }

        return responseBuilder.createPersonalResponse(response);
      } finally {
        Activator.getContext().ungetService(responseBuilderReference);
      }
    }
  }
}
