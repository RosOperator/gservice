package ru.codeinside.gses.webui.form;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by nprovorkova on 12.10.2016.
 */
public class JRDataSourceImpl implements JRDataSource {

    private Map fieldCollections;
    private int collectionLength = 0;

    public JRDataSourceImpl(Map fieldsCollection) {
        this.fieldCollections = fieldsCollection;
        for (Object key : fieldsCollection.keySet()) {
          ArrayList<String> collection = (ArrayList<String>) fieldsCollection.get(key);
          if(collectionLength < collection.size()) {
              collectionLength = collection.size();
          }
        }
    }

    @Override
    public boolean next() throws JRException {
        if(collectionLength > 0) {
            collectionLength--;
            return true;
        }
        return false;
    }

    @Override
    public Object getFieldValue(JRField jrField) throws JRException {
        ArrayList<String> collection = (ArrayList<String>) fieldCollections.get(jrField.getName());
        String value = null;
        if(collection.size() > 0) {
            value = collection.get(0);
            collection.remove(0);
        }
        return value;
    }
}
