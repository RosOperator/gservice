/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2014, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.gses.webui.form;


import com.google.common.base.Function;
import com.vaadin.Application;
import com.vaadin.terminal.FileResource;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.*;
import commons.Streams;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.util.DefaultFormatFactory;
import net.sf.jasperreports.engine.util.FormatFactory;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.*;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.adm.database.Bid;
import ru.codeinside.adm.database.Employee;
import ru.codeinside.adm.database.ProcedurePrintForm;
import ru.codeinside.gses.activiti.forms.api.definitions.PropertyNode;
import ru.codeinside.gses.activiti.forms.definitions.NBlock;
import ru.codeinside.gses.form.FormData;
import ru.codeinside.gses.form.FormEntry;
import ru.codeinside.gses.webui.osgi.FormConverterCustomicer;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class PrintPanel extends CustomComponent {

  final static ThemeResource PRINT_ICON = new ThemeResource("../custom/icon/printer22.png");
  final static ThemeResource DOWNLOAD_ICON = new ThemeResource("../custom/icon/download22.png");

  final static AtomicInteger SERIAL = new AtomicInteger(0);

  File htmlFile;
  File docxFile;

  public PrintPanel(FormDataSource dataSource, Application app, String procedureName, String processDefinitionId, Bid bid) {
    setSizeFull();
    try {
      htmlFile = Streams.createTempFile("form-", ".html");
      docxFile = Streams.createTempFile("form-", ".docx");
      FormData data = new FormData();

      data.htmlFile = htmlFile.getAbsolutePath();
      data.docxFile = docxFile.getAbsolutePath();

      List<ProcedurePrintForm> procedurePrintForms = AdminServiceProvider.get().getPrintFormsByDefinitionId(processDefinitionId);

      if (procedurePrintForms == null || procedurePrintForms.isEmpty()) {
          FormEntry formEntry = dataSource.createFormTree();
          data.entries = formEntry.children;
          FormConverterCustomicer.convert(data);
      } else {
          JasperFormEntry jasperFormEntry = dataSource.createJasperFormTree();
          jaspertPrintFormPrepear(jasperFormEntry, data, procedureName,  bid,
                  procedurePrintForms);
      }

      String classId = "doc" + SERIAL.incrementAndGet();

      Button print = new Button("Печатать", new PrintAction(classId));
      print.setStyleName("img-button");
      print.setIcon(PRINT_ICON);
      print.setImmediate(true);

      Button download = new Button("Скачать", new DownloadAction(docxFile));
      download.setStyleName("img-button");
      download.setIcon(DOWNLOAD_ICON);
      download.setImmediate(true);

      Panel documentPanel = createDocumentPanel(app, classId);

      HorizontalLayout buttons = new HorizontalLayout();
      buttons.setImmediate(true);
      buttons.setSpacing(true);
      buttons.addComponent(print);
      buttons.addComponent(download);

      VerticalLayout printLayout = new VerticalLayout();
      printLayout.setSizeFull();
      printLayout.addComponent(buttons);
      printLayout.addComponent(documentPanel);
      printLayout.setExpandRatio(documentPanel, 1f);

      setCompositionRoot(printLayout);
    } catch (IOException e) {
      setCompositionRoot(new Label(e.getMessage()));
    } catch (JRException e) {
        setCompositionRoot(new Label(e.getMessage()));
    }
  }

    public PrintPanel(EForm eForm, Application app, String procedureName, String processDefinitionId, Bid bid) {
        setSizeFull();
        try {
            htmlFile = Streams.createTempFile("form-", ".html");
            docxFile = Streams.createTempFile("form-", ".docx");
            FormData data = new FormData();

            data.htmlFile = htmlFile.getAbsolutePath();
            data.docxFile = docxFile.getAbsolutePath();

            List<ProcedurePrintForm> procedurePrintForms = AdminServiceProvider.get().getPrintFormsByDefinitionId(processDefinitionId);

            if (procedurePrintForms == null || procedurePrintForms.isEmpty()) {
                buildHtmlFile("<span color='#00F' font-style='italic' text-align='left' font-size='18px'>" +
                        "&#1055;&#1077;&#1095;&#1072;&#1090;&#1085;&#1072;&#1103;&#32;&#1092;&#1086;&#1088;&#1084;" +
                        "&#1072;&#32;&#1085;&#1077;&#32;&#1085;&#1072;&#1081;&#1076;&#1077;&#1085;&#1072;</span>");
            } else {
                JasperFormEntry jasperFormEntry = createJasperFormTree(eForm.formValue.getFormDefinition().getNodes(),
                        eForm.fields);
                jaspertPrintFormPrepear(jasperFormEntry, data, procedureName, bid, procedurePrintForms);
            }

            String classId = "doc" + SERIAL.incrementAndGet();

            Button print = new Button("Печатать", new PrintAction(classId));
            print.setStyleName("img-button");
            print.setIcon(PRINT_ICON);
            print.setImmediate(true);

            Button download = new Button("Скачать", new DownloadAction(docxFile));
            download.setStyleName("img-button");
            download.setIcon(DOWNLOAD_ICON);
            download.setImmediate(true);

            Panel documentPanel = createDocumentPanel(app, classId);

            HorizontalLayout buttons = new HorizontalLayout();
            buttons.setImmediate(true);
            buttons.setSpacing(true);
            buttons.addComponent(print);
            buttons.addComponent(download);

            VerticalLayout printLayout = new VerticalLayout();
            printLayout.setSizeFull();
            printLayout.addComponent(buttons);
            printLayout.addComponent(documentPanel);
            printLayout.setExpandRatio(documentPanel, 1f);

            setCompositionRoot(printLayout);
        } catch (IOException e) {
            setCompositionRoot(new Label(e.getMessage()));
        } catch (JRException e) {
            setCompositionRoot(new Label(e.getMessage()));
        }
    }

  PrintPanel(String content, Application app) {
    try {
      htmlFile = Streams.createTempFile("form-", ".html");
      String classId = "doc" + SERIAL.incrementAndGet();

      Button print = new Button("Печатать", new PrintAction(classId));
      print.setStyleName("img-button");
      print.setIcon(PRINT_ICON);
      print.setImmediate(true);

      buildHtmlFile(content);
      Panel documentPanel = createDocumentPanel(app, classId);
      documentPanel.setHeight(500, UNITS_PIXELS);

      HorizontalLayout buttons = new HorizontalLayout();
      buttons.setImmediate(true);
      buttons.setSpacing(true);
      buttons.addComponent(print);

      VerticalLayout printLayout = new VerticalLayout();
      printLayout.setSizeFull();
      printLayout.addComponent(buttons);
      printLayout.addComponent(documentPanel);
      printLayout.setExpandRatio(documentPanel, 1f);

      setCompositionRoot(printLayout);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void buildHtmlFile(String content) throws IOException {
    OutputStream os = null;
    try {
      os = new BufferedOutputStream(
          new FileOutputStream(htmlFile.getAbsolutePath())
      );
      os.write(content.getBytes(Charset.forName("UTF-8")));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } finally {
      close(os);
    }
  }

  private Panel createDocumentPanel(Application app, String classId) {
    Embedded document = new Embedded(null, new FileResource(htmlFile, app));
    document.setDebugId(classId);
    document.setType(Embedded.TYPE_BROWSER);
    document.setSizeFull();

    VerticalLayout documentLayout = new VerticalLayout();
    documentLayout.setMargin(true);
    documentLayout.setSizeFull();
    documentLayout.addComponent(document);
    documentLayout.setExpandRatio(document, 1f);

    Panel documentPanel = new Panel(documentLayout);
    documentPanel.setSizeFull();
    return documentPanel;
  }

  private void jaspertPrintFormPrepear(JasperFormEntry jasperFormEntry, FormData data, String procedureName,
                                       Bid bid, List<ProcedurePrintForm> procedurePrintForms) throws JRException {

      String userName = "";
      String orgInn = "";
      String orgOgrn = "";
      data.serviceName = procedureName;
      data.orgName = "";
      if (bid != null) {
          data.receiptId = bid.getId();
          data.receiptDate = bid.getDateCreated();
          Employee employee = AdminServiceProvider.get().findEmployeeByLogin(bid.getDeclarant());
          userName = "";
          if (employee != null) {
              data.orgName = AdminServiceProvider.get().withEmployee(bid.getDeclarant(), new Function<Employee, String>() {
                  @Override
                  public String apply(Employee em) {
                      return em.getOrganization().getName();
                  }
              });
              orgInn = AdminServiceProvider.get().withEmployee(bid.getDeclarant(), new Function<Employee, String>() {
                  @Override
                  public String apply(Employee em) {
                      return em.getOrganization().getInn();
                  }
              });
              orgOgrn = AdminServiceProvider.get().withEmployee(bid.getDeclarant(), new Function<Employee, String>() {
                  @Override
                  public String apply(Employee em) {
                      return em.getOrganization().getOgrn();
                  }
              });
              userName = AdminServiceProvider.get().findEmployeeByLogin(bid.getDeclarant()).getFio();
          }
      } else {
          data.receiptDate = new Date();
      }

      ProcedurePrintForm printForm = procedurePrintForms.get(0);
      TimeZone timeZone = TimeZone.getDefault();
      Locale locale = Locale.getDefault();
      FormatFactory jrFormatFactory = new DefaultFormatFactory();
      jrFormatFactory.createDateFormat("dd.MM.yyyy HH:mm", locale, timeZone);
      Map params = new HashMap();
      params.put(JRParameter.REPORT_TIME_ZONE, timeZone);
      params.put(JRParameter.REPORT_LOCALE, locale);
      params.put(JRParameter.REPORT_FORMAT_FACTORY, jrFormatFactory);
      params.put("orgName", data.orgName);
      params.put("orgInn", orgInn);
      params.put("orgOgrn", orgOgrn);
      params.put("procedureName", data.serviceName);
      params.put("receiptId", data.receiptId);
      params.put("receiptDate", data.receiptDate);
      params.put("userName", userName);
      Map fieldsCollection = new HashMap<String, ArrayList<String>>();
      if(jasperFormEntry != null) {
          for (JasperFormEntry entry : jasperFormEntry.children) {
              if(entry.children == null) {
                  params.put(entry.property, entry.value);
              } else {
                  ArrayList<String> baseCollection = new ArrayList<String>();
                  baseCollection.add(" ");
                  baseCollection.add(entry.name);
                  fieldsCollection.put("collection", baseCollection);
                  for (JasperFormEntry children : entry.children) {
                      fillJasperReportCollection(children, params, fieldsCollection);
                  }
              }
          }
      }

      byte[] jrxml = printForm.getBytes().getBytes();
      JasperDesign jasperDesign = JRXmlLoader.load(new ByteArrayInputStream(jrxml));
      JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
      JasperPrint jasperPrint;
      if (fieldsCollection.size() == 0) {
          jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());
      } else {
          jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JRDataSourceImpl(fieldsCollection));
      }
      List<JasperPrint> jasperPrints = new ArrayList<JasperPrint>();
      jasperPrints.add(jasperPrint);

      JRDocxExporter docxExporter = new JRDocxExporter();

      docxExporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrints));
      docxExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(docxFile));
      SimpleDocxReportConfiguration docxConfiguration = new SimpleDocxReportConfiguration();
      docxConfiguration.setFlexibleRowHeight(true);
      docxExporter.setConfiguration(docxConfiguration);
      docxExporter.exportReport();

      HtmlExporter htmlExporter = new HtmlExporter();
      htmlExporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrints));
      htmlExporter.setExporterOutput(new SimpleHtmlExporterOutput(htmlFile));
      SimpleHtmlReportConfiguration htmlReportConfiguration = new SimpleHtmlReportConfiguration();
      htmlReportConfiguration.setRemoveEmptySpaceBetweenRows(true);
      htmlExporter.setConfiguration(htmlReportConfiguration);
      htmlExporter.exportReport();

  }

    private JasperFormEntry createJasperFormTree(PropertyNode[] nodes, Map<String, EField> fields) {
        List<JasperFormEntry> children = new ArrayList<JasperFormEntry>();
        for (int i = 0; i < nodes.length; i++) {
            JasperFormEntry entry = generateJasperFormData(nodes[i], fields, "");
            if(entry != null) {
                children.add(entry);
            }
        }
        if (!children.isEmpty()) {
            JasperFormEntry jasperFormEntry = new JasperFormEntry();
            jasperFormEntry.children = children.toArray(new JasperFormEntry[children.size()]);
            return jasperFormEntry;
        }
        return null;
    }

    private JasperFormEntry generateJasperFormData(PropertyNode entry, Map<String, EField> fields, String suffix) {

        JasperFormEntry jasperFormEntry = null;
        switch (entry.getPropertyType()) {
            case ITEM:
                if(fields.get(entry.getId() + suffix) != null && fields.get(entry.getId() + suffix).getValue() != null) {
                    jasperFormEntry = new PrintPanel.JasperFormEntry();
                    jasperFormEntry.property = entry.getId() + suffix;
                    jasperFormEntry.name = entry.getName();
                    jasperFormEntry.value = (String) fields.get(entry.getId() + suffix).getValue();
                }
                break;
            case BLOCK:
                if(fields.get(entry.getId() + suffix) != null) {
                    String value = (String) fields.get(entry.getId() + suffix).getValue();
                    if(value != null && Integer.parseInt(value) > 0) {
                        jasperFormEntry = new PrintPanel.JasperFormEntry();
                        jasperFormEntry.property = entry.getId() + suffix;
                        jasperFormEntry.name = entry.getName();
                        jasperFormEntry.value = value;
                    }
                }
                break;

            case TOGGLE:
            case VISIBILITY_TOGGLE:
            case ENCLOSURE:
                break;

            default:
                throw new IllegalStateException();
        }
        if (jasperFormEntry != null && entry instanceof NBlock && Integer.parseInt(jasperFormEntry.value) > 0) {
            List<JasperFormEntry> jasperFormEntryChildren = new ArrayList<JasperFormEntry>();
            for (int i = 0; i < Integer.parseInt(jasperFormEntry.value); i++) {
                for (PropertyNode child : ((NBlock) entry).getNodes()) {
                    JasperFormEntry childEntry = generateJasperFormData(child, fields, suffix + "_" + (i + 1));
                    if (childEntry != null) {
                        jasperFormEntryChildren.add(childEntry);
                    }
                }
            }

            if (!jasperFormEntryChildren.isEmpty()) {
                jasperFormEntry.children = jasperFormEntryChildren.toArray(new JasperFormEntry[jasperFormEntryChildren.size()]);
            }
        }
        return jasperFormEntry;
    }

    private void fillJasperReportCollection(JasperFormEntry entry, Map params, Map fieldsCollection) {
        if (entry.children == null) {
            String entryValue = entry.value != null ? entry.value : "";
            String collectionCurrentValue = entry.name == null ? ("     " + entryValue) : (entry.name + " : "+ entryValue);
            params.put(entry.property, entry.value);

            //for new print forms
            String listName = entry.property.replaceAll("_\\d+$", "");
            if(fieldsCollection.containsKey(listName)) {
               ((ArrayList<String>) fieldsCollection.get(listName)).add(entry.value);
            } else {
                ArrayList<String> property = new ArrayList<String>();
                property.add(entry.value);
                fieldsCollection.put(listName, property);
            }
            //for old print forms collections
            ((ArrayList<String>) fieldsCollection.get("collection")).add(collectionCurrentValue);
        } else {
            for (JasperFormEntry children : entry.children) {
                fillJasperReportCollection(children, params, fieldsCollection);
            }
            ((ArrayList<String>) fieldsCollection.get("collection")).add(" ");
        }
    }

  @Override
  public void detach() {
    if (htmlFile != null) {
      htmlFile.delete();
    }
    if (docxFile != null) {
      docxFile.delete();
    }
    super.detach();
  }

  final static class PrintAction implements Button.ClickListener {

    final String classId;

    public PrintAction(String classId) {
      this.classId = classId;
    }

    @Override
    public void buttonClick(Button.ClickEvent event) {
      String script = "try{var iframe = document.getElementById('" + classId + "').firstChild;" +
              "iframe.focus();" +
              "var printResult = iframe.contentWindow.document.execCommand('print', false, null);" +
              "if (!printResult) {" +
              "iframe.contentWindow.print();" +
              "}" +
              "} catch(e){alert(e);}";
      event.getButton().getWindow().getApplication().getMainWindow().executeJavaScript(script);
    }
  }

  final static class DownloadAction implements Button.ClickListener {

    final File file;

    DownloadAction(File file) {
      this.file = file;
    }

    @Override
    public void buttonClick(Button.ClickEvent event) {
      Button button = event.getButton();
      button.getWindow().open(new FileDownloadResource(false, file, button.getApplication()), "_top", false);
    }
  }

    public static class JasperFormEntry {

        public String property;

        public String name;

        public String value;

        public JasperFormEntry[] children;
    }

  private void close(OutputStream os) {
    if (os != null) {
      try {
        os.flush();
        os.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
