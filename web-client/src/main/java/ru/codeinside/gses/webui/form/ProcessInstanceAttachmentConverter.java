package ru.codeinside.gses.webui.form;

import org.activiti.engine.impl.cmd.CreateAttachmentCmd;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.task.Attachment;
import ru.codeinside.gses.activiti.FileValue;
import ru.codeinside.gses.activiti.forms.types.AttachmentType;
import ru.codeinside.gses.activiti.ftarchive.AttachmentFileValue;
import ru.codeinside.gses.beans.ExecutorServiceProvider;
import ru.codeinside.gws.api.Enclosure;
import ru.codeinside.smev.v3.service.api.UUID;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ProcessInstanceAttachmentConverter implements AttachmentConverter {

    private final String processInstanceId;

    public ProcessInstanceAttachmentConverter(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    @Override
    public Object convertAttachment(CommandContext commandContext, Object modelValue) {
        if (modelValue instanceof AttachmentFileValue) {
            AttachmentFileValue attachmentFileValue = (AttachmentFileValue) modelValue;
            Attachment attachment = attachmentFileValue.getAttachment();
            if (attachment instanceof TmpAttachment) {
                TmpAttachment tmp = (TmpAttachment) attachment;
                CreateAttachmentCmd createAttachmentCmd = new CreateAttachmentCmd(//
                        tmp.getType(), // attachmentType
                        null, // taskId
                        processInstanceId, // processInstanceId
                        tmp.getName(), // attachmentName
                        tmp.getDescription(), // attachmentDescription
                        null, // content
                        null // url
                );
                attachment = createAttachmentCmd.execute(commandContext);
                try {
                    ExecutorServiceProvider.get().saveFileContent(attachment.getId(), new FileInputStream(tmp.getFile()));
                } catch (FileNotFoundException e) {
                    throw new RuntimeException("Не удалось сохранить файл вложения: "+ e.getMessage());
                }
            }
            modelValue = attachment.getId() + AttachmentType.SUFFIX;
        } else if (modelValue instanceof FileValue) {
            FileValue fileValue = (FileValue) modelValue;
            CreateAttachmentCmd createAttachmentCmd = new CreateAttachmentCmd(//
                    fileValue.getMimeType(), // attachmentType
                    null, // taskId
                    processInstanceId, // processInstanceId
                    fileValue.getFileName(), // attachmentName
                    UUID.generate(), // attachmentDescription
                    null, // content
                    null // url
            );
            Attachment attachment = createAttachmentCmd.execute(commandContext);
            ExecutorServiceProvider.get().saveFileContent(attachment.getId(), fileValue.getContent().getContent());
            modelValue = attachment.getId() + AttachmentType.SUFFIX;
        } else if (modelValue instanceof Enclosure) {
            Enclosure enclosure = (Enclosure) modelValue;
            CreateAttachmentCmd createAttachmentCmd = new CreateAttachmentCmd(//
                    enclosure.mimeType, // attachmentType
                    null, // taskId
                    processInstanceId, // processInstanceId
                    enclosure.fileName, // attachmentName
                    enclosure.number, // attachmentDescription
                    null, // content
                    null // url
            );
            Attachment attachment = createAttachmentCmd.execute(commandContext);
            ExecutorServiceProvider.get().saveFileContent(attachment.getId(), enclosure.getContent());
            modelValue = attachment.getId() + AttachmentType.SUFFIX;
        }
        return modelValue;
    }
}
