package ru.codeinside.gses.webui.form;

import org.activiti.engine.impl.interceptor.CommandContext;
import ru.codeinside.gses.activiti.FileValue;
import ru.codeinside.gses.activiti.forms.types.AttachmentType;
import ru.codeinside.gses.activiti.ftarchive.AttachmentFileValue;

import java.io.File;

public class StartEventAttachmentConverter implements AttachmentConverter {

    private DataAccumulator dataAccumulator;

    StartEventAttachmentConverter(DataAccumulator dataAccumulator) {
        this.dataAccumulator = dataAccumulator;
    }

    @Override
    public Object convertAttachment(CommandContext commandContext, Object modelValue) {
        if (modelValue instanceof AttachmentFileValue) {
            AttachmentFileValue attachmentFileValue = (AttachmentFileValue) modelValue;
            modelValue = attachmentFileValue.getAttachment().getId() + AttachmentType.SUFFIX;
        } else if (modelValue instanceof FileValue) {
            FileValue fileValue = (FileValue) modelValue;
            TmpAttachment attachment =
                    createTmpAttachment(fileValue.getMimeType(), fileValue.getFileName(), fileValue.getFile());
            dataAccumulator.setAttachment(attachment);
            modelValue = attachment.getId() + AttachmentType.SUFFIX;
        }
        return modelValue;
    }

    public static TmpAttachment createTmpAttachment(String attachmentType, String attachmentName, File file, String number) {
        TmpAttachment attachment = createTmpAttachment(attachmentType, attachmentName, file);
        attachment.setDescription(number);
        return attachment;
    }

    private static TmpAttachment createTmpAttachment(String attachmentType, String attachmentName, File file) {
        TmpAttachment attachment = new TmpAttachment();
        attachment.setType(attachmentType);
        attachment.setName(attachmentName);
        attachment.setFile(file);
        return attachment;
    }
}
