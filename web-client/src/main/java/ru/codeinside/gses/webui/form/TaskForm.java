/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2014, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.gses.webui.form;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vaadin.data.Validator;
import com.vaadin.event.MouseEvents;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.*;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.impl.ServiceImpl;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.apache.commons.lang.StringUtils;
import ru.codeinside.adm.AdminServiceProvider;
import ru.codeinside.adm.database.Employee;
import ru.codeinside.adm.database.Organization;
import ru.codeinside.gses.API;
import ru.codeinside.gses.activiti.forms.FormID;
import ru.codeinside.gses.form.FormEntry;
import ru.codeinside.gses.service.BidID;
import ru.codeinside.gses.service.F2;
import ru.codeinside.gses.service.Fn;
import ru.codeinside.gses.service.Functions;
import ru.codeinside.gses.webui.Flash;
import ru.codeinside.gses.webui.components.api.WithTaskId;
import ru.codeinside.gses.webui.eventbus.TaskChanged;
import ru.codeinside.gses.webui.utils.Components;
import ru.codeinside.gses.webui.wizard.Wizard;
import ru.codeinside.gses.webui.wizard.WizardStep;
import ru.codeinside.gses.webui.wizard.event.*;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.vaadin.ui.Window.Notification.TYPE_ERROR_MESSAGE;
import static com.vaadin.ui.Window.Notification.TYPE_WARNING_MESSAGE;
import java.nio.charset.Charset;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import ru.codeinside.adm.database.Procedure;
import ru.codeinside.adm.database.ProcedureProcessDefinition;
import ru.codeinside.gses.lazyquerycontainer.LazyQueryContainer;
import ru.codeinside.gses.service.DeclarantServiceProvider;

final public class TaskForm extends VerticalLayout implements WithTaskId {

  private static final long serialVersionUID = 1L;
  final static ThemeResource EDITOR_ICON = new ThemeResource("../custom/icon/linedpaperpencil32.png");
  final static ThemeResource VIEW_ICON = new ThemeResource("../custom/icon/paper32.png");
  final static ThemeResource PRESET_ICON = new ThemeResource("../custom/icon/linedpaperplus32.png");
  final static ThemeResource SAVE_ICON = new ThemeResource("../custom/icon/linedpapersave32.png");
  final static ThemeResource DELETE_PRESET_ICON = new ThemeResource("../custom/icon/linedpaperminus32.png");

  private final FormID id;
  private final ImmutableList<FormSeq> flow;
  private final FormFlow formFlow;
  private final CloseListener closeListener;
  private final DataAccumulator accumulator;

  final Wizard wizard;

  private Logger log = Logger.getLogger(TaskForm.class.getName());

  public interface CloseListener extends Serializable {
    void onFormClose(TaskForm form);
  }

  final Embedded editorIcon;
  final HorizontalLayout header;
  final Embedded viewIcon;
  final Embedded presetIcon;
  final Embedded dePresIcon;

  Component mainContent;

  public TaskForm(final FormDescription formDesc, final CloseListener closeListener, final DataAccumulator accumulator) {
    this.closeListener = closeListener;
    flow = formDesc.flow;
    id = formDesc.id;
    formFlow = new FormFlow(id);
    this.accumulator = accumulator;

    header = new HorizontalLayout();
    header.setWidth(100, UNITS_PERCENTAGE);
    header.setSpacing(true);
    addComponent(header);
    
    final Procedure currentProcedure = 
            AdminServiceProvider.get().getProcedureByProcessDefinitionId(formDesc.processDefinition.getId());
    
    editorIcon = new Embedded(null, EDITOR_ICON);
    editorIcon.setDescription("Редактирование");
    editorIcon.setStyleName("icon-inactive");
    editorIcon.setHeight(32, UNITS_PIXELS);
    editorIcon.setWidth(32, UNITS_PIXELS);
    editorIcon.setImmediate(true);
    header.addComponent(editorIcon);
    header.setComponentAlignment(editorIcon, Alignment.BOTTOM_CENTER);
    
    if (flow.get(0) instanceof FormDataSource) {
      viewIcon = new Embedded(null, VIEW_ICON);
      viewIcon.setDescription("Предварительный просмотр");
      viewIcon.setStyleName("icon-inactive");
      viewIcon.setHeight(32, UNITS_PIXELS);
      viewIcon.setWidth(32, UNITS_PIXELS);
      viewIcon.setImmediate(true);
      header.addComponent(viewIcon);
      header.setComponentAlignment(viewIcon, Alignment.BOTTOM_CENTER);

      editorIcon.addListener(new MouseEvents.ClickListener() {
        @Override
        public void click(MouseEvents.ClickEvent event) {
          if (mainContent != wizard) {
            TaskForm.this.replaceComponent(mainContent, wizard);
            TaskForm.this.setExpandRatio(wizard, 1f);
            mainContent = wizard;
            editorIcon.setStyleName("icon-inactive");
            viewIcon.setStyleName("icon-active");
          }
        }
      });

      viewIcon.addListener(new MouseEvents.ClickListener() {
        @Override
        public void click(MouseEvents.ClickEvent event) {
          if (mainContent == wizard && flow.get(0) instanceof FormDataSource) {
            FormDataSource dataSource = (FormDataSource) flow.get(0);

            Map<String, String> response = null;
            String serviceLocation = AdminServiceProvider.get().getSystemProperty(API.PRINT_TEMPLATES_SERVICE_LOCATION);
            String json = buildJsonStringWithFormData(dataSource);

            boolean responseContainsTypeKey = false;
            if (serviceLocation != null &&
                !serviceLocation.isEmpty() &&
                json != null &&
                !json.isEmpty()) {

              response = callPrintService(serviceLocation, json);
              if (response != null && response.containsKey("type")) {
                responseContainsTypeKey = true;
                log.info("PRINT SERVICE. Response type: " + response.get("type"));
              } else {
                log.info("PRINT SERVICE. Response type: null");
              }
            }

            PrintPanel printPanel;
            if (responseContainsTypeKey &&
                response.get("type").equals("success") &&
                response.get("content") != null &&
                !response.get("content").isEmpty()) {
              printPanel = new PrintPanel(response.get("content"), getApplication());
            } else {              
                if (currentProcedure.getParentId().isEmpty()){
                    printPanel = new PrintPanel(dataSource, getApplication(), formDesc.procedureName, formDesc.processDefinition.getId(), AdminServiceProvider.get().getBidByTask(id.taskId));
                } else {
                    Procedure parent = 
                        AdminServiceProvider.get().getProcedureById(currentProcedure.getParentId());
                    ProcedureProcessDefinition pd = DeclarantServiceProvider.get().getProcessDefinition(parent);
                    printPanel = new PrintPanel(dataSource, getApplication(), parent.getName(), pd.getProcessDefinitionId(), AdminServiceProvider.get().getBidByTask(id.taskId));
                }
            }

            TaskForm.this.replaceComponent(wizard, printPanel);
            TaskForm.this.setExpandRatio(printPanel, 1f);
            mainContent = printPanel;
            
            editorIcon.setStyleName("icon-active");
            viewIcon.setStyleName("icon-inactive");
            editorIcon.setVisible(true);
          }
        }
      });
      viewIcon.setStyleName("icon-active");
      viewIcon.setVisible(true);      
    } else {
        viewIcon = new Embedded(null, VIEW_ICON);
        viewIcon.setDescription("Предварительный просмотр");
        viewIcon.setStyleName("icon-inactive");
        viewIcon.setHeight(32, UNITS_PIXELS);
        viewIcon.setWidth(32, UNITS_PIXELS);
        viewIcon.setImmediate(true);
        header.addComponent(viewIcon);
        header.setComponentAlignment(viewIcon, Alignment.BOTTOM_CENTER);

        editorIcon.addListener(new MouseEvents.ClickListener() {
            @Override
            public void click(MouseEvents.ClickEvent event) {
                if (mainContent != wizard) {
                    TaskForm.this.replaceComponent(mainContent, wizard);
                    TaskForm.this.setExpandRatio(wizard, 1f);
                    mainContent = wizard;
                    editorIcon.setStyleName("icon-inactive");
                    viewIcon.setStyleName("icon-active");
                }
            }
        });

        viewIcon.addListener(new MouseEvents.ClickListener() {
            @Override
            public void click(MouseEvents.ClickEvent event) {
            if (mainContent == wizard && flow.get(0) instanceof EFormBuilder) {
                EFormBuilder formBuilder = (EFormBuilder) flow.get(0);
                PrintPanel printPanel;
                if (currentProcedure.getParentId().isEmpty()){
                    printPanel = new PrintPanel(formBuilder.eForm, getApplication(), formDesc.procedureName,
                            formDesc.processDefinition.getId(), AdminServiceProvider.get().getBidByTask(id.taskId));
                } else {
                    Procedure parent =
                            AdminServiceProvider.get().getProcedureById(currentProcedure.getParentId());
                    ProcedureProcessDefinition pd = DeclarantServiceProvider.get().getProcessDefinition(parent);
                    printPanel = new PrintPanel(formBuilder.eForm, getApplication(), formDesc.procedureName,
                            pd.getProcessDefinitionId(), AdminServiceProvider.get().getBidByTask(id.taskId));
                }

                TaskForm.this.replaceComponent(wizard, printPanel);
                TaskForm.this.setExpandRatio(printPanel, 1f);
                mainContent = printPanel;

                editorIcon.setStyleName("icon-active");
                viewIcon.setStyleName("icon-inactive");
                editorIcon.setVisible(true);
            }
            }
        });
        viewIcon.setStyleName("icon-active");
        viewIcon.setVisible(true);     
    }
    
    presetIcon = new Embedded(null, PRESET_ICON);    
    presetIcon.setStyleName("icon-inactive");
    presetIcon.setHeight(32, UNITS_PIXELS);
    presetIcon.setWidth(32, UNITS_PIXELS);
    dePresIcon = new Embedded(null, DELETE_PRESET_ICON);    
    dePresIcon.setStyleName("icon-inactive");
    dePresIcon.setHeight(32, UNITS_PIXELS);
    dePresIcon.setWidth(32, UNITS_PIXELS);
    
    if (formDesc.task == null) {       
        if (currentProcedure.getParentId().isEmpty()) {            
            presetIcon.setDescription("Добавить шаблон");
            presetIcon.setSource(PRESET_ICON);
            presetIcon.addListener(new MouseEvents.ClickListener() {
            @Override
            public void click(MouseEvents.ClickEvent event) {
                
            if (checkForm()) {    
                    final Window mainWindow = getWindow();
                    final Window wnd = new Window();
                    wnd.setWidth("50%");
                    wnd.center();
                    wnd.setCaption("Введите название шаблона");                               
                    final VerticalLayout verticalLayout = new VerticalLayout();
                    verticalLayout.setSpacing(true);
                    verticalLayout.setMargin(true);                
                    final TextArea textArea = new TextArea();
                    textArea.setSizeFull();
                    HorizontalLayout buttons = new HorizontalLayout();
                    buttons.setSpacing(true);
                    buttons.setSizeFull();
                    final Button ok = new Button("Ok");
                    Button cancel = new Button("Cancel");

                    buttons.addComponent(ok);
                    buttons.addComponent(cancel);
                    buttons.setExpandRatio(ok, 0.99f);
                    verticalLayout.addComponent(textArea);
                    verticalLayout.addComponent(buttons);
                    verticalLayout.setExpandRatio(textArea, 0.99f);
                    wnd.setContent(verticalLayout);
                    mainWindow.addWindow(wnd);

                    ok.addListener(new Button.ClickListener() {
                        @Override
                        public void buttonClick(Button.ClickEvent event) {
                            if (mainContent == wizard && flow.get(0) instanceof FormDataSource) {
                                FormDataSource dataSource = (FormDataSource) flow.get(0);

                                Map<String, String> elems = getElementsSimple(dataSource);
                                DeclarantServiceProvider.get().savePreset(elems, currentProcedure, Flash.login(), (String) textArea.getValue());
                                getWindow().showNotification("Шаблон сохранен", Window.Notification.TYPE_HUMANIZED_MESSAGE);
                
                                refreshPresetContainer(false);
                            }
    //html форм на вводе нет, и не известно будут ли
    //                        if (mainContent == wizard && flow.get(0) instanceof EFormBuilder) {
    //                            EFormBuilder formBuilder = (EFormBuilder) flow.get(0);
    //                            Map<String, EField> eElems = formBuilder.eForm.fields;
    //                            getWindow().showNotification("Е-форма");
    //                        }
                            mainWindow.removeWindow(wnd);
                        }
                    });

                    cancel.addListener(new Button.ClickListener() {
                        @Override
                        public void buttonClick(Button.ClickEvent event) {
                            mainWindow.removeWindow(wnd);
                        }
                    });          
                }
            }
            });
        } else {
            presetIcon.setDescription("Изменить шаблон");
            presetIcon.setSource(SAVE_ICON);
            presetIcon.addListener(new MouseEvents.ClickListener() {
            @Override
            public void click(MouseEvents.ClickEvent event) {
                if (mainContent == wizard && flow.get(0) instanceof FormDataSource && checkForm()) {
                    FormDataSource dataSource = (FormDataSource) flow.get(0);
                    Map<String, String> elems = getElementsSimple(dataSource);
                    Procedure realParent = 
                        AdminServiceProvider.get().getProcedureById(currentProcedure.getParentId());
                    DeclarantServiceProvider.get().savePreset(elems, realParent, Flash.login(), currentProcedure.getName());
                    getWindow().showNotification("Шаблон изменен", Window.Notification.TYPE_HUMANIZED_MESSAGE);
                }                       
            }
            });
        }
        presetIcon.setImmediate(true);
        header.addComponent(presetIcon);
        header.setComponentAlignment(presetIcon, Alignment.BOTTOM_CENTER);       
        presetIcon.setStyleName("icon-active");
        presetIcon.setVisible(true);
        
        if (!currentProcedure.getParentId().isEmpty()) {
            dePresIcon.setDescription("Удалить шаблон");
            dePresIcon.addListener(new MouseEvents.ClickListener() {
                @Override
                public void click(MouseEvents.ClickEvent event) {
                    if (mainContent == wizard && flow.get(0) instanceof FormDataSource) {                    
                        DeclarantServiceProvider.get().archPreset(currentProcedure);                    
                        refreshPresetContainer(true);
                    }
                }
            });
            dePresIcon.setImmediate(true);
            header.addComponent(dePresIcon);
            header.setComponentAlignment(dePresIcon, Alignment.BOTTOM_CENTER);       
            dePresIcon.setStyleName("icon-active");
            dePresIcon.setVisible(true);
        }
        
    }

    VerticalLayout labels = new VerticalLayout();
    labels.setSpacing(true);
    header.addComponent(labels);
    header.setComponentAlignment(labels, Alignment.MIDDLE_LEFT);
    header.setExpandRatio(labels, 1f);
    addLabel(labels, formDesc.procedureName, "h1");
    addLabel(labels, formDesc.processDefinition.getDescription(), null);
    if (formDesc.task != null) {
      addLabel(labels, formDesc.task.getName(), "h2");
      addLabel(labels, formDesc.task.getDescription(), null);
    }
    
    wizard = new Wizard(accumulator);
    wizard.setImmediate(true);
    wizard.addListener(new ProgressActions());
    for (FormSeq seq : flow) {
      wizard.addStep(new FormStep(seq, formFlow, wizard));
    }
    mainContent = wizard;
    addComponent(wizard);
    setExpandRatio(wizard, 1f);
    if (formDesc.task != null) {
      setMargin(true);
    } else {
      // у заявителя уже есть обрамление
      setMargin(true, false, false, false);
    }
    setImmediate(true);
    setSpacing(true);
    setSizeFull();
    setExpandRatio(wizard, 1f);
  }

  private void refreshPresetContainer(Boolean dropSelection) {
    VerticalLayout vl = ((VerticalLayout) mainContent.getParent().getParent());
    Panel pnl = (Panel) vl.getComponent(0);
    for(Iterator<Component> i = pnl.getComponentIterator(); i.hasNext();) {
        Component c = i.next();
        if (c instanceof FormLayout) {                            
            Select s = (Select) ((FormLayout) c).getComponent(1);
            if (s.getCaption().equals("Шаблон")){
                if (dropSelection) {
                    s.select(null);
                    vl.removeComponent(vl.getComponent(1));
                }
                ((LazyQueryContainer) s.getContainerDataSource()).refresh();
            }                            
        }
    }
  }
  private boolean checkForm() {
    Form form = flow.get(0).getForm(id, null);
    try {
        form.validate();  
        return true;
    } catch (Validator.InvalidValueException e) {
        String msg = e.getMessage();
        if (StringUtils.isBlank(msg)) {
          if (e instanceof Validator.EmptyValueException) {
            msg = "Заполните все поля, отмеченные красной звёздочкой!";
          } else {
            msg = "Необходимо исправить ошибки!";
          }
        }
        boolean html = msg.contains("<br/>");
        form.getWindow().showNotification(null, msg, Window.Notification.TYPE_HUMANIZED_MESSAGE, html);
        return false;
    }
  }
  

  private void addLabel(ComponentContainer container, String text, String style) {
    text = StringUtils.trimToNull(text);
    if (text != null) {
      Label label = new Label(text);
      if (style != null) {
        label.setStyleName(style);
      }
      container.addComponent(label);
    }
  }

  @Override
  public String getTaskId() {
    return id.taskId;
  }

  void complete() {
    try {
      final boolean processed;
      final BidID bidID;
      if (id.taskId == null) {
        processed = true;
        bidID = Functions.withEngine(new StartTaskFormSubmitter(id.processDefinitionId, formFlow.getForms(), accumulator));
      } else {
        bidID = null;
        processed = Functions.withEngine(new TaskFormSubmitter(id.taskId, formFlow.getForms(), accumulator));
      }
      Flash.fire(new TaskChanged(this, id.taskId));
      if (!processed) {
        getWindow().showNotification("Ошибка", "Этап уже обработан или передан другому исполнителю", TYPE_ERROR_MESSAGE);
      }
      if (bidID != null) {
        getWindow().showNotification("Заявка " + bidID.bidId + " подана!", TYPE_WARNING_MESSAGE);
      }
      close();
    } catch (Exception e) {
      Components.showException(getWindow(), e);
    }
  }


  void close() {
    if (closeListener != null) {
      closeListener.onFormClose(this);
    }
  }

  final class ProgressActions implements WizardProgressListener {

    @Override
    public void activeStepChanged(WizardStepActivationEvent event) {
        List<WizardStep> steps = event.getWizard().getSteps();
        WizardStep step = event.getActivatedStep();
        if (viewIcon != null) {        
            viewIcon.setVisible(steps.indexOf(step) == 0);
        }
        if (presetIcon != null) {        
            presetIcon.setVisible(steps.indexOf(step) == 0);
        }
        if (dePresIcon != null) {        
            dePresIcon.setVisible(steps.indexOf(step) == 0);
        }
    }

    @Override
    public void stepSetChanged(WizardStepSetChangedEvent unusedEvent) {

    }

    @Override
    public void wizardCancelled(WizardCancelledEvent unusedEvent) {
      close();
    }

    @Override
    public void wizardCompleted(WizardCompletedEvent unusedEvent) {
      complete();
    }
  }

  private String buildJsonStringWithFormData(FormDataSource dataSource) {
    String taskId = getTaskId();

    String procedureCode = getProcedureCode(taskId);
    String organizationId = getOrganizationId();
    String userTaskId = null;

    if (taskId != null && !taskId.isEmpty()) {
      userTaskId = getUserTaskId(taskId);
    }

    List<Map<String, String>> elements = getElements(dataSource);

    Map<String, Object> data = new LinkedHashMap<String, Object>();
    data.put("procedure_id", procedureCode);
    data.put("organization_id", organizationId);
    data.put("task_id", userTaskId);
    data.put("elements", elements);

    Gson gson = new Gson();

    return gson.toJson(data);
  }

  private List<Map<String, String>> getElements(FormDataSource dataSource) {
    List<Map<String, String>> result = new LinkedList<Map<String, String>>();

    FormEntry formEntry = dataSource.createFormTree();
    FormEntry[] children = formEntry.children;

    if(children != null) {
        for (FormEntry childEntry : children) {
            Map<String, String> element = new LinkedHashMap<String, String>();
            element.put("name", childEntry.id);
            element.put("value", (childEntry.value == null || "value".equals(childEntry.value)) ? "" : childEntry.value);
            result.add(element);
        }
    }

    return result;
  }
  
  private Map<String, String> getElementsSimple(FormDataSource dataSource) {
    FormEntry formEntry = dataSource.createFormTree();
    FormEntry[] children = formEntry.children;
    Map<String, String> result = new LinkedHashMap<String, String>();    
    if(children != null) {
        for (FormEntry childEntry : children) {      
            result.put(
                    childEntry.id, 
                    (childEntry.value == null || "value".equals(childEntry.value)) ? "" : childEntry.value
            );           
        }
    }
    return result;
  }

  private Map<String, String> callPrintService(String serviceLocation, String json) {
    HttpURLConnection connection = null;

    Map<String, String> result = null;
    try {
      URL url = new URL(serviceLocation);
      connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("POST");
      connection.setDoOutput(true);
      connection.setConnectTimeout(5000);
      connection.setReadTimeout(5000);

      String postParameters = "data=" + json;
      OutputStream os = new BufferedOutputStream(connection.getOutputStream());
      os.write(postParameters.getBytes("UTF-8"));
      os.flush();
      os.close();

      BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));

      log.info("PRINT SERVICE. Response code: " + connection.getResponseCode());

      String line;
      StringBuffer stringBuffer = new StringBuffer();
      while ((line = input.readLine()) != null) {
        stringBuffer.append(line);
      }
      input.close();

      result = new Gson().fromJson(stringBuffer.toString(), new TypeToken<Map<String, String>>() {
      }.getType());
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (connection != null) {
        connection.disconnect();
      }
    }

    return result;
  }

  private String getProcedureCode(String taskId) {
    if (taskId != null && !taskId.isEmpty()) {
      return String.valueOf(AdminServiceProvider.get().getBidByTask(taskId).getProcedure().getRegisterCode());
    } else if (id.processDefinitionId != null && !id.processDefinitionId.isEmpty()){
      return String.valueOf(AdminServiceProvider.get().getProcedureCodeByProcessDefinitionId(id.processDefinitionId));
    } else {
      return null;
    }
  }

  private String getOrganizationId() {
    Employee user = AdminServiceProvider.get().findEmployeeByLogin(Flash.login());
    Organization organization = user.getOrganization();
    if (organization != null) {
      return organization.getId().toString();
    } else {
      return null;
    }
  }

  private String getUserTaskId(String taskId) {
    return Fn.withEngine(new GetUserTaskId(), Flash.login(), taskId);
  }

  final private static class GetUserTaskId implements F2<String, String, String> {
    @Override
    public String apply(ProcessEngine engine, String login, String taskId) {

      CommandExecutor commandExecutor = ((ServiceImpl) engine.getFormService()).getCommandExecutor();
      return String.valueOf(commandExecutor.execute(new GetUserTaskIdCommand(taskId)));
    }

    final private static class GetUserTaskIdCommand implements Command {
      private final String taskId;

      GetUserTaskIdCommand(String taskId) {
        this.taskId = taskId;
      }

      @Override
      public Object execute(CommandContext commandContext) {
        String processInstanceId = AdminServiceProvider.get().getBidByTask(taskId).getProcessInstanceId();


        ExecutionEntity execution = Context.getCommandContext()
            .getExecutionManager()
            .findExecutionById(processInstanceId);

        return execution.getActivity().getId();
      }
    }
  }
}
