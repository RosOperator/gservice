package ru.codeinside.gses.webui.form;

import org.activiti.engine.task.Attachment;
import ru.codeinside.smev.v3.service.api.UUID;

import java.io.File;
import java.io.Serializable;

public class TmpAttachment implements Attachment, Serializable {

  private static final long serialVersionUID = 1L;

  private final String id;
  private String name;
  private String description;
  private String type;
  private File file;

  public TmpAttachment() {
    id = UUID.generate();
    description = id;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String getTaskId() {
    return null;
  }

  @Override
  public String getProcessInstanceId() {
    return null;
  }

  @Override
  public String getUrl() {
    return null;
  }

  public void setFile(File file) {
    this.file = file;
  }

  public File getFile() {
    return file;
  }
}
