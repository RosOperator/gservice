package ru.codeinside.gses.webui.gws;

import ru.codeinside.smev.v3.service.api.consumer.Consumer;

import java.util.List;

public interface ConsumerRefRegistry {

  List<TRef<Consumer>> getConsumerRefs();

  TRef<Consumer> getConsumerByName(String name);
}
