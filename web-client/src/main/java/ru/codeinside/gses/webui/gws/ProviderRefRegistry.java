package ru.codeinside.gses.webui.gws;

import ru.codeinside.smev.v3.service.api.provider.Provider;

import java.util.List;

public interface ProviderRefRegistry {

  List<TRef<Provider>> getProviderRefs();

  TRef<Provider> getProviderByName(String name);
}
