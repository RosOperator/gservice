/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2013, MPL CodeInside http://codeinside.ru
 */

package ru.codeinside.gses.webui.gws;

import ru.codeinside.gses.webui.osgi.TRefRegistryImpl;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

@Singleton
@Startup
public class TRefProvider {

  @Inject
  ServiceRefRegistry serviceRegistry;

  @Inject
  ClientRefRegistry clientRegistry;

  @Inject
  ConsumerRefRegistry consumerRegistry;

  @Inject
  ProviderRefRegistry providerRegistry;

  transient static ServiceRefRegistry serviceRefInstance;
  transient static ClientRefRegistry clientRefInstance;
  transient static ProviderRefRegistry providerRefInstance;
  transient static ConsumerRefRegistry consumerRefInstance;

  @PostConstruct
  void initialize() {
    synchronized (TRefRegistryImpl.class) {
      if (serviceRefInstance == null) {
        serviceRefInstance = serviceRegistry;
      }
      if (clientRefInstance == null) {
        clientRefInstance = clientRegistry;
      }
      if (providerRefInstance == null) {
        providerRefInstance = providerRegistry;
      }
      if (consumerRefInstance == null) {
        consumerRefInstance = consumerRegistry;
      }
    }
  }

  @PreDestroy
  void shutdown() {
    synchronized (TRefRegistryImpl.class) {
      if (serviceRefInstance == serviceRegistry) {
        serviceRefInstance = null;
      }
      if (clientRefInstance == clientRegistry) {
        clientRefInstance = null;
      }
      if (providerRefInstance == providerRegistry) {
        providerRefInstance = null;
      }
      if (consumerRefInstance == consumerRegistry) {
        consumerRefInstance = null;
      }
    }
  }

  public static ServiceRefRegistry getServiceRefRegistry() {
    return validateReference(serviceRefInstance);
  }

  public static ClientRefRegistry getClientRefRegistry() {
    return validateReference(clientRefInstance);
  }

  public static ProviderRefRegistry getProviderRefRegistry() {
    return validateReference(providerRefInstance);
  }

  public static ConsumerRefRegistry getConsumerRefRegistry() {
    return validateReference(consumerRefInstance);
  }

  private static <T> T validateReference(T service) {
    if (service == null) {
      throw new IllegalStateException("Сервис не зарегистрирован!");
    }
    return service;
  }
}
