package ru.codeinside.gses.webui.osgi;

import com.google.common.base.Preconditions;
import org.osgi.framework.ServiceReference;

public final class OSGiUtils {
    public static <T> T service(Class<T> clazz) {
        Preconditions.checkNotNull(clazz);
        ServiceReference serviceReference = null;
        try {
            serviceReference = Activator.getContext().getServiceReference(clazz.getName());
            Preconditions.checkNotNull(serviceReference, "Сервис " + clazz.getSimpleName() + " недоступен.");
            T service = clazz.cast(Activator.getContext().getService(serviceReference));
            Preconditions.checkNotNull(service, "Объект сервиса " + clazz.getSimpleName() + " не получен.");
            return service;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (serviceReference != null) {
                Activator.getContext().ungetService(serviceReference);
            }
        }
    }
}
