package ru.codeinside.log;

import ru.codeinside.adm.UserItem;
import ru.codeinside.adm.database.Employee;
import ru.codeinside.adm.database.Organization;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Логирует импорт организаций и пользователей
 */
public class ImportLogger {

    private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger(SignatureLogger.class.getName());
    private static final String basePath = System.getProperty("com.sun.aas.instanceRoot");

    public ImportLogger() {
    }

    public void log(boolean success, String data, Organization org, Employee employee) {

        File logFile;
        try {
            if(success) {
                logFile = prepareSuccessLog();
            } else {
                logFile = prepareErrorLog();
            }
        } catch (IOException e) {
            logger.severe("IOException when prepare to logging: " + e);
            return;
        }

        try {
            writeToFile(logFile, data, org, employee);
        } catch (IOException e) {
            logger.severe("IOException when write data: " + e);
        }
    }

    private void writeToFile(File logFile, String data, Organization org, Employee employee) throws IOException {
        Writer fw = null;
        try {
            fw = new FileWriter(logFile, true)
                .append(String.format("%s %s%n", new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date()), data));
            if (org != null) {
                fw.append(String.format("Наименование: %s ИНН: %s%n%n", org.getName(), org.getInn()));
            }
            if (employee != null) {
                fw.append(String.format("Логин: %s ФИО: %s Наименование организации: %s ИНН: %s%n%n",
                        employee.getLogin(), employee.getFio(), employee.getOrganization().getName(), employee.getOrganization().getInn()));
            }
        } finally {
            if (fw != null) {
                fw.flush();
                fw.close();
            }
        }
    }

    private File prepareSuccessLog() throws IOException {
        File logsDir = new File(basePath, "logs");
        File importFile = new File(logsDir, "import");
        if (!importFile.exists()) {
            importFile.mkdirs();
        }

        String fileName = "import-succeed.log";
        File successFile = new File(importFile, fileName);
        successFile.createNewFile();
        return successFile;
    }

    private File prepareErrorLog() throws IOException {
        File logsDir = new File(basePath, "logs");
        File importFile = new File(logsDir, "import");
        if (!importFile.exists()) {
            importFile.mkdirs();
        }

        String fileName = "import-error.log";
        File errorFile = new File(importFile, fileName);
        errorFile.createNewFile();
        return errorFile;
    }
}
