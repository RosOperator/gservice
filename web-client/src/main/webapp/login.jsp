<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="ru.codeinside.adm.AdminServiceProvider" %>
<%@ page import="ru.codeinside.adm.database.News" %>
<%@ page import="ru.codeinside.gses.API" %>
<%@ page import="java.util.Iterator" %>
<%--
  ~ This Source Code Form is subject to the terms of the Mozilla Public
  ~ License, v. 2.0. If a copy of the MPL was not distributed with this
  ~ file, You can obtain one at http://mozilla.org/MPL/2.0/.
  ~ Copyright (c) 2013, MPL CodeInside http://codeinside.ru
  --%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
    <meta content="telephone=no" name="format-detection"/>
    <title>Вход</title>
</head>
<body style="background-color: #004375; font-family: 'Calibry', 'Arial', 'serif'; color: #ffffff">
    <jsp:useBean id="news" scope="session" class="java.util.Vector"/>
    <div class="header">
        <div style="width: 50%; float: left;">
            <div style="margin-right: 150px;">
                <div class="header-rkis-label" style="float: right">РКИС «ГУ-ЯО»</div>
                <div class="header-rkis-icon"></div>
            </div>
        </div>
        <div style="width: 50%;  float: right;">
            <div class="header-gbu-label">
                <div class="header-operator-label">Оператор электронного правительства</div>
                <div>ГБУ ЯО «Электронный регион»</div>
            </div>
            <div class="header-phone-label">8 (4852) 49-09-49</div>
        </div>
    </div>

    <div style="min-width: 960px;">
        <div style="width: 50%; float: left;">
            <form method="post" action="/web-client/authServlet">
                <div style="height: 130px;">
                    <div class="body-login-icon" style="margin: 72px 12px 0px 20px;"></div>
                    <div class="body-login-label" style="float: right;">Авторизация пользователя</div>
                </div>
                <div style="height: 67px;">
                    <input class="body-login-input" type="text" name="username" placeholder="Логин" style="float: right"/>
                </div>
                <div style="height: 68px;">
                    <input class="body-password-input" type="password" name="password" placeholder="Пароль" style="float: right"/>
                </div>
                <div style="height: 63px;">
                    <button type="submit" class="body-button" style="float: right">Вход <span class="body-button-icon"></span></button>
                </div>
            </form>
            <%
                String allowEsia = AdminServiceProvider.get().getSystemProperty(API.ALLOW_ESIA_LOGIN);
                String serviceAddress = AdminServiceProvider.get().getSystemProperty(API.ESIA_SERVICE_ADDRESS);
                if ("true".equals(allowEsia) && serviceAddress!= null && !serviceAddress.isEmpty()) {
            %>
            <form method="post" action="/web-client/esiaServlet">
                <div style="height: 68px;">
                    <div class="body-login-icon" style="margin: 11px 12px 0px 20px;"></div>
                    <div class="body-esia-label" style="margin: 6px 0px 0px 20px; float: right">Вход через ЕСИА</div>
                </div>
                <div>
                    <button type="submit" class="body-button" style="float: right">Вход <span class="body-button-icon"></span></button>
                </div>
            </form>
            <%
                }
            %>
        </div>

        <div style="width: calc(50% - 105px); float: right; margin-right: 105px;">
            <div style="height: 125px;">
                <div class="body-login-label" style="margin-left: 70px;">Новости</div>
                <div class="body-news-icon" style="margin: 72px 12px 0px 19px;"></div>
            </div>
            <div id="news" class="body-news">
                <%
                    for (Iterator i = news.iterator(); i.hasNext(); ) {
                        News currentNews = (News) i.next();
                %>
                    <div style="margin-left: 70px; margin-bottom: 20px;">
                        <span class="body-news-date-label"><%=currentNews.getDateCreated2()%></span>
                        <br>
                        <span style="font-size: 16pt;"><b><%=currentNews.getTitle()%></b></span>
                        <br>
                        <span style="font-size: 14pt;"><%=currentNews.getText()%></span>
                    </div>
                <% } %>
            </div>
        </div>
    </div>
    </table>
<!-- Scripts -->
<script src="/web-client/scripts/jquery/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="/web-client/scripts/jquery/jquery.maskedinput.min.js" type="text/javascript"></script>
<script src="/web-client/scripts/jquery/jquery.nicescroll.js" type="text/javascript"></script>
<script src="/web-client/scripts/auth-form.js" type="text/javascript"></script>
<link href="/web-client/css/auth.css" rel="stylesheet" type="text/css">
</body>
</html>