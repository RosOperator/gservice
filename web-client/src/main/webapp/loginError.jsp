<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%--
  ~ This Source Code Form is subject to the terms of the Mozilla Public
  ~ License, v. 2.0. If a copy of the MPL was not distributed with this
  ~ file, You can obtain one at http://mozilla.org/MPL/2.0/.
  ~ Copyright (c) 2013, MPL CodeInside http://codeinside.ru
  --%>

<html xmlns="[http://www.w3.org/1999/xhtml]" xml:lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>Ошибка входа</title>
</head>
<body style="background-color: #004375; font-family: 'Calibry', 'Arial', 'serif'; color: #ffffff">
<%
	String errorType = request.getAttribute("error").toString();
%>
	<div class="header">
		<div style="width: 50%; float: left;">
			<div style="margin-right: 150px;">
				<div class="header-rkis-label" style="float: right">РКИС «ГУ-ЯО»</div>
				<div class="header-rkis-icon"></div>
			</div>
		</div>
		<div style="width: 50%;  float: right;">
			<div class="header-gbu-label">
				<div class="header-operator-label">Оператор электронного правительства</div>
				<div>ГБУ ЯО «Электронный регион»</div>
			</div>
			<div class="header-phone-label">8 (4852) 49-09-49</div>
		</div>
	</div>
	<div>
		<div class="warning-icon"></div>
		<%
			if("snilsError".equals(errorType)) {
		%>
		<form method="post" action="${pageContext.request.contextPath}/login">
			<div class="warning-error-label" style="margin-top: 30px;">Неправильный СНИЛС или пароль!</div>
			<div class="warning-error-label">Провертье раскладку клавиатуры и режим CAPS LOCK</div>
			<button type="submit" class="warning-button">Повторить попытку</button>
		</form>
		<%
		} else if ("esiaError".equals(errorType)) {
		%>
		<form method="post" action="${pageContext.request.contextPath}/login">
			<div class="warning-error-label" style="margin-top: 30px;">Ошибка обращения к сервису ЕСИА!</div>
			<div class="warning-error-label">Попробуйте повторить позже</div>
			<button type="submit" class="warning-button">Повторить попытку</button>
		</form>
		<%
		} else if ("notUniqueSnils".equals(errorType)) {
		%>
		<form method="post" action="${pageContext.request.contextPath}/login">
			<div class="warning-error-label" style="margin-top: 30px;">Значение СНИЛС пользователя в системе не уникально</div>
			<div class="warning-error-label">Проверите правильность СНИЛС</div>
			<button type="submit" class="warning-button">Повторить попытку</button>
		</form>
		<%
		} else if ("snilsNotFound".equals(errorType)) {
		%>
		<form method="post" action="${pageContext.request.contextPath}/login">
			<div class="warning-error-label" style="margin-top: 30px;">Учетная запись ЕСИА не привязана к учетной записи пользователя РКИС ГУ-ЯО</div>
			<div class="warning-error-label">Проверите правильность СНИЛС</div>
			<button type="submit" class="warning-button">Повторить попытку</button>
		</form>
		<%
		} else {
		%>
		<form method="post" action="${pageContext.request.contextPath}/login">
			<div class="warning-error-label" style="margin-top: 30px;">Неправильный логин или пароль!</div>
			<div class="warning-error-label">Провертье раскладку клавиатуры и режим CAPS LOCK</div>
			<button type="submit" class="warning-button">Повторить попытку</button>
		</form>
		<%
			}
		%>
	</div>
<link href="/web-client/css/auth.css" rel="stylesheet" type="text/css">
</body>
</html>