<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="ru.codeinside.adm.AdminServiceProvider" %>
<%@ page import="ru.codeinside.gses.API" %>
<%--
  ~ This Source Code Form is subject to the terms of the Mozilla Public
  ~ License, v. 2.0. If a copy of the MPL was not distributed with this
  ~ file, You can obtain one at http://mozilla.org/MPL/2.0/.
  ~ Copyright (c) 2013, MPL CodeInside http://codeinside.ru
  --%>

<%
	if (session.getAttribute("isEsiaAuth") != null) {
		session.invalidate();
	    response.sendRedirect(AdminServiceProvider.get().getSystemProperty(API.ESIA_SERVICE_ADDRESS) + "/logout/?id=" + session.getId());
    } else {
    	session.invalidate();
        response.sendRedirect("login");
    }
%>