/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Copyright (c) 2015, MPL CodeInside http://codeinside.ru
 */
$(function() {

    setNewsHeight ();

    $(window).resize(setNewsHeight);

    $("#news").niceScroll({
        cursorcolor:"#009ee3",
        cursorborderradius: "8px",
        cursorwidth: 12,
        cursorborder: "0px",
        autohidemode: false
    });

    function setNewsHeight () {
        var newsHeight = document.documentElement.clientHeight;
        $("#news").css('height', newsHeight - 270 + "px");
        if(navigator.userAgent.toLowerCase().indexOf('chrome') >= 0) {
            $('input:-webkit-autofill').each(function () {
                $(this).after(this.outerHTML).remove();
                $('input[name=' + $(this).attr('name') + ']').val($(this).val());
            })
        }
    }
});

