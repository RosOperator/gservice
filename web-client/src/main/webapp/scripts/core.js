//=============================================================================
// Утилиты для определения нового javascript-"класса".
//=============================================================================
var Class =
{
	define: function (baseClass)
	{
		function theClass()
		{
			this.initialize.apply(this, arguments);
		}

		theClass.prototype.constructor = theClass;

		if (baseClass)
		{
			var subclass = function ()
			{ };
			subclass.prototype = baseClass.prototype;
			theClass.prototype = new subclass;
			theClass.base = baseClass.prototype;
		}

		if (!theClass.prototype.initialize)
		{
			theClass.prototype.initialize = function ()
			{ };
		}

		return theClass;
	}
};


var core = {};

//=============================================================================
// Вспомогательные утилиты.
//=============================================================================

var $A = function (iterable)
{
	if (!iterable)
		return [];

	var length = iterable.length || 0, results = [];
	while (length--) results[length] = iterable[length];
	return results;
};


Function.prototype.bind = function ()
{
	if (arguments.length < 2 && typeof (arguments[0]) == "undefined")
		return this;

	var __method = this;
	var args = $A(arguments);
	var object = args.shift();

	return function ()
	{ return __method.apply(object, args.concat($A(arguments))); };
};

core.onlyNumbers = function (e)
{
	var keynum;
	var keychar;
	var numcheck;

	if (e &&
	    (e.keyCode == 8 ||
	     e.keyCode == 9 ||
	     e.keyCode == 13 ||
	     e.keyCode == 37 ||
	     e.keyCode == 39 ||
	     e.keyCode == 46)
		)
		return true;

	if (window.event) // IE
	{
		keynum = e.keyCode;
	}
	else if (e.which) // Netscape/Firefox/Opera
	{
		keynum = e.which;
	}
	keychar = String.fromCharCode(keynum);
	numcheck = /\d/;
	return numcheck.test(keychar);
};

core.encodeHtml = function(html)
{
	return html ? html.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;') : html;
};

core.decodeHtml = function(html)
{
	return html ? html.replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>') : html;
};

core.makeObject = function(objectFields)
{
	var result = {};
	for (var i = 0, len = objectFields.length; i < len; ++i)
	{
		var field = objectFields[i];
		result[field.name] = field.value;
	}
	return result;
};

//=============================================================================
// Класс, реализующий пользовательские события (events) для объектов.
// К событию-экземпляру данного класса присоединяются функции-обработчики, 
// которые исполняются при каждом возбуждении (fire) события.
// 
// Пример:
//		var eventProvider = new Object();
//		// создается событие
//		eventProvider.MyEvent = new core.event();
//		// к событию присоединяются два обработчика
//		eventProvider.MyEvent.attachHandler(function(evt) {alert("Handler 1 - " + evt);});
//		eventProvider.MyEvent.attachHandler(function(evt) {alert("Handler 2 - " + evt);});
//		// возбудить событие
//		eventProvider.MyEvent.fire("MyEvent");
//=============================================================================

core.event = function()
{
	this._handlers = [];
};

core.event.prototype.attachHandler = function(handler)
{
	this._handlers.push(handler);
};

core.event.prototype.detachHandler = function(handler)
{
	if (handler === null) return;

	var index = null;
	for (var i = 0; i < this._handlers.length; i++)
	{
		if (this._handlers[i] == handler)
		{
			index = i;
			break;
		}
	}

	if (index !== null)
		this._handlers.splice(index, 1);
};

core.event.prototype.fire = function()
{
	for (var i = 0; i < this._handlers.length; ++i)
	{
		this._handlers[i].apply(this, $A(arguments));
	}
};


//=============================================================================
// Формирование POST-данных, понятных для asp.net mvc model binding'а.
//=============================================================================

var _preparePostData = function(name, value)
{
	// TODO: Значения типа Date пока не поддерживаются.
	if (value === null)
		return "";

	var valueType = typeof(value);
	if (valueType == "number" || valueType == "string" || valueType == "boolean" || value.constructor == Date)
	{
		if (!name || !name.length)
			throw "Post data element name can not be empty.";

		if (value.constructor == Date)
			return name + "=" + _encodeDate(value);
		else
			return name + "=" + value;
	}

	if (value.constructor == Function)
		return "";

	var result = "";
	if (jQuery.isArray(value))
	{
		if (!name || name.length === 0)
			throw "Post data element name can not be empty.";

		for (var i = 0; i < value.length; ++i)
		{
			var data = _preparePostData(name + "[" + i + "]", value[i]);
			if (data && data.length > 0)
			{
				if (result.length !== 0)	result += "&";
				result += data;
			}
		}
		return result;
	}

	for (var prop in value)
	{
		if (prop != "constructor")
		{
			var subName = (name && name.length > 0) ? name + "." + prop : prop;
			var data = _preparePostData(subName, value[prop]);
			if (data && data.length > 0)
			{
				if (result.length !== 0)	result += "&";
				result += data;
			}
		}
	}
	return result;
};

function _encodeDate(date)
{
	var pad = function (n)
	{
		return n < 10 ? "0" + n : n;
	};

	return date.getFullYear() + "-" +
	       pad(date.getMonth() + 1) + "-" +
	       pad(date.getDate()) + "T" +
	       pad(date.getHours()) + ":" +
	       pad(date.getMinutes()) + ":" +
	       pad(date.getSeconds());
}

core.preparePostData = function(name, value)
{
	return _preparePostData(name, value);
};

core.parseFloatToHtml = function(
	value,
	separator,
	separatorCssClass,
	wholeCssClass,
	fractionalCssClass)
{
	if (Ext.isNumber(value))
		value = value.toString();

	var parts = value.split(separator);
	return String.format(
		"<span class='{0}'>{1}</span><span class='{2}'>{3}</span><span class='{4}'>{5}</span>",
		wholeCssClass, parts[0], separatorCssClass, separator, fractionalCssClass, parts[1]);
};

core.isNullOrEmpty = function (str)
{
	return str == null || str == "";
};

// Simple string format like C# string.Format(...).
core.format = function ()
{
	var result = "";
	var first = true;

	for (var i = 0; i < arguments.length; i++)
	{
		if (i === 0)
		{
			result = arguments[i];
		}
		else
		{
			result = result.replace("{" + (i - 1) + "}", arguments[i]);
		}
	}

	return result;
};
// Encoding html text.
core.encodeHtml = function (htmlString)
{
	var result = htmlString.replace(/&/, "&amp;");
	result = result.replace(/</, "&lt;");
	result = result.replace(/<\//, "&lt;&#47;");
	result = result.replace(/>/, "&gt;");
	return result;
};

// Plural formating for string like "oneCase|twoCase|fiveCase".
core.formatPlural = function (casesString, count)
{
	var digit = count % 10;
	var twoDigits = count % 100;

	var cases = casesString.split("|");
	if (cases.length != 3)
		return $.format(casesString, count);

	if (10 <= twoDigits && twoDigits <= 20)
		return $.format(cases[2], count);
	if (digit == 1)
		return $.format(cases[0], count);
	else if (2 <= digit && digit <= 4)
		return $.format(cases[1], count);

	return $.format(cases[2], count);
};

/// <summary>
/// Determines whether the specified array is empty.
/// </summary>
/// <param name="array" type="Array">The array for checking.</param>	
core.isEmptyArray = function (array)
{
	return !(jQuery.isArray(array) && array.length > 0);
};

/// <summary>
/// Determines whether the specified array is not empty.
/// </summary>
/// <param name="array" type="Array">The array for checking.</param>	
core.isNotEmptyArray = function (array)
{
	return !$.isEmptyArray(array);
};

/******************************************************************************
 Function map is similar to jQuery.map but it differs in the case when
 iterator returns array. The result in this case will be array of arrays, not
 the flat array.
 *******************************************************************************/
core.mapExtended = function (iterable, iterator)
{
	var result = [];
	$.each(iterable, function (index, item)
	{ result.push(iterator(item)); });
	return result;
};

core.newGUID = function()
{
	var S4 = function()
	{
		return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
	};
	return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
};

var Date_to_format = function(date)
{
	var dd = date.getDate();
	if (dd < 10) dd = '0' + dd;
	var mm = date.getMonth() + 1;
	if (mm < 10) mm = '0' + mm;
	var yy = date.getFullYear();
	var Datestring = dd + "." + mm + "." + yy;
	return Datestring;
};

/*
* Date Format 1.2.3
* (c) 2007-2009 Steven Levithan <stevenlevithan.com>
* MIT license
*
* Includes enhancements by Scott Trenda <scott.trenda.net>
* and Kris Kowal <cixar.com/~kris.kowal/>
*
* Accepts a date, a mask, or a date and a mask.
* Returns a formatted version of the given date.
* The date defaults to the current date/time.
* The mask defaults to dateFormat.masks.default.
*/

var dateFormat = function () {
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
		    val = String(val);
		    len = len || 2;
		    while (val.length < len) val = "0" + val;
		    return val;
		};

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = (date != null) ? new Date(date) : new Date;
        if (isNaN(date)) throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
			    d: d,
			    dd: pad(d),
			    ddd: dF.i18n.dayNames[D],
			    dddd: dF.i18n.dayNames[D + 7],
			    m: m + 1,
			    mm: pad(m + 1),
			    mmm: dF.i18n.monthNames[m],
			    mmmm: dF.i18n.monthNames[m + 12],
			    yy: String(y).slice(2),
			    yyyy: y,
			    h: H % 12 || 12,
			    hh: pad(H % 12 || 12),
			    H: H,
			    HH: pad(H),
			    M: M,
			    MM: pad(M),
			    s: s,
			    ss: pad(s),
			    l: pad(L, 3),
			    L: pad(L > 99 ? Math.round(L / 10) : L),
			    t: H < 12 ? "a" : "p",
			    tt: H < 12 ? "am" : "pm",
			    T: H < 12 ? "A" : "P",
			    TT: H < 12 ? "AM" : "PM",
			    Z: utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
			    o: (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
			    S: ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
} ();

// Some common format strings
dateFormat.masks = {
    "default": "ddd mmm dd yyyy HH:MM:ss",
    shortDate: "m/d/yy",
    mediumDate: "mmm d, yyyy",
    longDate: "mmmm d, yyyy",
    fullDate: "dddd, mmmm d, yyyy",
    shortTime: "h:MM TT",
    mediumTime: "h:MM:ss TT",
    longTime: "h:MM:ss TT Z",
    isoDate: "yyyy-mm-dd",
    isoTime: "HH:MM:ss",
    isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'",

    CustomDate: "mm.dd.yyyy"
};

// Internationalization strings
dateFormat.i18n = {
    dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
    monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
    return dateFormat(this, mask, utc);
};
///////////////////////////////////////////////////////////////////////////////////////////////


//дополняет число нулями до заданной длины
function FormatNumberLength(num, length) {
    var r = "" + num;
    while (r.length < length) {
        r = "0" + r;
    }
    return r;
}


