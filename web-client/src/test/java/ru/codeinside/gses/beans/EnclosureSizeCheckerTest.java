package ru.codeinside.gses.beans;

import org.junit.Test;

import static org.junit.Assert.*;

public class EnclosureSizeCheckerTest {
    private static final long _2GB = (1024 * 1024 * 1024 * 2);
    private static final long _3GB = (1024 * 1024 * 1024 * 3);
    private static final long _700M = (1024 * 1024 * 700);
    private static final long _300M = (1024 * 1024 * 300);

    @Test
    public void testOverflow() {
        EnclosureSizeChecker checker = new EnclosureSizeChecker();

        checker.setTotalSize(0);
        assertTrue(checker.increaseTotalSize(_300M));
        assertTrue(checker.increaseTotalSize(_300M));
        assertFalse(checker.increaseTotalSize(_700M));
        checker.setTotalSize(0);
        assertTrue(checker.increaseTotalSize(_300M));
        assertFalse(checker.increaseTotalSize(_2GB));
        checker.setTotalSize(0);
        assertFalse(checker.increaseTotalSize(_2GB));
        checker.setTotalSize(0);
        assertFalse(checker.increaseTotalSize(_3GB));
    }
}